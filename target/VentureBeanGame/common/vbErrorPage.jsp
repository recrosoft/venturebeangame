
<html>
<head>
<link href="assets/css/Venturebean/errorPage.css" rel='stylesheet' type='text/css'>
<link href="assets/css/Venturebean/errorAnimate.css" rel='stylesheet' type='text/css'>
</head>
<body><%-- Oops!!! ${msg} --%>

<!--=== Content Part ===-->
  <div class="container content">
    <div class="row"> 
      <!-- Begin Content -->
      <div class="col-md-12">
         <div class="full-width"> 
    <!-- Start Container-->    
    <div class="container content-inner">
      <div class="row content">        
        <div class="col-md-12">
          <!-- Start 404 TEXT-->        
          <section class="main" style="text-align:center;">
            <h2 class="text404 animated Out">					
              <span class="text404-cut">Oops!
              </span>					
              <span class="text404-mid" style="text-align:center;">${msg}!
              </span>					
              <span class="text404-cut">Oops!
              </span>				
            </h2>        
          </section>
          <!-- End 404 TEXT-->
          <!-- Main TEXT-->       
          <section class="desc animated Out">    
            <h4>${msg}!</h4>
          </section>
          <!-- End Main TEXT-->
         
        </div> 
      </div> 
    </div>         
    <!-- End Container -->
    </div>   
        </div> 
    <!--/row--> 
  </div>
  <!--/container--> 
  <!--=== End Content Part ===--> 
  
</div>
</body>

<script type="text/javascript" src="assets/js/ventureBean/errorpage/errorPageAnimate.js"></script>
<script type="text/javascript" src="assets/js/ventureBean/errorpage/errorPageInit.js"></script>
</html>