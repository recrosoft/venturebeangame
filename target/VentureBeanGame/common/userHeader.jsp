

<%@ include file="/common/taglibs.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="icon" type="image/png" href="assets/img/vb-favicon.png">
<style>
.search-open ul {
	list-style-type: none;
}
.search-open ul li {
	display: block;
	padding: 5px;
}
.search-open ul li a {
	text-decoration: none
}
.search-open ul li a:hover {
	text-decoration: none
}
.user-logout{    padding-top: 4px;
    padding-left: 6px;
    color: #CB5527;}
</style>
<!--=== Header ===-->
<div class="header">
	<!-- Navbar -->
	<div class="navbar navbar-default" role="navigation"
		style="padding-top: 30px; padding-bottom: 30px; background: rgb(235, 239, 237);">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="fa fa-bars"></span>
				</button>
				<img id="logo-header"
					src="assets/img/LOGO.png" alt="Logo">
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-responsive-collapse">
				<ul class="nav navbar-nav">
					<c:if test="${adminDetailsSessionForm.userRole == 'FACILITATOR'}">
					<sec:authorize access="hasRole('FACILITATOR')">
						<li><a href="user/dashboard">Dashboard</a></li>
					<li><a href="user/showUsers">Add Users</a></li>
					<li><a href="user/showAssignDesignation"> Assign Role </a></li>
					</sec:authorize>
					</c:if>
					<!-- Search Block -->
					<li><img src="assets/img/user_login.png" width=25></li>
					<li><span style="font-size:20px"><a href="<%=request.getContextPath()%>/user/logout"><i class="fa fa-power-off user-logout"></i> </a></span></li>
					<!-- End Search Block -->
				</ul>
			</div>
			<!--/navbar-collapse-->
		</div>
	</div>
	<!-- End Navbar -->
	 <!--=== Breadcrumbs ===-->
  <div class="breadcrumbs"  style="color:#ffffff">
    <div class="container" id="borderHeader-id">
      <h4 class="pull-left" id="headingMsg"></h4>
      <h4 class="pull-right" id="timer-id"></h4>
    </div>
  </div>
  <!--/breadcrumbs--> 
  <!--=== End Breadcrumbs ===--> 
</div>
<!--=== End Header ===-->
