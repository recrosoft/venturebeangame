

<%@ include file="/common/taglibs.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="icon" type="image/png" href="assets/img/vb-favicon.png">
<style>
.search-open ul {
	list-style-type: none;
}
.search-open ul li {
	display: block;
	padding: 5px;
}
.search-open ul li a {
	text-decoration: none
}
.search-open ul li a:hover {
	text-decoration: none
}
.user-logout{    padding-top: 4px;
    padding-left: 6px;
    color: #CB5527;}
</style>
<!--=== Header ===-->
<div class="header">
	<!-- Navbar -->
	<div class="navbar navbar-default" role="navigation"
		style="padding-top: 30px; padding-bottom: 30px; background: rgb(235, 239, 237);">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="fa fa-bars"></span>
				</button>
				<img id="logo-header"
					src="assets/img/LOGO.png" alt="Logo">
			</div>
			<!-- Collect the nav links, forms, and other content for toggling -->
			
			<!--/navbar-collapse-->
		</div>
	</div>
	<!-- End Navbar -->
	 <!--=== Breadcrumbs ===-->
  <div class="breadcrumbs"  style="color:#ffffff">
    <div class="container" id="borderHeader-id">
      <h4 class="pull-left" id="headingMsg"></h4>
      <h4 class="pull-right" id="timer-id"></h4>
    </div>
  </div>
  <!--/breadcrumbs--> 
  <!--=== End Breadcrumbs ===--> 
</div>
<!--=== End Header ===-->
