<%@ include file="/common/taglibs.jsp"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<head>
<style>
.search-open ul {
	list-style-type: none;
}

.search-open a {
	text-decoration: none
}

.search-open a:hover {
	text-decoration: none
}
</style>

</head>
<!--=== Header ===-->
<div class="header">

	<!-- Navbar -->
	<div class="navbar navbar-default" role="navigation"
		style="padding-top: 30px; padding-bottom: 30px; background: rgb(235, 239, 237);">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-responsive-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="fa fa-bars"></span>
				</button>
				<img id="logo-header"
					src="assets/img/LOGO.png" alt="Logo">
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-responsive-collapse">
				<ul class="nav navbar-nav">
					
					<!-- End Contacts -->
					<!-- Search Block -->
					<li><img src="assets/img/user_login.png" width=25> <i
						class="search fa fa-caret-down search-btn"></i>
						<div class="search-open">
                <div class="input-group animated fadeInDown">
                  <div class="loginbar pull-right">
                    <div style="padding-bottom:5%; border-bottom:1px solid #dbdbdb; background-color: aliceblue; padding: 10px;"><a href="#" data-toggle='modal'
										data-target='#changePasswordpopUp'><i class="fa fa-cog"></i> &nbsp; Change Password</a></div>
                    <div style="padding-bottom:5%; background-color: aliceblue; padding: 10px;"><a href="<%=request.getContextPath()%>/adminLogout"><i class="fa fa-power-off"></i> &nbsp; Logout</a></div>
                  </div>
                  </span> </div>
              </div></li>
					<!-- End Search Block -->
				</ul>
			</div>
			<!--/navbar-collapse-->
		</div>
	</div>
	<!-- End Navbar -->
</div>
<!--=== End Header ===-->
<!--=== Breadcrumbs ===-->
		<div class="breadcrumbs" style="color: #ffffff">
			<div class="container">
				<h4 class="pull-left" id="headingId">Pending Requests</h4>
			</div>
		</div>
		<!--/breadcrumbs-->
		<!--=== End Breadcrumbs ===-->
<!--=== End Header ===-->
	<!-- changePassword popup starts -->
	<div class="modal fade" id="changePasswordpopUp" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Change Password</h4>
				</div>
				<form class="sky-form" action="changePassword">
					<input type="hidden" name="userName" id="userName"
						value="${adminDetailsSessionForm.userName}">

					<div class="modal-body">
						<!-- Begin Content -->
						<div class="col-md-12">
							<!-- General Unify Forms -->
							<fieldset>
								<section>
									<div class="row">
										<label class="label col col-4">Current Password</label>
										<div class="col col-8">
											<label class="input"> <i
												class="icon-prepend fa fa-lock"></i> <input type="password"
												name="currentPassword" id="currentPassword" />
											</label>
										</div>
									</div>
								</section>
								<section>
									<div class="row">
										<label class="label col col-4">New Password</label>
										<div class="col col-8">
											<label class="input"> <i
												class="icon-prepend fa fa-lock"></i> <input type="password"
												name="newPassword" id="newPassword" />
											</label>
										</div>
									</div>
								</section>
								<section>
									<div class="row">
										<label class="label col col-4">Confirm password</label>
										<div class="col col-8">
											<label class="input"><i
												class="icon-prepend fa fa-lock"></i> <input type="password"
												name="reNewPassword" id="reNewPassword" /> </label>
										</div>
									</div>
								</section>
							</fieldset>
							<!-- General Unify Forms -->
							<div class="margin-bottom-30"></div>
						</div>
						<!-- End Content -->
					</div>
					<div class="modal-footer" style="border-top: none;">
						<button type="submit" class="btn-u btn-u-primary">Change Password</button>
						<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- changePassword popup ends -->
