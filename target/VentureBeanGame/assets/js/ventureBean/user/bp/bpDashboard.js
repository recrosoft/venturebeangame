var pricePerLicense ;
var gameId ;
$(document).ready(function() {
	$('#headingId').text("Dashboard");
});

function  populateGame(gameName , gId){
	//alert( gameName +" "+gId);
	gameId = gId;
	$('.amount').val("");
	$('.gameName').val(gameName);
	$('.gameId').val(gId);
	
}

function  getPrice(licenseCount){
	var bpId = $('#bpId').val();
	if(pricePerLicense == undefined || pricePerLicense == ''){
		$.ajax({
			url: "bp/getPrice?gameId="+gameId+"&businessPartnerId="+bpId,
			type : "POST",
			success: function(result){
	        	//alert("result : "+result.ppLicense);
				pricePerLicense = result.ppLicense ;
				$('.amount').val(pricePerLicense*licenseCount);
	   		 },
			error: function(error){
				alert(" ajax Error occured"+error);
			}	
		});
	}else{
		$('.amount').val(pricePerLicense*licenseCount);
	}
}

function validateAddLicenseFields(){
	var gameId = $(".gameName").validationEngine("validate");
	var noLi = $("#noOfLicense").validationEngine("validate");
	var amount = $(".amount").validationEngine("validate");
	if(!gameId && !noLi && !amount){
		$('#operation-type').val("ADD");
		$('#msg').text("Are you sure,You want to add "+$("#noOfLicense").val()+ " licenses?");
		$('#confirmationpopup').modal('show');
	}
}
function validateCancelLicenseFields(){
	var gameId = $(".gameName").validationEngine("validate");
	var noLi = $("#cancel-noOfLicense").validationEngine("validate");
	var amount = $(".amount").validationEngine("validate");
	if(!gameId && !noLi && !amount){
		$('#msg').text("Are you sure,You want to cancel "+$("#cancel-noOfLicense").val()+" licenses?");
		$('#operation-type').val("CANCEL");
		$('#confirmationpopup').modal('show');
	}
}

function addCancelLicense() {
	if($('#operation-type').val() =="ADD"){
		var obj ={
				"businessPartnerId" : $('#bpId').val(),
				"gameId" : $('.gameId').val(),
				"noOfLicense" : $('#noOfLicense').val(),
				"amount" : $('.amount').val(),
			};
		$.ajax({
			url: "bp/addLicenses",
			type : "POST",
			data : obj,
			success: function(result){
			//	alert(" ajax success "+result);
				$('#addLicense').modal('hide');
				$('#confirmationpopup').modal('hide');
				$('#sucess-msg').text("Success! Request is wiating for admin approval");
				$('#success-pouup').modal('show');
			 },
			error: function(error){
				alert(" ajax Error occured"+error);
				$('#msg').val("Some Error Occured : "+error);
				$('.msgDiv').addClass('color-red');
				$('#confirmationpopup').modal('show');
			}	
		});
	}else if($('#operation-type').val() =="CANCEL"){
		
		$('#cancelLicense').modal('hide');
		$('#confirmationpopup').modal('hide');
		$('#sucess-msg').text("Licenses are cancelled");
		$('#success-pouup').modal('show');
		/*var obj ={
				"businessPartnerId" : $('#bpId').val(),
				"gameId" : $('.gameId').val(),
				"noOfLicense" : $('#cancel-noOfLicense').val(),
				"amount" : $('#cancel-amount').val(),
			};
		$.ajax({
			url: "bp/cancelLicense",
			type : "POST",
			data : obj,
			success: function(result){
			//	alert(" ajax success "+result);
				$('#cancelLicense').modal('hide');
				$('#confirmationpopup').modal('hide');
				$('#sucess-msg').val("Licenses are cancelled");
				$('#success-pouup').modal('show');
			 },
			error: function(error){
				alert(" ajax Error occured"+error);
				$('#msg').val("Some Error Occured : "+error);
				$('.msgDiv').addClass('color-red');
				$('#confirmationpopup').modal('show');
			}	
		});*/
	}
	
}