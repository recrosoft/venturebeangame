var editor; // use a global for the submit and return data rendering in the examples

$(document).ready(function() {
    editor = new $.fn.dataTable.Editor( {
       // data : data1,
    	ajax:
        {
        edit: {
            type: 'POST',
            url: "user/editPlayer",
       	}
        },
        table: "#example",
        idSrc:  'playerId',//this is used to tell the unique property in the data set , default is 'DT_RowId'
        fields: [  {
            label: "#",
            name: "playerId"
          },
          {
            label: "Name",
            name: "name"
          }, 
          {
            label: "Email Address",
            name: "emailAddress"
          }
        ]
    } );
 
    editor.on( 'preSubmit', function ( e, o, action ) {
        if ( action !== 'remove' ) {
            var firstName = editor.field( 'name' );
 
            // Only validate user input values - different values indicate that
            // the end user has not entered a value
            if ( ! firstName.isMultiValue() ) {
                if ( ! firstName.val() ) {
                    firstName.error( 'A first name must be given' );
                }
                 
                if ( firstName.val().length >= 20 ) {
                    firstName.error( 'The first name length must be less that 20 characters' );
                }
            }
 
            // ... additional validation rules
 
            // If any error was reported, cancel the submission so it can be corrected
            if ( this.inError() ) {
                return false;
            }
        }
    } );
    // Activate an inline edit on click of a table cell
    $('#example').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this, {
            buttons: { label: '<i class="fa fa-refresh"></i>', fn: function () { this.submit(); } }
        } );
    } );
 
    $('#example').DataTable( {
        dom: "Tfrtip",
        ajax : {
			"url" :"user/getPlayerList",
			"type" : 'POST',
			"data" : function(d) {
				 d.projectId = $('#projectId').val();
				}
        	}, 
        columnDefs: [
             { "targets" : 0,
                 data: "playerId"
               },
            { "targets" : 1,
              data: "name"
            },
            { "targets" : 2,
              data: "emailAddress" 
            },
        ],
       
        order: [ 1, 'asc' ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        }
    } );
} );

