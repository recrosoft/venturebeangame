jQuery(document).ready(function() {
	//$('#loader-wrapper').hide();
	 var prefix = "selectBox_";
     $("#add-company-form").validationEngine({promptPosition : "bottomLeft", prettySelect : true,
         usePrefix: prefix } , 'validate');
     /*if( $("#add-company-form").validationEngine('validate')){
    	 return false;
     }*/
	$('#headingId').text("Company List"); 
	$('#imgDiv').hide();
	populateuserList();
	
});
function showLoader(){
	if($("#add-company-form").validationEngine('validate')){
		$('#loader-wrapper').show();
		$("#add-company-form").submit();
	}
}
function populateuserList() {
	var roleName = $('#adminRole').val();
	if ($.fn.dataTable.isDataTable('#companyList')) {
		var oTable = $('#companyList').dataTable();
		oTable.fnDestroy();
	}
	$.ajaxSetup({ 
		   cache: false ,
	});
	var bpid = $('#bpId').val();
	var userList = $('#companyList').dataTable(
					{
						"language": {
					        "emptyTable": "<span style='color: #190C1D;'><img src='assets/img/empty.png' width=40>&nbsp;<strong>Kindly add a Company in the system</strong></span>"
					    },
						"sPaginationType" : "full_numbers",
						"lengthChange" : false,
						"iDisplayLength" : 4,
						"bFilter" : false,
						//"sDom": 't',
						"bPaginate": true, //hide pagination
						"ajax" : {
							"url" : "bp/getAllCompany",
							"type" : 'GET',
							"data" :function(d){
								if(bpid == ''){
									d.userName = $('#bpUserName').val();
								}else{
									d.bpId = $('#bpId').val();
								}
							}
						},
						"fnRowCallback": function(nRow, aaData, iDisplayIndex) {
							$(nRow).css('background-color', 'transparent');	
						},
						"fnDrawCallback": function(oSettings) {
					        //if ($('#userList tr').length < 3) 
							var rows = this.fnGetData();
							  if ( rows.length === 0 ) {
								  $('.dataTables_paginate').hide();
								  $('.dataTables_info').hide();
							  }else{
								  $('.dataTables_paginate').show();
								  $('.dataTables_info').show(); 
							  }
					    },
						"columns" : [
						             { "title": "Name",
									  "render" : function(data, type, full) {
										 // alert(full);
												if (full == null || full == "") {
													return '<span>-<span>';
												} else {
													var logo = '';
													if( full.companyLogoUrl != null){
														logo=full.companyLogoUrl;
													}else{
														logo="assets/img/ventureBean/company_logo_default.jpg";
													}
													var rowdata = "<span class='col-md-2'><a class='gallery-item' href='"+logo+"' title='Logo' data-gallery><img src="+logo+" width='90px' height ='90px'></a></span>"; 
													rowdata = rowdata + "<span class='col-md-10'>";
													if(roleName == 'BUSINESS_PARTNER'){
														 rowdata=rowdata+"<h3><a href='bp/viewCompany?companyId="+ full.companyId+"'>"+full.companyName+"</a></h3>";
													}else{
														 rowdata=rowdata+"<h3>"+full.companyName+"</h3>";
													}
										            rowdata=rowdata+ "<p><i class='fa fa-map-marker' style='font-size: 20px;color:#77925D;'></i>&nbsp;&nbsp;"+full.companyAddr+"&nbsp,&nbsp"+full.companyCity+" </p> </span>";
										          //  rowdata=rowdata+ "<span class='col-md-1 eye'> <i class='fa fa-eye color-green'></i> </span>";
										           return '<span>' + rowdata + '</span>';
												}
											}

										}
						             ]
					});
};

function showSelectedImg(event ){
	  // $("#imgLocation").attr("src", $('#image-selected').val());
	  $("#imgLocation").attr("src", URL.createObjectURL(event.target.files[0]));
	  $('#imgDiv').show();
};