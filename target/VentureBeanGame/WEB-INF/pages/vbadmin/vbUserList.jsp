<%@ include file="/common/taglibs.jsp"%>
<html lang="en">
<!--<![endif]-->
<head>
<title>VB Admin</title>
<style>
.dataTables_wrapper table thead {
	display: none;
}
</style>
<!-- Meta -->
<meta name="description" content="">
<meta name="author" content="">
<!-- Data table CSS -->
<link rel="stylesheet" type="text/css" id="theme" href="assets/css/plugins/datatable/jquery.dataTables.css" />
<link rel="stylesheet" type="text/css" id="theme" href="assets/css/Venturebean/loader.css" />
</head>
<body>

	<div class="wrapper">
		<!-- Floating Action Button -->
		<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
			<a class="btn-floating btn-large" data-toggle="modal"
				data-target="#create-role"> <i class="fa fa-plus"></i>
			</a>
		</div>
		<!-- Floating Action Button -->

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row">
				<div class="col-md-12"
					style="background-color: #ebefed;padding-top: 14px;padding-bottom: 14px;">
					<div class="table-responsive1">
						<table id="userList" class="table" >
						</table>
					</div>
				</div>
				<!--/col-md-9-->

			</div>
			<!--/row-->
		</div>
		<!--/container-->
		<!--=== End Content Part ===-->
		<!--Create Role popup Start -->
		<div class="modal fade" id="create-role" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div id="loader-wrapper">
			  <div id="loader"></div>
			  <div class="loader-section section-left"></div>
			  <div class="loader-section section-right"></div>
			</div>
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel"><strong>Create Role</strong></h4>
					</div>
					<div class="modal-body">
						<!-- Begin Content -->
						<div class="col-md-12">
							<!-- General Unify Forms -->
							<s:form action="admin/createRole" class="sky-form"  commandName="roleForm" method="post" id="create-role-form">
								<fieldset>
									<section>
										<div class="row">
											<label class="label col col-4">Type</label>
											<div class="col col-8">
												<label class="select"> <s:select path="userRoleId"
														id="userRoleId" onchange="getSuperiorAdmin(this.value)"
														class="validate[userRoleType]">
														<s:option value="">Please Select...</s:option>
													</s:select> <i></i>
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">E-mail</label>
											<div class="col col-8">
												<label class="input"> <i
													class="icon-prepend fa fa-envelope"></i> <s:input
														type="text" name="email" path="email"
														class="validate[emailAddress]" />
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">Name</label>
											<div class="col col-8">
												<label class="input"> <i
													class="icon-prepend fa fa-user"></i> <s:input type="text"
														path="name" class="validate[adminName]" />
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">Phone</label>
											<div class="col col-8">
												<label class="input"><i
													class="icon-prepend fa fa-phone"></i> <s:input type="text"
														path="phone" class="validate[adminPhone]" /> </label>
											</div>
										</div>
									</section>
									<section>
										<div class="row" id="assignToDiv">
											<label class="label col col-4">Assign to</label>
											<div class="col col-8">
												<label class="select"> <s:select path="assignTo"
														id="assignTo" class="validate[assignTo]" /> <i></i>
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row" id="packageDiv">
											<label class="label col col-4">Package</label>
											<div class="col col-8">
												<label class="select"> <s:select path="categoryName"
														class="validate[gamePackage]">
														<s:option value="">Please Select...</s:option>
														<s:options items="${packages}" />
													</s:select> <i></i>
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row" id="elicenseDiv">
											<label class="label col col-4">Extra license</label>
											<div class="col col-8">
												<label class="input"><i
													class="icon-prepend fa fa-file-text"></i> <s:input type="text"
														path="extraLicense" /> </label>
											</div>
										</div>
									</section>
								</fieldset>
								<!-- General Unify Forms -->
								<div class="margin-bottom-40"></div>
						</div>
						<!-- End Content -->
					</div>
					<div class="modal-footer" style="border-top: none;">
						<button type="button" onclick="startLoader()" class="btn-u btn-u-primary">Create Role</button>
						<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
					</div>
					</s:form>
				</div>
			</div>
		</div>
		<!-- Create Role popup Ends -->

		<!-- Add License popup Start -->
		<div class="modal fade" id="addLicense" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">Add License</h4>
					</div>
					<form class="sky-form" id="add-license-form">
						<div class="modal-body">
							<!-- Begin Content -->
							<div class="col-md-12">
								<!-- General Unify Forms -->
								<fieldset>
									<section>
										<div class="row">
											<label class="label col col-4">Game</label>
											<div class="col col-8">
												<label class="select"> <select name="gameId"
													id="gameId" data-live-search="true"
													class="validate[gameSelect]"></select> <i></i>
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">License(s)</label>
											<div class="col col-8">
												<label class="input"> <i
													class="icon-prepend fa fa-file-text"></i> <input type="text"
													name="noOfLicense" id="noOfLicense"
													class="validate[noOfLicense]" />
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">Bank Name</label>
											<div class="col col-8">
												<label class="input"> <i
													class="icon-prepend fa fa-money"></i> <input type="text"
													name="bankName" id="bankName" class="validate[bankName]" />
												</label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">Cheque No</label>
											<div class="col col-8">
												<label class="input"><i
													class="icon-prepend fa fa-list-alt"></i> <input type="text"
													name="chequeNumber" id="chequeNumber"
													class="validate[chequeNumber]" /> </label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">Amount</label>
											<div class="col col-8">
												<label class="input"><i
													class="icon-prepend fa fa-inr"></i> <input type="text"
													name="amount" id="amount" class="validate[amount]" /> </label>
											</div>
										</div>
									</section>
									<section>
										<div class="row">
											<label class="label col col-4">Cheque Date</label>
											<div class="col col-8">
												<label class="input"><i
													class="icon-prepend fa fa-calendar timepicker"></i> <input
													type="text" name="chequeDate" id="chequeDate"
													class="validate[chequeDate]" /> </label>
											</div>
										</div>
									</section>
								</fieldset>
								<!-- General Unify Forms -->
								<div class="margin-bottom-30"></div>
							</div>
							
							<!-- End Content -->
						</div>
						<div id="loader-wrapper-p">
						  <div id="loader"></div>
						  <div class="loader-section section-left"></div>
						  <div class="loader-section section-right"></div>
						</div>
						<div class="modal-footer" style="border-top: none;">
							<button type="button" class="btn-u btn-u-primary" onclick='validateAddLicenseFields()'>Add</button>
							<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
						</div>
					</form>
				</div>
			</div>
			
		</div>
		<!-- Add License popup Ends -->
		
		<!-- confirmation popup starts -->
		<div class="modal fade" id="confirmationpopup" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" style="background-color: azure">
					<div class="modal-header"
						style="padding-bottom: 16px; border: none">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<!-- Begin Content -->
						<div class="col-md-12">
							<!-- General Forms -->
							<div class="sky-form" >
								<div class="row">
									<div class="col col-12 msgDiv">
										<h4 class="color-green" id=msg>Are you sure,You want to add
											License?</h4>
									</div>
								</div>
							</div>
							<!-- General Forms -->
						</div>
						<!-- End Content -->
					</div>
					<div class="modal-footer" style="border-top: none;">
						<button type="submit" class="btn-u btn-u-primary" onclick='addLicense()'>OK</button>
						<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
			</div>
		</div>
		<!-- succ-err msg popup ends -->
		
			<!-- confirmation popup starts -->
		<div class="modal fade" id="success-error" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content" style="background-color: azure">
					<div class="modal-header"
						style="padding-bottom: 16px; border: none">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<!-- Begin Content -->
						<div class="col-md-12">
							<!-- General Forms -->
							<div class="sky-form" >
								<div class="row">
									<div class="col col-12 msgDiv">
										<h4 class="color-green" id=success-err-msg></h4>
									</div>
								</div>
							</div>
							<!-- General Forms -->
						</div>
						<!-- End Content -->
					</div>
					<div class="modal-footer" style="border-top: none;">
						<button type="submit" class="btn-u btn-u-primary" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>
		</div>
		<!-- succ-err msg popup ends -->

		<input type="hidden" id="selectedRole" name="selectedRole"value="${selectedRole}">
		<input type="hidden"id="superUserId" name="superUser" value="${superUserId}"> 
		<input type="hidden" id="urlSpecRoleName" name="urlSpecRoleName" value="${adminDetailsSessionForm.urlSpecRoleName}"> 
		<input type="hidden" id="headingText" name="headingText" value="${headingText}">
	</div>
	<!--/End Wrapepr-->

<!-- Start Page Loading -->

<!-- End Page Loading --> 
	<!-- JS Page Level -->
	<script type="text/javascript"
		src="assets/js/plugins/datatables/jquery.dataTables.js"></script>
	<script type="text/javascript"
		src="assets/js/ventureBean/admin/vbUserList.js"></script>
</body>
</html>