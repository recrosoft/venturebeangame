<%@ include file="/common/taglibs.jsp"%>
<%@ page import="com.ventureBean.constants.Constants" %>
<!DOCTYPE html>
<html lang="en">
<!--<![endif]-->
<head>
<title>VB Admin</title>
<style>
.dataTables_wrapper table thead{
    display:none;
}
</style>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">



<!-- CSS Customization -->
<link rel="stylesheet" href="assets/css/custom.css">
<!-- Data table CSS -->
<link rel="stylesheet" type="text/css" id="theme"  href="assets/css/plugins/datatable/jquery.dataTables.css" />
</head>

<body>
<div class="wrapper"> 

  <!--=== Content Part ===-->
  <div class="container content">
    <div class="row">
      <div class="col-md-12" style="background-color:#ebefed;"> 
      <div class="table-responsive1">
		<table id="userList" class="table ">
	
		</table>
	</div>
      </div>
      <!--/col-md-9--> 
      
    </div>
    <!--/row--> 
  </div>
  <!--/container--> 
  <!--=== End Content Part ===-->
  
</div>
<!--/End Wrapepr--> 

<script type="text/javascript">
    jQuery(document).ready(function() {
    	$('#headingId').text("Dashboard");
    	 var prefix = "selectBox_";
        populateuserList();
        $("#createRoleForm").validationEngine({promptPosition : "topRight", prettySelect : true,
            usePrefix: prefix },'validate');
    });
    $.ajaxSetup({ 
    	   cache: false ,
    	});
    function getSuperiorAdmin(roleId){
    //	alert($(userRoleId).find('option:selected').text());
    	var roleName = $(userRoleId).find('option:selected').text();
    	showHidePackageBasedOnRole(roleName);
    	if(roleName != undefined && roleName == 'BUSINESS HEAD'){
    		//hide assignTo label 
    		$('#assignToDiv').hide();
    	}else{
    		$('#assignToDiv').show();
    		$.ajax({
    			url: "admin/getSuperiorAdmin?userRoleId="+roleId,
    			type : "GET",
    			success: function(result){
    	        	//alert("result"+result);
    	        	populateAssignToDD(result);
    	   		 },
    			error: function(error){
    				alert(" ajax Error occured"+error);
    			}	
    		});
    	}
    }

    function showHidePackageBasedOnRole(roleName){
    	if(roleName != undefined && roleName == 'BUSINESS PARTNER'){
    		$('#packageDiv').show();
    		$('#elicenseDiv').show();
    	}else{
    		$('#packageDiv').hide();
    		$('#elicenseDiv').hide();
    	}
    }

    function populateAssignToDD(data){
    	var options = "<option value=''>select...</option>";
    	var list = data.list;
    	 $.each(list, function(index, value ) {
    	            // alert(value.userId + ' ' + value.name);
    	            options = options+"<option value='"+value.userId+"'>"+value.name+"</option>";
    	      });
    	 $('#assignTo').empty().append(options);
    }
    
    function populateuserList() {

		if ($.fn.dataTable.isDataTable('#userList')) {
			var oTable = $('#userList').dataTable();
			oTable.fnDestroy();
		}
		 var role = "<%=Constants.BH_ROLE %>";
		var rowCount = 0;
		//this array is used by DataTable for sorting
		var rowCount = 1;
		var userList = $('#userList').dataTable(
						{
							"language": {
						        "emptyTable": "<span style='color: #190C1D;'><img src='assets/img/empty.png' width=40>&nbsp;<strong>No Business Head added Yet</strong></span>"
						    },
							"sPaginationType" : "full_numbers",
							"lengthChange" : false,
							"iDisplayLength" : 90,
							"bFilter" : false,
							//"sDom": 't',
							"bPaginate": true, //hide pagination
							"ajax" : {
								"url" : "admin/getBusinessHeadList",
								"type" : 'POST',
								//"data":obj
								"data" : function(d) {
									 d.length=d.length,
									 d.roleName =role;
								} 
							}, 
							//"data":dataSet,
							"fnRowCallback": function(nRow, aaData, iDisplayIndex) {
								$(nRow).css('background-color', 'transparent');	
							},
							"fnDrawCallback": function(oSettings) {
						        //if ($('#userList tr').length < 3) 
								var rows = this.fnGetData();
								  if ( rows.length === 0 ) {
									  $('.dataTables_paginate').hide();
									  $('.dataTables_info').hide();
								  }else{
									  $('.dataTables_paginate').show();
									  $('.dataTables_info').show(); 
								  }
						    },
							"columns" : [
							             { "title": "Name",
										  "render" : function(data, type, full) {
											 // alert(full);
													if (full == null || full == "") {
														return '<span>-<span>';
														
													} else {
														var rowdata = "<span class='col-md-1'> <div class='circle'>"+rowCount++ +"</div> </span>"; 
														rowdata = rowdata + "<span class='col-md-10 admin-row-sub'>";
											            rowdata=rowdata+"<h3>"+full.name+"</h3>";
											            rowdata=rowdata+"<ul style='margin-left: -38px;'><li><i class='fa fa-envelope' style='color:#77925D;'></i>&nbsp; "+full.emailAddress+ "</li><li><i class='fa fa-user' style='color:#77925D;'></i>&nbsp;&nbsp;Admin : "+full.comesUnder+" </li></ul></span>";
											            rowdata=rowdata+ "<span class='col-md-1 eye'> <i class='fa fa-eye color-green'></i> </span>";
														return '<span>' + rowdata + '</span>';
													}
												}

											}
							             ]
						});
	};
	
</script> 
<input type="hidden" id="selectedRole" name="selectedRole"value="${selectedRole}">
<input type="hidden" value="<%=Constants.BH_ROLE %>">
<script type="text/javascript" src="assets/js/plugins/datatables/jquery.dataTables.js"></script>
<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/sky-forms/version-2.0.1/js/sky-forms-ie8.js"></script>
<![endif]-->
</body>
</html>