  <%@ include file="/common/taglibs.jsp"%>
  <!--=== Content Part ===-->
  <div class="container content">
    <div class="row"> 
      <!-- Begin Content -->
      <div class="col-md-12"> 
        <!-- Accordion v1 -->
        <div class="panel-group acc-v1" id="accordion-1">
          <c:forEach items="${games}" var="game" varStatus="loopCounter">
  			
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-${loopCounter.count}"><i class="fa fa-gamepad"></i>
				 ${game.gameName} 
				 <img src="assets/img/ventureBean/survey_icon.png" class="pull-right"> </a>
			</h4>
            </div>
             
            <div id="collapse-${loopCounter.count}" class="panel-collapse collapse">
             <div class="panel-body">
                <div class="row" style="background-color:#e8f4f9; padding:15px 0px 0px 0px">
                	
                 <form class="form-horizontal sky-form" id="gameForm" method="post" role="form" action="admin/updateGame" enctype='multipart/form-data'>
                  <input type="hidden" id="game-id-${loopCounter.count}" value="${game.gameId}" name="gameId">
                    <div class="col-md-10">
                      <h4  style="color:#cb5527;">Details</h4>
                      <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Title</label>
                        <div class="col-lg-10">
                          <input  class="form-control validate[gameName]" id="gameName-${loopCounter.count}" name="gameName" placeholder="Title" value="${game.gameName}">
                        </div>
                      </div>
                     
                      <div class="form-group">
                            <label for="inputEmail1" class="col-lg-2 control-label">Description</label>
                             <div class="col-lg-10">
                            <label class="textarea textarea-resizable">
                                <textarea rows="2" class="validate[gameDescription]" id="gameDescription-${loopCounter.count}" name='gameDescription' value="${game.description}">${game.description}</textarea>
                            </label>
                            </div>
                        </div>
                      <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">Logo</label>
                        <div class="col-lg-3">
                          <img id="imgLocation-${loopCounter.count}" src="${game.logoUrl}" width="100%">
                          <input type="hidden" name="logoUrl"  value="${game.logoUrl}">
                        </div>
                         <div class="col-lg-2">
                          <label for="file" class="input input-file">
                         <div class="button">
                            <input type="file" name="gameLogo" onchange='showSelectedImg(event,"imgLocation-${loopCounter.count}")'>
                            Browse</div>
                                </label>
                            </div>
                      </div>
                    </div>
                    <div class="col-md-2" style="margin-top:10%;">
                      <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                          <div  class="btn-u btn-u-green" onclick='validateGameFields(${loopCounter.count})'>Save</div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="row" style="margin-top:1%; background-color:#e8f4f9; padding:15px 0px 0px 0px">
					<div class="form-horizontal sky-form" role="form" id="category-update-form">
						<div class="col-md-10">
							<h4 style="color: #cb5527;">Categories</h4>
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 control-label">Package</label>
								<div class="col-lg-10">
									<label class="select"> <select class="validate[categoryName]"
										onchange='getCategoryDetails(this.value,${loopCounter.count})' id="category-${loopCounter.count}">
											<option value="">Choose Package</option>
											<c:forEach items="${game.categories}" var="category">
												<option value="${category.categoryId}">${category.categoryName}</option>
											</c:forEach>
									</select> <i></i>
									</label>
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 control-label">Free
									License</label>
								<div class="col-lg-10">
									<input type="text" class="form-control validate[freeLicense]"
										id="freeLicense-${loopCounter.count}"
										placeholder="Free License">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 control-label">Price</label>
								<div class="col-lg-10">
									<input type="text" class="form-control validate[amount]"
										id="price-${loopCounter.count}" placeholder="Price">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 control-label">Threshold</label>
								<div class="col-lg-10">
									<input type="text" class="form-control validate[amount]"
										id="threshold-${loopCounter.count}" placeholder="Threshold Value">
								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail1" class="col-lg-2 control-label">Package
									Description</label>
								<div class="col-lg-10">
									<label class="textarea textarea-resizable"> <textarea class="validate[categoryDesc]"
											rows="2" id='desc-${loopCounter.count}'></textarea>
									</label>
								</div>
							</div>
							<input type="hidden" name="categoryId" id="category-id-${loopCounter.count}" value="">	
						</div>
						<div class="col-md-2" style="margin-top: 12%">
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<button type="button" class="btn-u btn-u-green" onclick='validateCategoryDetails(${loopCounter.count})'>Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
              </div>
            </div>
          </div>
          </c:forEach>
        </div>
        <!-- End Accordion v1 -->
        
        <div class="margin-bottom-60"></div>
      </div>
      <!-- End Content --> 
      
    </div>
    <!--/row--> 
  </div>
  <!--/container-->
  
   <!-- confirmation popup starts -->
<div class="modal fade" id="confirmationpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color:azure">
        <div class="modal-header" style="padding-bottom:16px; border:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         
        </div>
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Forms -->
            <div>
                  <div class="row">
                    <div class="col col-12 msgDiv">
                      <h4 class="color-green" id=msg>Are you sure,You want to update this category?</h4>
                    </div>
                  </div>
                </section>
            </div>
            <!-- General Forms -->
          </div>
          <!-- End Content --> 
        </div>
         <div class="modal-footer" style="border-top:none;">
          <button type="submit" class="btn-u btn-u-primary"  onclick='updateCategory()'>OK</button>
          <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
 <!-- confirmation popup ends --> 
  
    <!-- confirmation popup starts -->
<div class="modal fade" id="confirmationGameUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color:azure">
        <div class="modal-header" style="padding-bottom:16px; border:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
         
        </div>
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Forms -->
            <div class="sky-form">
                  <div class="row">
                    <div class="col col-12 msgDiv">
                      <h4 class="color-green" id=msg>Are you sure,You want to update game details?</h4>
                    </div>
                  </div>
                </section>
            </div>
            <!-- General Forms -->
          </div>
          <!-- End Content --> 
        </div>
         <div class="modal-footer" style="border-top:none;">
          <button type="submit" class="btn-u btn-u-primary" onclick='updateGame()'>OK</button>
          <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
 <!-- confirmation popup ends --> 
  
  <script>
  var categories = '${gameCategories}';
  var cat = '';
  var rowcount='';
  $(document).ready(function(){
	  $('#headingId').text("Game Configuration");
	 cat = JSON.parse(categories);
	
  })
  function showSelectedImg(event , id){
	  // $("#imgLocation").attr("src", $('#image-selected').val());
	  $("#"+id).attr("src", URL.createObjectURL(event.target.files[0]));
  };
  
  function getCategoryDetails(categoryId , rowCount){
	  // count is used to prepare the ids which are generatd dynamically.
	//  alert("categoryId : "+categoryId +"  count : "+count);
	rowcount = rowCount;
		var result = cat.filter(function(v) {
		    return v.categoryId == categoryId; 
		})[0];
	if(result) {
		  $('#freeLicense-'+rowCount).val(result.freeLicense);
		  $('#desc-'+rowCount).val(result.desc);
		  $('#price-'+rowCount).val(result.pricePerLicense);
		  $('#category-id-'+rowCount).val(categoryId);
		  $('#threshold-'+rowCount).val(result.threshold);
		  
		}
  }
  function updateCategory(){
	 var obj = {};
	 var categoryId= $('#category-id-'+rowcount).val();
	 obj["freeLicense"]=$('#freeLicense-'+rowcount).val();
	 obj["desc"]=$('#desc-'+rowcount).val();
	 obj["pricePerLicense"]=$('#price-'+rowcount).val();
	 obj["categoryId"]=categoryId;
	 obj["threshold"]=$('#threshold-'+rowcount).val();;
	 $.ajax({
			url: "admin/updateCategory",
			type : "POST",
			data:obj,
			success: function(result){
	        	updateCategoryInLocal(categoryId ,obj);
	        	$('#confirmationpopup').modal('hide');
	   		 },
			error: function(error) {
				alert("Ajax Error occured "+error);
			}
		});
  }
  function updateCategoryInLocal( categoryId, obj ) {
	   for (var i in cat) {
	     if (cat[i].categoryId == categoryId) {
	    	 cat[i]=obj;
	        break; //Stop this loop, we found it!
	     }
	   }
	}
   function updateGame(){
	  $('#gameForm').submit();
  };
  
  function validateGameFields(rowId) {
    var gameName = $("#gameName-"+rowId).validationEngine("validate");
  	var gameDesc = $("#gameDescription-"+rowId).validationEngine("validate");
		if(!gameName && !gameDesc){
			$("#confirmationGameUpdate").modal('show');
		}
  }
  function validateCategoryDetails(rowId){
	  var category = $("#category-"+rowId).validationEngine("validate");
	  var freeLicense = $("#freeLicense-"+rowId).validationEngine("validate");
	  var price = $("#price-"+rowId).validationEngine("validate");
	  var desc = $("#desc-"+rowId).validationEngine("validate");
	  var threshold = $("#threshold-"+rowId).validationEngine("validate");
	 		if(!category && !freeLicense && !price && !desc && !threshold){
				$("#confirmationpopup").modal('show');
			} 
  }
  </script>