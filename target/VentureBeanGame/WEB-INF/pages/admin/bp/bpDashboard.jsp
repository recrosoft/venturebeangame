<%@ include file="/common/taglibs.jsp"%>
<html>
<head>
 <style>
	 .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{vertical-align:middle}
.table thead > tr > th {
     border-bottom: 1px solid gray;
}
 </style>    

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BP Dashboard</title>
</head>
  <!--=== Content Part ===-->
   <div class="container">
    <div class="row">
        <!-- Begin Content -->
            <div class="col-md-12">
                <!--Basic Table-->
                <div class="panel margin-bottom-40">
                    <div class="panel-body">
                     <div class="row clients-page" style="margin-top:20px;">
          <div class="col-md-1">
            <div class="circle" style="padding: 23px 24px 0px 26px;"><i class="fa fa-user" style="font-size:18px;"></i></div>
          </div>
          <div class="col-md-9">
            <h3>${bpDetails.name}</h3>
            <p><i class="fa fa-envelope" style="color:#77925D;"></i>&nbsp; ${bpDetails.emailAddress}</p>
            <p><i class="fa fa-phone-square color-light-green"></i>&nbsp; ${bpDetails.phone}</p>
          <!--   <button class="btn-u btn-u-sm rounded-3x btn-u-orange" type="button">Add License</button> -->
          </div>
          <div class="col-md-2" style="font-size:30px;"> <i class="fa fa-credit-card color-green"></i> ${bpDetails.wallet}</div>
        </div>
        <!-- End Clients Block-->
                    </div>                                      
                    <table class="table">
                        <thead>
                            <tr style="text-align:center;">
                                <th>Game</th>
                                  <th>Total</th>
                                <th class="hidden-sm">Available</th>
                                <th>Used</th>
                                <th></th><th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${bpGameMappings}" var="gameDetail">
                        	<tr>
                        	 <input type="hidden" name='gameId' id='gameId-${gameDetail.gameName}' value='${gameDetail.gameId}'/>
                                <td>${gameDetail.gameName}</td>
                                 <td>${gameDetail.totalLicense}</td>
                                <td class="hidden-sm">${gameDetail.availableLicense}</td>
                                <td>${gameDetail.usedLicense}</td>
                                 <td style="text-align: right;"><button class="btn btn-danger btn-sm" onclick='populateGame("${gameDetail.gameName}" ,${gameDetail.gameId})' data-toggle="modal" data-target="#cancelLicense"><i class="fa fa-trash-o"></i>&nbsp; Cancel</button></td> 
                                 <td><button class="btn  btn-sm" onclick='populateGame("${gameDetail.gameName}" ,${gameDetail.gameId})' data-toggle="modal" data-target="#addLicense" ><i class="fa fa-plus-square "></i> &nbsp; Add License</button></td>                          
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!--End Basic Table-->
</div>
            <!-- End Content -->
</div>
</div>
<!-- Add License popup Start --> 
  <div class="modal fade" id="addLicense" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Add License</h4>
        </div>
        <form  class="sky-form" id="add-license-form">
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Unify Forms -->
              <fieldset>
                <section>
                  <div class="row">
                    <label class="label col col-4">Game</label>
                   <div class="col col-8">
                      <label class="input"> <i class="icon-prepend fa fa-gamepad"></i>
                        <input type="text" name="gameName" class="gameName" class="validate[gameSelect]" readonly />
                         <input type="hidden" name="gameId" class="gameId"/>
                      </label>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row">
                    <label class="label col col-4">No of Licenses</label>
                    <div class="col col-8">
                      <label class="input"> <i class="icon-prepend fa fa-file-text"></i>
                        <input type="text" name="noOfLicense" id="noOfLicense" class="validate[noOfLicense]" onchange='getPrice(this.value)'/>
                      </label>
                    </div>
                  </div>
                </section>
               
                <section>
                  <div class="row" >
                    <label class="label col col-4">Amount</label>
                    <div class="col col-8">
                      <label class="input"><i class="icon-prepend fa fa-inr"></i>
                        <input type="text" name="amount" class="validate[amount] amount" readonly/>
                      </label>
                    </div>
                  </div>
                </section>
               
              </fieldset>
            <!-- General Unify Forms -->
          </div>
          <!-- End Content -->
        </div>
        <div class="modal-footer" style="border-top:none;">
          <button type="button" class="btn-u btn-u-primary" onclick='validateAddLicenseFields()'>Add</button>
           <button type="reset" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
        </div>
         </form>
      </div>
    </div>
  </div>
  <!-- Add License popup Ends -->
  
  <!-- Cancel License popup Start --> 
  <div class="modal fade" id="cancelLicense" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Cancel License</h4>
        </div>
        <form  class="sky-form" id="cancel-license-form">
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Unify Forms -->
              <fieldset>
                <section>
                  <div class="row">
                    <label class="label col col-4">Game</label>
                   <div class="col col-8">
                      <label class="input"> <i class="icon-prepend fa fa-user"></i>
                        <input type="text" name="gameName" class="gameName" class="validate[gameSelect]" readonly />
                         <input type="hidden" name="gameId" class="gameId"/>
                      </label>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row">
                    <label class="label col col-4">No of Licenses</label>
                    <div class="col col-8">
                      <label class="input"> <i class="icon-prepend fa fa-file-text"></i>
                        <input type="text" name="noOfLicense" id="cancel-noOfLicense" class="validate[noOfLicense]" onchange='getPrice(this.value)'/>
                      </label>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row" >
                    <label class="label col col-4">Amount</label>
                    <div class="col col-8">
                      <label class="input"><i class="icon-prepend fa fa-inr"></i>
                        <input type="text" name="amount"  class="validate[amount] amount" readonly/>
                      </label>
                    </div>
                  </div>
                </section>
               
              </fieldset>
            <!-- General Unify Forms -->
          </div>
          <!-- End Content --> 
        </div>
        <div class="modal-footer" style="border-top:none;">
          <button type="button" class="btn-u btn-u-primary" onclick='validateCancelLicenseFields()'>Cancel License</button>
          <button type="reset" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
        </div>
         </form>
      </div>
    </div>
  </div>
  <!-- Cancel License popup Ends -->
   <!-- confirmation popup starts -->
<div class="modal fade" id="confirmationpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color:azure">
        <div class="modal-header" style="padding-bottom:16px; border:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Forms -->
            <div>
                  <div class="row">
                    <div class="col col-12 msgDiv">
                      <h4 class="color-green" id=msg></h4>
                    </div>
                  </div>
            </div>
            <!-- General Forms -->
          </div>
          <!-- End Content --> 
        </div>
         <input type="hidden" id="operation-type">
         <div class="modal-footer" style="border-top:none;">
          <button type="submit" class="btn-u btn-u-primary"  onclick='addCancelLicense()' >OK</button>
          <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
 <!-- confirmation popup ends --> 
 
   <!-- success popup starts -->
<div class="modal fade" id="success-pouup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color:azure">
        <div class="modal-header" style="padding-bottom:16px; border:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Forms -->
            <div>
                  <div class="row">
                    <div class="col col-12 msgDiv">
                      <h4 class="color-green" id=sucess-msg></h4>
                    </div>
                  </div>
            </div>
            <!-- General Forms -->
          </div>
          <!-- End Content --> 
        </div>
          <div class="modal-footer" style="border-top:none;">
          <button type="button" class="btn-u btn-u-primary" data-dismiss="modal">OK</button>
        </div>
      </div>
    </div>
  </div>
 <!-- success popup ends --> 
 
  <input type="hidden" name='bpId' id= 'bpId' value='${bpDetails.userId}'/>
  <script type="text/javascript" src="assets/js/ventureBean/user/bp/bpDashboard.js"></script>
</html>