\<%@ include file="/common/taglibs.jsp"%>
<!--=== Content Part ===-->
<style>
.game-creation-content {
	background-color: rgb(235, 239, 237);
	margin-top: 2%;
	margin-bottom: 2%;
	padding-bottom: 0px;
	
}
</style>

<body>
<div class="container content game-creation-content">
	<div class="row">
		<!-- Begin Content -->
		<s:form action="bp/createProject" class="sky-form" commandName="projectForm" method="post" id="projectForm">
			<input type="hidden" name="bpId" value="${adminDetailsSessionForm.userId}" />
			<input type="hidden" name="companyId" value="${projectForm.companyId}" />
			<input type="hidden" name="projectId" value="${projectForm.projectId}" />
			<input type="hidden" id="operation" value="${operation}" />
			<div class="col-md-6">
				<div class="panel-body">
					<form class="margin-bottom-40 sky-form" role="form">
						<div class="form-group">
							<label for="exampleInputCompanyName">Company Name</label> <label
								class="input"> <i class="icon-prepend fa fa-university"></i> <s:input
									type="text" path="companyName" readonly="true" placeholder="Enter Company Name" />
						</div>
						<div class="form-group">
							<label for="exampleInputNumberOfPlayer">Player(s)</label>
							<label class="input"> <i
								class="icon-prepend fa fa-users"></i> <s:input type="text"
									path="noOfPlayers" placeholder="Enter Number of players" class="validate[noOfPlayers]" />
						</div>
						<div class="form-group">
							<label for="exampleInputHrEmail">HR Email address</label> <label
								class="input"> <i class="icon-prepend fa fa-envelope"></i>
								<s:input path="hrEmail" id="hrEmail"
									placeholder="Enter HR Email address"  class="validate[emailAddress]" />
						</div>
						<div class="form-group">
							<label for="exampleInputHrEmail">HR Name</label> <label
								class="input"> <i class="icon-prepend fa fa-user"></i>
								<s:input path="hrName" id="hrName" placeholder="Enter HR Name" class="validate[hrName]"  />
						</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel-body sky-form">
					<div class="form-group">
						<label for="exampleInputGame">Game</label> <label class="select">
							<s:select path="gameId" id="gameId" class="validate[gameSelect]">
								<s:option value="">Please Select...</s:option>
								<s:options items="${gameLicenseMap}" itemValue="gameId" itemLabel="gameLicensemap" />
							</s:select><i></i>
						</label>
					</div>
					<div class="form-group">
						<label for="exampleInputLocation">Start Date</label> <label
							class="input"> <i class="icon-prepend fa fa-calendar"></i>
							<s:input type="text" path="startDate" placeholder="Enter Game StartDate" class="validate[startDate]" />
					</div>
					<div class="form-group">
						<label for="exampleInputHrEmail">Facilitator Email address</label>
						<label class="input"> <i class="icon-prepend fa fa-envelope"></i> <s:input
								path="facilitatorEmail" id="facilitatorEmail"
								placeholder="Enter Facilitator Email address" class="validate[emailAddress]" />
					</div>
					<div class="form-group">
						<label for="exampleInputHrEmail">Facilitator Name</label> <label
							class="input"> <i class="icon-prepend fa fa-user"></i>
							<s:input path="facilitatorName" id="facilitatorName"
								placeholder="Enter Facilitator Name "  class="validate[facilitatorName]" />
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<!-- General Unify Forms -->
					<footer style="background:rgb(235, 239, 237);border-top: 0px;">
						<div class="row" style="text-align: center">
							<button type="button" onclick="showLoader()" class="btn-u" id="create-button"></button>
							<button type="reset" class="btn-u btn-u-default">Reset</button>
						</div>
					</footer>
					</form>
					<!-- General Unify Forms -->
				</div>
			</div>
		</s:form>
		<!-- End Content -->
	</div>
</div>
<!--/container-->
<!--=== End Content Part ===-->
<div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
<script>
function showLoader(){
	if($("#projectForm").validationEngine('validate')){
		$('#loader-wrapper').show();
		$("#projectForm").submit();
	}
	
}
$(document).ready(function() {
	$('#loader-wrapper').hide();
	 var prefix = "selectBox_";
     $("#projectForm").validationEngine({promptPosition : "bottomLeft", prettySelect : true,
         usePrefix: prefix } ,'validate');
	
	var operation = $('#operation').val();
	if(operation == 'create'){
		$('#headingId').text("Create Project");
		$('#create-button').text("Submit");
	}else if(operation == 'edit'){
		$('#headingId').text("Edit Project");
		$('#create-button').text("Update");
	}
	$('#startDate').datepicker({
		startDate : '+0d',
		autoclose : true,
		format: 'dd-mm-yyyy'
	});
});
</script>

</body>

