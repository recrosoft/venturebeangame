<%@ include file="/common/taglibs.jsp"%>
<%-- <%@ page import="com.ventureBean.constants.Constants" %> --%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Venturebean</title>
<style>
.dataTables_wrapper table thead{
    display:none;
}
</style>
<!-- Meta -->
<meta name="description" content="">
<meta name="author" content="">

<!-- CSS Customization -->
<link rel="stylesheet" href="assets/css/custom.css">
<!-- Data table CSS -->
<link rel="stylesheet" type="text/css" id="theme"  href="assets/css/plugins/datatable/jquery.dataTables.css" />
</head>

<body>
<div class="wrapper"> 

   <!--=== Content Part ===-->
  <div class="container content">
    <div class="row">
      <div class="col-md-12" style="background-color:#ebefed;"> 
      <div class="table-responsive1">
		<table id="userList" class="table "></table>
	 </div>
      </div>
      <!--/col-md-9--> 
    </div>
    <!--/row--> 
  </div>
  <!--/container--> 
  <!--=== End Content Part ===-->
  
</div>
<!--/End Wrapepr--> 

<script type="text/javascript">
var selectedRole = "${selectedRole}";
var currentUserId = "${currentUserId}";
var currentUserRole = "${currentUserRole}";
var urlSpecRoleName = "${adminDetailsSessionForm.urlSpecRoleName}";
var superUserId = "${superUserId}";
var headingText = "${headingText}";
var rolei='';
    jQuery(document).ready(function() {
    	 rolei = getSuperUserRole(selectedRole);
        if(superUserId != undefined && superUserId != ''){
        	populateuserList("getNestedList");
        }else{
        	populateuserList("getForNonVBAdmins");
        }
    	$('#headingId').text(headingText+ " Detail List"); 
    });
    function getSuperUserRole(role){
    	var roleName= '';
    	if(role != ''){
    		switch(role){
    		case "BM" :roleName="Business Head";break;
    		case "BDM" :roleName="Business Manager";break;
    		case "BP" :roleName="Business Dev Manager";break;
    		
    		}
    	}
    	return roleName;
    }
    function populateuserList(url) {

		if ($.fn.dataTable.isDataTable('#userList')) {
			var oTable = $('#userList').dataTable();
			oTable.fnDestroy();
		}
		
		var rowCount = 1;
		//this array is used by DataTable for sorting
		var userList = $('#userList').dataTable(
						{
							"language": {
					        	"emptyTable": "<span style='color: #190C1D;'><img src='assets/img/empty.png' width=40>&nbsp;<strong>No "+headingText+" added Yet</strong></span>"
					    	},
							"sPaginationType" : "full_numbers",
							"lengthChange" : false,
							"iDisplayLength" :2,
							"bFilter" : false,
							//"sDom": 't',
							"bPaginate": true, //hide pagination
							"ajax" : {
								"url" :url,
								"type" : 'POST',
								"data" : function(d) {
									 d.selectedRole =selectedRole;
									 d.currentUserId =currentUserId;
									 d.currentUserRole =currentUserRole;
									 d.superUserId = superUserId;
								} 
							},  
							"fnRowCallback": function(nRow, aaData, iDisplayIndex) {
								$(nRow).css('background-color', 'transparent');	
							},
							"fnDrawCallback": function(oSettings) {
						        //if ($('#userList tr').length < 3) 
								var rows = this.fnGetData();
								  if ( rows.length === 0 ) {
									  $('.dataTables_paginate').hide();
									  $('.dataTables_info').hide();
								  }else{
									  $('.dataTables_paginate').show();
									  $('.dataTables_info').show(); 
								  }
						    },
							"columns" : [
							             { "title": "Name",
										  "render" : function(data, type, full) {
											   if (full == null || full == "") {
													return '<span>-<span>';
												} else {
													var rowdata = "<span class='col-md-1'> <div class='circle'>"+rowCount++ +" </div> </span>"; 
													rowdata = rowdata + "<span class='col-md-10 admin-row-sub' >";
										            rowdata=rowdata+"<h3><a href='"+urlSpecRoleName+"/user/"+selectedRole+"/"+ full.userId+"'>"+full.name+"</a></h3>";
										            rowdata=rowdata+"<ul style='margin-left: -38px;'><li><i class='fa fa-envelope' style='color:#77925D;'></i>&nbsp; "+full.emailAddress+ "</li><li><i class='fa fa-user' style='color:#77925D;'></i>&nbsp;&nbsp;"+rolei+" : "+full.comesUnder+" </li></ul></span>";
										            rowdata=rowdata+ "<span class='col-md-1 eye'> <i class='fa fa-eye color-green'></i> </span>";
													return '<span>' + rowdata + '</span>';
												}
											}
										  }
							           ]
						});
	};
	
</script> 
<script type="text/javascript" src="assets/js/plugins/datatables/jquery.dataTables.js"></script>
</body>
</html>