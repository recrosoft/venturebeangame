<%@ include file="/common/taglibs.jsp"%>
<style>
 .circle { 
		width:111px; height:83px; padding:24px 24px 0px 24px; color:#FFFFFF; font-weight:bold;
		background-image:url(assets/img/sr_no_background.png); background-repeat:no-repeat;
	}
   </style>
  <div class="container content">
    <div class="row">
      <div class="col-md-12" style="background-color:#ebefed;"> 
        <!-- Clients Block-->
        <s:form action="user/addUsers" modelAttribute="userListForm" id="create-user-form" class="sky-form">
        	<input type="hidden" name="emailSuffix" value="${emailSuffix}">
          <!-- this loop will display the players those are already saved in DB -->
         <c:forEach var="player" items="${playerList}" varStatus="status">
        	 <!-- start first row-->
          <div class="row clients-page" style="border-bottom:solid 2px #d1d2d1; margin-top:20px;"">
            <div class="col-md-1">
              <div class="circle" style="padding: 21px 24px 0px 27px;">${status.count}</div>
            </div>
            <input type="hidden" name="userForm[${status.count}].projectUserId" value="${player.playerId}">
            <div class="col-md-4" style="margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-user"></i>
                  <input type="text" class="validate[adminName]" name="userForm[${status.count}].name" placeholder="Player Name" value="${player.name }">
                </label>
              </section>
            </div>
            <div class="col-md-4" style=" margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-envelope"></i>
                  <input type="text" class="validate[userName]" name="userForm[${status.count}].emailAddress" placeholder="Email address" value="${player.emailAddress }">
                </label>
              </section>
            </div>
            <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
               <span>${emailSuffix}</span>
              </section>
            </div>
          </div>
           <!-- End first row-->
        </c:forEach>
        <!-- this loop will display the rest no of empty rows that the Facilitator has to enter -->
        <c:forEach begin="${fn:length(playerList)+1}" end="${totalNoOfPlayer}" var="index" step="1" >
        	 <!-- start first row-->
          <div class="row clients-page" style="border-bottom:solid 2px #d1d2d1; margin-top:20px;"">
            <div class="col-md-1">
              <div class="circle">${index}</div>
            </div>
            <div class="col-md-4" style="margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-user"></i>
                  <input type="text" class="validate[adminName]" name="userForm[${index}].name" placeholder="Player Name">
                </label>
              </section>
            </div>
            <div class="col-md-4" style=" margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-envelope"></i>
                  <input type="text" class="validate[userName]" name="userForm[${index}].emailAddress" placeholder="Email address">
                </label>
              </section>
            </div>
               <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
               <span>${emailSuffix}</span>
              </section>
            </div>
          </div>
           <!-- End first row-->
        </c:forEach>
        	<div class="row">
				<div class="col-md-12">
					<!-- General Unify Forms -->
						<c:if test="${projectStatus == 'CREATED'}">
						<footer>
							<div class="row" style="text-align: center">
								<button type="button" onclick="showLoader()" class="btn-u">Submit</button>
								<button type="reset" class="btn-u btn-u-default">Reset</button>
							</div>
						</footer>
					</c:if>
					</form>
					<!-- General Unify Forms -->
				</div>
			</div>
       </s:form>
      </div>
      <!--/col-md-9--> 
    </div>
    <!--/row--> 
  </div>
  <!--/container--> 
  <!--=== End Content Part ===--> 
  </div>
	 <div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
  <<script type="text/javascript">
  function showLoader(){
		if($("#create-user-form").validationEngine('validate')){
			$('#loader-wrapper').show();
			$("#create-user-form").submit();
		}
		
	}
  $(document).ready(function() {
	  $('#loader-wrapper').hide();
	  $('#headingMsg').text("Create User");
	  var prefix = "selectBox_";
	  $('#create-user-form').validationEngine({promptPosition : "bottomLeft", prettySelect : true,
	         usePrefix: prefix },'validate');
	     
  });
</script>
