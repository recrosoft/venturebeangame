<%@ include file="/common/taglibs.jsp"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Facilitator</title>
<style>
.btn-padding{
	padding: 10px 12px;
}
.btn-reset{
 margin-top: 10px;
 box-shadow: 0 2px 8px 0 rgba(0, 0, 0, 0.6), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
}
.active-new:hover{background-color:#989090;}
</style>
</head>
<!--=== Content Part ===-->
<div class="container">
	<div class="row">
		<!-- Begin Content -->
		<div class="col-md-12">
			<!--Basic Table-->
			<div class="panel margin-bottom-40">
				<div class="panel-body">
					<c:choose>
						<c:when test="${game.projectStatus =='CREATED' || game.projectStatus =='COMPLETED'}">
							<div class="row clients-page" style="margin-top: 20px;">
								<div class="col-md-1">
									<div class="circle" style="padding: 23px 24px 0px 26px;"><i class="fa fa-user" style="color:#fff;font-size:18px;"></i></div>
								</div>
								<div class="col-md-9">
									<h3>${facilitator.name}</h3>
									<p><i class="fa fa-envelope" style="color:#77925D;"></i>&nbsp;&nbsp;
										${facilitator.emailAddress}
									</p>
									<!--  <button class="btn-u btn-u-sm rounded-3x btn-u-orange" type="button">Add License</button> -->
								</div>
							</div>
							<table class="table">
								<thead>
									<tr>
										<th>Game</th>
										<th>Company</th>
										<th>Player(s)</th>
										<th>Start Date</th>
										<th>Status</th>
										<th>Created On</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>${game.gameName}</td>
										<td>${game.companyName}</td>
										<td>${game.noOfPlayers}</td>
										<td>${game.startDate}</td>
										<td>${game.projectStatus}</td>
										<td>${game.createdOn}</td>
										<c:if test="${game.projectStatus =='CREATED'}">
											<td><a href="user/startGame?projectId=${game.projectId}"><button class="btn btn-danger btn-sm">
													Start</button></a></td>
										</c:if>
										<c:if test="${game.projectStatus =='COMPLETED'}">
											<td><a href="user/viewGameResult?projectId=${game.projectId}"><button class="btn btn-u-sm btn-sm">
													View Result</button></a></td>
										</c:if>
									</tr>
								</tbody>
							</table>
						</c:when>
						<c:otherwise>
							<div class="tab-v1">
								<ul class="nav nav-tabs" style="padding-bottom: 20px;">
									<c:forEach var="team" items="${teams}" varStatus="count">
										<c:choose>
											<c:when test="${count.index == 0}">
												<li class="active"><a href="#tab-${count.index}" data-toggle="tab">
														<button class="btn btn-u-lg rounded btn-white active-new down-arrow" type="button">${team.teamName}</button>
												</a></li>
											</c:when>
											<c:otherwise>
												<li><a href="#tab-${count.index}" data-toggle="tab">
														<button class="btn btn-u-lg rounded btn-red down-arrow" style="background-color:${team.teamColor}" type="button">${team.teamName}</button>
												</a></li>
											</c:otherwise>
										</c:choose>
									</c:forEach>
								</ul>
								<div class="tab-content" style="padding-top: 2%;">
								<c:forEach items="${teams}" var="team" varStatus="teamIndex">
										<c:choose>
											<c:when test="${teamIndex.index == 0}">
												<div class="tab-pane fade in active" id="tab-${teamIndex.index}">
											</c:when>
											<c:otherwise>
												<div class="tab-pane fade in" id="tab-${teamIndex.index}">
											</c:otherwise>
										</c:choose>
										
										<c:forEach items="${teamStatMap[fn:trim(team.teamName)]}" var="gameStat">
											<c:if test="${gameStat.status == 'COMPLETED'}">
												<c:if test="${gameStat.dayNo == 'DAY_01' || gameStat.dayNo == 'DAY_02' || gameStat.dayNo == 'DAY_03' }">
														<div class="row">
														<div class="col-md-12">
															<!--=== Sub Row 01 ===-->
															<div class="row clients-page row-bg-padding">
																<div class="col-md-11 row-col-padding">
																	<h4 class="row-h3-color">${gameStat.dayNo}</h4>
																</div>
																<form action="user/resetDay" id="resetForm-${fn:trim(team.teamName)}-${gameStat.dayNo}" method="POST">
										                    	<input type="hidden" name="gameStatId" value="${gameStat.gameStatId}">
										                    	<div class="col-md-1 row-col-fontsize">
											                      <button class="btn-u btn-u-sm rounded-3x btn-u-orange btn-reset" type="button" onclick='setFormId("resetForm-${fn:trim(team.teamName)}-${gameStat.dayNo}")' data-toggle="modal" data-target="#confirmationpopup">Reset</button>
											                    </div>
										                    </form>
															</div>
															<!--=== Sub Row 01 End ===-->
															
															<!--=== Sub Row 02 ===-->
															<div class="row clients-page" style="text-align: center;">
																<div class="col-md-6" style="border-right: 2px solid #388e3c">
																	<div class="row subrow-fontsize-bg">
																		<div class="col-md-12">
																			<h4 class="row-h3-color">Lives Saved</h4>
																		</div>
																	</div>
																	<div class="row" style="background-color: #c8e6c9;">
																		<div class="col-md-12 row-col-fontsize">
																			<h4 class="row-h3-color">${gameStat.livesSaved}</h4>
																		</div>
																	</div>
																</div>
																<div class="col-md-6">
																	<div class="row subrow-fontsize-bg">
																		<div class="col-md-12 subrow-fontsize-bg">
																			<h4 class="row-h3-color">Lives Lost</h4>
																		</div>
																	</div>
																	<div class="row" style="background-color: #c8e6c9;">
																		<div class="col-md-12 row-col-fontsize">
																			<h4 class="row-h3-color">${gameStat.livesLost}</h4>
																		</div>
																	</div>
																</div>
															</div>
															<!--=== Sub Row 02 End ===-->
														</div>
													</div>
													
												</c:if>
												
												<c:if test="${gameStat.dayNo == 'DAY_04' || gameStat.dayNo == 'DAY_05' || gameStat.dayNo == 'DAY_06' }">
													<div class="row">
										                <div class="col-md-12"> 
										                  <!--=== Sub Row 01 ===-->
										                  <div class="row clients-page row-bg-padding">
										                    <div class="col-md-11 row-col-padding">
										                      <h4 class="row-h3-color">${gameStat.dayNo}</h4>
										                    </div>
										                    <form action="user/resetDay" id="resetForm-${fn:trim(team.teamName)}-${gameStat.dayNo}" method="POST">
										                    	<input type="hidden" name="gameStatId" value="${gameStat.gameStatId}">
										                    	<div class="col-md-1 row-col-fontsize">
											                      <button class="btn-u btn-u-sm rounded-3x btn-u-orange btn-reset" type="button" onclick='setFormId("resetForm-${fn:trim(team.teamName)}-${gameStat.dayNo}")' data-toggle="modal" data-target="#confirmationpopup">Reset</button>
											                    </div>
										                    </form>
										                  </div>
										                  <!--=== Sub Row 01 End ===-->
										                  <!--=== Sub Row 02 ===-->
										                  <div class="row clients-page" style="text-align:center;">
										                    <div class="col-md-3" style="border-right:2px solid #388e3c">
										                      <div class="row subrow-fontsize-bg">
										                        <div class="col-md-12" >
										                          <h4  class="row-h3-color">Money Spent</h4>
										                        </div>
										                      </div>
										                      <div class="row" style="background-color:#c8e6c9;">
										                        <div class="col-md-12 row-col-fontsize">
										                          <h4 class="row-h3-color">${gameStat.moneySpent}</h4>
										                        </div>
										                      </div>
										                    </div>
										                    <div class="col-md-3" style="border-right:2px solid #388e3c">
										                      <div class="row subrow-fontsize-bg">
										                        <div class="col-md-12 subrow-fontsize-bg">
										                          <h4 class="row-h3-color">No. of Campus Built</h4>
										                        </div>
										                      </div>
										                      <div class="row" style="background-color:#c8e6c9;">
										                        <div class="col-md-12 row-col-fontsize">
										                          <h4 class="row-h3-color">${gameStat.campusBuilt}</h4>
										                        </div>
										                      </div>
										                    </div>
										                    <div class="col-md-3" style="border-right:2px solid #388e3c">
										                      <div class="row subrow-fontsize-bg">
										                        <div class="col-md-12 subrow-fontsize-bg">
										                          <h4 class="row-h3-color">Used Resource</h4>
										                        </div>
										                      </div>
										                      <div class="row" style="background-color:#c8e6c9;">
										                        <div class="col-md-12 row-col-fontsize">
										                          <h4 class="row-h3-color">${gameStat.resourcesUsed}</h4>
										                        </div>
										                      </div>
										                    </div>
										                    <div class="col-md-3">
										                      <div class="row subrow-fontsize-bg">
										                        <div class="col-md-12 subrow-fontsize-bg">
										                          <h4  class="row-h3-color">No. of Survivor</h4>
										                        </div>
										                      </div>
										                      <div class="row" style="background-color:#c8e6c9;">
										                        <div class="col-md-12 row-col-fontsize">
										                          <h4  class="row-h3-color">${gameStat.survivorsCount}</h4>
										                        </div>
										                      </div>
										                    </div>
										                  </div>
										                  <!--=== Sub Row 02 End ===-->
										                    
										                </div>
										              </div>
												</c:if>
											</c:if>	
										</c:forEach>
									</div>
								</c:forEach>
								<%-- ${teamStatMap} --%>
							</div>
							<c:if test="${startSession2 == true}">
								<form action="user/startSession2" id="session2-start-form" method="POST">
									<input type="hidden" name="projectId" value="${game.projectId}">
									<div class="col-md-12 row-col-fontsize" style="text-align: center;">
					                     <button  class="btn-u btn-u-sm rounded-3x btn-u-orange btn-padding" type="button" onclick='setMessage("session2-start")' data-toggle="modal" data-target="#confirmationpopup"> Start 2nd Session </button>
					                 </div>
								</form>
							</c:if>	
							<c:if test="${endGame == true}">
								<form action="user/endGame" id="game-end-form" method="POST">
									<input type="hidden" name="projectId" value="${game.projectId}">
									<div class="col-md-12 row-col-fontsize" style="text-align: center;">
					                     <button class="btn-u btn-u-sm rounded-3x btn-u-orange btn-padding" type="button" onclick='setMessage("game-end")' data-toggle="modal" data-target="#confirmationpopup">End Game </button>
					                 </div>
								</form>
							</c:if>	
						</c:otherwise>
					</c:choose>
					<!-- End Clients Block-->
				</div>
			</div>
			<!--End Basic Table-->
		</div>
		<!-- End Content -->
	</div>
</div>
<input type="hidden" id="game-name" value="${game.gameName}">
<input type="hidden" id="game-status" value="${game.projectStatus}">
 <!-- confirmation popup starts -->
<div class="modal fade" id="confirmationpopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color:azure">
        <div class="modal-header" style="padding-bottom:16px; border:none">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Forms -->
            <div class="sky-form">
                  <div class="row">
                    <div class="col col-12 msgDiv">
                      <h4 class="color-green" id="msg"></h4>
                    </div>
                  </div>
            </div>
            <!-- General Forms -->
          </div>
          <!-- End Content --> 
        </div>
        <input type="hidden" id="formId">
         <div class="modal-footer" style="border-top:none;">
          <button type="button" class="btn-u btn-u-primary"  onclick='submitForm()' >OK</button>
          <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
         </div>
      </div>
    </div>
    <script type="text/javascript" src="assets/js/ventureBean/user/facilitator/facilitatorHome.js"></script>
  </div>
 <!-- confirmation popup ends --> 
 
 