<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Player</title>
<!-- <meta http-equiv="refresh" content="04" URL="http://localhost:8080/VentureBeanGame/userShowHome"/>
    This will refresh page in every 5 seconds, change content= x to refresh page after x seconds -->
</head><!--=== Content Part ===-->
<div class="container content">

<c:forEach items="${gameStats}" var="gameStat">
	<c:if test="${gameStat.dayNo == 'DAY_01' || gameStat.dayNo == 'DAY_02' || gameStat.dayNo == 'DAY_03'}">
		<c:if test="${gameStat.status == 'ACTIVE'}">
			<div class="row">
			<input type="hidden" id="day-start-time" value="${gameStat.startTimeMills}">	
			<div class="col-md-12">
			<form action="user/recordGameStat" id="dayStatForm">
				<input type="hidden" name="gameStatId" value="${gameStat.gameStatId}">	
		
				<!--=== Sub Row 01 ===-->
				<div class="row clients-page row-bg-padding">
					<div class="col-md-11 row-col-padding">
						<h4 class="row-h3-color">${gameStat.dayNo}</h4>
					</div>
				</div>
				<!--=== Sub Row 01 End ===-->
	
				<!--=== Sub Row 02 ===-->
				<div class="row clients-page" style="text-align: center;">
					<div class="col-md-6" style="border-right: 2px solid #388e3c">
						<div class="row subrow-fontsize-bg">
							<div class="col-md-12">
								<h4 class="row-h3-color">Lives Saved</h4>
							</div>
						</div>
						<div class="row" style="background-color: #c8e6c9;">
							<div class="col-md-6" style="font-size: 30px; margin: 12px 25% 0px 25%">
								<div class="form-group">
								<c:if test="${gameStat.dayNo != 'DAY_03'}">
									<input type="text" class="form-control validate[livesSaved]" id="livesSaved" name="livesSaved"
										placeholder="Enter Value" style="text-align: center">
								</c:if>
								<c:if test="${gameStat.dayNo == 'DAY_03'}">
									<input type="text" class="form-control validate[livesSaved-day3]" id="livesSaved" name="livesSaved"
										placeholder="Enter Value" style="text-align: center">
								</c:if>			
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="row subrow-fontsize-bg">
							<div class="col-md-12 subrow-fontsize-bg">
								<h4 class="row-h3-color">Lives Lost</h4>
							</div>
						</div>
						<div class="row" style="background-color: #c8e6c9;">
							<div class="col-md-6"
								style="font-size: 30px; margin: 12px 25% 0px 25%">
								<div class="form-group">
									<input type="text" class="form-control validate[livesLost]" id="livesLost" name="livesLost" 
										placeholder="Enter Value" style="text-align: center">
								</div>
							</div>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row">
						<div class="col-md-12">
							<!-- General Unify Forms -->
							<footer style="background: rgb(235, 239, 237); border-top: 0px;margin-top:0.5%">
								<div class="row" style="text-align: center">
									<button type="reset" class="btn-u btn-u-default">Reset</button>
									<button type="submit" class="btn-u">Submit</button>
								</div>
							</footer>
							</div>
							<!-- General Unify Forms -->
						</div>
					</div>
				</div>
				<!--=== Sub Row 02 End ===-->
			</div>
		</form>
		<!--=== Second Row End ===-->
		<div class="divider"></div>
	</c:if>
	<c:if test="${gameStat.status == 'COMPLETED'}">
		<!--=== First Row Start ===-->
	<div class="row">
		<div class="col-md-12">
			<!--=== Sub Row 01 ===-->
			<div class="row clients-page row-bg-padding">
				<div class="col-md-11 row-col-padding">
					<h4 class="row-h3-color">${gameStat.dayNo}</h4>
				</div>
			</div>
			<!--=== Sub Row 01 End ===-->
			
			<!--=== Sub Row 02 ===-->
			<div class="row clients-page" style="text-align: center;">
				<div class="col-md-6" style="border-right: 2px solid #388e3c">
					<div class="row subrow-fontsize-bg">
						<div class="col-md-12">
							<h4 class="row-h3-color">Lives Saved</h4>
						</div>
					</div>
					<div class="row" style="background-color: #c8e6c9;">
						<div class="col-md-12 row-col-fontsize">
							<h4 class="row-h3-color">${gameStat.livesSaved}</h4>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row subrow-fontsize-bg">
						<div class="col-md-12 subrow-fontsize-bg">
							<h4 class="row-h3-color">Lives Lost</h4>
						</div>
					</div>
					<div class="row" style="background-color: #c8e6c9;">
						<div class="col-md-12 row-col-fontsize">
							<h4 class="row-h3-color">${gameStat.livesLost}</h4>
						</div>
					</div>
				</div>
			</div>
			<!--=== Sub Row 02 End ===-->
		</div>
	</div>
	<!--=== First Row End ===-->
	<div class="divider"></div>
	</c:if>
	
	</c:if>
	
	<c:if test="${gameStat.dayNo == 'DAY_04' || gameStat.dayNo == 'DAY_05' || gameStat.dayNo == 'DAY_06'}">
		<c:if test="${gameStat.status == 'COMPLETED'}">
		  <div class="row">
                <div class="col-md-12"> 
                
                  <!--=== Sub Row 01 ===-->
                  <div class="row clients-page row-bg-padding">
                    <div class="col-md-11 row-col-padding">
                      <h4 class="row-h3-color">${gameStat.dayNo}</h4>
                    </div>
                  </div>
                  <!--=== Sub Row 01 End ===-->
                  <!--=== Sub Row 02 ===-->
                  <div class="row clients-page" style="text-align:center;">
                    <div class="col-md-3" style="border-right:2px solid #388e3c">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12" >
                          <h4  class="row-h3-color">Money Spent</h4>
                        </div>
                      </div>
                      <div class="row" style="background-color:#c8e6c9;">
                        <div class="col-md-12 row-col-fontsize">
                          <h4 class="row-h3-color">${gameStat.moneySpent}</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3" style="border-right:2px solid #388e3c">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12 subrow-fontsize-bg">
                          <h4 class="row-h3-color">No. of Campus Built</h4>
                        </div>
                      </div>
                      <div class="row" style="background-color:#c8e6c9;">
                        <div class="col-md-12 row-col-fontsize">
                          <h4 class="row-h3-color">${gameStat.campusBuilt}</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3" style="border-right:2px solid #388e3c">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12 subrow-fontsize-bg">
                          <h4 class="row-h3-color">Used Resource</h4>
                        </div>
                      </div>
                      <div class="row" style="background-color:#c8e6c9;">
                        <div class="col-md-12 row-col-fontsize">
                          <h4 class="row-h3-color">${gameStat.resourcesUsed}</h4>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12 subrow-fontsize-bg">
                          <h4  class="row-h3-color">No. of Survivor</h4>
                        </div>
                      </div>
                      <div class="row" style="background-color:#c8e6c9;">
                        <div class="col-md-12 row-col-fontsize">
                          <h4  class="row-h3-color">${gameStat.survivorsCount}</h4>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--=== Sub Row 02 End ===-->
                    
                </div>
              </div>
              <div class="divider"></div>
	</c:if>
	<c:if test="${gameStat.status == 'ACTIVE'}">
		  <!--=== Third Row Start ===-->
		  <input type="hidden" id="day-start-time" value="${gameStat.startTimeMills}">	
              <div class="row">
                <div class="col-md-12"> 
                <form action="user/recordGameStat" id="dayStatForm">
					<input type="hidden" name="gameStatId" value="${gameStat.gameStatId}">	
                  <!--=== Sub Row 01 ===-->
                  <div class="row clients-page row-bg-padding">
                    <div class="col-md-11 row-col-padding">
                      <h4 class="row-h3-color">${gameStat.dayNo}</h4>
                    </div>
                  </div>
                  <!--=== Sub Row 01 End ===-->
               
                  <!--=== Sub Row 02 ===-->
                  <div class="row clients-page" style="text-align:center;">
                    <div class="col-md-3" style="border-right:2px solid #388e3c">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12" >
                          <h4 class="row-h3-color">Money Spent</h4>
                        </div>
                      </div>
                      <div class="row"  style="background-color:#c8e6c9;">
                        <div class="col-md-6" style="font-size:30px;  margin:12px 25% 0px 25%">
                          <div class="form-group">
                       		<input type="text" class="form-control validate[moneySpent]" id="moneySpent" name="moneySpent" placeholder="Enter Value" style="text-align:center">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3" style="border-right:2px solid #388e3c">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12" style="font-size:30px; background-color:#a5d6a7 ">
                          <h4 class="row-h3-color">No. of Campus Built</h4>
                        </div>
                      </div>
                      <div class="row"  style="background-color:#c8e6c9;">
                        <div class="col-md-6" style="font-size:30px; margin:12px 25% 0px 25%">
                          <div class="form-group">
                       		<input type="text" class="form-control validate[campusBuilt]" id="campusBuilt" name="campusBuilt" placeholder="Enter Value" style="text-align:center">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3" style="border-right:2px solid #388e3c">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12 subrow-fontsize-bg">
                          <h4 class="row-h3-color">Used Resource</h4>
                        </div>
                      </div>
                      <div class="row"  style="background-color:#c8e6c9;">
                        <div class="col-md-6" style="font-size:30px; margin:12px 25% 0px 25%">
                          <div class="form-group">
                       		<input type="text" class="form-control validate[resourcesUsed]" id="resourcesUsed" name="resourcesUsed" placeholder="Enter Value" style="text-align:center">
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="row subrow-fontsize-bg">
                        <div class="col-md-12 subrow-fontsize-bg">
                          <h4 class="row-h3-color">No. of Survivor</h4>
                        </div>
                      </div>
                      <div class="row"  style="background-color:#c8e6c9;">
                        <div class="col-md-6" style="font-size:30px; margin:12px 25% 0px 25%">
                          <div class="form-group">
                       		<input type="text" class="form-control validate[survivorsCount]" id="survivorsCount" name="survivorsCount" placeholder="Enter Value" style="text-align:center">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--=== Sub Row 02 End ===-->
                   <div class="divider"></div>
					<div class="row">
						<div class="col-md-12">
							<!-- General Unify Forms -->
							<footer style="/* background: rgb(235, 239, 237); */ border-top: 0px;">
								<div class="row" style="text-align: center">
									<button type="submit" class="btn-u">Submit</button>
									<button type="reset" class="btn-u btn-u-default">Reset</button>
								</div>
							</footer>
							</div>
							<!-- General Unify Forms -->
						</div>  
					</form>
                </div>
              </div>
              <!--=== Third Row End ===-->
              <div class="divider"></div>
	</c:if>
	</c:if>
	
</c:forEach>	

</div>

<!--=== End Content Part ===-->
<input type="hidden" id="team-name" value="${gameStats[0].teamName}">
<input type="hidden" id="team-color" value="${gameStats[0].teamColor}">
<input type="hidden" id="refreshButton" value="${refreshButton}">

<script>
var DEFAULT_MONEY_SPENT = 20;
var DEFAULT_LIVES_SAVED = 3;
var DEFAULT_LIVES_SAVED_3 =6;
var DEFAULT_CAMPUS_BUILT = 0;
var DEFAULT_RESOURSE_USED = 0;
var DEFAULT_SERVIVORS_COUNT = 0;
var DEFAULT_LIVES_LOST = 0;

 $(document).ready(function() {
	 var color =$('#team-color').val();
	 var rehfresh = $('#refreshButton').val();
	 $('#headingMsg').wrapInner("<span >Team : <button style='background:"+color+";'>"+$('#team-name').val()+"</button></span>");
	if(rehfresh == "Y"){
		 $('#borderHeader-id').children('#timer-id').append('<button style="background:#e67e22;" id="btReload" class="btn-u">Refresh</button>');
	}else{
		 $('#borderHeader-id').children('#timer-id').append('<span id="timer"></span>');
	}
	 setTimer();
	 // validate Form
	 var prefix = "selectBox_";
     $("#dayStatForm").validationEngine({promptPosition : "bottomLeft", prettySelect : false,
         usePrefix: prefix ,scroll: false },'validate');
     
  // RELOAD PAGE ON BUTTON CLICK EVENT.
     $('#btReload').click(function () {
    	 var rehfresh = $('#refreshButton').val();
    	 if(rehfresh == 'Y'){
    		 location.reload(true); 
    	 }
        
     }); 
     // SET AUTOMATIC PAGE RELOAD TIME TO 5000 MILISECONDS (5 SECONDS).
     setInterval('refreshPage()', 9000);
 })
 
 function setTimer(){
	 var startTime = $('#day-start-time').val(); 
	 var tenMinInMills = 10*60*1000 ; //actual  10*60*1000
	 var initialTime = parseInt(startTime) +tenMinInMills - Date.now();
	 if(initialTime > 0){
		 var timerTime = new Date(initialTime);
		 $('#timer').text(timerTime.toUTCString().split(" ")[4]);  
		 var intervalId = setInterval(function() {		
			initialTime = initialTime - 1000;
				var time =  new Date(initialTime).toUTCString().split(" ")[4];
			     //var t = d.toLocaleTimeString();
			     if(initialTime <= 0) {
			    	 clearInterval(intervalId);
			    	setDefaultValues();
			    	$('#dayStatForm').submit();
			     }
			     $('#timer').text(time); 
			}, 1000);
		 } else {
			 $('#timer').text("00:00:00");
			 setDefaultValues();
			 $('#dayStatForm').submit();
		 }
 }

 function refreshPage() { 
	 var rehfresh = $('#refreshButton').val();
	 if(rehfresh == 'Y'){
		 location.reload(); 
	 }
}

 function setDefaultValues(){
	 if( $('#livesSaved').hasClass('validate[livesSaved-day3]')){
		 if($('#livesSaved').val() == ''){
			 $('#livesSaved').val(DEFAULT_LIVES_SAVED_3);
		 }else if($('#livesSaved').val() < 6 || $('#livesSaved').val() > 30){
			 $('#livesSaved').val(DEFAULT_LIVES_SAVED_3);
		 }
	}else{
		 if($('#livesSaved').val() == ''){
			 $('#livesSaved').val(DEFAULT_LIVES_SAVED);
		 }else if($('#livesSaved').val() < 2 || $('#livesSaved').val() > 10){
			 $('#livesSaved').val(DEFAULT_LIVES_SAVED);
		 }
	}
	 if($('#livesLost').val() == ''){
		 $('#livesLost').val(DEFAULT_LIVES_LOST);
	 }
	 if($('#moneySpent').val() == ''){
		 $('#moneySpent').val(DEFAULT_MONEY_SPENT);
		 
	 }
	 if($('#campusBuilt').val() == ''){
		 $('#campusBuilt').val(DEFAULT_CAMPUS_BUILT);
	 }
	 if($('#resourcesUsed').val() == ''){
		 $('#resourcesUsed').val(DEFAULT_RESOURSE_USED);
	 }
	 if($('#survivorsCount').val() == ''){
		 $('#survivorsCount').val(DEFAULT_SERVIVORS_COUNT);
	 }
 }
 </script>
