<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<head>
<link rel="icon" type="image/png" href="assets/img/vb-favicon.png">
<!-- CSS INCLUDE -->
 <c:set var="url">${pageContext.request.requestURL}</c:set>
  <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />

 <!-- loader css starts-->
<link rel="stylesheet" type="text/css" id="theme" href="assets/css/Venturebean/loader.css" />
 
<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/style.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
<link rel="stylesheet" href="assets/css/materialize.css">
<link rel="stylesheet" href="assets/css/plugins/validation/jquery.validationEngine.css">
<!-- CSS Theme -->
<link rel="stylesheet" href="assets/css/themes/default.css" id="style_color">
 <link rel="stylesheet" href="assets/css/pages/page_log_reg_v1.css">    

<!-- CSS Customization -->
<link rel="stylesheet" href="assets/css/custom.css">

<!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->


<script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script> 
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script> 

<script type='text/javascript' src='assets/plugins/bootstrap/js/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/validationengine/languages/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="assets/js/plugins/validationengine/jquery.validationEngine.js"></script>

<script type="text/javascript" src="assets/plugins/back-to-top.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script> 
<script type="text/javascript" src="assets/js/app.js"></script> 

 <script type="text/javascript" src="assets/plugins/back-to-top.js"></script> 
<!-- END SCRIPTS -->
 </head> 
<body  onunload="">
    <div id="content">
      <tiles:insertAttribute name="body" />
    </div>
<%
response.setHeader("Cache-Control","no-cache,no-store,must-revalidate");
response.setHeader("Pragma","no-cache");
response.setDateHeader("Expires", 0);
%>

</body>
