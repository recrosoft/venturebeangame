var selectedRole = '';
var superUserId = '';
var urlSpecRoleName ='';
var headingText = '';
var bpId = '';
var superiorRole= '';
var roleArr =  [   	{"roleId":2, "roleName":"BUSINESS HEAD"},
                     {"roleId":3, "roleName":"BUSINESS MANAGER"}, 
                     {"roleId":4,"roleName":"BDM"},
                     {"roleId":5, "roleName":"BUSINESS PARTNER"}, 
                  ];
                  
jQuery(document).ready(function() {
	$('#loader-wrapper').hide();
	$('#loader-wrapper-p').hide();
	 var prefix = "selectBox_";
     $("#create-role-form").validationEngine({promptPosition : "bottomLeft", prettySelect : true,
         usePrefix: prefix },'validate');
     $("#add-license-form").validationEngine({promptPosition : "bottomLeft", prettySelect : true,
         usePrefix: prefix },'validate');
    
        selectedRole = $('#selectedRole').val();
        superUserId = $('#superUserId').val();
        superiorRole = getSuperiorRole(selectedRole);
        urlSpecRoleName =$('#urlSpecRoleName').val();
        headingText = $('#headingText').val();
        if(superUserId != undefined && superUserId != ''){
        	populateuserList("getNestedList");
        }else{
        	populateuserList("getList");
        }
        populateRoleTypeSelect('userRoleId' , roleArr);
        if(headingText != ''){
        	$('#headingId').text(headingText+ " Detail List"); 
        }
    	
    	$('#chequeDate').datepicker({
			format : 'dd-mm-yyyy',
			autoclose : true
		});
    });
    
function getSuperiorRole(currentRole){
	var superRole = '';
	if(currentRole != undefined && currentRole != ''){
		switch(currentRole){
		case "BH":superRole = "Admin";break;
		case "BM":superRole = "BH";break;
		case "BDM":superRole = "BM";break;
		case "BP":superRole = "BDM";break;
		}
	}
	return superRole;
}

function startLoader(){
	var valid = $("#create-role-form").validationEngine('validate');
	if(valid){
		$('#loader-wrapper').show();
		$('#create-role-form').submit();
	}
}
function getSuperiorAdmin(roleId){
    //	alert($(userRoleId).find('option:selected').text());
    	var roleName = $(userRoleId).find('option:selected').text();
    	// here send roleId as 'roleId-1' bcz we need the superAdminList , If we send 'roleId' then we will get UserList with same Role
    	roleId =parseInt(roleId)-1;
    	showHidePackageBasedOnRole(roleName);
    	//hide assignTo label 
    	if(roleName != undefined && roleName == 'BUSINESS HEAD'){
    		$('#assignToDiv').hide();
    	}else{
    		$('#assignToDiv').show();
    		$.ajax({
    			url: "admin/getSuperiorAdmin?userRoleId="+roleId,
    			type : "GET",
    			success: function(result){
    	        	//alert("result"+result);
    	        	populateAssignToSelect('assignTo' , result.list);
    	   		 },
    			error: function(error){
    				alert(" ajax Error occured"+error);
    			}	
    		});
    	}
    }
 function showHidePackageBasedOnRole(roleName){
    	if(roleName != undefined && roleName == 'BUSINESS PARTNER'){
    		$('#packageDiv').show();
    		$('#elicenseDiv').show();
    	}else{
    		$('#packageDiv').hide();
    		$('#elicenseDiv').hide();
    	}
    }

    function populateAssignToSelect(selectId , data){
    	var options = "<option value=''>select...</option>";
    	 $.each(data, function(index, value ) {
    	            // alert(value.userId + ' ' + value.name);
    	            options = options+"<option value='"+value.userId+"'>"+value.name+"</option>";
    	      });
    	 $('#'+selectId+'').empty().append(options);
    }
    
    function populateRoleTypeSelect(selectId , data){
    	var options = "<option value=''>select...</option>";
    	 $.each(data, function(index, value ) {
    	            options = options+"<option value='"+value.roleId+"'>"+value.roleName+"</option>";
    	      });
    	 $('#'+selectId+'').empty().append(options);
    }
    
    function populateGames(selectId , data){
    	var options = "<option value=''>select...</option>";
    	 $.each(data, function(index, value ) {
    	            options = options+"<option value='"+value.gameId+"'>"+value.gameName+"</option>";
    	      });
    	 $('#'+selectId+'').empty().append(options);
    }
    
    function getGameList(bpId){
    	this.bpId = bpId;
    	$('#bpId').val(bpId);
    	$.ajax({
			url: "admin/getAllGame",
			type : "GET",
			success: function(result){
				populateGames('gameId',result.list);
	   		 },
			error: function(error){
				alert(" ajax Error occured"+error);
			}	
		});
	}
    
    function validateAddLicenseFields(){
    	var gameId = $("#gameId").validationEngine("validate");
    	var noLi = $("#noOfLicense").validationEngine("validate");
		var bankName = $("#bankName").validationEngine("validate");
		var chequeNo = $("#chequeNumber").validationEngine("validate");
		var amount = $("#amount").validationEngine("validate");
		var chequeDate = $("#chequeDate").validationEngine("validate");
		if(!gameId && !noLi && !bankName && !chequeNo && !amount && !chequeDate){
			$('#confirmationpopup').modal('show');
		}
    }
    
    function addLicense() {
    	$('#confirmationpopup').modal('hide');
    	$('#loader-wrapper-p').show();
    	var obj ={
				"businessPartnerId" : bpId,
    			"gameId" : $('#gameId').val(),
    			"noOfLicense" : $('#noOfLicense').val(),
    			"bankName" : $('#bankName').val(),
    			"chequeNumber" : $('#chequeNumber').val(),
    			"amount" : $('#amount').val(),
    			"chequeDate" : $('#chequeDate').val()
			};
    	$.ajax({
			url: "admin/addLicenses",
			type : "POST",
			data : obj,
			success: function(result){
			//	alert(" ajax success "+result);
				$('#addLicense').modal('hide');
				$('#success-err-msg').text("Successfully sent for admin approval");
				$('#success-error').modal('show');
				$('#loader-wrapper-p').hide();
			 },
			error: function(error){
				alert(" ajax Error occured"+error);
				$('#success-err-msg').text("Some Error Occured : "+error);
				$('.msgDiv').addClass('color-red');
				$('#success-error').modal('show');
				$('#loader-wrapper-p').hide();
			}	
		});
    }
    
    function populateuserList(url) {
    	
		if ($.fn.dataTable.isDataTable('#userList')) {
			var oTable = $('#userList').dataTable();
			oTable.fnDestroy();
		}
		var rowCount = 1;
		var userList = $('#userList').dataTable(
						{	"language": {
					        	"emptyTable":"<span style='color: #190C1D;'><img src='assets/img/empty.png' width=40>&nbsp;<strong>Please add a "+headingText+"</strong></span>"
					    	},
							"sPaginationType" : "full_numbers",
							"lengthChange" : false,
							"iDisplayLength" : 3,
							"bFilter" : false,
							//"sDom": 't',
							"bPaginate": true, //hide pagination
							"ajax" : {
								"url" : url,
								"type" : 'POST',
								"data" : function(d) {
									 d.roleName = selectedRole;
								     d.superUserId = superUserId;
								} 
							},  
							"fnRowCallback": function(nRow, aaData, iDisplayIndex) {
								$(nRow).css('background-color', 'transparent');	
							},
							"fnDrawCallback": function(oSettings) {
						        //if ($('#userList tr').length < 3) 
								var rows = this.fnGetData();
								  if ( rows.length === 0 ) {
									  $('.dataTables_paginate').hide();
									  $('.dataTables_info').hide();
								  }else{
									  $('.dataTables_paginate').show();
									  $('.dataTables_info').show(); 
								  }
						    },
							"columns" : [
							             { "title": "Name",
										  "render" : function(data, type, full) {
											 // alert(full);
													if (full == null || full == "") {
														return '<span>-<span>';
														
													} else {
														var rowdata = "<span class='col-md-1'> <div class='circle'>"+rowCount++ +" </div> </span>"; 
														rowdata = rowdata + "<span class='col-md-9 admin-row-sub'>";
														if(selectedRole == 'SA'){
															 rowdata=rowdata+"<h3>"+full.name+"</h3>";
															 rowdata=rowdata+ "<p><i class='fa fa-envelope' style='color:#77925D;'>&nbsp;&nbsp;</i>"+full.emailAddress+"</p> </span>";
														}else{
															 rowdata=rowdata+"<h3><a href='"+urlSpecRoleName+"/user/"+selectedRole+"/"+ full.userId+"'>"+full.name+"</a></h3>";
													         rowdata=rowdata+"<ul style='margin-left: -38px;'><li><i class='fa fa-envelope' style='color:#77925D;'></i>&nbsp; "+full.emailAddress+ "</li><li><i class='fa fa-user' style='color:#77925D;'></i>&nbsp;&nbsp;"+superiorRole+" : "+full.comesUnder+" </li></ul></span>";
														}
											            if(headingText == "Business Partner"){
											                rowdata=rowdata+ "<span class='col-md-2 btn wht' data-toggle='modal' data-target='#addLicense' style='margin-top: 24px' onclick=getGameList("+full.userId+")><i class='fa fa-plus-square'></i>&nbsp;&nbsp;Add License </span>";
											            }
											           return '<span>' + rowdata + '</span>';
													}
												}

											}
							             ]
						});
	};
