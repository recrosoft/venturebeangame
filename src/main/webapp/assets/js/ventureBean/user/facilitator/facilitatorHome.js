$(document).ready(function(){
	 var gameStatus = $('#game-status').val();
	//var game = $('#game-name').val();
	 $('#headingMsg').wrapInner("<span >Game: <button style='background:#e67e22;border-radius: 4px !important;'>"+$('#game-name').val()+"</button></span>");
		
	//$('#headingMsg').text("Game: " + game);
	
	if(gameStatus == "ACTIVE"){
		 $('#borderHeader-id').children('#timer-id').append('<button style="background:#e67e22;border-radius: 4px !important;" id="btReload" class="btn-u">Refresh</button>');
	}
	$('input:submit').click(function(){
		  //	$('p').text("Form submiting.....");
			$('input:submit').attr("disabled", true);	
		});
	// RELOAD PAGE ON BUTTON CLICK EVENT.
    $('#btReload').click(function () {
   	 		 location.reload(true); 
   	}); 
    // SET AUTOMATIC PAGE RELOAD TIME TO 5000 MILISECONDS (5 SECONDS).
    setInterval('refreshPage()', 10000);
	
});
function submitForm(){
	var formId = $('#formId').val();
	$('#'+formId).submit();
	
}

function setFormId(formId){
	$('#msg').text("Are you sure,You want to reset this Day Statistics");
	$('#formId').val(formId);
}

function setMessage(key){
	if(key == 'game-end'){
		$('#msg').text("Are you sure,You want to end this game");
		$('#formId').val('game-end-form');
	}else if(key == 'session2-start'){
		$('#msg').text("Are you sure, You want to start session2");
		$('#formId').val('session2-start-form');
	}
}
function refreshPage() { 
	 var gameStatus = $('#game-status').val();
	 if(gameStatus == 'ACTIVE'){
		 location.reload(); 
	 }
}
