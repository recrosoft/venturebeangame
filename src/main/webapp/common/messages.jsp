<%@ include file="/common/taglibs.jsp"%>

<!-- SUCCESS MESSAGE START -->
<c:if test="${not empty successMessages}">
	
	<!-- <div class="alert alert-danger fade in alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Oh snap!</strong> Change a few things up and try submitting again.
     </div>   -->
	
	
	
	<div class="alert alert-success" role="alert" style="line-height: 24px;text-align:center;">
		
		<button type="button" class="close" data-dismiss="alert"
			style="line-height: 24px; padding-right:20px">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		</button>
		<strong
			style="padding-left: 10px; padding-top: 20px; font-size: 16px;"><img src="assets/img/success-icon.png" width=22></strong>
		<c:forEach var="success" items="${successMessages}">
			<c:out value="${success}" />
			<br />
		</c:forEach>
	</div>
</c:if>
<!-- SUCCESS MESSAGE END -->
<!-- ERROR MESSAGE START -->
<c:if test="${not empty errorMessages}">

<!-- <div class="alert alert-danger fade in alert-dismissable" style="text-align:center">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Oh snap!</strong> Change a few things up and try submitting again.
                            </div>   -->
	<div class="alert alert-danger" role="alert" style="line-height: 24px;text-align:center;">
		<button type="button" class="close" data-dismiss="alert" style="padding-right:20px">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		</button>
		<strong style="padding-left: 10px; padding-top: 5px; font-size: 16px;"><img src="assets/img/error.icon.png" width=22></strong>
		<c:forEach var="error" items="${errorMessages}">
			<c:out value="${error}" />
			<br />
		</c:forEach>
	</div>
</c:if>
<!-- ERROR MESSAGE END -->

<%
	session.setAttribute("successMessages", null);
	session.setAttribute("errorMessages", null);
%>