<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" type="image/png" href="assets/img/vb-favicon.png">

<!-- JS Global Compulsory --> 
 <c:set var="url">${pageContext.request.requestURL}</c:set>
  <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
<!-- loader css starts-->
<link rel="stylesheet" type="text/css" id="theme" href="assets/css/Venturebean/loader.css" />

  
<!-- CSS Global Compulsory -->
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/plugins/bootstrap/css/datepicker.css">

<link rel="stylesheet" href="assets/plugins/bootstrap/css/datepicker.min.css">

<link rel="stylesheet" href="assets/css/style.css">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="assets/plugins/line-icons/line-icons.css">
<link rel="stylesheet" href="assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/plugins/sky-forms/version-2.0.1/css/custom-sky-forms.css">
<link rel="stylesheet" href="assets/css/materialize.css">
<link rel="stylesheet" href="assets/css/plugins/validation/jquery.validationEngine.css">
<!-- CSS Theme -->
<link rel="stylesheet" href="assets/css/themes/default.css" id="style_color">
<link rel="stylesheet" href="assets/compiled/flipclock.css">

<!-- CSS Customization -->
<link rel="stylesheet" href="assets/css/custom.css">

<!--[if lt IE 9]>
        <link rel="stylesheet" href="assets/plugins/sky-forms/version-2.0.1/css/sky-forms-ie8.css">
    <![endif]-->
<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->




<script type="text/javascript" src="assets/plugins/jquery-1.10.2.min.js"></script> 
<script type="text/javascript" src="assets/plugins/jquery-migrate-1.2.1.min.js"></script> 
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script> 

<script type='text/javascript' src='assets/plugins/bootstrap/js/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="assets/js/plugins/validationengine/languages/jquery.validationEngine-en.js"></script>
<script type="text/javascript" src="assets/js/plugins/validationengine/jquery.validationEngine.js"></script>

<script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap-select.js"></script> 
<!-- JS Implementing Plugins --> 
<script type="text/javascript" src="assets/plugins/back-to-top.js"></script> 
<script type="text/javascript" src="assets/js/app.js"></script> 
<script type="text/javascript" src="assets/compiled/flipclock.js"></script>

<body onunload="">

	<div id="wrapper" class="page-container">
		<tiles:insertAttribute name="menu" />
		<div id="content" class="page-content">
			<tiles:insertAttribute name="header" />
			<div id="rightcontent" style="min-height: 100%">
				<jsp:include page="/common/messages.jsp" flush="true" />
				<tiles:insertAttribute name="body" />
			</div>
			<!-- right content ends-->
			<div class="clearboth"></div>
			<!--Footer Content-->
			<tiles:insertAttribute name="footer" />
			<!--Footer Content Ends-->
			<!--Body Content Ends-->
		</div>


	</div>

<script type="text/javascript">
	jQuery(document).ready(function() {
	    App.init();
	});
</script>
	<%
		response.setHeader("Cache-Control",
				"no-cache,no-store,must-revalidate");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
	%>
</body>


<!--[if lt IE 9]>
    <script src="assets/plugins/respond.js"></script>
    <script src="assets/plugins/html5shiv.js"></script>
    <script src="assets/plugins/sky-forms/version-2.0.1/js/sky-forms-ie8.js"></script>
<![endif]-->
