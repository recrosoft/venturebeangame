<%@ include file="/common/taglibs.jsp"%>
<html>
<head>
<title>CompanyProfile</title>
<link rel="stylesheet" type="text/css" id="theme"
	href="assets/css/plugins/datatable/jquery.dataTables.css" />
<style>
.cancel_css a{
color:#fff;
}
.cancel_css a:hover{
color:#fff;
text-decoration:none;
}

.table>thead>tr>th,.table>tbody>tr>th,.table>tfoot>tr>th,.table>thead>tr>td,.table>tbody>tr>td,.table>tfoot>tr>td
	{
	vertical-align: middle
	}
</style>
</head>
<!--=== Content Part ===-->
<div class="container">
	<div class="row">
		<!-- Begin Content -->
		<div class="col-md-12">
			<!--Basic Table-->
			<div class="panel margin-bottom-40">

				<div class="panel-body">
					<div class="row clients-page" style="margin-top: 20px;">
						<div class="col-md-2">
						<c:choose>
							<c:when test="${company.companyLogoUrl == null}">
									<img src="assets/img/ventureBean/company_logo_default.jpg" width=100% />
							</c:when>
							<c:otherwise>
								<img src="${company.companyLogoUrl}" width=100% />
							</c:otherwise>
						</c:choose>
						</div>
						<div class="col-md-9">
							<h3>${company.companyName}</h3>
							<p>
								<i class="fa fa-map-marker color-light-green" style='font-size:16px'></i>${company.companyCity} &nbsp;,&nbsp;${company.companyAddr}
							</p>
							<%-- <p>&nbsp;&nbsp; ${company.createdOn}</p> --%>
							<a class="btn-u btn-u-sm rounded-3x btn-u-orange"
								href="bp/showCreateProject/${company.companyId}?companyName=${company.companyName}">Create Project</a>
						</div>
					</div>
					<!-- End Clients Block-->
				</div>
				<table class="table">
					<thead>
						<tr>
							<th>Game</th>
							<th class="hidden-sm">Status</th>
							<th>Created on</th>
							<th>Player</th>
							<th>Game Date</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					<c:if test="${empty projectList }">
						<tr>
							<td colspan="6"><div align="center"><span style='color: #190C1D;'><img src='assets/img/empty.png' width=40>&nbsp;<strong>Kindly create a project</strong></span></div></td>
						</tr>	
					</c:if>
					<c:forEach items="${projectList}" var="project">
						<tr>
							<td>${project.gameName}</td>
							<td class="hidden-sm">${project.projectStatus}</td>
							<td>${project.createdOn}</td>
							<td>${project.noOfPlayers}</td>
							<td>${project.startDate}</td>
							<c:choose>
								<c:when test="${project.projectStatus == 'CREATED' }">
								<td style="text-align:right;"><button class="btn btn-danger btn-sm cancel_css">
									<span onclick='cancelProject(${project.projectId})'><i class="fa fa-trash-o"></i>&nbsp;Delete</span>
								</button></td>
							<td><button   class="btn btn-danger btn-sm cancel_css">
									<a id="edit-project" href="bp/project/edit?projectId=${project.projectId}"><i class="fa fa-pencil"></i>&nbsp;Edit</a>
								</button></td>	
							</c:when>
							<c:otherwise>
								<td style="text-align:right;"><button class="btn btn-danger btn-sm cancel_css" disabled>
									<i class="fa fa-trash-o"></i><span >&nbsp;Delete</span>
								</button></td>
							<td><button class="btn btn-danger btn-sm cancel_css" disabled>
									<i class="fa fa-pencil"></i><a>&nbsp;Edit</a>
								</button></td>
							</c:otherwise>
							</c:choose>
						</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
			<!--End Basic Table-->
		</div>
		<!-- End Content -->
	</div>
</div>
 <div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
<!-- confirmation popup starts -->
		<div class="modal fade" id="confirmationpopup" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<form action="bp/project/cancel">
				<input type="hidden" name="projectId" id="projectId" value="">
				<input type="hidden" name="companyId" id="companyId" value="${company.companyId}">
				<div class="modal-content" style="background-color: azure">
					<div class="modal-header"
						style="padding-bottom: 16px; border: none">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<!-- Begin Content -->
						<div class="col-md-12">
							<!-- General Forms -->
							<div>
								<div class="row">
									<div class="col col-12 msgDiv">
										<h4 class="color-green" id=msg>Are you sure,You want to cancel 
											this Game?</h4>
									</div>
								</div>
							</div>
							<!-- General Forms -->
						</div>
						<!-- End Content -->
					</div>
					<div class="modal-footer" style="border-top: none;">
						<button type="submit" class="btn-u btn-u-primary">OK</button>
						<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
					</div>
				</div>
				</form>
			</div>
		</div>
		<!-- confirmation popup ends -->
<script type="text/javascript">
$(document).ready(function(){
	$('#loader-wrapper').hide(); 
	  $('#headingId').text("Company Profile");
	  $("#edit-project").click(function(){
		   $('#loader-wrapper').show(); 
		});
})

function cancelProject(projectId){
	//alert("projectId "+projectId);
	$('#projectId').val(projectId);
	$('#confirmationpopup').modal('show');
};
</script>
</html>