<%@ include file="/common/taglibs.jsp"%>
<html lang="en">
<!--<![endif]-->
<head>
<link rel="stylesheet" type="text/css" id="theme"
	href="assets/css/plugins/datatable/jquery.dataTables.css" />
<title>BP Company</title>
<style>
.dataTables_wrapper table thead{
    display:none;
}
</style>
<!-- Meta -->
<meta name="description" content="">
<meta name="author" content="">
</head>

<body>

<div class="wrapper"> 
  <c:if test='${adminDetailsSessionForm.userRole == "BUSINESS_PARTNER"}'>
  	 <!-- Floating Action Button -->
 	 <div class="fixed-action-btn" style="bottom: 45px; right: 24px;"> <a class="btn-floating btn-large" data-toggle="modal" data-target="#add-company"> <i class="fa fa-plus"></i> </a> </div>
 	 <!-- Floating Action Button --> 
  </c:if>
  <!--=== Content Part ===-->
  <div class="container content">
    <div class="row">
      <div class="col-md-12" style="background-color:#ebefed;padding-bottom:10px"> 
      <div class="table-responsive1">
		<table id="companyList" class="table ">
		</table>
	</div>
      </div>
      <!--/col-md-9--> 
    </div>
    <!--/row--> 
  </div>
  <!--/container--> 
  <!--=== End Content Part ===-->
 
 <!-- Add License popup Start --> 
  <div class="modal fade" id="add-company" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
      <div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Add Company</h4>
        </div>
        <form  class="sky-form" enctype="multipart/form-data" action="bp/addCompany" method="post" id="add-company-form">
        <div class="modal-body"> 
          <!-- Begin Content -->
          <div class="col-md-12"> 
            <!-- General Unify Forms -->
              <fieldset>
                <section>
                  <div class="row">
                    <label class="label col col-4">Company Name</label>
                    <div class="col col-8">
                      <label class="input"> <i class="icon-prepend fa fa-university"></i>
                        <input type="text" name="companyName" id="companyName" class="validate[companyName]"/>
                      </label>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row">
                    <label class="label col col-4">Email Suffix</label>
                    <div class="col col-8">
                      <label class="input"><i class="icon-prepend fa fa-envelope"></i>
                        <input type="text" name="companyEmail" id="companyEmail" class="validate[emailSuffix]"/>
                      </label>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row">
                    <label class="label col col-4">City</label>
                    <div class="col col-8">
                      <label class="input"> <i class="icon-prepend fa fa-building"></i>
                        <input type="text" name="companyCity" id="companyCity" class="validate[companyCity]"/>
                      </label>
                    </div>
                  </div>
                </section>
                <section>
                  <div class="row">
                    <label class="label col col-4">Address</label>
                    <div class="col col-8">
                      <label class="input"><i class="icon-prepend fa fa-map-marker"></i>
                        <input type="text" name="companyAddr" id="companyAddr" class="validate[companyAddr]"/>
                      </label>
                    </div>
                  </div>
                </section>
                
                <section>
                  <div class="row" >
                    <label class="label col col-4">Company Logo</label>
                   	 <div class="col-lg-3">
                          <label for="file" class="input input-file">
                         <div class="button">
                            <input type="file" name="companyLogo" id="companyLogoo" value="" onchange='showSelectedImg(event,"imgLocation")'>
                             Browse</div>
                        </label>        
                     </div>
                    <div class="col-lg-3" id="imgDiv"> <img id="imgLocation" src="" width="100%"></div>
                  </div>
                </section>
               
              </fieldset>
            <!-- General Unify Forms -->
            <div class="margin-bottom-30"></div>
          </div>
          <!-- End Content --> 
        </div>
        <div class="modal-footer" style="border-top:none;">
          <button type="button"  onclick="showLoader()" class="btn-u btn-u-primary"  data-toggle="modal" data-target="#confirmationpopup">Add Company</button>
          <button type="button" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
        </div>
         </form>
      </div>
    </div>
  </div>
  <!-- Add License popup Ends -->
 </div>
	
<!--/End Wrapepr--> 
<input type="hidden" name="bpUserName" id="bpUserName" value="${adminDetailsSessionForm.userName}">
<input type="hidden" name="adminRole" id="adminRole" value="${adminDetailsSessionForm.userRole}">

<input type="hidden" name="bpId" id="bpId" value="${superUserId}">

<script type="text/javascript" src="assets/js/app.js"></script> 
<script type="text/javascript" src="assets/js/plugins/datatables/jquery.dataTables.js"></script>
<script type="text/javascript" src="assets/js/ventureBean/admin/bp/companyList.js"></script>
</body>
</html>