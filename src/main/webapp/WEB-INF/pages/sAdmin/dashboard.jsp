<%@ include file="/common/taglibs.jsp"%>
<html lang="en">
<head>
<title>Super Admin</title>
<style>
.dataTables_wrapper table thead {
	display: none;
}

#uperAd-ul ul {
	list-style-type: none;
}

#superAd-ul ul li {
	float: left;
	padding-right: 70px;
	text-align: left;
}
</style>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<!-- CSS Theme -->
<link rel="stylesheet" href="assets/css/themes/default.css"
	id="style_color">

<!-- CSS Customization -->
<link rel="stylesheet" href="assets/css/custom.css">
<!-- Data table CSS -->
<link rel="stylesheet" type="text/css" id="theme"
	href="assets/css/plugins/datatable/jquery.dataTables.css" />
</head>

<body>
	<div class="wrapper">

		<!--=== Content Part ===-->
		<div class="container content">
			<div class="row">
				<div class="col-md-12"
					style="background-color: #ebefed; padding-top: 14px;">
					<div class="table-responsive1">
						<table id="approvalList" class="table "></table>
					</div>
				</div>
				<!--/col-md-9-->
			</div>
			<!--/row-->
		</div>
		<!--/container-->
		<!--=== End Content Part ===-->

	</div>
	<!--/End Wrapepr-->
	<!-- confirmation popup starts -->
	<div class="modal fade" id="confirmationpopup" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="padding-bottom: 16px; border: none">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<!-- Begin Content -->
					<div class="col-md-12">
						<!-- General Forms -->
						<div>
							<input type="hidden" name="trnx-type" id="trnx-type" value="">
							<input type="hidden" name="transactionId" id="transactionId">
							<section>
								<div class="row">
									<div class="col col-12 msgDiv">
										<h4 class="color-green" id="msg"></h4>
									</div>
								</div>
							</section>
						</div>
					</div>
					<!-- End Content -->
				</div>
				<div class="modal-footer" style="border-top: none;">
					<button type="submit" class="btn-u btn-u-primary" onclick=approveRejectTrnx()>OK</button>
					<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

	<!-- confirmation popup ends -->

	<!-- sucessErrorMsg popup starts -->
	<div class="modal fade" id="suc-err-popup" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="padding-bottom: 16px; border: none">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<!-- Begin Content -->
					<div class="col-md-12">
						<!-- General Forms -->
						<div>
							<section>
								<div class="row">
									<div class="col col-12 msgDiv">
										<h4 class="color-green" id="suc-err-msg"></h4>
									</div>
								</div>
							</section>
						</div>
					</div>
					<!-- End Content -->
				</div>
				<div class="modal-footer" style="border-top: none;">
					<button type="button" class="btn-u " data-dismiss="modal">OK</button>
				</div>
			</div>
		</div>
	</div>

	<!-- sucessErrorMsg popup ends -->
<div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
	
<script type="text/javascript">
$(document).ready(function() { 
	$('#loader-wrapper').hide();
	populateuserList(); 
});
    function approveTrnx(trnxId){
    	$('#transactionId').val(trnxId);
    	$('#trnx-type').val("approve");
    	$('#msg').text("Are you sure,You want to approve this request?");
    }
    function rejectTrnx(trnxId){
    	$('#transactionId').val(trnxId);
    	$('#trnx-type').val("reject");
    	$('#msg').text("Are you sure,You want to reject this request?");
    }
    function approveRejectTrnx(){
    	$('#confirmationpopup').modal('hide');
    	$('#loader-wrapper').show();
    	var url = '';
    	var trnxId = $('#transactionId').val();
    	var type = $('#trnx-type').val();
    	if(type != undefined && type == "approve"){
    		url ="trnx/approve";
    	}else{
    		url ="trnx/reject";
    	}
    	
    	$.ajax({
			url: url+"?transactionId="+trnxId,
			type : "POST",
			success: function(result){
				$('#suc-err-msg').text(result.successOrErrorMsg);
				$('#suc-err-popup').modal('show');
				populateuserList();
				$('#loader-wrapper').hide();
				
	   		 },
			error: function(error){
				$('#suc-err-msg').text("Some error occurred");
				$('#suc-err-popup').modal('show');
				$('#loader-wrapper').hide();
			}	
		});
    }
    function populateuserList() {

		if ($.fn.dataTable.isDataTable('#approvalList')) {
			var oTable = $('#approvalList').dataTable();
			oTable.fnDestroy();
		}
		var rowCount = 1;
		var approvalList = $('#approvalList').dataTable(
						{
						 	"language": {
						        "emptyTable":"<span style='color: #190C1D;'><img src='assets/img/empty.png' width=40>&nbsp;<strong>No requests for Approval</strong></span>"
						    },
							"sPaginationType" : "full_numbers",
							"lengthChange" : false,
							"iDisplayLength" : 6,
							"bFilter" : false,
							//"sDom": 't',
							"bPaginate": true, //hide pagination
							"ajax" : {
								"url" : "getPendingTrnxList",
								"type" : 'POST',
							},  
							"fnRowCallback": function(nRow, aaData, iDisplayIndex) {
								$(nRow).css('background-color', 'transparent');	
							},
							"fnDrawCallback": function(oSettings) {
						        //if ($('#userList tr').length < 3) 
								var rows = this.fnGetData();
								  if ( rows.length === 0 ) {
									  $('.dataTables_paginate').hide();
									  $('.dataTables_info').hide();
								  }else{
									  $('.dataTables_paginate').show();
									  $('.dataTables_info').show(); 
								  }
						    },
							"columns" : [
							             { "title": "Name",
										  "render" : function(data, type, full) {
											 // alert(full);
													if (full == null || full == "") {
														return '<span>-<span>';
													} else {
														var rowdata = "<span class='col-md-1'> <div class='circle'>"+rowCount++ +" </div> </span>"; 
														rowdata = rowdata + "<span class='col-md-8' id='superAd-ul'>";
											            rowdata=rowdata+"<h3>"+full.bpName+"</h3>";
											            rowdata=rowdata+ "<ul style='margin-left: -54px;'><li class='col-md-4'><i class='fa fa-gamepad'></i>&nbsp; "+full.gameName+ "</li><li> Type :"+full.licenseType+" </li><li><i class='fa fa-file-text'></i>&nbsp;&nbsp;License(s) :"+full.licenseCount+" </li></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "+full.categoryName+"</ul></span>";
											            rowdata=rowdata+ "<span class='col-md-1 btn-success wht ' data-toggle='modal' data-target='#confirmationpopup' style='margin-right:4px;margin-top: 24px; padding: 6px 5px 6px 5px;' onclick='approveTrnx("+full.trnxId+")'><i class='fa fa-check-square'></i>&nbsp;&nbsp;Approve</span>";
											            rowdata=rowdata+ "<span class='col-md-1 btn-reject wht' data-toggle='modal' data-target='#confirmationpopup' style='margin-top: 24px; padding: 6px 6px 6px 7px;' onclick='rejectTrnx("+full.trnxId+")'><i class='fa fa-times-circle'></i>&nbsp;&nbsp;Reject </span>";
											           return '<span>' + rowdata + '</span>';
													}
												}
											}
							             ]						});
	};
 
</script>
	<script type="text/javascript"
		src="assets/js/plugins/datatables/jquery.dataTables.js"></script>

</body>
</html>