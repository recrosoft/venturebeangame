<%@ include file="/common/taglibs.jsp"%>
<style>
.circle { 
	width:111px; height:83px; padding:24px 24px 0px 24px; color:#FFFFFF; font-weight:bold;
	background-image:url(assets/img/sr_no_background.png); background-repeat:no-repeat;
}
.sky-form .input input, .sky-form .select select, .sky-form .textarea textarea{
    width: 100%;
}
.col-md-4{
	width: 20%;
}


table {
    border-collapse: collapse;
    width: 100%;
}

th, td {
    text-align: left;
    padding: 20px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}

</style>
  <div class="container content">
 
    <div class="row">
      <div class="col-md-12" style="background-color:#ebefed;"> 
      
       <!-- <p style="border:3px; border-style:outset; border-color:#FF0000; padding: 1em;">
  
  Name</p> -->
  
  
  <table>
  <tr align="right">
    <th >SNo</th>
    <th >Name</th>
    <th>Email</th>
    <th>Group</th>
    <th>TeamName</th>
  </tr>
  </table>
        <!-- Clients Block-->
        <%-- <ul>
	        <c:forEach var="player1" items="${userForms}" varStatus="status">
	        	<li>${status.count} ${player1.name}</li>
	        </c:forEach>
        </ul> --%>
        <s:form action="user/addUsers" modelAttribute="userListForm" id="create-user-form" class="sky-form">
        	<input type="hidden" name="emailSuffix" value="${emailSuffix}">
          <!-- this loop will display the players those are already saved in DB -->
         <c:forEach var="player" items="${userForms}" varStatus="status">
        	 <!-- start first row-->
          <div class="row clients-page" style="border-bottom:solid 2px #d1d2d1; margin-top:20px;">
            <div class="col-md-1">
              <div class="circle" style="padding: 21px 24px 0px 27px;">${status.count}</div>
            </div>
            <s:input type="hidden" path="userForm[${status.index}].projectUserId"/>
            <div class="col-md-4" style="margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-user"></i>
                  <s:input type="text" class="validate[adminName]" path="userForm[${status.index}].name" placeholder="Player Name" />
                </label>
              </section>
            </div>
            <div class="col-md-4" style=" margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-envelope"></i>
                  <s:input type="text" class="validate[userName]" path="userForm[${status.index}].emailAddress" placeholder="Email address"/>
                </label>
              </section>
            </div>
            <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
               <span>${emailSuffix}</span>
              </section>
            </div>
            <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
              <s:select path="userForm[${status.index}].teamId" >
              	 <s:option value="">Select Team</s:option>
              	 <s:options items="${teams}" itemLabel="teamName" itemValue="teamId"/>
				</s:select>
              </section>
            </div>
             <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
              <%-- <select name="userForm[${status.count}].roleId" >
             	 <option value="">Select Role</option>
              	<c:forEach items="${gameRoles}" var="gameRole">
              	<option value="${gameRole.roleId}">${gameRole.roleName}</option>
              	</c:forEach>
				</select> --%>
				<s:select path="userForm[${status.index}].roleId" >
              	 <s:option value="">Select Role</s:option>
              	 <s:options items="${gameRoles}" itemLabel="roleName" itemValue="roleId"/>
				</s:select>
              </section>
            </div>  
           </div>   
           
           <!-- End first row-->
        </c:forEach>
        
        <!-- this loop will display the rest no of empty rows that the Facilitator has to enter -->
       <c:forEach begin="${fn:length(userForms)+1}" end="${totalNoOfPlayer}" var="index" step="1" >
        	 <!-- start first row-->
          <div class="row clients-page" style="border-bottom:solid 2px #d1d2d1; margin-top:20px;"">
            <div class="col-md-1">
              <div class="circle">${index}</div>
            </div>
            <div class="col-md-4" style="margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-user"></i>
                  <input type="text" class="validate[adminName]" name="userForm[${index}].name" placeholder="Player Name">
                </label>
              </section>
            </div>
            <div class="col-md-4" style=" margin-top: 15px;">
              <section>
                <label class="input"> <i class="icon-prepend fa fa-envelope"></i>
                  <input type="text" class="validate[userName]" name="userForm[${index}].emailAddress" placeholder="Email address">
                </label>
              </section>
            </div>
            <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
               <span>${emailSuffix}</span>
              </section>
            </div>
            <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
                <select name="userForm[${index}].teamId" >
                 <option value="">Select Team</option>
              	<c:forEach items="${teams}" var="team">
              		 <option value="${team.teamId}">${team.teamName}</option>
              	</c:forEach>
				</select>
              </section>
            </div>
             <div class="col-md-2" style=" margin-top: 18px;padding-left: 0px;font-size: 15px;">
              <section>
                <select name="userForm[${index}].roleId" >
                 <option value="">Select Role</option>
              	<c:forEach items="${gameRoles}" var="gameRole">
              		 <option value="${gameRole.roleId}">${gameRole.roleName}</option>
              	</c:forEach>
				</select>			
              </section>
            </div>  
           
          </div>
           <!-- End first row-->
        </c:forEach> 
        	<div class="row">
				<div class="col-md-12">
					<!-- General Unify Forms -->
						<c:if test="${projectStatus == 'CREATED'}">
						<footer>
							<div class="row" style="text-align: center">
								<button type="button" onclick="showLoader()" class="btn-u">Submit</button>
								<button type="reset" class="btn-u btn-u-default">Reset</button>
							</div>
						</footer>
					</c:if>
					<!-- General Unify Forms -->
				</div>
			</div>
       </s:form>
      </div>
      <!--/col-md-9--> 
    </div>
    <!--/row--> 
  </div>
  <!--/container--> 
  <!--=== End Content Part ===--> 
  </div>
	 <div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
  <script type="text/javascript">
  function showLoader(){
		if($("#create-user-form").validationEngine('validate')){
			$('#loader-wrapper').show();
			$("#create-user-form").submit();
		}
		
	}
  $(document).ready(function() {
	  $('#loader-wrapper').hide();
	  $('#headingMsg').text("Create User");
	  var prefix = "selectBox_";
	  $('#create-user-form').validationEngine({promptPosition : "bottomLeft", prettySelect : true,
	         usePrefix: prefix },'validate');
	     
  });
  </script> 
