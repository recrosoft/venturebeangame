 <%@ include file="/common/taglibs.jsp"%>
 <title>Result</title>
 <%@ page import="java.util.List , java.util.ArrayList , com.ventureBean.dto.CGameResultDTO ,com.ventureBean.model.CrisisGameIdealResult " %> 
  <style>
.team-scorecard-new{background-color:#f1f9f2; margin-top:2%; margin-bottom:2%; padding-bottom:0px; border:2px solid #c8e6c9;}
.first-heading-title-bg{ background-color:#cacbcc}
.second-heading-title-bg{background-color:#dfe0e1}
	.row-bg-padding{background-color:#81c784; padding-bottom: 10px;}
				.row-col-padding{padding-top: 8px;}
				.row-h3-color{color:#000000}
				.row-col-fontsize{font-size:30px;}
				.subrow-fontsize-bg{font-size:30px; background-color:#a5d6a7}
				.divider{ margin:1%}
				.alpha-row-bg{background-color:#f4efef}
					.bravo-row-bg{ background-color:#ffeb3b}
					.charli-row-bg{ background-color:#f44336}
					.delta-row-bg{ background-color:#2196f3}
					.echo-row-bg{ background-color:#4caf50}
					.vertical-total-bg{background-color:#505150; color:#fff; font-weight:bold;}
					.th-center{text-align:center}
					.horizontal-total-bg{ background-color:#9a9a9b; font-weight:bold}
    .tab-v1 table tr td{
    word-break: break-all;
    }
    </style>
    
  <!--=== Content Part ===-->
  <div class="container content team-scorecard-new" style="padding-top:0px; width:98%;border:2px solid #EAEAEA">
    <div class="row"> 
      <!-- Begin Content -->
      <div class="col-md-12"> 
        <!-- Tab v1 -->
        <div class="tab-v1">
          <div class="tab-content" style="padding:2% 0% 0px 0%">
            <div class="tab-pane fade in active" id="alpha"> 
              
              <!--=== MAIN ROW START ===-->
              <div class="row"> 
                <!--=== LEFT SIDE DIV START ===-->
                <div class="col-md-6">
                  <div class="headline">
                    <h4>Day 1, 2 & 3</h4>
                  </div>
                  <!--=== LEFT SIDE FIRST IDEAL SECTION START ===-->
                  <div class="row">
                    <div class="col-md-12"> 
                      <!--Table Bordered-->
                      <div class="panel panel-grey margin-bottom-40">
                        <div class="panel-heading">
                          <h3 class="panel-title"><i class="fa fa-globe"></i> Actual</h3>
                        </div>
                        <div class="panel-body ">
                          <table class="table table-bordered" style="text-align:center">
                            <thead>
                              <tr>
                                <th class="th-center first-heading-title-bg">Team</th>
                                <th class="th-center first-heading-title-bg">Day 01</th>
                                <th class="th-center first-heading-title-bg">Day 02</th>
                                <th class="th-center first-heading-title-bg">Day 03</th>
                                <th class="th-center horizontal-total-bg">Total</th>
                              </tr>
                            </thead>
                            <tbody>
                           <c:forEach var="result" items="${teamStatMap}" varStatus="count">
							    <c:if test="${result.key != 'Total456'}">
							    	<c:if test="${result.key != 'Total123'}">
							    		 <tr style="background-color:${teams[count.index].teamColor};">
							    		 	<td>${result.key} </td>
							    	</c:if>
							    	 <c:if test="${result.key == 'Total123'}">
							    		 <tr style="background-color:#9a9a9b;">
							    		 	<td>Total</td>
							    	</c:if> 
		                                <td>${result.value[0].livesSaved}</td>
		                                <td>${result.value[1].livesSaved}</td>
		                                <td>${result.value[2].livesSaved}</td>
		                                <td class="horizontal-total-bg">${result.value[0].livesSaved + result.value[1].livesSaved +result.value[2].livesSaved}</td>
		                              </tr>
							    </c:if>
							</c:forEach>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <!--=== LEFT SIDE FIRST IDEAL SECTION END ===--> 
                  
                  <!--=== LEFT SIDE SECOND IDEAL SECTION START ===-->
                  
                  <div class="row">
                    <div class="col-md-12">
                      <div class="headline">
                        <h4>Day 4, 5 & 6</h4>
                      </div>
                      <!--Table Bordered-->
                      <div class="panel panel-grey margin-bottom-40">
                        <div class="panel-heading">
                          <h3 class="panel-title"><i class="fa fa-globe"></i> Actual</h3>
                        </div>
                        <div class="panel-body">
                          <table class="table table-bordered" style="text-align:center">
                            <thead>
                              <tr>
                                <th class="th-center first-heading-title-bg"></th>
                                <th class="th-center first-heading-title-bg" colspan="2">Relocation Camp</th>
                                <th class="th-center first-heading-title-bg" colspan="2">Relief Camp</th>
                                <th class="th-center first-heading-title-bg" colspan="2">Epedimic Camp</th>
                                <th class="th-center first-heading-title-bg" colspan="2">Total</th>
                              </tr>
                            </thead>
                            <thead>
                              <tr>
                                <th class="th-center second-heading-title-bg">Team</th>
                                <th class="th-center second-heading-title-bg">Lives</th>
                                <th class="th-center second-heading-title-bg">Cost</th>
                                <th class="th-center second-heading-title-bg">Lives</th>
                                <th class="th-center second-heading-title-bg">Cost</th>
                                <th class="th-center second-heading-title-bg">Lives</th>
                                <th class="th-center second-heading-title-bg">Cost</th>
                                <th class="th-center horizontal-total-bg">Lives</th>
                                <th class="th-center horizontal-total-bg">Cost</th>
                              </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="result" items="${teamStatMap}" varStatus="count">
							    <c:if test="${result.key != 'Total123' && result.key != 'Total456'}">
							    	   <tr style="background-color:${teams[count.index].teamColor};">
		                                <td>${result.key}</td>
		                                <td>${result.value[3].livesSaved}</td>
		                                <c:choose>
		                                	<c:when test="${result.value[3].cost == -1}"><td>Mission failed</td></c:when>
		                                	<c:otherwise> <td>${result.value[3].cost}</td></c:otherwise>
		                                </c:choose>
		                                <td>${result.value[4].livesSaved}</td>
		                                <c:choose>
		                                	<c:when test="${result.value[4].cost == -1}"><td>Mission failed</td></c:when>
		                                	<c:otherwise> <td>${result.value[4].cost}</td></c:otherwise>
		                                </c:choose>
		                                <td>${result.value[5].livesSaved}</td>
		                              	<c:choose>
		                                	<c:when test="${result.value[5].cost == -1}"><td>Mission failed</td></c:when>
		                                	<c:otherwise> <td>${result.value[5].cost}</td></c:otherwise>
		                                </c:choose>
		                                <td class="horizontal-total-bg">${result.value[5].livesSaved}</td>
		                                <c:choose>
		                                	<c:when test="${result.value[3].cost + result.value[4].cost +result.value[5].cost < 0}"><td class="horizontal-total-bg">0</td></c:when>
		                                	<c:otherwise><td class="horizontal-total-bg">${result.value[3].cost + result.value[4].cost +result.value[5].cost}</td></c:otherwise>
		                                </c:choose>
		                              </tr>
							    </c:if>
							    <c:if test="${result.key == 'Total456'}">
							    	   <tr style="background-color:#9a9a9b;">
		                                <td>Total</td>
		                                <td>${result.value[0].livesSaved}</td>
		                                <c:choose>
		                                	<c:when test="${result.value[0].cost == -1}"><td>Mission failed</td></c:when>
		                                	<c:otherwise> <td>${result.value[0].cost}</td></c:otherwise>
		                                </c:choose>
		                                <td>${result.value[1].livesSaved}</td>
		                                <c:choose>
		                                	<c:when test="${result.value[1].cost == -1}"><td>Mission failed</td></c:when>
		                                	<c:otherwise> <td>${result.value[1].cost}</td></c:otherwise>
		                                </c:choose>
		                                <td>${result.value[2].livesSaved}</td>
		                              	<c:choose>
		                                	<c:when test="${result.value[2].cost == -1}"><td>Mission failed</td></c:when>
		                                	<c:otherwise> <td>${result.value[2].cost}</td></c:otherwise>
		                                </c:choose>
		                                <td class="horizontal-total-bg">${result.value[2].livesSaved}</td>
		                                <c:choose>
		                                	<c:when test="${result.value[0].cost + result.value[1].cost +result.value[2].cost < 0}"><td class="horizontal-total-bg">0</td></c:when>
		                                	<c:otherwise><td class="horizontal-total-bg">${result.value[0].cost + result.value[1].cost +result.value[2].cost}</td></c:otherwise>
		                                </c:choose>
		                              </tr>
							    </c:if>
							</c:forEach>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!--End Table Bordered--> 
                    </div>
                  </div>
                  <!--=== LEFT SIDE SECOND IDEAL SECTION END ===--> 
                  
                </div>
                <!--=== LEFT SIDE DIV END ===--> 
                
                <!--=== RIGHT SIDE DIV START ===-->
                <div class="col-md-6">
                  <div class="headline">
                    <h4>Day 1, 2 & 3</h4>
                  </div>
                  <!--=== RIGHT SIDE FIRST ACTUAL SECTION START ===-->
                  <div class="row">
                    <div class="col-md-12"> 
                      <!--Table Bordered-->
                      <div class="panel panel-grey margin-bottom-40">
                        <div class="panel-heading">
                          <h3 class="panel-title"><i class="fa fa-globe"></i> Ideal</h3>
                        </div>
                        <div class="panel-body">
                          <table class="table table-bordered" style="text-align:center">
                            <thead>
                              <tr>
                                <th class="th-center first-heading-title-bg">Team</th>
                                <th class="th-center first-heading-title-bg">Day 01</th>
                                <th class="th-center first-heading-title-bg">Day 02</th>
                                <th class="th-center first-heading-title-bg">Day 03</th>
                                <th class="th-center horizontal-total-bg">Total</th>
                              </tr>
                            </thead>
                            <tbody>
                            
                             <c:forEach var="result" items="${idealResultMap}" varStatus="count">
							    <c:if test="${result.key != 'Total456'}">
							    	  <c:if test="${result.key != 'Total123'}">
							    		 <tr style="background-color:${teams[count.index].teamColor};">
							    		 	<td>${result.key}</td>
							    	</c:if>
							    	 <c:if test="${result.key == 'Total123'}">
							    		 <tr style="background-color:#9a9a9b;">
							    		 	<td>Total</td>
							    	</c:if> 
		                                <td>${result.value[0].livesSaved}</td>
		                                <td>${result.value[1].livesSaved}</td>
		                                <td>${result.value[2].livesSaved}</td>
		                                <td class="horizontal-total-bg">${result.value[0].livesSaved + result.value[1].livesSaved +result.value[2].livesSaved}</td>
		                              </tr>
							    </c:if>
							</c:forEach>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!--End Table Bordered--> 
                    </div>
                  </div>
                  <!--=== RIGHT SIDE FIRST ACTUAL SECTION END ===--> 
                  
                  <!--=== RIGHT SIDE SECOND ACTUAL SECTION START ===-->
                  <div class="row">
                    <div class="col-md-12">
                      <div class="headline">
                        <h4>Day 4, 5 & 6</h4>
                      </div>
                      <!--Table Bordered-->
                      <div class="panel panel-grey margin-bottom-40">
                        <div class="panel-heading">
                          <h3 class="panel-title"><i class="fa fa-globe"></i> Ideal</h3>
                        </div>
                        <div class="panel-body">
                          <table class="table table-bordered" style="text-align:center">
                            <thead>
                              <tr>
                                <th class="th-center first-heading-title-bg"></th>
                                <th class="th-center first-heading-title-bg" colspan="2">Relocation Camp</th>
                                <th class="th-center first-heading-title-bg" colspan="2">Relief Camp</th>
                                <th class="th-center first-heading-title-bg" colspan="2">Epedimic Camp</th>
                                <th class="th-center first-heading-title-bg" colspan="2">Total</th>
                              </tr>
                            </thead>
                            <thead>
                              <tr>
                                <th class="th-center second-heading-title-bg">Team</th>
                                <th class="th-center second-heading-title-bg">Lives</th>
                                <th class="th-center second-heading-title-bg">Cost</th>
                                <th class="th-center second-heading-title-bg">Lives</th>
                                <th class="th-center second-heading-title-bg">Cost</th>
                                <th class="th-center second-heading-title-bg">Lives</th>
                                <th class="th-center second-heading-title-bg">Cost</th>
                                <th class="th-center horizontal-total-bg">Lives</th>
                                <th class="th-center horizontal-total-bg">Cost</th>
                              </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="result" items="${idealResultMap}" varStatus="count">
							    <c:if test="${result.key != 'Total123' && result.key != 'Total456'}">
							    	   <tr style="background-color:${teams[count.index].teamColor};">
		                                <td>${result.key}</td>
		                                <td>${result.value[3].livesSaved}</td>
		                                <td>${result.value[3].cost}</td>
		                                <td>${result.value[4].livesSaved}</td>
		                                <td>${result.value[4].cost}</td>
		                                <td>${result.value[5].livesSaved}</td>
		                                <td>${result.value[5].cost}</td>
		                                <td class="horizontal-total-bg">${result.value[5].livesSaved}</td>
		                                <td class="horizontal-total-bg">${result.value[3].cost + result.value[4].cost +result.value[5].cost}</td>
		                              </tr>
							    </c:if>
							    <c:if test="${result.key == 'Total456'}">
                             		<tr style="background-color:#9a9a9b;">
		                                <td>Total</td>
		                                <td>${result.value[0].livesSaved}</td>
		                                <td>${result.value[0].cost}</td>
		                                <td>${result.value[1].livesSaved}</td>
		                                <td>${result.value[1].cost}</td>
		                                <td>${result.value[2].livesSaved}</td>
		                                <td>${result.value[2].cost}</td>
		                                <td class="horizontal-total-bg">${result.value[2].livesSaved}</td>
		                                <td class="horizontal-total-bg">${result.value[0].cost + result.value[1].cost +result.value[2].cost}</td>
		                              </tr>
                             </c:if>
							</c:forEach>
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <!--End Table Bordered--> 
                    </div>
                  </div>
                  <!--=== RIGHT SIDE SECOND ACTUAL SECTION END ===--> 
                  
                </div>
                
                <!--=== RIGHT SIDE DIV END ===--> 
              </div>
              <!--=== MAIN Row End ===-->
              
              <div class="divider"></div>
            </div>
          </div>
        </div>
        <!-- End Tab v1 -->
        
        <div class="margin-bottom-30"></div>
      </div>
      <!-- End Content --> 
    </div>
    <script type="text/javascript">
    $(document).ready(function() {
    	$('#headingMsg').text("Game Result");
    });
    </script>
  </div>
  <!--/container--> 
  <!--=== End Content Part ===--> 
  