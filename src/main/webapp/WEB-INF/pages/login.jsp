<%@ include file="/common/taglibs.jsp"%>
<head>
    <title>VB Admin</title>

       <!-- Favicon 
    <link rel="shortcut icon" href="favicon.ico"> -->
</head> 

<body>
<div class="wrapper">
  <!--=== Header ===-->
  <div class="header"> 
    <!-- Navbar -->
    <div class="navbar navbar-default" role="navigation">
      <div class="container"> 
        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse1 navbar-collapse1 navbar-responsive-collapse1">
          <ul class="nav navbar-nav">
            <!-- Home -->
            <li class="dropdown"> 
              <!-- Topbar -->
              <div class="topbar" style="margin-top:30px;">
                <div class="container" style="height:70px;"> 
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header"> <a class="navbar-brand" href="index.html"> <img id="logo-header" src="assets/img/LOGO.png" alt="Logo"> </a> </div>
                </div>
              </div>
              <!-- End Topbar --> 
            </li>
            <!-- End Home -->
          </ul>
        </div>
        <!--/navbar-collapse--> 
      </div>
    </div>
    <!-- End Navbar --> 
  </div>
  <!--=== End Header ===-->   
    <!--=== Breadcrumbs ===-->
    <div class="breadcrumbs">
        <div class="container"> </div>
    </div><!--/breadcrumbs-->
    <!--=== End Breadcrumbs ===-->
<!-- SUCCESS MESSAGE START -->
<c:if test="${not empty successMessages}">
	
	<!-- <div class="alert alert-danger fade in alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Oh snap!</strong> Change a few things up and try submitting again.
     </div>   -->
	
	
	
	<div class="alert alert-success" role="alert" style="line-height: 24px;text-align:center;">
		
		<button type="button" class="close" data-dismiss="alert"
			style="line-height: 24px; padding-right:20px">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		</button>
		<strong
			style="padding-left: 10px; padding-top: 20px; font-size: 16px;"><img src="assets/img/success-icon.png" width=22></strong>
		<c:forEach var="success" items="${successMessages}">
			<c:out value="${success}" />
			<br />
		</c:forEach>
	</div>
</c:if>
<!-- SUCCESS MESSAGE END -->
<!-- ERROR MESSAGE START -->
<c:if test="${not empty forgotpassError}">

<!-- <div class="alert alert-danger fade in alert-dismissable" style="text-align:center">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                <strong>Oh snap!</strong> Change a few things up and try submitting again.
                            </div>   -->
	<div class="alert alert-danger" role="alert" style="line-height: 24px;text-align:center;">
		<button type="button" class="close" data-dismiss="alert" style="padding-right:20px">
			<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
		</button>
		<strong style="padding-left: 10px; padding-top: 5px; font-size: 16px;"><img src="assets/img/error.icon.png" width=22></strong>
		<c:forEach var="error" items="${forgotpassError}">
			<c:out value="${error}" />
			<br />
		</c:forEach>
	</div>
</c:if>
<!-- ERROR MESSAGE END -->
    <!--=== Content Part ===-->
    <div class="container content">		
    	<div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <form class="reg-page" action="j_spring_security_check" method="post" id="login-form">
                    <div class="reg-header">            
                        <h2>Login to your account</h2>
                    </div>
                    <c:if test="${not empty errorMessages}">
						<div class="login-title"
						style="color: #C00; font-size: 13px; font-weight: bold; margin-bottom: 4px;text-align: center;">
						<c:forEach var="error" items="${errorMessages}">
							<c:out value="${error}" />
							<br />
						</c:forEach>
						</div>
					</c:if>
					<input type="hidden" name="userType" value="${userType}">
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                        <input type="text" placeholder="Username" class="form-control"  name="j_username" id="j_username">
                    </div>                    
                    <div class="input-group margin-bottom-20">
                        <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        <input type="password" placeholder="Password" class="form-control"  name="j_password" id="j_password">
                    </div>                    

                    <div class="row">
                        <div class="col-md-6">
                            <label class="checkbox"><input type="checkbox"> Stay signed in</label>                        
                        </div>
                        <div class="col-md-6">
                            <button onclick="showLoader()" class="btn-u pull-right" type="submit">Login</button>                        
                        </div>
                    </div>
                    <p class="login-footer"><span data-toggle='modal' data-target='#forgotPasswordpopUp'><a><strong>Forgot Password ?</strong></a> </span></p>
                </form>            
            </div>
        </div><!--/row-->
    </div><!--/container-->		
    <!--=== End Content Part ===-->
    <!--=== Footer ===-->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 md-margin-bottom-40">
                    <!-- About -->
                    <div class="headline"><h2>Address</h2></div>  
                    <address class="md-margin-bottom-40"><span style="font-weight:bold; font-size:16px;">VentureBean Consulting</span><br>
						A 704, Mantri Classic, 4th Block,<br> Koramangala, Bangalore 560034,<br> Karnataka, India</address>    
                    <!-- End About -->
                                          
                    <!-- End Monthly Newsletter -->
                </div><!--/col-md-4-->  
                
                <div class="col-md-4 md-margin-bottom-40">
                    <!-- Recent Blogs -->
                    <div class="posts">
                        <div class="headline"><h2>Contact</h2></div>
                        <dl class="dl-horizontal">
                                 <p>beanie@venturebean.com <br>
									Mathew: (+91) 9845-452-199 <br>
									Vinod: (+91) 9845-499-770</p> 
                        </dl>
                    </div>
                    <!-- End Recent Blogs -->                    
                </div><!--/col-md-4-->
                <div class="col-md-4">
                    <!-- Social Links -->
                    <div class="headline"><h2>Stay Connected</h2></div> 
                    <ul class="social-icons">
                        <li><a href="#" data-original-title="Feed" class="social_rss"></a></li>
                        <li><a href="#" data-original-title="Facebook" class="social_facebook"></a></li>
                        <li><a href="#" data-original-title="Twitter" class="social_twitter"></a></li>
                        <li><a href="#" data-original-title="Goole Plus" class="social_googleplus"></a></li>
                        <li><a href="#" data-original-title="Pinterest" class="social_pintrest"></a></li>
                        <li><a href="#" data-original-title="Linkedin" class="social_linkedin"></a></li>
                        <li><a href="#" data-original-title="Vimeo" class="social_vimeo"></a></li>
                    </ul>
                    <!-- End Social Links -->
                </div><!--/col-md-4-->
            </div>
        </div> 
    </div><!--/footer-->
    <!--=== End Footer ===-->

    <!--=== Copyright ===-->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">                     
                    <p>
                        2015 &copy; ALL Rights Reserved. 
                       
                    </p>
                </div>
               
            </div>
        </div> 
    </div><!--/copyright--> 
    <!--=== End Copyright ===-->
</div><!--/wrapper-->

		<!-- forgot password popup starts -->
	<div class="modal fade" id="forgotPasswordpopUp" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">Forgot Password</h4>
				</div>
				<form class="sky-form" action="forgotPassword" method="post" id='forgot-password-form'>
					<div class="modal-body">
						<!-- Begin Content -->
						<div class="col-md-12">
							<!-- General Unify Forms -->
							<fieldset>
								<section>
									<div class="row">
										<label class="label col col-md-2">Username</label>
										<div class="col col-md-10">
											<label class="input"> <i
												class="icon-prepend fa fa-user"></i> <input type="userName"
												name="userName" id="userName" class="validate[userName]" />
											</label>
										</div>
									</div>
								</section>
							</fieldset>
							<!-- General Unify Forms -->
							<div class="margin-bottom-30"></div>
						</div>
						<!-- End Content -->
					</div>
					<div class="modal-footer" style="border-top: none;">
						<button type="submit" class="btn-u btn-u-primary">Submit</button>
						<button type="button" class="btn-u btn-u-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- forgot password popup ends -->
	<div id="loader-wrapper">
	  <div id="loader"></div>
	  <div class="loader-section section-left"></div>
	  <div class="loader-section section-right"></div>
	</div>
</body>
	
<script type="text/javascript">
	$(document).ready(function(){
		$('#loader-wrapper').hide();
		 var prefix = "selectBox_";
	     $("#forgot-password-form").validationEngine({promptPosition : "bottomLeft", prettySelect : true,
	         usePrefix: prefix },'validate');
	     $("#login-form").validationEngine({promptPosition : "bottomLeft", prettySelect : true,
	         usePrefix: prefix },'validate');
	     
	});
	
	function showLoader(){
  	 $('#loader-wrapper').show();
	}
	</script>
</html> 