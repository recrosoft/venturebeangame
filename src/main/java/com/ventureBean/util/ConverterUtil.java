package com.ventureBean.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ventureBean.constants.Constants;
import com.ventureBean.dto.BpGameMappingDTO;
import com.ventureBean.dto.CGameResultDTO;
import com.ventureBean.dto.CategoryDTO;
import com.ventureBean.dto.CompanyDTO;
import com.ventureBean.dto.CrisisGameStatDTO;
import com.ventureBean.dto.GameDTO;
import com.ventureBean.dto.PlayerDTO;
import com.ventureBean.dto.TransactionDetailDTO;
import com.ventureBean.dto.UserDetailDTO;
import com.ventureBean.dto.UserRoleDTO;
import com.ventureBean.dto.crisis.CrisisGameUserDTO;
import com.ventureBean.enums.CategoryType;
import com.ventureBean.enums.CrisisGameDayEnum;
import com.ventureBean.enums.LicenseTypeEnum;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.form.RoleForm;
import com.ventureBean.form.UserForm;
import com.ventureBean.model.BPCategoryMapping;
import com.ventureBean.model.Category;
import com.ventureBean.model.Company;
import com.ventureBean.model.CrisisGameRoleMaster;
import com.ventureBean.model.CrisisGameStatistics;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.model.CrisisGameUser;
import com.ventureBean.model.Game;
import com.ventureBean.model.Player;
import com.ventureBean.model.Project;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.model.User;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.model.UserTransaction;

public class ConverterUtil {

	/**
	 * log variable.
	 */
	public static final Log log = LogFactory.getLog(ConverterUtil.class);

	public static List<UserDetailDTO> convertUserToDTO(List<User> users) {
		log.debug("start of method convertModelToPOJO(List<User> users)");
		List<UserDetailDTO> userDetailDTOs = new ArrayList<>();
		if (Format.isCollectionNotEmtyAndNotNull(users)) {
			for (int i = 0; i < users.size(); i++) {
				Object user = (Object) users.get(i);
				Object[] uData = (Object[]) user;
				UserDetailDTO detail = new UserDetailDTO();
				detail.setUserId((Integer) uData[0]);
				detail.setName((String) uData[1]);
				detail.setEmailAddress((String) uData[2]);
				if (uData.length > Constants.THREE)
					detail.setComesUnder((String) uData[3]);
				userDetailDTOs.add(detail);
			}
		}
		log.debug("end of method convertModelToPOJO(List<User> users)");
		return userDetailDTOs;
	}

	public static String getUrlSpecRole(String role) {
		log.debug("start of method getUrlSpecRole");
		String roleName = "";
		if (Format.isStringNotEmptyAndNotNull(role)) {
			switch (role) {
			case Constants.ADMIN_ROLE:
				roleName = Constants.VB_ADMIN_SHORT;
				break;

			case Constants.BH_ROLE:
				roleName = Constants.BH_ROLE_SHORT;
				break;

			case Constants.BM_ROLE:
				roleName = Constants.BM_ROLE_SHORT;
				break;

			case Constants.BDM_ROLE:
				roleName = Constants.BDM_ROLE_SHORT;
				break;

			case Constants.BP_ROLE:
				roleName = Constants.BP_ROLE_SHORT;
				break;
			}
		}
		log.debug("end of method getUrlSpecRole");
		return roleName;
	}

	public static String getDBSpecRoleName(String roleName) {
		log.debug("start of method getDBSpecRoleName");
		String dbRole = null;
		switch (roleName) {
		case Constants.BH_ROLE_SHORT:
			dbRole = Constants.BH_ROLE;
			break;

		case Constants.BM_ROLE_SHORT:
			dbRole = Constants.BM_ROLE;
			break;

		case Constants.BDM_ROLE_SHORT:
			dbRole = Constants.BDM_ROLE;
			break;

		case Constants.BP_ROLE_SHORT:
			dbRole = Constants.BP_ROLE;
			break;
		case Constants.SADMIN_ROLE_SHORT:
			dbRole = Constants.SADMIN_ROLE;
			break;
		default:
			dbRole = "";
		}
		log.debug("end of method getDBSpecRoleName");
		return dbRole;
	}

	public static Object getText(String selectedRole) {
		log.debug("start of method getHeadingText");
		String role = "";
		if (Format.isStringNotEmptyAndNotNull(selectedRole)) {
			switch (selectedRole) {
			case "BH":
				role = Constants.BH_ROLE_TEXT;
				break;
			case "BUSINESS_HEAD":
				role = Constants.BH_ROLE_TEXT;
				break;

			case "BM":
				role = Constants.BM_ROLE_TEXT;
				break;

			case "BUSINESS_MANAGER":
				role = Constants.BM_ROLE_TEXT;
				break;

			case "BDM":
				role = Constants.BDM_ROLE_TEXT;
				break;

			case "BP":
				role = Constants.BP_ROLE_TEXT;
				break;

			case "BUSINESS_PARTNER":
				role = Constants.BP_ROLE_TEXT;
				break;
			case "SA":
				role = Constants.SA_ROLE_TEXT;
				break;
			case "SUPER_ADMIN":
				role = Constants.SA_ROLE_TEXT;
				break;

			}

		}

		log.debug("End of method getHeadingText");
		return role;
	}

	/**
	 * @author Satyajit This method will convert UserRole model to UserRoleDTO
	 * @param userRoles
	 * @return List<UserRoleDTO>
	 */
	public static List<UserRoleDTO> convertRoleToDTO(
			List<UserRoleMaster> userRoles) {
		log.debug("End of method convertRoleToDTO");
		List<UserRoleDTO> roleDTOs = new ArrayList<>();
		UserRoleDTO dto = null;
		for (UserRoleMaster userRole : userRoles) {
			dto = new UserRoleDTO();
			dto.setUserRoleId(userRole.getUserRoleId().toString());
			dto.setRoleName(userRole.getRoleName());
			roleDTOs.add(dto);
		}
		log.debug("End of method convertRoleToDTO");
		return roleDTOs;
	}

	public static List<GameDTO> convertGameToDTO(List<Game> games) {
		log.debug("start of method convertGameToDTO");
		List<GameDTO> list = new ArrayList<>();
		if (Format.isCollectionNotEmtyAndNotNull(games)) {
			GameDTO dto = null;
			for (Game game : games) {
				dto = new GameDTO();
				dto.setGameId(game.getGameId());
				dto.setGameName(game.getGameName());
				list.add(dto);
			}
		}
		log.debug("end of method convertGameToDTO");
		return list;
	}

	public static List<TransactionDetailDTO> convertUTrnxToDTO(
			List<UserTransaction> transacions) {
		log.debug("start of method convertUTrnxToDTO(List<TransactionDetailDTO> users)");
		List<TransactionDetailDTO> detailDTOs = new ArrayList<>();
		if (Format.isCollectionNotEmtyAndNotNull(transacions)) {
			for (int i = 0; i < transacions.size(); i++) {
				Object user = (Object) transacions.get(i);
				Object[] uData = (Object[]) user;
				TransactionDetailDTO detail = new TransactionDetailDTO();
				detail.setTrnxId((Integer) uData[0]);
				detail.setBpName((String) uData[1]);
				detail.setGameName((String) uData[2]);
				detail.setLicenseType((String) uData[3]);
				if (uData[4].toString().equals(CategoryType.GOLD.toString())
						|| uData[4].toString().equals(
								CategoryType.PLATINUM.toString())) {
					detail.setCategoryName((String) uData[4]);
				} else {
					detail.setCategoryName("");

				}

				if (detail.getLicenseType().equals(
						LicenseTypeEnum.PAID.toString())) {
					detail.setLicenseCount((Integer) uData[5]);
				} else {
					detail.setLicenseCount((Integer) uData[6]);
				}

				detail.setAmount((Long) uData[7]);

				detailDTOs.add(detail);
			}
		}
		log.debug("end of method convertUTrnxToDTO(List<TransactionDetailDTO> users)");
		return detailDTOs;
	}

	public static List<GameDTO> conertoGameToDTO(List<Game> games) {
		log.debug("start of method conertGameToDTO");
		List<GameDTO> gameDTOs = new ArrayList<>();
		GameDTO dto = null;
		if (Format.isCollectionNotEmtyAndNotNull(games)) {
			for (Game game : games) {
				dto = new GameDTO();
				dto.setDescription(game.getDescription());
				dto.setGameId(game.getGameId());
				dto.setGameName(game.getGameName());
				dto.setLogoUrl(game.getLogo());
				List<CategoryDTO> categories = new ArrayList<>();
				if (Format.isCollectionNotEmtyAndNotNull(game.getCategories())) {
					Set<Category> catSet = game.getCategories();
					Iterator<Category> iterator = catSet.iterator();
					Category category = new Category();
					CategoryDTO categoryDTO = null;
					while (iterator.hasNext()) {
						category = iterator.next();
						categoryDTO = new CategoryDTO();
						categoryDTO.setCategoryId(category.getCategoryId());
						categoryDTO.setCategoryName(category.getCategoryName());
						categories.add(categoryDTO);
					}
					dto.setCategories(categories);
				}
				gameDTOs.add(dto);
			}
		}
		log.debug("end of method conertGameToDTO");
		return gameDTOs;
	}

	public static List<CategoryDTO> conertoCategoryToDTO(List<Game> games) {
		log.debug("start of method conertoCategoryToDTO");
		List<CategoryDTO> categories = new ArrayList<>();
		for (Game game : games) {
			if (Format.isCollectionNotEmtyAndNotNull(game.getCategories())) {
				Category category = new Category();
				CategoryDTO categoryDTO = null;
				Set<Category> catSet = game.getCategories();
				Iterator<Category> iterator = catSet.iterator();
				while (iterator.hasNext()) {
					category = iterator.next();
					categoryDTO = new CategoryDTO();
					categoryDTO.setCategoryId(category.getCategoryId());
					categoryDTO.setCategoryName(category.getCategoryName());
					categoryDTO.setDesc(category.getDescription());
					categoryDTO.setFreeLicense(category.getFreeLicense());
					categoryDTO.setPricePerLicense(category
							.getPricePerLicense());
					categoryDTO.setGameId(category.getGame().getGameId());
					categoryDTO.setThreshold(category.getThreshold());
					categories.add(categoryDTO);
				}
			}
		}
		log.debug("end of method conertoCategoryToDTO");
		return categories;
	}

	public static List<CompanyDTO> convertCompanyToDTO(List<Company> companies) {
		log.debug("start of method convertCompanyToDTO");
		List<CompanyDTO> companyDTOs = new ArrayList<>();
		CompanyDTO dto = null;
		if (Format.isCollectionNotEmtyAndNotNull(companies)) {
			for (Company company : companies) {
				dto = new CompanyDTO();
				dto.setCompanyId(company.getCompanyId());
				dto.setCompanyName(company.getCompanyName());
				dto.setCompanyCity(company.getCompanyCity());
				dto.setCompanyLogoUrl(company.getCompanyLogoUrl());
				dto.setCompanyAddr(company.getCompanyAddr());
				companyDTOs.add(dto);
			}
		}
		log.debug("end of method convertCompanyToDTO");
		return companyDTOs;
	}

	public static UserDetailDTO convertUserToDTO(User user) {
		log.debug("start of method convertUserToDTO");
		UserDetailDTO detailDTO = null;
		if (Format.isObjectNotEmptyAndNotNull(user)) {
			detailDTO = new UserDetailDTO();
			detailDTO.setName(user.getName());
			detailDTO.setEmailAddress(user.getEmailAddress());
			detailDTO.setPhone(user.getPhone());
			detailDTO.setUserId(user.getUserId());
			detailDTO.setWallet(Format.isNotNull(user.getWallet()) ? user
					.getWallet() : Constants.ZERO);
		}
		log.debug("end of method convertUserToDTO");
		return detailDTO;
	}

	public static Object convertCompanyToDTO(Company company) {
		log.debug("start of method convertCompanyToDTO");
		CompanyDTO companyDTO = null;
		if (Format.isNotNull(company)) {
			companyDTO = new CompanyDTO();
			companyDTO.setCompanyId(company.getCompanyId());
			companyDTO.setCompanyName(company.getCompanyName());
			companyDTO.setCompanyCity(company.getCompanyCity());
			companyDTO.setCompanyLogoUrl(company.getCompanyLogoUrl());
			companyDTO.setCompanyAddr(company.getCompanyAddr());
		}
		log.debug("end of method convertCompanyToDTO");
		return companyDTO;
	}

	public static List<PlayerDTO> convertProjectUserToPlayer(
			List<ProjectUser> palyers, boolean emailSuffix) {
		log.debug("start of method convertProjectUserToPlayer");
		List<PlayerDTO> playerDTOs = new ArrayList<>();
		if (Format.isCollectionNotEmtyAndNotNull(palyers)) {
			for (int i = 0; i < palyers.size(); i++) {
				Object player = (Object) palyers.get(i);
				Object[] uData = (Object[]) player;
				PlayerDTO dto = new PlayerDTO();
				dto.setPlayerId((Integer) uData[0]);
				dto.setName((String) uData[1]);
				String emailAddr = (String) uData[2];
				if (emailSuffix) {
					dto.setEmailAddress((String) uData[2]);
				} else {
					if (Format.isStringNotEmptyAndNotNull(emailAddr)
							&& emailAddr.contains("@")) {
						dto.setEmailAddress(emailAddr.substring(0,
								emailAddr.indexOf('@')));
					} else {
						dto.setEmailAddress((String) uData[2]);
					}
				}

				playerDTOs.add(dto);
			}
		}
		log.debug("end of method convertProjectUserToPlayer");
		return playerDTOs;
	}

	public static List<CrisisGameUserDTO> convertToCrisisGameUserDTO(
			List<CrisisGameUser> gameUsers) {
		List<CrisisGameUserDTO> dtos = new ArrayList<>();

		log.debug("start of method convertToCrisisGameUserDTO");
		if (Format.isCollectionNotEmtyAndNotNull(gameUsers)) {

			for (CrisisGameUser user : gameUsers) {
				CrisisGameUserDTO dto = new CrisisGameUserDTO();
				dto.setName(user.getUser().getName());
				dto.setEmailAddress(user.getUser().getEmailAddress());
				dto.setRoleName(user.getRole().getRoleName());
				dto.setTeamName(user.getTeam().getTeamName());
				dtos.add(dto);
			}
		}
		log.debug("end of method convertToCrisisGameUserDTO");
		return dtos;
	}

	public static List<UserForm> convertToCrisisGameUsers(List<CrisisGameUser> crisisGameUsers)
	{
		//List<PlayerDTO> dtos = new ArrayList<>();
		List<UserForm>	userForms = new ArrayList<>();
		if(Format.isCollectionNotEmtyAndNotNull(crisisGameUsers)){
			for(CrisisGameUser gameUser : crisisGameUsers){
				UserForm player = new UserForm();
				ProjectUser projectUser  = gameUser.getUser();
				if(Format.isNotNull(projectUser)){
					String email=projectUser.getEmailAddress();
						
					if (Format.isStringNotEmptyAndNotNull(email)
								&& email.contains("@")) {
							player.setEmailAddress(email.substring(0,
									email.indexOf('@')));
						} else {
							player.setEmailAddress((String)projectUser.getEmailAddress());
						}
					
					if(Format.isStringNotEmptyAndNotNull(projectUser.getName())){
						player.setName(projectUser.getName());
					}
					CrisisGameTeam team = gameUser.getTeam();
					if(Format.isNotNull(team)){
						player.setTeamId(team.getTeamId().toString());
					}
				   CrisisGameRoleMaster role=gameUser.getRole();
					if(Format.isNotNull(role)){
						player.setRoleId(role.getRoleId().toString());
					}
				}
				userForms.add(player);
			}
		}
		
		return userForms;
	}

	public static List<CrisisGameStatDTO> converToCrisisGameStatDTO(
			List<CrisisGameStatistics> gameStatistics) {
		log.debug("start of method converToCrisisGameStatDTO");
		List<CrisisGameStatDTO> dtos = new ArrayList<>();
		if (Format.isCollectionNotEmtyAndNotNull(gameStatistics)) {
			for (CrisisGameStatistics statistics : gameStatistics) {
				CrisisGameStatDTO dto = new CrisisGameStatDTO();
				dto.setGameStatId(statistics.getGameStatId());
				if (Format.isNotNull(statistics.getTeam())) {
					dto.setTeamName(statistics.getTeam().getTeamName().trim());
					dto.setTeamColor(statistics.getTeam().getTeamColor());
				}
				dto.setLivesSaved(statistics.getLivesSaved());
				dto.setLivesLost(statistics.getLivesLost());
				dto.setDayNo(statistics.getDayNo().toString());
				dto.setMoneySpent(statistics.getMoneySpent());
				dto.setCampusBuilt(statistics.getCampsBuilt());
				dto.setResourcesUsed(statistics.getResourcesUsed());
				dto.setStatus(statistics.getStatus().toString());
				dto.setSurvivorsCount(statistics.getSurvivorsCount());
				//dto.setResourceName(statistics.getResourcesName());
				if (Format.isDateNotEmtyAndNotNull(statistics.getStartTime())) {
					dto.setStartTimeMills(statistics.getStartTime().getTime());
				}
				if(Format.isStringNotEmptyAndNotNull(statistics.getResourcesName()))
				{
					dto.setResourceName(statistics.getResourcesName());
				}
				dtos.add(dto);
			}
		}
		log.debug("end of method converToCrisisGameStatDTO");
		return dtos;
	}

	public static List<BpGameMappingDTO> convertBPCategoryMappingToDTO(
			List<BPCategoryMapping> gameDetails) {
		List<BpGameMappingDTO> dtos = new ArrayList<>();
		log.debug("start of method convertBPCategoryMappingToDTO");
		if (Format.isCollectionNotEmtyAndNotNull(gameDetails)) {
			for (BPCategoryMapping mapping : gameDetails) {
				BpGameMappingDTO dto = new BpGameMappingDTO();
				if (Format.isNotNull(mapping.getCategory())) {
					dto.setCategoryName(mapping.getCategory().getCategoryName());
				}
				if (Format.isNotNull(mapping.getGame())) {
					dto.setGameName(mapping.getGame().getGameName());
				}
				dto.setAvailableLicense(mapping.getFreeLicense());
				// dto.setUsedLicense(mapping.getPaidLicense());
				dto.setTotalLicense(mapping.getFreeLicense()
						+ mapping.getPaidLicense());
				dtos.add(dto);

			}
		}
		log.debug("end of method convertBPCategoryMappingToDTO");
		return dtos;
	}

	public static CGameResultDTO convertToCGResultDTO(
			CrisisGameStatistics statistics) {
		CGameResultDTO resultDTO = null;
		if (Format.isNotNull(statistics)) {
			resultDTO = new CGameResultDTO();
			String day = statistics.getDayNo().toString();
			switch (day) {
			case "DAY_01":
				resultDTO.setDayNO(CrisisGameDayEnum.DAY_01.toString());
				if (Format.isNotNull(statistics.getLivesSaved())) {
					resultDTO.setLivesSaved(statistics.getLivesSaved());
				}
				break;
			case "DAY_02":
				resultDTO.setDayNO(CrisisGameDayEnum.DAY_02.toString());
				if (Format.isNotNull(statistics.getLivesSaved())) {
					resultDTO.setLivesSaved(statistics.getLivesSaved());
				}
				break;
			case "DAY_03":
				resultDTO.setDayNO(CrisisGameDayEnum.DAY_03.toString());
				if (Format.isNotNull(statistics.getLivesSaved())) {
					resultDTO.setLivesSaved(statistics.getLivesSaved());
				}
				break;
			case "DAY_04":
				resultDTO.setDayNO(CrisisGameDayEnum.DAY_04.toString());
				setDay456(statistics, resultDTO);
				break;
			case "DAY_05":
				resultDTO.setDayNO(CrisisGameDayEnum.DAY_05.toString());
				setDay456(statistics, resultDTO);
				break;
			case "DAY_06":
				resultDTO.setDayNO(CrisisGameDayEnum.DAY_06.toString());
				setDay456(statistics, resultDTO);
				break;

			}
		}
		return resultDTO;
	}

	private static void setDay456(CrisisGameStatistics statistics,
			CGameResultDTO resultDTO) {
		int totalCost = Constants.ZERO;
		int cost;
		// for day 4,5,6 livesSaved=ServivorCount
		if (Format.isNotNull(statistics.getSurvivorsCount())) {
			resultDTO.setLivesSaved(statistics.getSurvivorsCount());
		}
		// cost = (moneySpent+(resourceUsed*50))/servivorCoount
		int moneySpent = Format.isNotNull(statistics.getMoneySpent()) ? statistics
				.getMoneySpent().intValue() : Constants.ZERO;
		int usedResource = Format.isNotNull(statistics.getResourcesUsed()) ? statistics
				.getResourcesUsed() : Constants.ZERO;
		totalCost = moneySpent + (usedResource * 50);
		try {
			if ((cost = totalCost / statistics.getSurvivorsCount()) > 0) {
				resultDTO.setCost(cost);
			} else {
				resultDTO.setCost(Constants.ONE_NEGATIVE);
			}
		} catch (Exception e) {
			resultDTO.setCost(Constants.ONE_NEGATIVE);
		}
	}

	public static ProjectForm convertProjectToForm(Project project) {
		log.debug("start of method convertProjectToForm");
		ProjectForm projectForm = null;
		if (Format.isNotNull(project)) {
			projectForm = new ProjectForm();
			projectForm.setProjectId(project.getProjectId());
			if (Format.isNotNull(project.getCompany())) {
				projectForm.setCompanyId(project.getCompany().getCompanyId());
				projectForm.setCompanyName(project.getCompany()
						.getCompanyName());
			}
			projectForm.setHrEmail(project.getHrEmail());
			projectForm.setHrName(project.getHrName());
			projectForm.setFacilitatorEmail(project.getFacilitatorEmail());
			projectForm.setFacilitatorName(project.getFacilitatorName());
			if (Format.isDateNotEmtyAndNotNull(project.getStartDate())) {
				projectForm.setStartDate(Format.formatDate(project
						.getStartDate()));
			}
			if (Format.isStringNotEmptyAndNotNull(project.getProjectStatus())) {
				projectForm.setProjectStatus(project.getProjectStatus());
			}
			if (Format.isDateNotEmtyAndNotNull(project.getCreatedOn())) {
				projectForm.setCreatedOn(Format.formatDate(project
						.getCreatedOn()));
			}
			projectForm.setNoOfPlayers(project.getNoOfPlayers());
			if (Format.isNotNull(project.getGame())) {
				projectForm.setGameId(project.getGame().getGameId());
				projectForm.setGameName(project.getGame().getGameName());
			}
		}
		log.debug("end of method convertProjectToForm");
		return projectForm;
	}

	public static String getLowerLevelRole(String selectedRole) {
		log.debug("start of method getSuperiorRole");
		String role = null;
		switch (selectedRole) {
		case "BH":
			role = Constants.BM_ROLE_TEXT;
			break;
		case "BUSINESS_HEAD":
			role = Constants.BM_ROLE_TEXT;
			break;

		case "BM":
			role = Constants.BDM_ROLE_TEXT;
			break;

		case "BUSINESS_MANAGER":
			role = Constants.BDM_ROLE_TEXT;
			break;

		case "BDM":
			role = Constants.BP_ROLE_TEXT;
			break;

		}
		log.debug("end of method getSuperiorRole");
		return role;
	}

	public static List<ProjectForm> convertProjectListToFormList(
			List<Project> projects) {
		log.debug("start of method convertProjectListToFormList");
		List<ProjectForm> forms = new ArrayList<>();
		ProjectForm pForm = new ProjectForm();
		if (Format.isCollectionNotEmtyAndNotNull(projects)) {
			for (Project project : projects) {
				pForm = new ProjectForm();
				pForm = convertProjectToForm(project);
				forms.add(pForm);
			}
		}
		log.debug("end of method convertProjectListToFormList");
		return forms;
	}

}