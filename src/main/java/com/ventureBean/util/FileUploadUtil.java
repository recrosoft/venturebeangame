package com.ventureBean.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.ventureBean.constants.Constants;

public class FileUploadUtil {

	/**
	 * log variable.
	 */
	public static final Log log = LogFactory.getLog(FileUploadUtil.class);
	
	private static ResourceBundle resourceBundle = ResourceBundle.getBundle("ftp");
	
	/**
	 * This method is used for uploading files to s3.
	 * @param file
	 * @return
	 */
	@SuppressWarnings("unused")
	public static String uploadFileToAWSS3(MultipartFile file) {
		log.debug("Start of method uploadFileToAWSS3");
		String existingBucketName = Constants.S3_BUCKET_FILE_UPLOAD;
		Date date = new Date(); 
		String fileName = file.getOriginalFilename();
		log.debug("orginal file name : " + fileName);
		String fileExtension = fileName.substring(fileName.indexOf(".")+1);
		String fileNameWithoutExt = fileName.substring(0,fileName.indexOf("."));
		fileName = fileNameWithoutExt + Constants.HYPEN + date.getTime() + Constants.DOT + fileExtension;
		log.debug("Key name : " + fileName);
		File convFile = new File(fileName);
		try {
			convFile.createNewFile(); 
			FileOutputStream fos = new FileOutputStream(convFile); 
			fos.write(file.getBytes());
			fos.close(); 
			AmazonS3 s3client = new AmazonS3Client(new  BasicAWSCredentials(resourceBundle.getString("s3.accessKey"), resourceBundle.getString("s3.secretKey")));
			PutObjectResult putObjectResult = s3client.putObject(new PutObjectRequest(existingBucketName, fileName, convFile).withCannedAcl(CannedAccessControlList.PublicRead));
		} catch (IOException e) {
			log.error("Exception occurred in uploading : ", e);
		}
		log.debug("End of method uploadFileToAWSS3");
		log.debug(" uploaded url " + Constants.S3_BASE_URL+ "/" + Constants.S3_BUCKET_FILE_UPLOAD + "/" + fileName);
		return Constants.S3_BASE_URL+ "/" + Constants.S3_BUCKET_FILE_UPLOAD + "/" + fileName ;
	}
	
	/**
	 * This method will delete file.
	 * @param keyName
	 */
	public static void deleteFile (String keyName) {
		log.debug("Start of method deleteFile");
		AmazonS3 s3client = new AmazonS3Client(new  BasicAWSCredentials(resourceBundle.getString("s3.accessKey"), resourceBundle.getString("s3.secretKey")));
		String existingBucketName = Constants.S3_BUCKET_FILE_UPLOAD;
		try {
            s3client.deleteObject(new DeleteObjectRequest(existingBucketName, keyName));
        } catch (Exception e) {
            log.error("Exception occurred in deleting image : ", e);
        }
		log.debug("End of method deleteFile");
	}
	
	/**
	 * This method will download file from its key name.
	 * @param keyName
	 * @return
	 */
	public static S3ObjectInputStream downloadFile (String keyName) {
		log.debug("Start of method downloadFile");
		AmazonS3 s3client = new AmazonS3Client(new  BasicAWSCredentials("AKIAIXFNN7AF2WUUQSPA", "mvB563xwhcuMlpUnscVNd5khdoxIGE/aTI6z4xM/"));
		S3Object s3object = s3client.getObject(new GetObjectRequest(
				Constants.S3_BUCKET_FILE_UPLOAD, keyName));
        log.debug("End of method downloadFile");
        return s3object.getObjectContent();
	}
	
}
