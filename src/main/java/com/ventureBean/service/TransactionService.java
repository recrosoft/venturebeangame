package com.ventureBean.service;

import java.util.List;

import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.model.Category;
import com.ventureBean.model.UserTransaction;

public interface TransactionService extends GenericService<UserTransaction, String> {

	/**@author Satyajit
	 * This method will record a user transaction
	 * @param transactionForm
	 * @param category 
	 * @throws NoRecordsFoundException 
	 */
	void recordTransaction(TransactionForm transactionForm, Category category) ;
	
	/**@THIS WILL return  pending transaction(s)
	 * @param transactionForm
	 * @return
	 */
	List<UserTransaction> getPendingTrnxList(TransactionForm transactionForm);
	
	/**This will approve or reject a User transaction
	 * @param transactionForm
	 * @param trnxActionType
	 * @throws NoRecordsFoundException
	 */
	void approveRejectTrnx(TransactionForm transactionForm, String trnxActionType) throws NoRecordsFoundException;
}
