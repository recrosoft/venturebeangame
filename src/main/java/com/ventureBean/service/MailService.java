package com.ventureBean.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ventureBean.exception.MailNotSentException;

public interface MailService {
	
	/**
	 * This method will publish mail to users.
	 * @param recipientMailIdList
	 * @param requiredValues
	 * @param templateName
	 * @param fromPersonMailId
	 * @param mailSubject
	 * @param ccEmailAddress
	 * @param bccList
	 * @throws MailNotSentException
	 */
	void sendMailUsingVelocity(List<String> recipientMailIdList, HashMap<String, String> requiredValues,
			String templateName, String fromPersonMailId, String mailSubject, String ccEmailAddress, List<String> bccList) throws MailNotSentException;
	
	/**
	 * @author Satyajit
	 * This will send mail with attachment files
	 * @param recipientMailIdList
	 * @param requiredValues
	 * @param templateName
	 * @param fromPersonMailId
	 * @param mailSubject
	 * @param ccEmailAddress
	 * @param bccList
	 * @param multipartFile
	 * @throws MailNotSentException
	 */
	public void sendMailWithAttachement(List<String> recipientMailIdList, HashMap<String, String> requiredValues,
			String templateName, String fromPersonMailId, String mailSubject, String ccEmailAddress, List<String> bccList , MultipartFile multipartFile ) throws MailNotSentException;
}
