package com.ventureBean.service;

import java.io.Serializable;
import java.text.ParseException;
import java.util.List;

import com.ventureBean.exception.GameNotFoundException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.ProjectNotInCreatedStateException;
import com.ventureBean.exception.RoleNotAssignedException;
import com.ventureBean.exception.StartDateNotAchivedException;
import com.ventureBean.form.BPCategoryMappingForm;
import com.ventureBean.form.CategoryForm;
import com.ventureBean.form.GameForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.model.BPCategoryMapping;
import com.ventureBean.model.Category;
import com.ventureBean.model.Game;
import com.ventureBean.model.Project;
import com.ventureBean.model.User;

public interface GameService extends GenericService<Game, Serializable> {

	/**@author Satyajit
	 * This method will return allGames from DB.
	 * @return List of Games.
	 */
	List<Game> getAllGames();
	
	/**@author Satyajit
	 * This will update a game
	 * @param gameForm
	 * @throws GameNotFoundException
	 */
	void updateGame(GameForm gameForm) throws GameNotFoundException;
	
	/**@author Satyajit
	 * This will update a GameCategory
	 * @param categoryForm
	 */
	void updateGameCategory(CategoryForm categoryForm);

	/**@author Satyajit
	 * This method will create a Project 
	 * @param projectForm
	 * @throws MailNotSentException 
	 * @throws ProjectNotInCreatedStateException 
	 * @throws ParseException 
	 */
	void createProject(ProjectForm projectForm) throws MailNotSentException, ProjectNotInCreatedStateException, ParseException;

	/**@author Satyajit
	 * This method will start the game
	 * @param form
	 * @return 
	 * @throws StartDateNotAchivedException 
	 * @throws ProjectNotInCreatedStateException 
	 * @throws RoleNotAssignedException 
	 */
	Project startProject(ProjectForm form) throws StartDateNotAchivedException, ProjectNotInCreatedStateException, RoleNotAssignedException;

	/**@author Satyajit
	 * This will return Bp Mapping to the games assigned 
	 * @param mappingForm
	 * @return
	 */
	List<BPCategoryMapping> getassignedGameDetails(BPCategoryMappingForm mappingForm);
	
	/**@author Satyajit
	 * This will add a new mapping entry into DB
	 * @param bp
	 * @param game
	 * @param category
	 * @param paidLicense
	 * @param freeLicense
	 */
	void createBusinessPartnerCategoryMap(User bp, Game game, Category category, Integer paidLicense, Integer freeLicense);

	/**@author Satyajit
	 * This will cancel the project.
	 * @param projectForm
	 */
	void cancelProject(ProjectForm projectForm);
	
	/**@author Satyajit
	 * This will edit the project
	 * @param projectForm
	 * @return
	 * @throws ParseException
	 * @throws ProjectNotInCreatedStateException
	 * @throws MailNotSentException
	 */
	Project editProject(ProjectForm projectForm ) throws ParseException, ProjectNotInCreatedStateException, MailNotSentException;
	
	/**@author Satyajit
	 * This will return Project(s) based on parameter(s) provided
	 * @param projectForm
	 * @return
	 */
	Project getProject(ProjectForm projectForm );

	/**@author Satyajit
	 * This will clear all Project User and Game User related to the project from DB
	 * @param project
	 */
	void clearProjectUserData(ProjectForm project);
	
}
