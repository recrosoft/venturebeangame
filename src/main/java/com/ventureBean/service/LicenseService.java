package com.ventureBean.service;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.dto.BpGameMappingDTO;
import com.ventureBean.form.LicenseForm;

public interface LicenseService extends GenericService<String, Serializable> {

	/**@author Satyajit
	 * This method will generate given no of licenses against the given Game and assign to the given Licensor
	 * @param licensor the Licensor whom the Licenses will be assigned
	 * @param game the game against which the licenses will be generated
	 * @param noOfLicense the no of icenses to generate
	 */
	//void generateLicense(LicenseForm licenseForm);

	/**@author Satyajit
	 * This will create the given no of license against the given user
	 * @param noOfLicense
	 * @param licenseType
	 * @param gameId
	 * @param userId
	 * @param licenseStatus
	 */
	void createLicenses(Integer noOfLicense, String licenseType, int gameId, Integer userId, String licenseStatus);
	
	/**@author Satyajit
	 * This will add license to the User . basically its send request to the Super Admin
	 * @param licenseForm
	 */
	void addLicenses(LicenseForm licenseForm);

	/**@author Satyajit
	 * This will return License map for Bp Game wise
	 * @param licenseForm
	 * @return
	 */
	List<BpGameMappingDTO> getGameLicenseMapForBP(LicenseForm licenseForm);
	
	/**This will return price ofr each license basd on which category it belongs to
	 * @param licenseForm
	 * @return
	 */
	Double getPricePerLicense(LicenseForm licenseForm);
}
