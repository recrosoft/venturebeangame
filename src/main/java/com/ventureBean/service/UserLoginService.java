package com.ventureBean.service;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.exception.CrisisGameLoginException;
import com.ventureBean.exception.EmailAddressAlreadyRegisteredException;
import com.ventureBean.exception.LinkExpiredException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.form.UserForm;
import com.ventureBean.form.UserListForm;
import com.ventureBean.model.ProjectUser;

public interface UserLoginService extends GenericService<ProjectUser, Serializable> {

	boolean ValidateAccessToken(String accessToken) throws LinkExpiredException, NoRecordsFoundException;

	/**@author Satyajit
	 * This method will get the User based on the criteria provided
	 * @param userForm
	 * @return
	 */
	ProjectUser getUser(UserForm userForm);

	/**@author Satyajit
	 * This service will return list of players of a particular project
	 * @param form
	 * @return
	 */
	List<ProjectUser> getPlayerList(UserForm form);

	/**@author Satyajit
	 * This method will take list of players and add or edit based on new or existing one  respectivly
	 * @param userListForm
	 * @throws MailNotSentException 
	 */
	void addEditPlayers(UserListForm userListForm) throws EmailAddressAlreadyRegisteredException, MailNotSentException;

	/**@author Satyajit
	 * @param projectId
	 * @return
	 * @throws CrisisGameLoginException 
	 */
	boolean validateTeamLeader(Integer projectId) throws CrisisGameLoginException;
}
