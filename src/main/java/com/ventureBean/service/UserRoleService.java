package com.ventureBean.service;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.model.UserRoleMaster;

public interface UserRoleService extends GenericService<UserRoleMaster, Serializable> {

	
	/**@author Satyajit
	 * This will return List of roles based on Id
	 * @param userName
	 * @return
	 */
	List<UserRoleMaster> getRoles(String userName);
	
	/**@author Satyajit
	 * This will return all Role
	 * @return
	 */
	List<UserRoleMaster> getRoles();
}
