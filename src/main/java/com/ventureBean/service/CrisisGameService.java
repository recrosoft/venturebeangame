package com.ventureBean.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.ventureBean.dto.CGameResultDTO;
import com.ventureBean.exception.GameNotCompletedException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.ProjectNotFoundException;
import com.ventureBean.form.GameStatForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.form.TeamPlayerMappingForm;
import com.ventureBean.form.UserForm;
import com.ventureBean.model.CrisisGameIdealResult;
import com.ventureBean.model.CrisisGameRoleMaster;
import com.ventureBean.model.CrisisGameStatistics;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.model.CrisisGameUser;
import com.ventureBean.model.Project;
import com.ventureBean.model.ProjectUser;

public interface CrisisGameService extends GenericService<ProjectUser, Serializable> {

	void assignDesignation(TeamPlayerMappingForm teamPlayerMappingForm) throws MailNotSentException;
	
	List<CrisisGameRoleMaster> getAllGameRoles();

	List<CrisisGameTeam> getAllTeam();
	
	List <CrisisGameUser> getGameUsers(UserForm form);

	/**@author Satyajit
	 * This method will return Statistics of a team.
	 * @param projectUserId 
	 * @return
	 */
	List<CrisisGameStatistics> getGameStatsByTeam(Integer projectUserId);

	/**@author Satyajit
	 * This method will save the Game statistics 
	 * @param statForm
	 */
	void recordGameStats(GameStatForm statForm);

	/**@author Satyajit
	 * This will get all the players those are alredy assigned with the role.
	 * @param form
	 * @return 
	 */
	List<CrisisGameUser> getAssignedPlayers(UserForm form);

	Project getProjectById(Integer projectId);

	List<CrisisGameStatistics> getGameStats(Integer projectId);
	
	void reSetDay(GameStatForm statForm);

	void startSession2(GameStatForm statForm);

	void endGame(ProjectForm projectForm) throws GameNotCompletedException;

	Map<String, List<CGameResultDTO>> getGameResult(ProjectForm projectForm) throws ProjectNotFoundException;

	Map<String, List<CrisisGameIdealResult>> getIdealResult();
	
	void deleteProjectUser(ProjectForm projectForm);
}
