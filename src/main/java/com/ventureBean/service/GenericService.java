/**
 * 
 */
package com.ventureBean.service;

import java.io.Serializable;

/**
 * @author R000101
 *
 */
public interface GenericService<T, ID extends Serializable> {

    /**
     * This method will return the Hibernate proxy object. <br />
     * Data inside might have to be initialised within the same session and will not be accessible. <br />
     *
     * @param id the id
     * @param lock the lock
     * @return the t
     */
    T findById(ID id, boolean lock);

    /**
     * This method will never return a hibernate proxy object.
     *
     * @param id the id
     * @param lock the lock
     * @return the by id
     */
    T getById(ID id, boolean lock);

    /**
     * Find all.
     *
     * @return the list
     */
   

    /**
     * Save or update.
     *
     * @param entity the entity
     * @return the t
     */
    T saveOrUpdate(T entity);
    
    /**
     * create and Save the Entity.
     *
     * @param entity the entity
     * @return the t
     */
    T create(T entiry);

    /**
     * Removes the.
     *
     * @param entity the entity
     */
    void remove(T entity);
    
    /**
     * @param entity
     */
    T merge(T entity);
    
    /**
     * @param entity
     * @return the T
     */
    T update(T entity);

    /**
     * @param entity
     * @return
     */
    T save(T entity);
}
