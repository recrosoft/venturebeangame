package com.ventureBean.service;

import java.util.List;

import com.ventureBean.exception.ProjectNotFoundException;
import com.ventureBean.form.CompanyForm;
import com.ventureBean.model.Company;
import com.ventureBean.model.Project;

public interface CompanyService extends GenericService<Company, Integer> {

	/**@author Satyajit
	 * This method will return All the Companies Registered .
	 * @return
	 */
	List<Company> getAllCompanyList();

	/**@author Satyajit
	 * This method will will return a list of Company based on the criteria provided
	 * @param companyForm
	 * @return
	 */
	List<Company> getCompanyList(CompanyForm companyForm);

	/**@author Satyajit
	 * This method will add a company to Db
	 * @param companyForm
	 */
	void addCompany(CompanyForm companyForm);

	/**@author Satyajit
	 * This will return Company(s) based Id
	 * @param companyForm
	 * @return
	 */
	Company getCompanyById(CompanyForm companyForm);

	/**@author Satyajit
	 * This will return Project(s) based on parameter(s) provided
	 * @param companyForm
	 * @return
	 * @throws ProjectNotFoundException
	 */
	List<Project> getProjectsByCompany(CompanyForm companyForm) throws ProjectNotFoundException;
}
