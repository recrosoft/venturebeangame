/**
 * 
 */
package com.ventureBean.service;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.exception.EmailAddressAlreadyRegisteredException;
import com.ventureBean.exception.GameNotFoundException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.form.ChangePasswordForm;
import com.ventureBean.form.RoleForm;
import com.ventureBean.form.AdminForm;
import com.ventureBean.form.UserSearchForm;
import com.ventureBean.model.User;

/**
 * @author R000101
 *
 */
public interface AdminLoginService extends GenericService<User, Serializable>{
	
	
	/**@author Satyajit
	 * This will validate user
	 * @param adminForm
	 * @return
	 */
	boolean validateUser(AdminForm  adminForm) ;

	/**@author Satyajit
	 * This method will the the admin based on the condition provided 
	 * @param adminForm which carries all the condition 
	 * @return  the User
	 */
	User getUser(AdminForm adminForm);

	/**
	 * @return
	 */
	List<User> getAllLicensor();
	
	/**@author Satyajit
	 * This method will return Admin(s) based on the Condition provided 
	 * @param adminForm
	 * @return List of Users
	 */
	
	List<User> getAdminList(AdminForm adminForm);

	/**@author Satyajit
	 * This method will create/ add a new Admin  by the VBAdmin
	 * @param roleForm
	 * @return RoleName
	 * @throws EmailAddressAlreadyRegisteredException if the email address already exits 
	 * @throws MailNotSentException if mail no sent to the given mail address
	 * @throws GameNotFoundException 
	 * @throws NoRecordsFoundException 
	 */
	String createRole(RoleForm roleForm) throws EmailAddressAlreadyRegisteredException, MailNotSentException, GameNotFoundException, NoRecordsFoundException;

	/**@author Satyajit
	 * This method will return Admin(s) based on Role provided
	 * @param adminForm
	 * @return
	 */
	List<User> getUsersBasedOnRole(AdminForm adminForm);

	/**@author Satyajit
	 * This method will return Users List based on the search criteria provided
	 * @param searchForm
	 * @return
	 */
	List<User> getUsersForNonVBAdmin(UserSearchForm searchForm);

	/**@author Satyajit
	 * This will return Role of the admin based on ID
	 * @param id
	 * @return
	 */
	String getUserRoleByUserId(Integer id);

	/**@author Satyajit
	 * This method will change the password of the current logged-in user.
	 * @param form
	 * @throws NoRecordsFoundException 
	 */
	void changePassword(ChangePasswordForm form) throws NoRecordsFoundException;

	/**@author Satyajit
	 * This will send new password to the user
	 * @param form
	 * @throws NoRecordsFoundException
	 * @throws MailNotSentException
	 */
	void forgotPassword(ChangePasswordForm form) throws NoRecordsFoundException, MailNotSentException;

}
