/**
 * 
 */
package com.ventureBean.service.impl;

import java.io.Serializable;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ventureBean.dao.GenericDAO;
import com.ventureBean.service.GenericService;

/**
 * @author R000101
 *
 */
@Service(GenericServiceImpl.SERVICE_NAME)
@Transactional(propagation=Propagation.REQUIRED)
public class GenericServiceImpl<T, ID extends Serializable> implements GenericService<T, ID>{

	/**
	 * Declared Service name.
	 */
	public static final String SERVICE_NAME = "genericService";
	
	/**
     * log variable.
     */
    protected final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private GenericDAO<T, Serializable> genericDAO;
	
	public T findById(Serializable id, boolean lock) {
		return genericDAO.findById(id, lock);
	}

	public T getById(Serializable id, boolean lock) {
		return genericDAO.getById(id, lock);
	}

	public T saveOrUpdate(T entity) {
		return genericDAO.saveOrUpdate(entity);
	}

	public T create(T entiry) {
		return genericDAO.create(entiry);
	}

	public void remove(T entity) {
		genericDAO.remove(entity);
		
	}

	 /**
     * @param entity
     */
    public T merge(T entity) {
    	return genericDAO.merge(entity);
    }
    
    /**
     * @param entity
     */
    public T update(T entity) {
    	return genericDAO.update(entity);
    }
    
    public T save(T entity) {
    	return genericDAO.save(entity);
    }
}
