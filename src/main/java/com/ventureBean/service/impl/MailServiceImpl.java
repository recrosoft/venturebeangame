package com.ventureBean.service.impl;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.service.MailService;
import com.ventureBean.util.Format;
@Service(MailServiceImpl.SERVICE_NAME)
public class MailServiceImpl implements MailService {

	private final Log log = LogFactory.getLog(getClass());
	
	private final String className = getClass().getName();
	
	public static final String SERVICE_NAME = "mailService";

	/**
	 * Injected mailSender.
	 */
	@Autowired
	private JavaMailSender mailSender;

	/**
	 * Variable for instance of the SimpleMailMessage used in a created
	 * Service/Change Request email.
	 */
	@Autowired
	private SimpleMailMessage simpleMailMessage;

	/**
	 * Injected velocityEngine.
	 */
	@Autowired
	private VelocityEngine velocityEngine;
	
	public void setMailSender(JavaMailSenderImpl mailSender) {
		this.mailSender = mailSender;
	}
	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}
	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}
	public void sendMailUsingVelocity(List<String> recipientMailIdList, HashMap<String, String> requiredValues,
			String templateName, String fromPersonMailId, String mailSubject, String ccEmailAddress, List<String> bccList) throws MailNotSentException {

		log.info("Inside sendMailUsingVelocity method of MailManagerImpl");

		Template template = null;
		if(templateName != null && !templateName.equals("")){
			log.info("************"+velocityEngine);
			template = velocityEngine.getTemplate(templateName);
		}
		else{
			log.info("template Name is empty.");
		}

		if(recipientMailIdList!=null && recipientMailIdList.size()>0){

			log.info("recipientMailIdList : " + recipientMailIdList.size());

			String recipientMail = (String)recipientMailIdList.get(0);
			log.info("recipientMailIdValue : " + recipientMail);
		}
		try {
			VelocityContext velocityContext = new VelocityContext();
			StringWriter writer = new StringWriter();		
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true,"UTF-8");
			if(mailSubject != null && !mailSubject.equals("") ){
				simpleMailMessage.setSubject(mailSubject);
				mimeMessageHelper.setSubject(simpleMailMessage.getSubject());
			}
			Set<String> keys = requiredValues.keySet();
			if (!keys.isEmpty())
			{
				for (String key : keys) {
					String keyString = key.toString();
					velocityContext.put(keyString, requiredValues.get(key));
				}		
			}
			template.merge(velocityContext, writer);
			//adding to email addresses
			if (recipientMailIdList != null && !recipientMailIdList.isEmpty()) {
				Object[] toPerson = recipientMailIdList.toArray();
				String toPersonArray[] = new String[toPerson.length];
				for (int count = 0; count < toPerson.length; count++) {
					toPersonArray[count] = toPerson[count].toString();
				}
				mimeMessageHelper.setTo(toPersonArray);
			}
			// adding bcc email addresses.
			if (bccList != null && !bccList.isEmpty()) {
				Object[] bccPerson = bccList.toArray();
				String bccPersonArray[] = new String[bccPerson.length];
				for (int count = 0; count < bccPerson.length; count++) {
					bccPersonArray[count] = bccPerson[count].toString();
				}
				mimeMessageHelper.setBcc(bccPersonArray);
			}
			
			if(Format.isStringNotEmptyAndNotNull(ccEmailAddress))
			{
				mimeMessageHelper.setCc(ccEmailAddress);
			}

			if(fromPersonMailId != null && !fromPersonMailId.equals("")){
				mimeMessageHelper.setFrom(fromPersonMailId);
			}
			else{
				log.info("From person mail Id is empty.");
			}
			mimeMessageHelper.setText(writer.toString(), true);
			mailSender.send(mimeMessage);

			log.info("Mail sent successfully");	
		} catch (Exception e) {
			throw new MailNotSentException(e);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.dealocx.service.MailService#sendMailWithAttachement(java.util.List, java.util.HashMap, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.List, org.springframework.web.multipart.MultipartFile)
	 */
	public void sendMailWithAttachement(List<String> recipientMailIdList, HashMap<String, String> requiredValues,
			String templateName, String fromPersonMailId, String mailSubject, String ccEmailAddress, List<String> bccList , MultipartFile multipartFile) throws MailNotSentException {
		log.debug("Start of method "+className + ".sendMailWithAttachement()");
		
		Template template = null;
		if(Format.isStringNotEmptyAndNotNull(templateName) ){
			template = velocityEngine.getTemplate(templateName);
		}
		else{
			log.debug("template Name is empty.");
		}

	try {
			VelocityContext velocityContext = new VelocityContext();
			StringWriter writer = new StringWriter();		
			MimeMessage mimeMessage = mailSender.createMimeMessage();
			MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true,"UTF-8");
			
			if(Format.isStringNotEmptyAndNotNull(mailSubject) ){
				//simpleMailMessage.setSubject(mailSubject);
				
				mimeMessageHelper.setSubject(mailSubject);
			}
			Set<String> keys = requiredValues.keySet();
			if (!keys.isEmpty())
			{
				for (String key : keys) {
					String keyString = key.toString();
					velocityContext.put(keyString, requiredValues.get(key));
				}		
			}
			template.merge(velocityContext, writer);
			mimeMessageHelper.setText(writer.toString(), true);
			
			//adding to email addresses
			if (Format.isCollectionNotEmtyAndNotNull(recipientMailIdList )) {
				
				String[] toEMailIds = recipientMailIdList.toArray(new String[recipientMailIdList.size()]);
				mimeMessageHelper.setTo(toEMailIds);
				
			}
			
			//mimeMessageHelper.setTo("sutar.satyajit@gmail.com");
			
			// adding bcc email addresses.
			if (Format.isCollectionNotEmtyAndNotNull(bccList)) {
				
				String[] bccEmailIds = bccList.toArray(new String[bccList.size()]);
				mimeMessageHelper.setBcc(bccEmailIds);
			}
			
			//adding ccemailids
			if(Format.isStringNotEmptyAndNotNull(ccEmailAddress))
			{
				mimeMessageHelper.setCc(ccEmailAddress);
			}
			
			if(Format.isStringNotEmptyAndNotNull(fromPersonMailId )){
				mimeMessageHelper.setFrom(fromPersonMailId);
			}
			else{
				log.info("From person mail Id is empty.");
			}
			
			// adding attachement to mail
			if(null != multipartFile){
				//InputStream  stream = multipartFile.getInputStream();
				byte[] byteArray  = multipartFile.getBytes();
				//mimeMessageHelper.addAttachment(multipartFile.getName(), new InputStreamResource(stream) , mimeType);
				mimeMessageHelper.addAttachment(multipartFile.getName(), new ByteArrayResource(byteArray));
			}
			mailSender.send(mimeMessage);

			log.info("Mail sent successfully with attachement .");	
		} catch (Exception e) {
			e.printStackTrace();
			throw new MailNotSentException(e);
		}
		log.debug("End of method "+className + ".sendMailWithAttachement()");
	}

}
