package com.ventureBean.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ventureBean.constants.Constants;
import com.ventureBean.dao.AdminLoginDAO;
import com.ventureBean.dao.CompanyDAO;
import com.ventureBean.dao.ProjectDAO;
import com.ventureBean.exception.ProjectNotFoundException;
import com.ventureBean.form.CompanyForm;
import com.ventureBean.model.Company;
import com.ventureBean.model.Project;
import com.ventureBean.service.CompanyService;
import com.ventureBean.util.FileUploadUtil;
import com.ventureBean.util.Format;

@Service(CompanyServiceImpl.SERVICE_NAME)
public class CompanyServiceImpl extends GenericServiceImpl<Company, Integer> implements CompanyService{

	/**
	 * @Declared  companyService
	 */
	public static final String SERVICE_NAME = "companyService";

	/**
	 * @Injected companyDAO
	 */
	@Autowired
	private CompanyDAO companyDAO;
	
	@Autowired
	private AdminLoginDAO  adminLoginDAO;
	
	@Autowired
	private ProjectDAO projectDAO;
	/* (non-Javadoc)
	 * @see com.ventureBean.service.CompanyService#getAllCompanyList()
	 */
	@Override
	public List<Company> getAllCompanyList() {
		log.debug("start of method getAllCompanyList() ");
		 List<Company> companies = companyDAO.getAll(new Company());
		log.debug("end of method getAllCompanyList() ");
		return companies;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.CompanyService#getCompanyList(com.ventureBean.form.CompanyForm)
	 */
	@Override
	public List<Company> getCompanyList(CompanyForm companyForm) {
		log.debug("start of method getCompanyList() ");
		List<Company>  companies = companyDAO.getCompany(companyForm);
		log.debug("end of method getCompanyList() ");
		return companies;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.service.CompanyService#addCompany(com.ventureBean.form.CompanyForm)
	 */
	@Override
	public void addCompany(CompanyForm companyForm) {
		log.debug("start of method addCompany() ");
		Company company  = new Company();
		company.setCompanyName(companyForm.getCompanyName());
		company.setEmailSuffix(companyForm.getCompanyEmail());
		company.setCompanyCity(companyForm.getCompanyCity());
		company.setCompanyAddr(companyForm.getCompanyAddr());
		if(companyForm.getCompanyLogo().getSize()>Constants.ZERO){
			String logoUrl= FileUploadUtil.uploadFileToAWSS3(companyForm.getCompanyLogo());
			company.setCompanyLogoUrl(logoUrl);
		}
		company.setCreatedOn(new Date());
		company.setCreatedBy(companyForm.getCreatedBy());
		company.setBusinessPartner(adminLoginDAO.getByUserName(companyForm.getCreatedBy()));
		companyDAO.save(company);
		log.debug("end of method addCompany() ");
		
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.CompanyService#getCompanyById(com.ventureBean.form.CompanyForm)
	 */
	@Override
	public Company getCompanyById(CompanyForm companyForm) {
		log.debug("start of method  getCompanyById");
		Company  company = companyDAO.getById(companyForm.getCompanyId(), true);
		log.debug("end of method  getCompanyById");
		return company;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.CompanyService#getProjectsByCompany(com.ventureBean.form.CompanyForm)
	 */
	@Override
	public List<Project> getProjectsByCompany(CompanyForm companyForm) throws ProjectNotFoundException {
		log.debug("start of method getProjectsByCompany ");
		List<Project>  projects = projectDAO.getProjects(companyForm);
		if(Format.isCollectionEmtyOrNull(projects)){
			throw new ProjectNotFoundException();
		}
		log.debug("end of method getProjectsByCompany ");
		return projects;
	}
	
}
