package com.ventureBean.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ventureBean.constants.Constants;
import com.ventureBean.constants.CrisisGameConstants;
import com.ventureBean.dao.AppConfigurationDAO;
import com.ventureBean.dao.ProjectDAO;
import com.ventureBean.dao.UserLoginDAO;
import com.ventureBean.dao.crisisGame.CrisisGameUserDAO;
import com.ventureBean.enums.ProjectStatusEnum;
import com.ventureBean.enums.ProjectUserEnum;
import com.ventureBean.exception.CrisisGameLoginException;
import com.ventureBean.exception.EmailAddressAlreadyRegisteredException;
import com.ventureBean.exception.LinkExpiredException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.form.GameUserForm;
import com.ventureBean.form.MailFormData;
import com.ventureBean.form.UserForm;
import com.ventureBean.form.UserListForm;
import com.ventureBean.model.AppConfiguration;
import com.ventureBean.model.CrisisGameRoleMaster;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.model.CrisisGameUser;
import com.ventureBean.model.Project;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.service.MailService;
import com.ventureBean.service.UserLoginService;
import com.ventureBean.util.Format;
import com.ventureBean.util.StringGeneratorUtil;

@Service(UserLoginServiceImpl.SERVICE_NAME)
public class UserLoginServiceImpl extends GenericServiceImpl<ProjectUser, Serializable> implements UserLoginService {

	/**
	 * Declared SERVICE_NAME
	 */
	
	public static final String SERVICE_NAME="userLoginService";
	/**
	 * @Declared appConfigurationDAO
	 */
	@Autowired
	private AppConfigurationDAO appConfigurationDAO;
	
	/**
	 * Injected userLoginDAO
	 */
	@Autowired
	private UserLoginDAO userLoginDAO;
	
	/**
	 * @Declared encoder
	 */
	private Md5PasswordEncoder encoder = new Md5PasswordEncoder();
	
	/**
	 * @Injected crisisGameUserDAO
	 * 
	 */
	@Autowired
	private CrisisGameUserDAO crisisGameUserDAO;

	/**
	 * @declared projectDAO
	 */
	@Autowired
	private ProjectDAO projectDAO;
	/**
	 * @Instantiated mailResourceBundle
	 */
	ResourceBundle resourceBundle = ResourceBundle.getBundle("ApplicationResources");
	
	@Autowired
	private CrisisGameUserDAO gameUserDAO;
	
	/**
	 * @Autowaired mailService
	 */
	@Autowired
	private MailService mailService;
	/* (non-Javadoc)
	 * @see com.ventureBean.service.UserLoginService#ValidateAccessToken(java.lang.String)
	 */
	@Override
	public boolean ValidateAccessToken(String accessToken) throws LinkExpiredException, NoRecordsFoundException {
		log.debug("start of method ValidateAccessToken");
		ProjectUser user = null;
		boolean isValid = true;
		UserForm form = new UserForm();
		form.setAccessToken(accessToken);
		List<ProjectUser> users = userLoginDAO.getUser(form);
		if(Format.isCollectionNotEmtyAndNotNull(users)){
			user= users.get(Constants.GET_ZERO_INDEX_OBJECT);
			int validityPeriod = user.getAccValidity();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(user.getCreatedOn());
			calendar.add(Calendar.DAY_OF_MONTH , validityPeriod);
			Date currentDate = new Date();
			if(!currentDate.before(calendar.getTime())){
				throw new LinkExpiredException();
			}
		log.info("-------------token is valid------------");	
		}else{
			throw new NoRecordsFoundException();
		}
		log.debug("end of method ValidateAccessToken");
		return isValid;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.UserLoginService#getUser(com.ventureBean.form.UserForm)
	 */
	@Override
	public ProjectUser getUser(UserForm userForm) {
		log.debug("start of method getUser");
		ProjectUser user= null;
		List<ProjectUser> projectUsers = userLoginDAO.getUser(userForm);
		if(Format.isCollectionNotEmtyAndNotNull(projectUsers)){
	       user =  projectUsers.get(Constants.GET_ZERO_INDEX_OBJECT);
		}
		log.debug("end of method getUser");
		return user;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.UserLoginService#getPlayerList(com.ventureBean.form.UserForm)
	 */
	@Override
	public List<ProjectUser> getPlayerList(UserForm form) {
		log.debug("start of method getPlayerList");
		form.setUserType(ProjectUserEnum.PLAYER.toString());
		List<ProjectUser> players = userLoginDAO.getUser(form);
		log.debug("start of method getPlayerList");
		return players;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.UserLoginService#addEditPlayers(com.ventureBean.form.UserListForm)
	 */
	@Override
	public void addEditPlayers(UserListForm userListForm)  throws EmailAddressAlreadyRegisteredException, MailNotSentException {
		log.debug("start of method addEditPlayers");
		// first delete all the entry from tbl_crisis_game_user 
		Project project = new Project();
		project.setProjectId(userListForm.getProjectId());
		UserRoleMaster roleMaster = new UserRoleMaster();
		roleMaster.setUserRoleId(Constants.PLAYER_ROLE_ID);
		roleMaster.setRoleName(Constants.PLAYER_ROLE);
		String emailAddr = null;
		boolean emailAlreadyExists = false;
		String duplicateEmail =null;
		if(Format.isNotNull(userListForm.getUserForm())){
			for(UserForm form : userListForm.getUserForm()){
				ProjectUser player = null;
				if(Format.isIntegerNotEmtyAndNotZero(form.getProjectUserId())){
					try {
						emailAddr = form.getEmailAddress()+userListForm.getEmailSuffix();
						player = userLoginDAO.getById(form.getProjectUserId(), true);
						if(!player.getEmailAddress().equals(emailAddr)){
							player.setName(form.getName());
							player.setUserName(form.getEmailAddress());
							form.setUserName(form.getEmailAddress());
							player.setEmailAddress(emailAddr);
							player.setModifiedOn(new Date());
							player.setModifiedBy(userListForm.getCreatedBy());
							userLoginDAO.update(player);
						}
					}catch (Exception e) {
						log.error("Exception occurred :  EmailAddressAlreadyExistException "+emailAddr);
						emailAlreadyExists = true;
						duplicateEmail =emailAddr;
					}
				}
				//	means  new User/player
				else  if(Format.isStringNotEmptyAndNotNull(form.getEmailAddress()) && Format.isStringNotEmptyAndNotNull(form.getName()))
				{
					   //check for duplicate entry
						String userName = null;
						UserForm userForm = new UserForm();
						//userForm.setProjectId(userListForm.getProjectId());
						userForm.setUserName(form.getEmailAddress());//at this point email addr field doesnot contain the @ suffix
						userForm.setUserType(ProjectUserEnum.PLAYER.toString());
						List<ProjectUser> projectUsers = userLoginDAO.getUser(userForm);
						if(Format.isCollectionNotEmtyAndNotNull(projectUsers)){
							player = projectUsers.get(Constants.GET_ZERO_INDEX_OBJECT);
								if(player.getProject().getProjectId().equals(userListForm.getProjectId())){
									throw new EmailAddressAlreadyRegisteredException();
								}
								else{
									userName = userLoginDAO.generateUserName(userForm.getUserName());
								}
						}
						else{
							userName = form.getEmailAddress();
						}
						player = new ProjectUser();
						String emailAddrs = form.getEmailAddress()+userListForm.getEmailSuffix();
						player.setEmailAddress(emailAddrs);
						player.setName(form.getName());
						player.setUserName(userName);
						form.setUserName(userName);
						player.setUserType(ProjectUserEnum.PLAYER.toString());
						player.setUserRole(roleMaster);
						player.setProject(project);
						player.setUserRole(roleMaster);
						player.setCreatedOn(new Date());
						player.setCreatedBy(userListForm.getCreatedBy());
						userLoginDAO.save(player);
						form.setProjectUserId(player.getProjectUserId());
						System.out.println("*****************"+form.getTeamId()+"team id");
					}
				}
			}
			saveCrisisGameUsers(userListForm);
			if(emailAlreadyExists){
				throw new EmailAddressAlreadyRegisteredException(duplicateEmail);
			}
		
	  log.debug("end of method addEditPlayers");
	}

	private void saveCrisisGameUsers(UserListForm userListForm) throws MailNotSentException {
		GameUserForm gForm = new GameUserForm();
		gForm.setProjectId(userListForm.getProjectId());
		gameUserDAO.removeUsers(gForm);
		Map<MailFormData ,String> mailMap = new HashMap<>();
		for(UserForm form1 : userListForm.getUserForm()){
			//UserForm userForm = new UserForm();
			//if(Format.isNotNull(form1.getProjectUserId())){
			if(Format.isStringNotEmptyAndNotNull(form1.getEmailAddress()) && Format.isStringNotEmptyAndNotNull(form1.getName())){
				try {
					System.out.println("------------------");
						ProjectUser projectUser =new ProjectUser();
						projectUser.setProjectUserId(form1.getProjectUserId());
						CrisisGameUser gameUser = new CrisisGameUser();
						CrisisGameRoleMaster role = new CrisisGameRoleMaster();
					    role.setRoleId(Integer.parseInt(form1.getRoleId()));
				     	gameUser.setRole(role);//roleid
						CrisisGameTeam team = new CrisisGameTeam();
						team.setTeamId(Integer.parseInt(form1.getTeamId()));
						gameUser.setTeam(team);
						gameUser.setUser(projectUser);
						gameUser.setModifiedBy(form1.getModifiedBy());
						gameUser.setModifiedOn(form1.getModifiedOn());
						Project project = new Project();
						project.setProjectId(userListForm.getProjectId());
						gameUser.setProject(project);
						gameUser.setCreatedBy(form1.getCreatedBy());
			            gameUser.setCreatedOn(new Date());
			            crisisGameUserDAO.save(gameUser);
			        if(form1.getTeamId().equals("1"))
						{
						// update the ProjectUser with Link , password and
						// send loginCredential  through credentials  Only to leader who can update game details
						//if(player.getAssignedRole().equals(CrisisGameConstants.ROLE_LEADER)){
			        		projectUser = userLoginDAO.findById(form1.getProjectUserId(), false);
							AppConfiguration appConfiguration = appConfigurationDAO.getAppConfigurationByName(resourceBundle.getString("appConfig.name.accessTokenValidity.user"));
							projectUser.setAccValidity(Integer.parseInt(appConfiguration.getAppConfigurationValue()));
							String accessToken = StringGeneratorUtil.generateString(Constants.PIN_MIN_LENGTH, Constants.PIN_MAX_LENGTH, Constants.PIN_NO_OF_CAPS_ALPHABETS, Constants.PIN_NO_OF_DIGITS, Constants.PIN_NO_OF_SPECIALCHAR);
							projectUser.setAccessToken(accessToken);
							String pass= StringGeneratorUtil.generatePassword(projectUser.getName(),null);
							log.debug("access Token leader : "+ accessToken +"  password : "+ pass);
							projectUser.setPassword(encoder.encodePassword(pass, null));
							projectUser.setModifiedOn(new Date());
							//projectUser.setModifiedBy(.getCreatedby());
							userLoginDAO.save(projectUser);
							MailFormData mailData = new MailFormData();
							mailData.setName(form1.getName());
							mailData.setMailAddress(form1.getEmailAddress()+userListForm.getEmailSuffix());
							mailData.setUserName(form1.getUserName());
							mailData.setPassword(pass);
							mailData.setLink(Constants.BASE_LINK+accessToken);
							mailData.setDesc("mail.welcomeUser.body");
							mailData.setTemplateName("mail.template.loginLink");
							mailData.setSubject("mail.welcomeUser.subject");
							mailMap.put(mailData ,form1.getTeamId());
						}
					else{
							MailFormData mailData = new MailFormData();
							mailData.setName(form1.getName());
							mailData.setMailAddress(form1.getEmailAddress()+userListForm.getEmailSuffix());
							mailData.setUserName(form1.getUserName());
							mailData.setDesc("mail.welcomeUser.body");
							mailData.setTemplateName("mail.template.common");
							mailData.setSubject("mail.welcomeUser.subject");
							mailMap.put(mailData , form1.getTeamId());
						}
											}
					catch (Exception e) {
						log.error("Exception orrured while assigning designation to players :",e);
					}
			}
			//   crisisGameUserDAO.save(gameUser);
		
		log.debug("end of method assignDesignation ");
		}
		sendMailToUser(mailMap);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.UserLoginService#validateTeamLeader(java.lang.Integer)
	 */
	@Override
	public boolean validateTeamLeader(Integer projectId) throws CrisisGameLoginException {
		log.debug("start of method validateTeamLeader ");
		boolean isValid = false;
		if (Format.isNotNull(projectId)) {
			Project project = projectDAO.getById(projectId, false);
			if (Format.isNotNull(project)) {
				String status = project.getProjectStatus();
				if (status.equals(ProjectStatusEnum.ACTIVE.toString())) {
					return  true;
				}else if(status.equals(ProjectStatusEnum.CANCELLED.toString())) {
					throw new CrisisGameLoginException(CrisisGameConstants.GAME_CANCELLED);
				}else if(status.equals(ProjectStatusEnum.COMPLETED.toString())) {
					throw new CrisisGameLoginException(CrisisGameConstants.GAME_COMPLETED);
				}else if(status.equals(ProjectStatusEnum.CREATED.toString())) {
					throw new CrisisGameLoginException(CrisisGameConstants.GAME_NOT_STARTED);
				}else{
						throw new CrisisGameLoginException();
				}
			}
		}
		log.debug("end of method validateTeamLeader ");
		return isValid;
	}
	

	/**@author Satyajit
	 * This method will send mail to the Leader  with the login credential
	 * @param projectForm
	 * @param pass
	 * @param accessToken
	 * @throws MailNotSentException
	 */
	private void sendMailToUser(Map<MailFormData, String> mailMap) throws MailNotSentException {
		log.debug("Start of method sendMailToUser");
		ResourceBundle mailResourceBundle = ResourceBundle.getBundle("mail");
		String fromMailAddress = mailResourceBundle.getString("mail.template.fromMailAddress");
		String mailBody = null;
		String templateName =null;
		String mailSubject = null;
		MailFormData mailData = null;
		if(Format.isNotNull(mailMap)){
			for (Map.Entry<MailFormData, String> entry : mailMap.entrySet()){
				mailData = entry.getKey();
				String role = entry.getValue();
				mailBody = mailResourceBundle.getString(mailData.getDesc());
				templateName = mailResourceBundle.getString(mailData.getTemplateName());
				mailSubject = mailResourceBundle.getString(mailData.getSubject())+" "+role;
				List<String> recipientMailIdList = new ArrayList<>();
				recipientMailIdList.add(mailData.getMailAddress());
				HashMap<String, String> requiredValues = new HashMap<>();
				if(role.equals(CrisisGameConstants.ROLE_LEADER)){
					requiredValues.put("name", mailData.getName());
					requiredValues.put("description",mailBody+role);
					requiredValues.put("userName",mailData.getUserName());
					requiredValues.put("password", mailData.getPassword());
					requiredValues.put("link",mailData.getLink());
				}else{
					requiredValues.put("name", mailData.getName());
					requiredValues.put("description",mailBody);
				}
				try {
					log.debug("sending mail : user :"+mailData.getUserName() +" ROLE : "+role);
					mailService.sendMailUsingVelocity(recipientMailIdList, requiredValues, templateName, fromMailAddress, mailSubject,null, null);
				} catch (Exception e) {
					log.debug("MailNotSentException from AWS : ",e);
				}
			}
		}
		log.debug("End of method sendMailToUser");
	}
}
