package com.ventureBean.service.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ventureBean.dao.AdminLoginDAO;
import com.ventureBean.dao.UserRoleDAO;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.service.MailService;
import com.ventureBean.service.UserRoleService;
@Service(UserRoleServiceImpl.SERVICE_NAME)
public class UserRoleServiceImpl extends GenericServiceImpl<UserRoleMaster, Serializable> implements
		UserRoleService {

	/**
	 * Declared service name
	 */
	public static final String SERVICE_NAME = "userRoleService";
	
	/**
	 * Injected mailService.
	 */
	@Autowired
	private MailService mailService;
	
	/**
	 * @Injectd userDAO
	 */
	@Autowired
	private AdminLoginDAO adminLoginDAO;
	
	@Autowired
	private UserRoleDAO userRoleDAO;
	
	@Override
	public List<UserRoleMaster> getRoles(String userName) {
		log.debug("inside of method getRoles()");
		return userRoleDAO.getUserRoles(userName);
		
	}

	public List<UserRoleMaster> getRoles() {
		log.debug("inside of method getRoles()");
		return adminLoginDAO.getAllRoles();
		
	}

	/**
	 * This method will send mail to user on successful registration.
	 * @param userLogin
	 * @throws MailNotSentException
	 *//*
	private void sendMailOnSuccessFulCreation(MailFormData maildata) throws MailNotSentException {
		log.debug("Start of method sendMailForSuccessFulRegistration");
		String toMailAddress = maildata.getMailAddress();
		String fromMailAddress = mailResourceBundle.getString("mail.template.fromMailAddress");
		String templateName = mailResourceBundle.getString("mail.template.common");
		String mailSubject = mailResourceBundle.getString("mail.welcomeUser.subject");
		String mailBody = mailResourceBundle.getString("mail.welcomeUser.body");
		List<String> recipientMailIdList = new ArrayList<>();
		recipientMailIdList.add(toMailAddress);
		HashMap<String, String> requiredValues = new HashMap<>();
		requiredValues.put("name", maildata.getName());
		requiredValues.put("description", mailBody);
		requiredValues.put("userName", maildata.getMailAddress());
		requiredValues.put("password", maildata.getPassword());
		requiredValues.put("link", maildata.getLink());
		mailService.sendMailUsingVelocity(recipientMailIdList, requiredValues, templateName, fromMailAddress, mailSubject, null, null);
		log.debug("End of method sendMailForSuccessFulRegistration");
	}*/
}
