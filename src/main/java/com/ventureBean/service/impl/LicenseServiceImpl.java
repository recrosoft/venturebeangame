package com.ventureBean.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ventureBean.constants.Constants;
import com.ventureBean.dao.AdminLoginDAO;
import com.ventureBean.dao.BPCategoryMappingDAO;
import com.ventureBean.dao.GameDAO;
import com.ventureBean.dao.LicenseDAO;
import com.ventureBean.dto.BpGameMappingDTO;
import com.ventureBean.enums.LicenseStatusEnum;
import com.ventureBean.enums.LicenseTypeEnum;
import com.ventureBean.enums.TransactionStatusEnum;
import com.ventureBean.enums.TransactionTypeEnum;
import com.ventureBean.form.BPCategoryMappingForm;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.model.BPCategoryMapping;
import com.ventureBean.model.Category;
import com.ventureBean.model.Game;
import com.ventureBean.model.License;
import com.ventureBean.model.User;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.service.GameService;
import com.ventureBean.service.LicenseService;
import com.ventureBean.service.TransactionService;
import com.ventureBean.util.Format;

/**
 * @author Satyajit
 *
 */
@Service(LicenseServiceImpl.SERVICE_NAME)
public class LicenseServiceImpl extends GenericServiceImpl<String, Serializable>implements LicenseService  {

	/**
	 * Declared service name
	 */
	public static final String  SERVICE_NAME = "licenseService";
	
	/**
	 * @injected gameService
	 */
	@Autowired
	private GameService gameService;
	
	
	/**
	 * @injected loginService
	 */
	@Autowired
	private AdminLoginService loginService;
	
	/**
	 * @Injected licenseDAO
	 */
	@Autowired
	private LicenseDAO licenseDAO;
	
	/**
	 * @Injected transactionService
	 */
	@Autowired
	private TransactionService transactionService;
	
	/**
	 * @Injected gameDAO
	 */
	@Autowired
	private GameDAO gameDAO;
	
	/**
	 * @Injected userDAO
	 */
	@Autowired
	private AdminLoginDAO adminLoginDAO;
	
	@Autowired
	private BPCategoryMappingDAO bpCategoryMappingDAO;
	
	/* (non-Javadoc)
	 * @see com.ventureBean.service.LicenseService#generateLicense(com.ventureBean.model.User, com.ventureBean.model.Game, int)
	 
	@Override
	public void generateLicense(LicenseForm form) {
		log.debug("start of method generateLicense() ");
		License  license = null;
		for(int index=0; index<form.getNoOfLicense(); index++){
			license = new License();
			license.setLicenseCode(generateLicenseCode(form));
			license.setCreatedOn(new Date());
			license.setType(LicenseTypeEnum.PAID.toString());
			Game game = new Game();
			game.setGameId(form.getGameId());
			license.setGame(game);
			User bp = new User();
			bp.setUserId(form.getBusinessPartnerId());
			license.setBusinessPartner(bp);
			//setting playerId as null bcz this license is not assigned to any player till now.
			license.setPlayer(null);
			license.setStatus(LicenseStatusEnum.NEW.toString());
			Calendar calendar =  Calendar.getInstance();
			calendar.set(Constants.LICENSSE_EXPIRY_YEAR, Constants.LICENSE_EXPIRY_MONTH, Constants.LICENSE_EXPIRY_DAY);
			license.setExpiryDate(calendar.getTime());
			licenseDAO.save(license);
		}
		if(licenseType.equals(LicenseTypeEnum.FREE.toString())){
			gameService.createBusinessPartnerCategoryMap(bp, game, null ,Constants.ZERO ,licenseCount);
		}else if(licenseType.equals(LicenseTypeEnum.PAID.toString())){
			gameService.createBusinessPartnerCategoryMap(bp, game, null,licenseCount ,Constants.ZERO);
		}
		log.debug("end of method generateLicense() ");

	}*/

	/**@author Satyajit
	 * This method will generate  12 alpfanumeric licenseCode
	 * @return
	 *//*
	private String generateLicenseCode(LicenseForm form) {
		return UUID.randomUUID().toString().replaceAll("-", "").substring(0,12).toUpperCase();
		//return form.get
	}
*/
	/* (non-Javadoc)
	 * @see com.ventureBean.service.LicenseService#createLicenses(java.lang.Integer, java.lang.String, int, java.lang.Integer, java.lang.String)
	 */
	@Override
	public void createLicenses(Integer licenseCount, String licenseType, int gameId,
			Integer userId, String licenseStatus) {
		log.debug("start of method createLicense ");
		License  license = null;
		Game game = null;
		User bp = null;
		if(Format.isIntegerNotEmtyAndNotZero(licenseCount)){
			for(int index =0 ; index < licenseCount ; index++){
				license = new License();
				license.setCreatedOn(new Date());
				license.setType(licenseType);
				game = new Game();
				game.setGameId(gameId);
				license.setGame(game);
				bp = new User();
				bp.setUserId(userId);
				license.setBusinessPartner(bp);
				//setting playerId as null bcz this license is not assigned to any player till now.
				license.setPlayer(null);
				license.setStatus(licenseStatus);
				Calendar calendar =  Calendar.getInstance();
				calendar.set(Constants.LICENSSE_EXPIRY_YEAR, Constants.LICENSE_EXPIRY_MONTH, Constants.LICENSE_EXPIRY_DAY);
				license.setExpiryDate(calendar.getTime());
				licenseDAO.save(license);
			}
		}
		if(licenseType.equals(LicenseTypeEnum.FREE.toString())){
			gameService.createBusinessPartnerCategoryMap(bp, game, null ,Constants.ZERO ,licenseCount);
		}else if(licenseType.equals(LicenseTypeEnum.PAID.toString())){
			gameService.createBusinessPartnerCategoryMap(bp, game, null,licenseCount ,Constants.ZERO);
		}
		log.debug("end of method createLicense ");
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.LicenseService#addLicenses(com.ventureBean.form.LicenseForm)
	 */
	@Override
	public void addLicenses(LicenseForm licenseForm){
		log.debug("start of method addLicenses");
		Game game = gameDAO.getById(licenseForm.getGameId(), true);
		User businessPartner = adminLoginDAO.getById(licenseForm.getBusinessPartnerId(), true);
		TransactionForm form = new TransactionForm();
		form.setLicenseType(LicenseTypeEnum.PAID.toString());
		form.setFreeLicenses(Constants.ZERO);
		form.setPaidLicenses(licenseForm.getNoOfLicense());
		form.setTransactionType(TransactionTypeEnum.BUY.toString());
		form.setTransactionStatus(TransactionStatusEnum.PENDING.toString());
		if(Format.isStringNotEmptyAndNotNull(licenseForm.getAmount())){
			form.setAmount(Integer.valueOf(licenseForm.getAmount()));
		}
		form.setGame(game);
		form.setBusinessPartner(businessPartner);
		transactionService.recordTransaction(form , null);
		log.debug("end of method addLicenses");
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.LicenseService#getGameLicenseMapForBP(com.ventureBean.form.LicenseForm)
	 */
	@Override
	public List<BpGameMappingDTO> getGameLicenseMapForBP(LicenseForm licenseForm) {
		log.debug("start of method getGameLicenseMapForBP");
		List<BpGameMappingDTO> dtos = new ArrayList<>();
		List<Game> games = gameService.getAllGames();
		
		for(Game game : games){
			BpGameMappingDTO dto = new BpGameMappingDTO();
			LicenseForm lForm = new LicenseForm();
			lForm.setBusinessPartnerId(licenseForm.getBusinessPartnerId());
			lForm.setGameId(game.getGameId());
			List<License> licenses = licenseDAO.getLicense(lForm);
			int usedLicense =Constants.ZERO;
			int availableLicense =Constants.ZERO;
			for(License license :licenses){
				if(license.getStatus().equals(LicenseStatusEnum.NEW.toString())){
					availableLicense++;
				}else if(license.getStatus().equals(LicenseStatusEnum.USED.toString())){
					usedLicense++;
				}else if(license.getStatus().equals(LicenseStatusEnum.ACTIVE.toString())){
					usedLicense++;
				}
			}
			dto.setTotalLicense(licenses.size());
			dto.setAvailableLicense(availableLicense);
			dto.setUsedLicense(usedLicense);
			dto.setGameName(game.getGameName());
			dto.setGameId(game.getGameId());
			if(Format.isNotNull(licenseForm.getGameId())){
				if(game.getGameId() == licenseForm.getGameId()){
					availableLicense += licenseForm.getNoOfLicense();
				}
			}
			dto.setGameLicensemap(game.getGameName()+"("+availableLicense+")");
			dtos.add(dto);
		}
		log.debug("end of method getGameLicenseMapForBP");
		return  dtos;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.LicenseService#getPricePerLicense(com.ventureBean.form.LicenseForm)
	 */
	@Override
	public Double getPricePerLicense(LicenseForm licenseForm) {
		log.debug("start of method getPricePerLicense ");
		BPCategoryMapping mapping = null;
		Double pricePerLicense = null;
		BPCategoryMappingForm form = new BPCategoryMappingForm();
		form.setBpId(licenseForm.getBusinessPartnerId());
		form.setGameId(licenseForm.getGameId());
		List<BPCategoryMapping>  categoryMappings = bpCategoryMappingDAO.getMapping(form);
		if(Format.isCollectionNotEmtyAndNotNull(categoryMappings)){
			mapping = categoryMappings.get(Constants.GET_ZERO_INDEX_OBJECT);
			Category category = mapping.getCategory();
			pricePerLicense = category.getPricePerLicense();
		}
		log.debug("end of method getPricePerLicense ");
		return pricePerLicense;
	}

}
