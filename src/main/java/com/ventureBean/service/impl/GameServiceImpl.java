package com.ventureBean.service.impl;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ventureBean.constants.Constants;
import com.ventureBean.constants.CrisisGameConstants;
import com.ventureBean.dao.AppConfigurationDAO;
import com.ventureBean.dao.BPCategoryMappingDAO;
import com.ventureBean.dao.CategoryDAO;
import com.ventureBean.dao.GameDAO;
import com.ventureBean.dao.LicenseDAO;
import com.ventureBean.dao.ProjectDAO;
import com.ventureBean.dao.TransactionDAO;
import com.ventureBean.dao.UserLoginDAO;
import com.ventureBean.dao.crisisGame.CrisisGameStatDAO;
import com.ventureBean.dao.crisisGame.CrisisGameTeamDAO;
import com.ventureBean.dao.crisisGame.CrisisGameUserDAO;
import com.ventureBean.enums.CategoryType;
import com.ventureBean.enums.CrisisGameDayEnum;
import com.ventureBean.enums.CrisisGameDayStatusEnum;
import com.ventureBean.enums.LicenseStatusEnum;
import com.ventureBean.enums.ProjectStatusEnum;
import com.ventureBean.enums.ProjectUserEnum;
import com.ventureBean.exception.GameNotFoundException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.ProjectNotInCreatedStateException;
import com.ventureBean.exception.RoleNotAssignedException;
import com.ventureBean.exception.StartDateNotAchivedException;
import com.ventureBean.form.BPCategoryMappingForm;
import com.ventureBean.form.CategoryForm;
import com.ventureBean.form.GameForm;
import com.ventureBean.form.GameUserForm;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.form.UserForm;
import com.ventureBean.model.AppConfiguration;
import com.ventureBean.model.BPCategoryMapping;
import com.ventureBean.model.Category;
import com.ventureBean.model.Company;
import com.ventureBean.model.CrisisGameStatistics;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.model.CrisisGameUser;
import com.ventureBean.model.Game;
import com.ventureBean.model.License;
import com.ventureBean.model.Project;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.model.User;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.model.UserTransaction;
import com.ventureBean.service.GameService;
import com.ventureBean.service.MailService;
import com.ventureBean.util.FileUploadUtil;
import com.ventureBean.util.Format;
import com.ventureBean.util.StringGeneratorUtil;

@Service(GameServiceImpl.SERVICE_NAME)
public class GameServiceImpl extends GenericServiceImpl<Game, Serializable> implements
		GameService {
	
	/**
	 * @Delcared Md5PasswordEncoder
	 */
	Md5PasswordEncoder encoder = new Md5PasswordEncoder();
	/**
	 * Declared Service Name
	 */
	public static final String SERVICE_NAME="gameService";
	
	/**
	 * @injected gameDAO
	 */
	@Autowired
	private GameDAO gameDAO ;
	
	@Autowired
	private CategoryDAO categoryDAO;
	
	/**
	 * @Declared projectDAO
	 */
	@Autowired
	private ProjectDAO projectDAO;
	
	/**
	 * @Instantiated mailResourceBundle
	 */
	ResourceBundle mailResourceBundle = ResourceBundle.getBundle("mail");
	
	/**
	 * @Instantiated mailResourceBundle
	 */
	ResourceBundle resourceBundle = ResourceBundle.getBundle("ApplicationResources");
	
	/**
	 * @Injected mailService
	 */
	@Autowired
	private MailService mailService;
	
	/**
	 * @Declared appConfigurationDAO
	 */
	@Autowired
	private AppConfigurationDAO appConfigurationDAO;
	
	/**
	 * Declared projectUserDAO
	 */
	@Autowired
	private UserLoginDAO userLoginDAO;
	
	/**
	 * @Injected crisisGameTeamDAO
	 */
	@Autowired
	private CrisisGameTeamDAO crisisGameTeamDAO;
	
	/**
	 * @Injected crisisGameStatDAO
	 */
	@Autowired
	private CrisisGameStatDAO crisisGameStatDAO;
	

	/**
	 * @Injected crisisGameUserDAO
	 */
	@Autowired
	private CrisisGameUserDAO crisisGameUserDAO;
	
	
	/**
	 * @Injected TransactionDAO
	 */
	@Autowired
	private TransactionDAO transactionDAO;
	
	
	@Autowired
	private BPCategoryMappingDAO bpCategoryMappingDAO;
	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#getAllGames()
	 */
	
	@Autowired
	private LicenseDAO  licenseDAO;
	
	@Override
	public List<Game> getAllGames() {
		log.debug("start of method getAllGames() ");
		List<Game> games = gameDAO.getAll(new Game());
		log.debug("end of method getAllGames() ");
		return games;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#updateGame(com.ventureBean.form.GameForm)
	 */
	@Override
	public void updateGame(GameForm gameForm) throws GameNotFoundException  {
		log.debug("start of method updateGame");
		Game game = gameDAO.getById(gameForm.getGameId(), true);
		if(Format.isObjectNotEmptyAndNotNull(game)){
			game.setGameName(gameForm.getGameName());
			game.setDescription(gameForm.getGameDescription());
			if(Constants.ZERO < gameForm.getGameLogo().getSize()){
				//FileUploadUtil.deleteFile(gameForm.getLogoUrl());
				String logoUrl = FileUploadUtil.uploadFileToAWSS3(gameForm.getGameLogo());
				game.setLogo(logoUrl);
			}
			game.setModifiedOn(new Date());
			game.setModifiedBy(gameForm.getModifiedBy());
			gameDAO.update(game);
		}else{
			throw new GameNotFoundException();
		}
		log.debug("end of method updateGame");
		
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#updateGameCategory(com.ventureBean.form.CategoryForm)
	 */
	@Override
	public void updateGameCategory(CategoryForm categoryForm) {
		log.debug("start of method updateGameCategory ");
		List<Category> categories= gameDAO.getCategory(categoryForm);
		if(Format.isCollectionNotEmtyAndNotNull(categories)){
		 Category category = categories.get(Constants.GET_ZERO_INDEX_OBJECT);
		 category.setFreeLicense(categoryForm.getFreeLicense());
		 category.setPricePerLicense(categoryForm.getPricePerLicense());
		 category.setDescription(categoryForm.getDesc());
		 category.setThreshold(categoryForm.getThreshold());
		 categoryDAO.update(category);
		}
		log.debug("end of method updateGameCategory ");
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#createProject(com.ventureBean.form.ProjectForm)
	 */
	@Override
	public void createProject(ProjectForm projectForm) throws MailNotSentException, ProjectNotInCreatedStateException, ParseException {
		log.debug("start of method createProject ");
		Project project = new Project();
		Game game = new Game();
		game.setGameId(projectForm.getGameId());
		// setting this to empty as bcz this Field is Declared as NotNull in DB
		game.setGameName("");
		User bp = new User();
		bp.setUserId(projectForm.getBpId());
		project.setBusinessPartner(bp);
		Company  company = new Company();
		company.setCompanyId(projectForm.getCompanyId());
		project.setCompany(company);
		project.setGame(game);
		project.setFacilitatorEmail(projectForm.getFacilitatorEmail());
		project.setFacilitatorName(projectForm.getFacilitatorName());
		project.setHrEmail(projectForm.getHrEmail());
		project.setHrName(projectForm.getHrName());
		if(Format.isStringNotEmptyAndNotNull(projectForm.getStartDate())){
			project.setStartDate(Format.parseDateDDMMYYYYFormat(projectForm.getStartDate()));
		}
		project.setNoOfPlayers(projectForm.getNoOfPlayers());
		project.setProjectStatus(ProjectStatusEnum.CREATED.toString());
		project.setCreatedOn(new Date());
		project.setCreatedBy(projectForm.getCreatedBy());
		projectDAO.save(project);
		//update license for this game for the BP
		occupyLicenseForProject(projectForm);
		// create facilitator and save in Db && send the credential to given mail id
		initializeFacilitator(projectForm, project);
		log.debug("end of method createProject ");
	}

	private void occupyLicenseForProject(ProjectForm projectForm) {
		LicenseForm licenseForm = new LicenseForm();
		licenseForm.setBusinessPartnerId(projectForm.getBpId());
		licenseForm.setGameId(projectForm.getGameId());
		licenseForm.setLicenseStatus(LicenseStatusEnum.NEW.toString());
		List<License> licenses = licenseDAO.getLicense(licenseForm);
		if(Format.isCollectionNotEmtyAndNotNull(licenses)){
			for(int index=0 ; index<projectForm.getNoOfPlayers() ; index++){
				License license = licenses.get(index);
				license.setStatus(LicenseStatusEnum.ACTIVE.toString());
				license.setModifiedOn(new Date());
				license.setModifiedBy("");
				licenseDAO.update(license);
			}
		}
	}

	/**@author Satyajit
	 * This method will create facilitator and send credentials through mail
	 * @param projectForm
	 * @param project
	 * @throws MailNotSentException
	 */
	private void initializeFacilitator(ProjectForm projectForm, Project project)
			throws MailNotSentException {
		String username = userLoginDAO.generateUserName(StringGeneratorUtil.generateUserName(projectForm.getFacilitatorEmail()));
		ProjectUser facilitator = new ProjectUser();
		facilitator.setUserName(username);
		facilitator.setName(projectForm.getFacilitatorName());
		facilitator.setEmailAddress(projectForm.getFacilitatorEmail());
		String pass= StringGeneratorUtil.generatePassword(projectForm.getFacilitatorName(), projectForm.getFacilitatorEmail());
		facilitator.setPassword(encoder.encodePassword(pass, null));
		UserRoleMaster role= new UserRoleMaster();
		role.setUserRoleId(Constants.FACILITATOR_ROLE_ID);
		facilitator.setUserRole(role);
		facilitator.setUserType(ProjectUserEnum.FACILITATOR.toString());
		AppConfiguration appConfiguration = appConfigurationDAO
				.getAppConfigurationByName(resourceBundle.getString("appConfig.name.accessTokenValidity.facilitator"));
		facilitator.setAccValidity(Integer.parseInt(appConfiguration.getAppConfigurationValue()));
		facilitator.setProject(project);
		String accessToken = StringGeneratorUtil.generateString(Constants.PIN_MIN_LENGTH, Constants.PIN_MAX_LENGTH, Constants.PIN_NO_OF_CAPS_ALPHABETS, Constants.PIN_NO_OF_DIGITS, Constants.PIN_NO_OF_SPECIALCHAR);
		log.debug("access Token facilitator : "+ accessToken +"  password : "+ pass);
		facilitator.setAccessToken(accessToken);
		facilitator.setCreatedOn(new Date());
		facilitator.setCreatedBy(projectForm.getCreatedBy());
		userLoginDAO.save(facilitator);
		//send the credentials to facilitator
		sendMailToFacilitator(facilitator ,pass , accessToken);
		//send mail to HR
		sendMailToHR(projectForm);
	}

	/**
	 * @param projectForm
	 * @throws MailNotSentException
	 */
	private void sendMailToHR(ProjectForm projectForm)
			throws MailNotSentException {
		String fromMailAddress = mailResourceBundle.getString("mail.template.fromMailAddress");
		String mailBody = mailResourceBundle.getString("mail.welcomeUser.body");
		String templateName = mailResourceBundle.getString("mail.template.common");
		String mailSubject = mailResourceBundle.getString("mail.welcomeUser.subject");
		
		List<String> recipientMailIdList = new ArrayList<>();
		recipientMailIdList.add(projectForm.getHrEmail());
		HashMap<String, String> requiredValues = new HashMap<>();
		requiredValues.put("name", projectForm.getHrName());
		requiredValues.put("description",mailBody);
		mailService.sendMailUsingVelocity(recipientMailIdList, requiredValues, templateName, fromMailAddress, mailSubject,null, null);
	}
	
	/**@author Satyajit
	 * This method will send mail to the newly created Facilitator with the login credential
	 * @param projectForm
	 * @param pass
	 * @param accessToken
	 * @throws MailNotSentException
	 */
	private void sendMailToFacilitator(ProjectUser facilitator , String pass , String accessToken) throws MailNotSentException {
		log.debug("Start of method sendMailToFacilitator");
		String fromMailAddress = mailResourceBundle.getString("mail.template.fromMailAddress");
		String mailBody = mailResourceBundle.getString("mail.welcomeUser.body");
		String templateName = mailResourceBundle.getString("mail.template.loginLink");
		String mailSubject = mailResourceBundle.getString("mail.welcomeUser.subject");
		
		List<String> recipientMailIdList = new ArrayList<>();
		recipientMailIdList.add(facilitator.getEmailAddress());
		HashMap<String, String> requiredValues = new HashMap<>();
		requiredValues.put("name", facilitator.getName());
		requiredValues.put("description",mailBody);
		requiredValues.put("userName", facilitator.getUserName());
		requiredValues.put("password", pass);
		requiredValues.put("link",Constants.BASE_LINK+accessToken);
		mailService.sendMailUsingVelocity(recipientMailIdList, requiredValues, templateName, fromMailAddress, mailSubject,null, null);
		log.debug("End of method sendMailToFacilitator");
	}
	

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#clearProjectUserData(com.ventureBean.form.ProjectForm)
	 */
	@Override
	public void clearProjectUserData(ProjectForm projectForm) {
		log.debug("start of method clearProjectUserData ");
		// remove all Entry from  assigned GameUser for this Project
		GameUserForm form = new GameUserForm();
		form.setProjectId(projectForm.getProjectId());
		crisisGameUserDAO.removeUsers(form);
		// remove all Entry from ProjectUser for this Project.
		UserForm  userForm = new UserForm();
		userForm.setProjectId(projectForm.getProjectId());
		if(Format.isStringNotEmptyAndNotNull(projectForm.getUserType())){
			userForm.setUserType(projectForm.getUserType());
		}
		userLoginDAO.removeUsers(userForm);
		log.debug("start of method clearProjectUserData ");
	}

	/**
	 * @param projectForm
	 * @param project
	 */
	private void availAssignedLicense(ProjectForm projectForm, Project project) {
		int gameId = project.getGame().getGameId();
		LicenseForm licenseForm = new LicenseForm();
		licenseForm.setBusinessPartnerId(projectForm.getBpId());
		licenseForm.setGameId(gameId);
		licenseForm.setLicenseStatus(LicenseStatusEnum.ACTIVE.toString());
		List<License> licenses = licenseDAO.getLicense(licenseForm);
		if (Format.isCollectionNotEmtyAndNotNull(licenses)) {
			int licenseDiff = project.getNoOfPlayers() - projectForm.getNoOfPlayers();
			for (int index = 0; index < licenseDiff; index++) {
				License license = licenses.get(index);
				license.setStatus(LicenseStatusEnum.NEW.toString());
				license.setModifiedOn(new Date());
				license.setModifiedBy("");
				licenseDAO.update(license);
			}
		}
	}

	/**
	 * @param projectForm
	 * @param project
	 */
	private void assignExtraLicense(ProjectForm projectForm, Project project) {
		int oldGameId = project.getGame().getGameId();
		LicenseForm licenseForm = new LicenseForm();
		licenseForm.setBusinessPartnerId(projectForm.getBpId());
		licenseForm.setGameId(oldGameId);
		licenseForm.setLicenseStatus(LicenseStatusEnum.NEW.toString());
		List<License> licenses = licenseDAO.getLicense(licenseForm);
		int licenseDiff = projectForm.getNoOfPlayers() - project.getNoOfPlayers();
		if (Format.isCollectionNotEmtyAndNotNull(licenses)) {
				for (int index = 0; index < licenseDiff; index++) {
					License license = licenses.get(index);
					license.setStatus(LicenseStatusEnum.ACTIVE.toString());
					license.setModifiedOn(new Date());
					license.setModifiedBy("");
					licenseDAO.update(license);
				}
			}

	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#startProject(com.ventureBean.form.ProjectForm)
	 */
	@Override
	public Project startProject(ProjectForm form) throws StartDateNotAchivedException, ProjectNotInCreatedStateException, RoleNotAssignedException {
		log.debug("start of method startProject");
		CrisisGameStatistics statistics = null;
		CrisisGameUser  gameUser = null;
		Project  project = projectDAO.getById(form.getProjectId(), false);
		if(Format.isNotNull(project)){
			Date startDate = project.getStartDate();
			if(Format.isDateNotEmtyAndNotNull(startDate)){
				if(new Date().before(startDate)){
					throw new StartDateNotAchivedException();
				}
			}
			String status = project.getProjectStatus();
			if(!(status.equals(ProjectStatusEnum.CREATED.toString()))){
				throw new ProjectNotInCreatedStateException("may be in Cancelled , Active , Completed");
			}
			UserForm userForm = new UserForm();
			userForm.setProjectId(form.getProjectId());
			userForm.setUserType(ProjectUserEnum.PLAYER.toString());
			List<ProjectUser> projectUsers = userLoginDAO.getUser(userForm);
			if(Format.isNotNull(projectUsers)){
				if(projectUsers.size() < CrisisGameConstants.MIN_USER_PER_GAME ){
					throw new RoleNotAssignedException();
				}
			}
			project.setProjectStatus(ProjectStatusEnum.ACTIVE.toString());
			projectDAO.update(project);
			List<CrisisGameTeam> teams = crisisGameTeamDAO.getAll(new CrisisGameTeam());
			for(CrisisGameTeam team : teams){
				for(CrisisGameDayEnum day : CrisisGameDayEnum.values()){
					statistics = new CrisisGameStatistics();
					statistics.setProject(project);
					statistics.setDayNo(day);
					if(day.equals(CrisisGameDayEnum.DAY_01)){
						statistics.setStatus(CrisisGameDayStatusEnum.ACTIVE);
						statistics.setStartTime(new Date());
					}else{
						statistics.setStatus(CrisisGameDayStatusEnum.CREATED);
					}
					statistics.setTeam(team);
					GameUserForm gameuserForm = new GameUserForm();
					gameuserForm.setRoleName(CrisisGameConstants.ROLE_LEADER);
					gameuserForm.setTeamName(team.getTeamName());
					gameuserForm.setProjectId(project.getProjectId());
					List<CrisisGameUser> gameUsers = crisisGameUserDAO.getGameUser(gameuserForm);
					if(Format.isCollectionNotEmtyAndNotNull(gameUsers)){
						gameUser = gameUsers.get(Constants.GET_ZERO_INDEX_OBJECT);
						statistics.setGameUser(gameUser);
					}
					crisisGameStatDAO.save(statistics);
				}
			}
		}else{
			
		}
		log.debug("end of method startProject");
		return project;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#getassignedGameDetails(com.ventureBean.form.BPCategoryMappingForm)
	 */
	@Override
	public List<BPCategoryMapping> getassignedGameDetails(BPCategoryMappingForm mappingForm) {
		log.debug("start of method getassignedGameDetails");
		List<BPCategoryMapping> mappings= bpCategoryMappingDAO.getMapping(mappingForm);
		log.debug("end of method getassignedGameDetails");
		return mappings;
	}

	/**@author Satyajit
	 * @param bpId
	 * @param gameId
	 * @param categoryId
	 * @param paidLicense
	 * @param freeLicense
	 */
	@Override
	public void createBusinessPartnerCategoryMap(User bp, Game game, Category category, Integer paidLicense, Integer freeLicense) {
		log.debug("start of method createBusinessPartnerCategoryMap");
		BPCategoryMapping mapping = null;
		BPCategoryMappingForm form = new BPCategoryMappingForm();
		form.setBpId(bp.getUserId());
		form.setGameId(game.getGameId());
		List<BPCategoryMapping> categoryMappings = bpCategoryMappingDAO.getMapping(form);
		if(Format.isCollectionNotEmtyAndNotNull(categoryMappings)){
			mapping = categoryMappings.get(Constants.GET_ZERO_INDEX_OBJECT);
			mapping.setFreeLicense(mapping.getFreeLicense()+freeLicense);
			mapping.setPaidLicense(mapping.getPaidLicense()+paidLicense);
			if(Format.isNull(category)){
				category = mapping.getCategory();
			}
			Integer threshold = category.getThreshold();
			String catName ="";
			
			
			TransactionForm transactionForm=new TransactionForm();
			
			transactionForm.setGame(game);
			transactionForm.setPaidLicenses(paidLicense);
			transactionForm.setTransactionType("PAID");
			transactionForm.setTransactionStatus("APPROVED");
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.MONTH, 01);
			calendar.set(Calendar.DATE, 01);
			Date starDate = calendar.getTime();
			calendar.set(Calendar.MONTH, 12);
			calendar.set(Calendar.DATE, 01);
			Date endDate = calendar.getTime();
			
			transactionForm.setStartDate(starDate);
			transactionForm.setEndDate(endDate);
			List<UserTransaction>  transactions = transactionDAO.getTransactions(transactionForm);
			int paidLicenses=0;
			for(UserTransaction userTransaction:transactions)
			{
				paidLicenses=paidLicenses+userTransaction.getPaidLicenses();
				
			}
	
			if(threshold <= paidLicenses){
				
				upgradeCategory(game, category, mapping, catName);
			}
			/*if(threshold <= mapping.getPaidLicense()){
				upgradeCategory(game, category, mapping, catName);
			}*/
			bpCategoryMappingDAO.save(mapping);
		}else{
			mapping = new BPCategoryMapping();
			mapping.setBusinessPartner(bp);
			mapping.setCategory(category);
			mapping.setFreeLicense(freeLicense);
			mapping.setPaidLicense(paidLicense);
			mapping.setGame(game);
			bpCategoryMappingDAO.save(mapping);
		}
		log.debug("end of method createBusinessPartnerCategoryMap");
		
	}

	/**
	 * @param game
	 * @param category
	 * @param mapping
	 * @param catName
	 */
	private void upgradeCategory(Game game, Category category,
			BPCategoryMapping mapping, String catName) {
		if(category.getCategoryName().equals(CategoryType.SILVER.toString())){
			catName = CategoryType.SILVER.toString();
		}else if(category.getCategoryName().equals(CategoryType.GOLD.toString())){
			catName = CategoryType.GOLD.toString();
		}
			else if(category.getCategoryName().equals(CategoryType.PLATINUM.toString())){
				catName = CategoryType.PLATINUM.toString();
		}
		CategoryForm categoryForm = new CategoryForm();
		categoryForm.setCategoryName(catName);
		categoryForm.setGameId(game.getGameId());
		List<Category> categories = gameDAO.getCategory(categoryForm);
		if(Format.isCollectionNotEmtyAndNotNull(categories)){
			category = categories.get(Constants.GET_ZERO_INDEX_OBJECT);
		}
		mapping.setCategory(category);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#cancelProject(com.ventureBean.form.ProjectForm)
	 */
	@Override
	public void cancelProject(ProjectForm projectForm) {
		log.debug(" start of method cancelPeoject");
		Project project = projectDAO.getById(projectForm.getProjectId(), true);
		if(Format.isNotNull(project)){
			project.setProjectStatus(ProjectStatusEnum.CANCELLED.toString());
			projectDAO.update(project);
			// cancel all assigned licenses
			LicenseForm licenseForm = new LicenseForm();
			licenseForm.setBusinessPartnerId(projectForm.getBpId());
			licenseForm.setGameId(projectForm.getGameId());
			licenseForm.setLicenseStatus(LicenseStatusEnum.ACTIVE.toString());
			List<License> licenses = licenseDAO.getLicense(licenseForm);
			int licenseCount = project.getNoOfPlayers();
			if(Format.isCollectionNotEmtyAndNotNull(licenses)){
				for(int index=0 ; index < licenseCount ; index++){
					License license = licenses.get(index);
					license.setStatus(LicenseStatusEnum.NEW.toString());
					license.setModifiedOn(new Date());
					license.setModifiedBy("");
					licenseDAO.update(license);
				}
			}
			projectForm = new ProjectForm();
			projectForm.setProjectId(project.getProjectId());
			clearProjectUserData(projectForm);
		}else{
			
		}
		log.debug(" end of method cancelPeoject");
		
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#editProject(com.ventureBean.form.ProjectForm)
	 */
	@Override
	public Project editProject(ProjectForm projectForm) throws ParseException, ProjectNotInCreatedStateException, MailNotSentException {
		log.debug("start of method editProject");
		Project project = null;
		project =  projectDAO.getById(projectForm.getProjectId(), true);
		if(Format.isNotNull(project)){
			if(project.getProjectStatus().equals(ProjectStatusEnum.CREATED.toString())){
				//update license for this game for this BP
				if(projectForm.getGameId().equals(project.getGame().getGameId())){
					if(project.getNoOfPlayers() > projectForm.getNoOfPlayers()){
						availAssignedLicense(projectForm, project);
					}else if(project.getNoOfPlayers() < projectForm.getNoOfPlayers()){
						assignExtraLicense(projectForm, project);
					}
				}else{
					updateLicense(projectForm , project);
				}
				Game game = new Game();
				game.setGameId(projectForm.getGameId());
				// setting this to empty as bcz this Field is Declared as NotNull in DB
				game.setGameName("");
				User bp = new User();
				bp.setUserId(projectForm.getBpId());
				project.setBusinessPartner(bp);
				Company  company = new Company();
				company.setCompanyId(projectForm.getCompanyId());
				project.setCompany(company);
				project.setGame(game);
				project.setFacilitatorEmail(projectForm.getFacilitatorEmail());
				project.setFacilitatorName(projectForm.getFacilitatorName());
				project.setHrEmail(projectForm.getHrEmail());
				project.setHrName(projectForm.getHrName());
				if(Format.isStringNotEmptyAndNotNull(projectForm.getStartDate())){
					project.setStartDate(Format.parseDateDDMMYYYYFormat(projectForm.getStartDate()));
				}
				project.setProjectStatus(ProjectStatusEnum.CREATED.toString());
				project.setCreatedOn(new Date());
				project.setCreatedBy(projectForm.getCreatedBy());
				
				project.setNoOfPlayers(projectForm.getNoOfPlayers());
				projectDAO.update(project);
				// now reset game users as the game is edited.
				ProjectForm pForm = new ProjectForm();
				pForm.setProjectId(project.getProjectId());
				clearProjectUserData(pForm);
			
			}else{
				throw new ProjectNotInCreatedStateException();
			}
		}
		initializeFacilitator(projectForm, project);
		
		log.debug("end of method editProject");
		return  project;
	}

	private void updateLicense(ProjectForm projectForm, Project project) {
		log.debug("start of method updateLicense ");
		Integer gameId = null;
		List<License> licenses = null;
		if(Format.isNotNull(project.getGame())){
			gameId = project.getGame().getGameId();
			LicenseForm licenseForm = new LicenseForm();
			licenseForm.setGameId(gameId);
			licenseForm.setBusinessPartnerId(projectForm.getBpId());
			licenseForm.setLicenseStatus(LicenseStatusEnum.ACTIVE.toString());
			licenses = licenseDAO.getLicense(licenseForm);
			if(Format.isCollectionNotEmtyAndNotNull(licenses)){
				for(int index=0 ; index <project.getNoOfPlayers() ; index++){
					License license = licenses.get(index);
					license.setStatus(LicenseStatusEnum.NEW.toString());
					license.setModifiedOn(new Date());
					license.setModifiedBy(projectForm.getCreatedBy());
					licenseDAO.update(license);
				}
			}
			licenseForm = new LicenseForm();
			licenseForm.setGameId(projectForm.getGameId());
			licenseForm.setBusinessPartnerId(projectForm.getBpId());
			licenseForm.setLicenseStatus(LicenseStatusEnum.NEW.toString());
			licenses = licenseDAO.getLicense(licenseForm);
			if(Format.isCollectionNotEmtyAndNotNull(licenses)){
				for(int index= 0; index < projectForm.getNoOfPlayers();index++){
					License license = licenses.get(index);
					license.setStatus(LicenseStatusEnum.ACTIVE.toString());
					license.setModifiedOn(new Date());
					license.setModifiedBy(projectForm.getCreatedBy());
					licenseDAO.update(license);
				}
			}
		}
		log.debug("end of method updateLicense ");
		
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.GameService#getProject(com.ventureBean.form.ProjectForm)
	 */
	@Override
	public Project getProject(ProjectForm projectForm) {
		log.debug("start of method getProject");
		Project project =  projectDAO.getById(projectForm.getProjectId(), true);
		log.debug("end of method getProject");
		return  project;
	}

}
