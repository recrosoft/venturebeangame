/**
 * 
 */
package com.ventureBean.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;

import com.ventureBean.constants.Constants;
import com.ventureBean.dao.AdminLoginDAO;
import com.ventureBean.dao.BPCategoryMappingDAO;
import com.ventureBean.dao.GameDAO;
import com.ventureBean.dao.TransactionDAO;
import com.ventureBean.dao.UserRoleDAO;
import com.ventureBean.enums.CategoryType;
import com.ventureBean.enums.LicenseStatusEnum;
import com.ventureBean.enums.LicenseTypeEnum;
import com.ventureBean.enums.TransactionStatusEnum;
import com.ventureBean.enums.TransactionTypeEnum;
import com.ventureBean.enums.UserStatusEnum;
import com.ventureBean.exception.EmailAddressAlreadyRegisteredException;
import com.ventureBean.exception.GameNotFoundException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.exception.UserBadCredentialsException;
import com.ventureBean.form.AdminForm;
import com.ventureBean.form.CategoryForm;
import com.ventureBean.form.ChangePasswordForm;
import com.ventureBean.form.MailFormData;
import com.ventureBean.form.RoleForm;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.form.UserSearchForm;
import com.ventureBean.model.Category;
import com.ventureBean.model.Game;
import com.ventureBean.model.User;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.service.GameService;
import com.ventureBean.service.LicenseService;
import com.ventureBean.service.MailService;
import com.ventureBean.service.TransactionService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;
import com.ventureBean.util.StringGeneratorUtil;

/**
 * @author Satyajit
 *
 */
@Service(AdminLoginServiceImpl.SERVICE_NAME)
public class AdminLoginServiceImpl extends GenericServiceImpl<User, Serializable> implements AdminLoginService{
	
	private Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
	
	/**
	 * Declared Service name.
	 */
	public static final String SERVICE_NAME = "adminLoginService";
	
	/**
	 * @Injected userRoleDAO
	 */
	@Autowired
	private UserRoleDAO userRoleDAO;

	/**
	 * Injected adminDAO
	 */
	@Autowired
	private AdminLoginDAO adminLoginDAO;
	
	/**
	 * @Injected licenseService
	 */
	@Autowired
	private LicenseService licenseService;
	
	/**
	 * Injected  gameDAO
	 */
	@Autowired
	private GameDAO gameDAO;
	
	/**
	 * Instantiated resourceBundle.
	 */
	private ResourceBundle mailResourceBundle = ResourceBundle.getBundle("mail");
	
	/**
	 * Injected mailService.
	 */
	@Autowired
	private MailService mailService;
	
	/**
	 * @Injected gameService
	 */
	@Autowired
	private GameService gameService;
	
	/**
	 * @Injected gameService
	 */
	@Autowired
	private TransactionDAO transactionDAO;
	
	/**
	 * @Injected bpCategoryMappingDAO
	 */ 
	@Autowired
	private BPCategoryMappingDAO bpCategoryMappingDAO;
	
	/**
	 * @Injected transactionService
	 */
	@Autowired
	private TransactionService transactionService;
	
	/* (non-Javadoc)
	 * @see com.ventureBean.service.UserLoginService#createRole(com.ventureBean.form.RoleForm)
	 */
	@Override
	public String createRole(RoleForm roleForm) throws EmailAddressAlreadyRegisteredException , MailNotSentException , GameNotFoundException, NoRecordsFoundException {
		log.debug("start of method createRole(RoleForm)");
		UserRoleMaster userRole  = null;
			//check whether an user exists with given email id.
			AdminForm form = new AdminForm();
			form.setEmailAddress(roleForm.getEmail());
			if((adminLoginDAO.getUser(form)).isEmpty()){
				User user = new User();
				user.setEmailAddress(roleForm.getEmail());
				String password = StringGeneratorUtil.generatePassword(roleForm.getName(), roleForm.getPhone());
				log.debug("password Genetarted : "+ password);
				user.setPassword(passwordEncoder.encodePassword(password, null));
				user.setName(roleForm.getName());
				user.setPhone(roleForm.getPhone());
				user.setStatus(UserStatusEnum.Active.toString());
				user.setAssignTo(roleForm.getAssignTo());
				user.setCreatedBy(roleForm.getCreatedBy());
				user.setCreatedOn(new Date());
				// get the  Role with  userRoleId given and then assign the Role to this User
				userRole = userRoleDAO.getById(roleForm.getUserRoleId(), true);
				user.setUserRole(userRole);
				if(userRole.getRoleName().equals(Constants.BP_ROLE)){
					user.setWallet(Constants.WALLET_INIT_VALUE);
				}
				try {
					adminLoginDAO.save(user);
					MailFormData mailData = new  MailFormData();
					
					if(Format.isStringNotEmptyAndNotNull(userRole.getRoleName()) && userRole.getRoleName().equals(Constants.BP_ROLE)){
						initializeBPDetails(user , roleForm);
						//mailData.setCcList(ccList);
					}
					mailData.setMailAddress(roleForm.getEmail());
					mailData.setName(roleForm.getName());
					mailData.setPassword(password);
					mailData.setLink(Constants.BASE_LINK_ADMIN);
					String mailSubject = mailResourceBundle.getString("mail.welcomeUser.subject");
					String templateName = mailResourceBundle.getString("mail.template.loginLink");
					String mailBody = mailResourceBundle.getString("mail.welcomeUser.body");
					mailData.setDesc(mailBody);
					mailData.setTemplateName(templateName);
					mailData.setSubject(mailSubject);
					mailData.setCcList(roleForm.getAdminEmail());
					sendMailToUser(mailData);
				} catch (Exception e) {
					log.debug("Exception Occured : ",e);
				}
				
			}else{
				throw new EmailAddressAlreadyRegisteredException();
			}
		log.debug("end of method createRole(RoleForm)");
		return userRole.getRoleName();
	}

	/**@author Satyajit
	 * This will generate the noOfLicense 
	 * @param user
	 * @param userRole
	 * @throws GameNotFoundException
	 * @throws NoRecordsFoundException 
	 */
	private void initializeBPDetails(User user , RoleForm roleForm) throws GameNotFoundException, NoRecordsFoundException {
		log.debug("start of method initializeBPDetails() ");
		TransactionForm transactionForm =null;
		Integer freeLicense = null;
		List<Game> games = gameService.getAllGames();
			if(Format.isCollectionNotEmtyAndNotNull(games)){
				for(Game game : games){
					CategoryForm categoryForm = new CategoryForm();
					categoryForm.setGameId(game.getGameId());
					categoryForm.setCategoryName(roleForm.getCategoryName());
					List<Category> categories = gameDAO.getCategory(categoryForm);
					if(Format.isCollectionNotEmtyAndNotNull(categories)){
						Category category =categories.get(Constants.GET_ZERO_INDEX_OBJECT);
						freeLicense = category.getFreeLicense();
					    transactionForm = new TransactionForm();
						transactionForm.setBusinessPartner(user);
						transactionForm.setGame(game);
						transactionForm.setFreeLicenses(category.getFreeLicense());
						transactionForm.setPaidLicenses(Constants.ZERO);
						transactionForm.setAmount(Constants.ZERO);
						transactionForm.setLicenseType(LicenseTypeEnum.FREE.toString());
						transactionForm.setTransactionType(TransactionTypeEnum.BUY.toString());
						transactionForm.setCreatedOn(new Date());
						//If category is gold and platinum 
						if(category.getCategoryName().equals(CategoryType.GOLD.toString()) ||
							category.getCategoryName().equals(CategoryType.PLATINUM.toString())){
							transactionForm.setTransactionStatus(TransactionStatusEnum.PENDING.toString());
							transactionService.recordTransaction(transactionForm ,category);
						}else{
							transactionForm.setTransactionStatus(TransactionStatusEnum.APPROVED.toString());
							transactionService.recordTransaction(transactionForm , category);
							gameService.createBusinessPartnerCategoryMap(user, game, category, Constants.ZERO, category.getFreeLicense());
							licenseService.createLicenses(freeLicense , LicenseTypeEnum.FREE.toString(), game.getGameId(), user.getUserId(), LicenseStatusEnum.NEW.toString() );
							
						}
							
				//if extra license is given by admin , then send it for approval from admin
				if(Format.isIntegerNotEmtyAndNotZero(roleForm.getExtraLicense())){
							transactionForm.setBusinessPartner(user);
							transactionForm.setGame(game);
							transactionForm.setFreeLicenses(roleForm.getExtraLicense());
							transactionForm.setPaidLicenses(Constants.ZERO);
							transactionForm.setAmount(Constants.ZERO);
							transactionForm.setLicenseType(LicenseTypeEnum.FREE.toString());
							transactionForm.setTransactionType(TransactionTypeEnum.BUY.toString());
							transactionForm.setTransactionStatus(TransactionStatusEnum.PENDING.toString());
							transactionForm.setCreatedOn(new Date());
							transactionService.recordTransaction(transactionForm ,null); 
							System.out.println("********"+categoryForm.getCategoryName());
						}
					}
					
				}
			}else{
				throw new GameNotFoundException();
			}
		log.debug("end of method initializeBPDetails() ");
	}

	

	/**
	 * This method will send mail to user on successful registration.
	 * @param maildata
	 * @throws MailNotSentException
	 */
	private void sendMailToUser(MailFormData maildata) throws MailNotSentException {
		log.debug("Start of method sendMailToUser");
		String fromMailAddress = mailResourceBundle.getString("mail.template.fromMailAddress");
		List<String> recipientMailIdList = new ArrayList<>();
		recipientMailIdList.add(maildata.getMailAddress());
		HashMap<String, String> requiredValues = new HashMap<>();
		requiredValues.put("name", maildata.getName());
		requiredValues.put("description", maildata.getDesc());
		requiredValues.put("userName", maildata.getMailAddress());
		requiredValues.put("password", maildata.getPassword());
		requiredValues.put("link", maildata.getLink());
		mailService.sendMailUsingVelocity(recipientMailIdList, requiredValues, maildata.getTemplateName(), fromMailAddress, maildata.getSubject(), null, null);
		log.debug("End of method sendMailToUser");
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#validateUser(com.ventureBean.form.AdminForm)
	 */
	@Override
	public boolean validateUser(AdminForm adminForm) {
		log.debug("start of method validateUser()");
		List<User> users= adminLoginDAO.getUser(adminForm);
		if(null != users){
			return true;
		}
		log.debug("start of method validateUser()");
		return false;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#getUser(com.ventureBean.form.AdminForm)
	 */
	@Override
	public User getUser(AdminForm adminForm) {
		log.debug("start of method getUser()");
		User user= null;
		List<User> users= adminLoginDAO.getUser(adminForm);
		if(Format.isCollectionNotEmtyAndNotNull(users)){
			user = users.get(Constants.GET_ZERO_INDEX_OBJECT);
		}
		log.debug("start of method getUser()");
		return user;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#getAllLicensor()
	 */
	@Override
	public List<User> getAllLicensor() {
		log.debug("start of method getAllLicensor()");
		List<User> licensors = adminLoginDAO.getAllLicensor();
		log.debug("end of method getAllLicensor()");
		return licensors;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#getAdminList(com.ventureBean.form.AdminForm)
	 */
	@Override
	public List<User> getAdminList(AdminForm adminForm) {
		log.debug("start of method getAdminList()");
		List<User> adminList = adminLoginDAO.getUser(adminForm);
		log.debug("end of method getAdminList()");
		return adminList;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#getUsersBasedOnRole(com.ventureBean.form.AdminForm)
	 */
	@Override
	public List<User> getUsersBasedOnRole(AdminForm adminForm) {
		log.debug("start of method getUsersBasedOnRole()");
		adminForm.setRoleName(ConverterUtil.getDBSpecRoleName(adminForm.getRoleName()));
		List<User> adminList = adminLoginDAO.getUser(adminForm);
		log.debug("end of method getUsersBasedOnRole()");
		return adminList;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#getUsersForNonVBAdmin(com.ventureBean.form.UserSearchForm)
	 */
	@Override
	public List<User> getUsersForNonVBAdmin(UserSearchForm searchForm) {
		log.debug("end of method getUsersForNonVBAdmin()");
		List<User> userList = null;
		String currentuserRole= searchForm.getCurrentUserRole();
		searchForm.setSelectedRole(ConverterUtil.getDBSpecRoleName(searchForm.getSelectedRole()));
		
		if(Format.isStringNotEmptyAndNotNull(currentuserRole)){
			switch(currentuserRole){
			
			case Constants.BH_ROLE :switch(searchForm.getSelectedRole())
										{
											case Constants.BM_ROLE : userList = adminLoginDAO.getUserWithOneSelect(searchForm); break;
											
											case Constants.BDM_ROLE : userList = adminLoginDAO.getUserWithTwoSelect(searchForm); break;
											
											case Constants.BP_ROLE : userList = adminLoginDAO.getUserWithThreeSelect(searchForm); break;
										} 
									break;
			case Constants.BM_ROLE : switch(searchForm.getSelectedRole())
									    {
											case Constants.BDM_ROLE : userList = adminLoginDAO.getUserWithOneSelect(searchForm); break;
											
											case Constants.BP_ROLE : userList = adminLoginDAO.getUserWithTwoSelect(searchForm); break;
										} 
									break;
			case Constants.BDM_ROLE : switch(searchForm.getSelectedRole())
									    {
											case Constants.BP_ROLE : userList = adminLoginDAO.getUserWithOneSelect(searchForm); break;
										} 
									 break;
			}
		}
		log.debug("end of method getUsersForNonVBAdmin()");
		return userList;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#getUserRoleByUserId(java.lang.Integer)
	 */
	@Override
	public String getUserRoleByUserId(Integer id) {
		log.debug("start of method getUserRoleByUserId( )");
		Object roleObj = null;
		AdminForm form =new  AdminForm();
		form.setFieldsToSelect("userRole.roleName");
		form.setSuperUserId(id);
		List<User> user = adminLoginDAO.getUser(form);
		if(Format.isCollectionNotEmtyAndNotNull(user)){
			 roleObj =(Object)user.get(Constants.GET_ZERO_INDEX_OBJECT);
		}
		log.debug("end of method getUserRoleByUserId( )");
		return (String)roleObj;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#changePassword(com.ventureBean.form.ChangePasswordForm)
	 */
	@Override
	public void changePassword(ChangePasswordForm form) throws NoRecordsFoundException {
		log.debug("start of method changePassword");
		String current = form.getCurrentPassword();
		String newPass = form.getNewPassword();
		String confirmPass = form.getConfirmPassword();
		
		if(Format.isStringNotEmptyAndNotNull(current) && Format.isStringNotEmptyAndNotNull(newPass) && Format.isStringNotEmptyAndNotNull(confirmPass)){
			if(current.equals(newPass)){
				throw new UserBadCredentialsException("current password and new password should not be same");
			}else if(!newPass.equals(confirmPass)){
				throw new UserBadCredentialsException("new password and confirm password should be same");
			}
		}else{
			throw new UserBadCredentialsException("Invalid data");
		}
		AdminForm uform =new  AdminForm();
		uform.setPassword(passwordEncoder.encodePassword(form.getCurrentPassword(), null));
		uform.setEmailAddress(form.getUserName());
		List<User> users = adminLoginDAO.getUser(uform);
		if(Format.isCollectionNotEmtyAndNotNull(users)){
			User user =users.get(Constants.GET_ZERO_INDEX_OBJECT);
			user.setPassword(passwordEncoder.encodePassword(form.getNewPassword(), null));
			adminLoginDAO.update(user);
		}else{
			throw new NoRecordsFoundException();
		}
		log.debug("end of method changePassword");
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.service.AdminLoginService#forgotPassword(com.ventureBean.form.ChangePasswordForm)
	 */
	@Override
	public void forgotPassword(ChangePasswordForm form) throws NoRecordsFoundException, MailNotSentException {
		log.debug("start of method forgotPassword");
		User user = null;
		AdminForm adminForm  = new AdminForm();
		adminForm.setEmailAddress(form.getUserName());
		List<User> users= adminLoginDAO.getUser(adminForm);
		if(Format.isCollectionNotEmtyAndNotNull(users)){
			user = users.get(Constants.GET_ZERO_INDEX_OBJECT);
			String password = StringGeneratorUtil.generatePassword(user.getName(), user.getPhone());
			log.debug("new password Genetarted : "+ password);
			user.setPassword(passwordEncoder.encodePassword(password, null));
			adminLoginDAO.update(user);
			MailFormData mailData = new MailFormData();
			mailData.setMailAddress(user.getEmailAddress());
			mailData.setName(user.getName());
			mailData.setPassword(password);
			String mailSubject = mailResourceBundle.getString("mail.welcomeUser.subject");
			String templateName = mailResourceBundle.getString("mail.template.forgotpassword");
			String mailBody = mailResourceBundle.getString("mail.forgotpassword.doby");
			mailData.setDesc(mailBody);
			mailData.setTemplateName(templateName);
			mailData.setSubject(mailSubject);
			sendMailToUser(mailData);
		}else{
			throw new NoRecordsFoundException();
		}
		log.debug("end of method forgotPassword");
		
	}	

}
