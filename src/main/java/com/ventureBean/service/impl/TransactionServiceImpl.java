package com.ventureBean.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ventureBean.constants.Constants;
import com.ventureBean.dao.TransactionDAO;
import com.ventureBean.enums.LicenseStatusEnum;
import com.ventureBean.enums.LicenseTypeEnum;
import com.ventureBean.enums.TransactionStatusEnum;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.model.Category;
import com.ventureBean.model.UserTransaction;
import com.ventureBean.service.LicenseService;
import com.ventureBean.service.TransactionService;
import com.ventureBean.util.Format;

@Service(TransactionServiceImpl.SERVICE_NAME)
public class TransactionServiceImpl extends GenericServiceImpl<UserTransaction, String> implements
		TransactionService {

	/**
	 * @Declared transactionService
	 */
	public static final String SERVICE_NAME = "transactionService";

	/**
	 * @Injected transactionDAO
	 */
	@Autowired
	private TransactionDAO transactionDAO ;
	
	/**
	 * @Injected  licenseService
	 */
	@Autowired
	LicenseService  licenseService;
	/* (non-Javadoc)
	 * @see com.ventureBean.service.TransactionService#recordTransaction(com.ventureBean.form.TransactionForm)
	 */
	@Override
	public void recordTransaction(TransactionForm transactionForm , Category category)  {
		log.debug("start of method recordTransaction( )");
		UserTransaction transaction = null;
		if(Format.isIntegerNotEmtyAndNotZero(transactionForm.getTransactionId())){
		    transaction = transactionDAO.getById(transactionForm.getTransactionId().toString(), true);
		    if(Format.isObjectNotEmptyAndNotNull(transaction)){
		    	transaction.setTransactionStatus(transactionForm.getTransactionStatus());
				transaction.setModifiedOn(new Date());
				transaction.setModifiedBy(transactionForm.getModifiedBy());
				transactionDAO.saveOrUpdate(transaction);
		    }
		}else{
			// create a new Trx and save
			transaction = new UserTransaction();
			transaction.setBusinessPartner(transactionForm.getBusinessPartner());
			transaction.setLicenseType(transactionForm.getLicenseType());
			transaction.setTransactionType(transactionForm.getTransactionType());
			transaction.setGame(transactionForm.getGame());
			transaction.setFreeLicenses(transactionForm.getFreeLicenses());
			transaction.setPaidLicenses(transactionForm.getPaidLicenses());
			transaction.setTransactionStatus(transactionForm.getTransactionStatus());
			transaction.setAmount(transactionForm.getAmount());
			transaction.setCreatedOn(new Date());
			transaction.setCreatedby(transactionForm.getCreatedby());
			if(Format.isNotNull(category)){
				transaction.setCategory(category);
			}
			transactionDAO.save(transaction);
		}
		log.debug("end of method recordTransaction( )");
	}
	/* (non-Javadoc)
	 * @see com.ventureBean.service.TransactionService#getPendingTrnxList(com.ventureBean.form.TransactionForm)
	 */
	@Override
	public List<UserTransaction> getPendingTrnxList(
			TransactionForm transactionForm) {
		log.debug("start of method getPendingTrnxList( )");
		List<UserTransaction> list = transactionDAO.getTransactions(transactionForm);
		log.debug("end of method getPendingTrnxList( )");
		return list;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.service.TransactionService#approveRejectTrnx(com.ventureBean.form.TransactionForm, java.lang.String)
	 */
	@Override
	public void approveRejectTrnx(TransactionForm transactionForm  , String trnxActionType)
			throws NoRecordsFoundException {
		log.debug("Start of method approveRejectTrnx");
		transactionForm.setTransactionStatus(TransactionStatusEnum.PENDING.toString());
		List<UserTransaction> transactions = transactionDAO.getTransactions(transactionForm);
	    if(Format.isObjectNotEmptyAndNotNull(transactions)){
	    	UserTransaction transaction = transactions.get(Constants.GET_ZERO_INDEX_OBJECT);
	    	if(trnxActionType.equals(Constants.TRNX_APPROVE)){
	    		int noOfLicense  = transaction.getLicenseType().equals(LicenseTypeEnum.PAID.toString()) ? transaction.getPaidLicenses() : transaction.getFreeLicenses();
	    		licenseService.createLicenses(noOfLicense, transaction.getLicenseType(), transaction.getGame().getGameId(), transaction.getBusinessPartner().getUserId(), LicenseStatusEnum.NEW.toString());
	    		transaction.setTransactionStatus(TransactionStatusEnum.APPROVED.toString());
	    	}else{
	    		transaction.setTransactionStatus(TransactionStatusEnum.REJECTED.toString());
	    	}
			transaction.setModifiedOn(new Date());
			transaction.setModifiedBy(transactionForm.getModifiedBy());
			transactionDAO.update(transaction);
	    }else{
	    	throw  new NoRecordsFoundException();
	    }
		log.debug("end of method approveRejectTrnx");
	}
	
}
