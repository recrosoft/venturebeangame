package com.ventureBean.form;

import java.util.List;

public class UserListForm {

	private Integer projectId;
	
	private String emailSuffix;
	
	List<UserForm> userForm;

	private String createdBy;
	
	public List<UserForm> getUserForm() {
		return userForm;
	}

	public void setUserForm(List<UserForm> userForm) {
		this.userForm = userForm;
	}

	public String getEmailSuffix() {
		return emailSuffix;
	}

	public void setEmailSuffix(String emailSuffix) {
		this.emailSuffix = emailSuffix;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
}
