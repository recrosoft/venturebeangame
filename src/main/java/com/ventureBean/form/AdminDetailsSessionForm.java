package com.ventureBean.form;

public class AdminDetailsSessionForm {

	private String userName;
	
	private String password;
	
	private String userRole;
	
	private Integer userId;
	
	private String name;

	private String urlSpecRoleName ;
	
	/**
	 * This is used for getting the players list for facilitator
	 */
	private Integer projectId;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserRole() {
		return userRole;
	}

	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUrlSpecRoleName() {
		return urlSpecRoleName;
	}

	public void setUrlSpecRoleName(String urlSpecRoleName) {
		this.urlSpecRoleName = urlSpecRoleName;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
