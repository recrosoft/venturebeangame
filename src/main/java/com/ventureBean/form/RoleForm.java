package com.ventureBean.form;

public class RoleForm {

	private Integer userRoleId ;
	
	private String name ;
	
	private String email ;
	
	private Integer assignTo ;
	
	private String  phone ;

	private String  categoryName;
	
	private Integer extraLicense ;

	private String createdBy;
	
	private String adminEmail;
	
	public Integer getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getAssignTo() {
		if(this.assignTo != null){
			return assignTo;
		}
		return 1;
	}

	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getExtraLicense() {
		return extraLicense;
	}

	public void setExtraLicense(Integer extraLicense) {
		this.extraLicense = extraLicense;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getAdminEmail() {
		return adminEmail;
	}

	public void setAdminEmail(String adminEmail) {
		this.adminEmail = adminEmail;
	}
	
}