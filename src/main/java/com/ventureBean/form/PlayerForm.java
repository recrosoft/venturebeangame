package com.ventureBean.form;

import java.util.Map;


public class PlayerForm {

	private Integer playerId;
	
	private String  palyerName;
	
	private String emailAddress;
	
	private String fieldsToSelect;
	
	private Integer projectId;

	private String action;
	
	private String assignedRole;
	
	private Integer roleId;
	
	private Map<String ,PlayerForm> data;
	
	public Integer getPlayerId() {
		return playerId;
	}

	public void setPlayerId(Integer playerId) {
		this.playerId = playerId;
	}

	public String getPalyerName() {
		return palyerName;
	}

	public void setPalyerName(String palyerName) {
		this.palyerName = palyerName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public String getFieldsToSelect() {
		return fieldsToSelect;
	}

	public void setFieldsToSelect(String fieldsToSelect) {
		this.fieldsToSelect = fieldsToSelect;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public Map<String, PlayerForm> getData() {
		return data;
	}

	public void setData(Map<String, PlayerForm> data) {
		this.data = data;
	}

	public String getAssignedRole() {
		return assignedRole;
	}

	public void setAssignedRole(String assignedRole) {
		this.assignedRole = assignedRole;
	}

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}

