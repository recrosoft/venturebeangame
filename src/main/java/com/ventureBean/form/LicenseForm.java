package com.ventureBean.form;


public class LicenseForm {

	private Integer userId;
	
	private Integer gameId; 
	
	private Integer noOfLicense;

	private String  bankName;
	
	private String  chequeNubmer;

	private String  amount;
	
	private String  chequeDate;

	private Integer businessPartnerId;

	private String createdBy;
	
	private String licenseStatus;
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public Integer getNoOfLicense() {
		return noOfLicense;
	}

	public void setNoOfLicense(Integer noOfLicense) {
		this.noOfLicense = noOfLicense;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public Integer getBusinessPartnerId() {
		return businessPartnerId;
	}

	public void setBusinessPartnerId(Integer businessPartnerId) {
		this.businessPartnerId = businessPartnerId;
	}

	public String getChequeNubmer() {
		return chequeNubmer;
	}

	public void setChequeNubmer(String chequeNubmer) {
		this.chequeNubmer = chequeNubmer;
	}

	public String getChequeDate() {
		return chequeDate;
	}

	public void setChequeDate(String chequeDate) {
		this.chequeDate = chequeDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getLicenseStatus() {
		return licenseStatus;
	}

	public void setLicenseStatus(String licenseStatus) {
		this.licenseStatus = licenseStatus;
	}
}
