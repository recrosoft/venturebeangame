package com.ventureBean.form;

import java.util.List;

public class TeamForm {

	List<PlayerForm> players;
	
	private String teamName;
	
	private Integer teamId;
	
	public List<PlayerForm> getPlayers() {
		return players;
	}

	public void setPlayers(List<PlayerForm> players) {
		this.players = players;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

}
