package com.ventureBean.form;


public class CategoryForm {

private Integer categoryId;
	
	private String categoryName;
	
	private String desc;
	
	private Double pricePerLicense;
	
	private Integer freeLicense;
	
	private Integer  gameId;

	private Integer threshold;
	
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Double getPricePerLicense() {
		return pricePerLicense;
	}

	public void setPricePerLicense(Double pricePerLicense) {
		this.pricePerLicense = pricePerLicense;
	}

	public Integer getFreeLicense() {
		return freeLicense;
	}

	public void setFreeLicense(Integer freeLicense) {
		this.freeLicense = freeLicense;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}

}
