package com.ventureBean.form;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class GameForm {

	private Integer gameId;
	
	private String gameName;
	
	private String categoryName;
	
	private String[] gameIds;

	private String gameDescription;
	
	private Date createdOn;

	private Date createdBy;

	private String modifiedBy;
	
	private MultipartFile gameLogo;
	
	private String logoUrl;
	
	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String[] getGameIds() {
		return gameIds;
	}

	public void setGameIds(String[] gameIds) {
		this.gameIds = gameIds;
	}

	public String getGameDescription() {
		return gameDescription;
	}

	public void setGameDescription(String gameDescription) {
		this.gameDescription = gameDescription;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Date createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public MultipartFile getGameLogo() {
		return gameLogo;
	}

	public void setGameLogo(MultipartFile gameLogo) {
		this.gameLogo = gameLogo;
	}

	public String getLogoUrl() {
		return logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	
}
