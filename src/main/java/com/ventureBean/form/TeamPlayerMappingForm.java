package com.ventureBean.form;

import java.util.List;

public class TeamPlayerMappingForm {

	List<TeamForm> teamForm;
	
	private String createdby;
	
	private Integer projectId;
	
	public List<TeamForm> getTeamForm() {
		return teamForm;
	}

	public void setTeamForm(List<TeamForm> teamForm) {
		this.teamForm = teamForm;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

} 
