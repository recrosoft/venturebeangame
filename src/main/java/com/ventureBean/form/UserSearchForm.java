package com.ventureBean.form;

public class UserSearchForm {

	private String selectedRole;
	
	private Integer  currentUserId;
	
	private String currentUserRole;
	
	private String fieldsToSelect;

	private Integer superUserId;
	
	public String getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(String selectedRole) {
		this.selectedRole = selectedRole;
	}

	public Integer getCurrentUserId() {
		return currentUserId;
	}

	public void setCurrentUserId(Integer currentUserId) {
		this.currentUserId = currentUserId;
	}

	public String getCurrentUserRole() {
		return currentUserRole;
	}

	public void setCurrentUserRole(String currentUserRole) {
		this.currentUserRole = currentUserRole;
	}


	public String getFieldsToSelect() {
		return fieldsToSelect;
	}

	public void setFieldsToSelect(String fieldsToSelect) {
		this.fieldsToSelect = fieldsToSelect;
	}

	public Integer getSuperUserId() {
		return superUserId;
	}

	public void setSuperUserId(Integer superUserId) {
		this.superUserId = superUserId;
	}
}

