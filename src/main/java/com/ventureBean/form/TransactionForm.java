package com.ventureBean.form;

import java.util.Date;

import com.ventureBean.model.Game;
import com.ventureBean.model.User;

public class TransactionForm {

	private Integer transactionId;
	
	private String licenseType;
	
	private String transactionType;
	
	private Integer paidLicenses;
	
	private Integer freeLicenses;
	
	private String transactionStatus;
	
	private long amount;
	
	private Date createdOn;
	
	private String createdby;
	
	private Date modifiedOn;
	
	private String modifiedBy;
	
	private Date startDate;
	
	private Date endDate;
	
	private Game game;
	
	private User businessPartner;

	private String fieldToSelect;
	
	private String licenseCount;
	
	
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public Integer getPaidLicenses() {
		return paidLicenses;
	}

	public void setPaidLicenses(Integer paidLicenses) {
		this.paidLicenses = paidLicenses;
	}

	public Integer getFreeLicenses() {
		return freeLicenses;
	}

	public void setFreeLicenses(Integer freeLicenses) {
		this.freeLicenses = freeLicenses;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public User getBusinessPartner() {
		return businessPartner;
	}

	public void setBusinessPartner(User businessPartner) {
		this.businessPartner = businessPartner;
	}
	
	public String getFieldToSelect() {
		return fieldToSelect;
	}

	public void setFieldToSelect(String fieldToSelect) {
		this.fieldToSelect = fieldToSelect;
	}

	public String getLicenseCount() {
		return licenseCount;
	}

	public void setLicenseCount(String licenseCount) {
		this.licenseCount = licenseCount;
	}

	@Override
	public String toString() {
		return "TransactionForm [transactionId=" + transactionId
				+ ", licenseType=" + licenseType + ", transactionType="
				+ transactionType + ", paidLicenses=" + paidLicenses
				+ ", freeLicenses=" + freeLicenses + ", transactionStatus="
				+ transactionStatus + ", amount=" + amount + ", createdOn="
				+ createdOn + ", createdby=" + createdby + ", modifiedOn="
				+ modifiedOn + ", modifiedBy=" + modifiedBy + ", game=" + game
				+ ", businessPartner=" + businessPartner + ", fieldToSelect="
				+ fieldToSelect + ", licenseCount=" + licenseCount + "]";
	}

	
	
	
}
