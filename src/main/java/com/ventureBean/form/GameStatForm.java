package com.ventureBean.form;

import java.util.List;

import com.ventureBean.dto.ResourceGameStructereDTO;
import com.ventureBean.enums.CrisisGameDayEnum;
import com.ventureBean.enums.CrisisGameDayStatusEnum;
import com.ventureBean.model.ResourceGameStructere;

public class GameStatForm {

	private Integer gameUserId;
	
	private String teamName;
	
	private Integer teamId;

	private String roleName;
	
	private Integer gameStatId;
	
	private Integer livesSaved;
	
	private Integer livesLost;
	
	private Double moneySpent;
	
	private Integer campusBuilt;
	
	private Integer resourcesUsed;
	
	private Integer survivorsCount;
	
	private CrisisGameDayEnum dayno;
	
	private CrisisGameDayStatusEnum status;

	private Integer projectId;
	
	private String resourceName;
	
	private List<String> statusList;
	
	private String note;
	
	private String missionName;
		
	private List<ResourceGameStructereDTO> resourcesGiven;

	private List<ResourceGameStructereDTO> unusedResources;
	
	private List<ResourceGameStructereDTO> resourcesRecived;
	
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getMissionName() {
		return missionName;
	}

	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}

	
	public List<ResourceGameStructereDTO> getResourcesGiven() {
		return resourcesGiven;
	}

	public void setResourcesGiven(List<ResourceGameStructereDTO> resourcesGiven) {
		this.resourcesGiven = resourcesGiven;
	}

	public List<ResourceGameStructereDTO> getUnusedResources() {
		return unusedResources;
	}

	public void setUnusedResources(List<ResourceGameStructereDTO> unusedResources) {
		this.unusedResources = unusedResources;
	}

	public List<ResourceGameStructereDTO> getResourcesRecived() {
		return resourcesRecived;
	}

	public void setResourcesRecived(List<ResourceGameStructereDTO> resourcesRecived) {
		this.resourcesRecived = resourcesRecived;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public Integer getGameUserId() {
		return gameUserId;
	}

	public void setGameUserId(Integer gameUserId) {
		this.gameUserId = gameUserId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Integer getGameStatId() {
		return gameStatId;
	}

	public void setGameStatId(Integer gameStatId) {
		this.gameStatId = gameStatId;
	}

	public Integer getLivesSaved() {
		return livesSaved;
	}

	public void setLivesSaved(Integer livesSaved) {
		this.livesSaved = livesSaved;
	}

	public Integer getLivesLost() {
		return livesLost;
	}

	public void setLivesLost(Integer livesLost) {
		this.livesLost = livesLost;
	}

	public Double getMoneySpent() {
		return moneySpent;
	}

	public void setMoneySpent(Double moneySpent) {
		this.moneySpent = moneySpent;
	}

	public Integer getCampusBuilt() {
		return campusBuilt;
	}

	public void setCampusBuilt(Integer campusBuilt) {
		this.campusBuilt = campusBuilt;
	}

	public Integer getResourcesUsed() {
		return resourcesUsed;
	}

	public void setResourcesUsed(Integer resourcesUsed) {
		this.resourcesUsed = resourcesUsed;
	}

	public Integer getSurvivorsCount() {
		return survivorsCount;
	}

	public void setSurvivorsCount(Integer survivorsCount) {
		this.survivorsCount = survivorsCount;
	}

	public CrisisGameDayEnum getDayno() {
		return dayno;
	}

	public void setDayno(CrisisGameDayEnum dayno) {
		this.dayno = dayno;
	}

	public CrisisGameDayStatusEnum getStatus() {
		return status;
	}

	public void setStatus(CrisisGameDayStatusEnum status) {
		this.status = status;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public List<String> getStatusList() {
		return statusList;
	}

	public void setStatusList(List<String> statusList) {
		this.statusList = statusList;
	}

	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}
}
