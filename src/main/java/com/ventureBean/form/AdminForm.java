package com.ventureBean.form;

/**
 * @author Satyajit
 *
 */
public class AdminForm {

	private Integer userId ;
	
	private String emailAddress;
	
	private String password;
	
	private Integer userRoleId ;
	
	private String fieldsToSelect;
	
	private String userName;
	
	private String roleName;

	private Integer[] assignToList;
	
	private Integer superUserId ;
	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Integer getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getFieldsToSelect() {
		return fieldsToSelect;
	}
	public void setFieldsToSelect(String fieldsToSelect) {
		this.fieldsToSelect = fieldsToSelect;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer[] getAssignToList() {
		return assignToList;
	}
	public void setAssignToList(Integer[] assignToList) {
		this.assignToList = assignToList;
	}
	public Integer getSuperUserId() {
		return superUserId;
	}
	public void setSuperUserId(Integer superUserId) {
		this.superUserId = superUserId;
	}
	
}
