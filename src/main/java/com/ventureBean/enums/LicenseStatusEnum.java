package com.ventureBean.enums;

public enum LicenseStatusEnum {
NEW(0),ACTIVE(1),EXPIRED(2),USED(3),CANCELLED(4);

private Integer value;

LicenseStatusEnum() {
		value = null;
	}

	private LicenseStatusEnum(Integer value) {
		this.value = value;

	}

	/**
	 * @return Integer
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * @param value
	 */
	public void setValue(Integer value) {
		this.value = value;
	}
}
