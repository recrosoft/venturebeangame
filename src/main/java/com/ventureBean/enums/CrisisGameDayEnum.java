package com.ventureBean.enums;

public enum CrisisGameDayEnum {
DAY_01(1),DAY_02(2),DAY_03(3),DAY_04(4),DAY_05(5),DAY_06(6);

private Integer value;

private static CrisisGameDayEnum[] vals = values();
public CrisisGameDayEnum next()
{
    return vals[(this.ordinal()+1) % vals.length];
}

CrisisGameDayEnum(){}

CrisisGameDayEnum(Integer value){
	this.value = value;
}

public Integer getValue() {
	return value;
}

public void setValue(Integer value) {
	this.value = value;
}

}
