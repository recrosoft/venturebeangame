package com.ventureBean.enums;

public enum ProjectUserEnum {

	FACILITATOR(0),PLAYER(1);
	
	private Integer value;
	
	private ProjectUserEnum(){}
	
	private ProjectUserEnum(Integer value){
		this.value= value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
