package com.ventureBean.enums;

public enum ProjectStatusEnum {
	CREATED(1),ACTIVE(2),COMPLETED(3),CANCELLED(4);
	
	private Integer value;
	
	private ProjectStatusEnum(){}
	
	private ProjectStatusEnum(Integer value){
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
}
