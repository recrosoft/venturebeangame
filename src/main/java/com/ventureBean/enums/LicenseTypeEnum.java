package com.ventureBean.enums;

public enum LicenseTypeEnum {
		PAID(0),FREE(1);

		private Integer value;

		LicenseTypeEnum() {
				value = null;
			}

			private LicenseTypeEnum(Integer value) {
				this.value = value;

			}

			/**
			 * @return Integer
			 */
			public Integer getValue() {
				return value;
			}

			/**
			 * @param value
			 */
			public void setValue(Integer value) {
				this.value = value;
			}
}
