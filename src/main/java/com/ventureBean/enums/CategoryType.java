package com.ventureBean.enums;

public enum CategoryType {

	PLATINUM(2),GOLD(0),SILVER(1);
	
	private Integer value;
	
	private CategoryType(int value){
		this.value = value;
	}

	/**
	 * @return
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * @param value
	 */
	public void setValue(Integer value) {
		this.value = value;
	}
	
	
}
