package com.ventureBean.enums;

public enum TransactionTypeEnum {

	BUY(0),CANCELLED(1);
	
	private Integer value;
	
	TransactionTypeEnum(){
		
	}
	TransactionTypeEnum(Integer value){
		this.value = value;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	
}
