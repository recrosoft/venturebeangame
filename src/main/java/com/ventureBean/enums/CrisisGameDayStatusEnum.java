package com.ventureBean.enums;

public enum CrisisGameDayStatusEnum {
CREATED(1),ACTIVE(2),COMPLETED(3);

private Integer value;

private CrisisGameDayStatusEnum() {
}
CrisisGameDayStatusEnum(Integer value){
	this.value= value;
}
public Integer getValue() {
	return value;
}
public void setValue(Integer value) {
	this.value = value;
}

}
