package com.ventureBean.enums;

public enum TransactionStatusEnum {

	PENDING(0),APPROVED(1),REJECTED(2);
	
	private Integer value;
	
	private TransactionStatusEnum(){}
	
	private TransactionStatusEnum(Integer value){
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
	
}
