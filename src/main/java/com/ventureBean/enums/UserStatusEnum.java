package com.ventureBean.enums;

public enum UserStatusEnum {

	Active(0),Blocked(1);
	
	private Integer value;
	
	private UserStatusEnum(){
		
	}
	private UserStatusEnum(int value){
		this.value = value;
	}
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	
	
}
