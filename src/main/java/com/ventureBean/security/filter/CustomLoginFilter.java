package com.ventureBean.security.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.ventureBean.constants.Constants;
import com.ventureBean.security.authenticationToken.AdminUsernamePasswordAuthenticationToken;
import com.ventureBean.security.authenticationToken.UserUsernamePasswordAuthenticationToken;


/**
 * Servlet Filter implementation class CustomLoginFilter
 */
public class CustomLoginFilter extends  UsernamePasswordAuthenticationFilter {


	protected static Logger log = Logger.getLogger(CustomLoginFilter.class);
	


	@Override
	public Authentication attemptAuthentication(HttpServletRequest request,
			HttpServletResponse response) throws AuthenticationException {
		log.debug("start of method attemptAuthentication of CustomFilter");
		
		 UsernamePasswordAuthenticationToken authRequest = null;
		  String uname=request.getParameter("j_username");
		 
		 String pwd=request.getParameter("j_password");
		 
		 String type=request.getParameter("userType");
		 log.debug("user Type : "+type);
		 if (uname == null) {
	            uname = "";
	        }
	        if (pwd == null) {
	            pwd = "";
	        }
			if(type.equals(Constants.USER_TYPE_ADMIN)){
			     authRequest = new AdminUsernamePasswordAuthenticationToken(uname, pwd);
		    }
		    else if(type.equals(Constants.USER_TYPE_USER)) {
		        authRequest = new UserUsernamePasswordAuthenticationToken(uname, pwd);
		    }
			HttpSession session = request.getSession(false);
			if(null == session){
				session = request.getSession(true);
			}
			session.setAttribute("userType", type);
			setDetails(request, authRequest);
		   
		log.debug("end of method attemptAuthentication ");
		 return super.getAuthenticationManager().authenticate(authRequest);
	}

	 

}
