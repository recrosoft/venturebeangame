package com.ventureBean.security.failureHandler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.ExceptionMappingAuthenticationFailureHandler;

import com.ventureBean.exception.AdminBadCredentialsException;
import com.ventureBean.exception.UserBadCredentialsException;

public class CustomUrlAuthenticationFailureHandler extends ExceptionMappingAuthenticationFailureHandler {

	 private static final Logger log = Logger.getLogger(CustomUrlAuthenticationFailureHandler.class);
	 
	 private  final String adminDefaultFailureUrl ="/adminLoginFailure";
	 private  final String  userDefaultFailureUrl ="/userLoginFailure";
	 
	    @Override
	    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
	            AuthenticationException exception) throws IOException, ServletException {
	        log.debug("inside onAuthenticationFailure  of CustomLoginFailureHandler ");
	    	log.debug("invalid login  : " + exception.getMessage() );
	        
	        if (exception instanceof AdminBadCredentialsException) {
	        	 request.getRequestDispatcher(adminDefaultFailureUrl).forward(request, response);
	        } else if(exception instanceof UserBadCredentialsException){
	        	 request.getRequestDispatcher(userDefaultFailureUrl).forward(request, response);
	        }else{
	        	super.onAuthenticationFailure(request, response, exception);
	        }
	    }
	    
	    
	    
}



