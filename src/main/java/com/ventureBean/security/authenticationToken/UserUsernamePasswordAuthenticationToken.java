package com.ventureBean.security.authenticationToken;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class UserUsernamePasswordAuthenticationToken  extends UsernamePasswordAuthenticationToken{

	
	public UserUsernamePasswordAuthenticationToken(Object principal,
			Object credentials) {
		super(principal, credentials);

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -8649503376614445796L;

}
