package com.ventureBean.security.authenticationToken;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
public class AdminUsernamePasswordAuthenticationToken extends UsernamePasswordAuthenticationToken {

	public AdminUsernamePasswordAuthenticationToken(Object principal,
			Object credentials) {
		super(principal, credentials);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1577898740775173112L;

}
