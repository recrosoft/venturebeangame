package com.ventureBean.security.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.ventureBean.exception.UserBadCredentialsException;
import com.ventureBean.form.UserForm;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.security.authenticationToken.UserUsernamePasswordAuthenticationToken;
import com.ventureBean.service.UserLoginService;
import com.ventureBean.util.Format;
@Component
public class UserCustomAuthenticationProvider implements AuthenticationProvider{

	Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
	
	private UserLoginService userLoginService;

	protected static Logger log = Logger.getLogger(UserCustomAuthenticationProvider.class);
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		log.debug("start of method  authenticate in UserCustomAuthenticationProvider");
		ProjectUser user = null;
		UserForm userForm = new UserForm();
		userForm.setUserName(authentication.getName());
 		//userForm.setPassword(passwordEncoder.encodePassword((String)authentication.getCredentials() ,null) );
		user = userLoginService.getUser(userForm);
		if (Format.isNull(user)) {
			throw new UserBadCredentialsException("User does not exists!");
		} else {
			if(passwordEncoder.isPasswordValid(user.getPassword(), (String)authentication.getCredentials(), null)){
				List<UserRoleMaster> roleList = new ArrayList<UserRoleMaster>();
				roleList.add(user.getUserRole());
				return new UsernamePasswordAuthenticationToken(authentication.getName(),
						authentication.getCredentials(),
						getAuthorities(roleList));
			}else {
				throw new UserBadCredentialsException("Wrong Password!");
			}
		}
	}

	/**
	 * Retrieves the correct ROLE type depending on the access level, where
	 * access level is an Integer. Basically, this interprets the access value
	 * whether it's for a regular user or admin. access an integer value
	 * representing the access of the user
	 * 
	 * @param roleList as List
	 * @return collection of granted authorities
	 */
	public Collection<GrantedAuthority> getAuthorities(List<UserRoleMaster> roleList) {
		log.debug("Inside getAuthorities of CustomAuthenticationManager");
		// Create a list of grants for this user
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(4);
		log.debug("roleList -------------> " + roleList.size());
		if (roleList != null) {
			for (UserRoleMaster role : roleList) {
				String roleName = String.valueOf(role.getRoleName());
				log.debug("Role : " + roleName);
				authList.add(new SimpleGrantedAuthority(roleName));
			}
		}
		// Return list of granted authorities
		return authList;
	}
	
	@Override
	public boolean supports(Class<?> authentication) {
		log.debug("start of method  support in UserCustomAuthenticationProvider");
		return (UserUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

	public UserLoginService getUserLoginService() {
		return userLoginService;
	}

	public void setUserLoginService(UserLoginService userLoginService) {
		this.userLoginService = userLoginService;
	}

}
