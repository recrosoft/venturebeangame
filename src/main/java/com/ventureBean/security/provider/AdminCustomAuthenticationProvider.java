package com.ventureBean.security.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.ventureBean.exception.AdminBadCredentialsException;
import com.ventureBean.form.AdminForm;
import com.ventureBean.model.User;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.security.authenticationToken.AdminUsernamePasswordAuthenticationToken;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.util.Format;
@Component
public class AdminCustomAuthenticationProvider implements AuthenticationProvider {

	private AdminLoginService adminLoginService;

	private Md5PasswordEncoder passwordEncoder = new Md5PasswordEncoder();
	
	protected static Logger log = Logger.getLogger(AdminCustomAuthenticationProvider.class);
	
	User user = null;
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		log.debug("inside  authenticate in AdminCustomAuthenticationProvider");
		AdminForm adminForm = new AdminForm();
		adminForm.setEmailAddress(authentication.getName());
		user = adminLoginService.getUser(adminForm);
		if (Format.isNull(user)) {
			throw new AdminBadCredentialsException("User does not exists!");
		} else {
			if(passwordEncoder.isPasswordValid(user.getPassword(), (String)authentication.getCredentials(), null)){
				List<UserRoleMaster> roleList = new ArrayList<UserRoleMaster>();
				roleList.add(user.getUserRole());
				return new UsernamePasswordAuthenticationToken(authentication.getName(),
						authentication.getCredentials(),
						getAuthorities(roleList));
			}else {
				throw new AdminBadCredentialsException("Wrong Password!");
			}
		}
		
	}

	/**
	 * Retrieves the correct ROLE type depending on the access level, where
	 * access level is an Integer. Basically, this interprets the access value
	 * whether it's for a regular user or admin. access an integer value
	 * representing the access of the user
	 * 
	 * @param roleList as List
	 * @return collection of granted authorities
	 */
	public Collection<GrantedAuthority> getAuthorities(List<UserRoleMaster> roleList) {
		log.debug("Inside getAuthorities of CustomAuthenticationManager");
		// Create a list of grants for this user
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(4);
		log.debug("roleList -------------> " + roleList.size());
		if (roleList != null) {
			for (UserRoleMaster role : roleList) {
				String roleName = String.valueOf(role.getRoleName());
				log.debug("Role : " + roleName);
				authList.add(new SimpleGrantedAuthority(roleName));
			}
		}
		// Return list of granted authorities
		return authList;
	}
	
	
	@Override
	public boolean supports(Class<?> authentication) {
		log.debug("start of method  support in AdminCustomAuthenticationProvider");
		return (AdminUsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
	}

	public AdminLoginService getAdminLoginService() {
		return adminLoginService;
	}

	public void setAdminLoginService(AdminLoginService adminLoginService) {
		this.adminLoginService = adminLoginService;
	}

}
