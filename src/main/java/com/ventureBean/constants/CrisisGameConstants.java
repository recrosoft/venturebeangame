package com.ventureBean.constants;

public class CrisisGameConstants {

	/**
	 * @Declared 
	 */
	public static final String ROLE_LEADER ="Leader";
	
	public static final Integer TimeBetween2DAY = 10;
	
	public static final Integer MIN_USER_PER_GAME = 05;
	
	public static final String GAME_NOT_STARTED = "Game is not yet started";
	
	public static final String GAME_COMPLETED = "Game is already started";

	public static final String GAME_CANCELLED = "Game is Cancelled";
}
