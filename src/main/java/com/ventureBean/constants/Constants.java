package com.ventureBean.constants;

public class Constants {

	/**
	 * Declared constant to get zero index object  from List.
	 */
	public static final int  GET_ZERO_INDEX_OBJECT = 0;
	/**
	 * Declared adminDetailsSessionForm.
	 */
	public static final String ADMINDETAILSFORMSESSION = "adminDetailsSessionForm";
	/**
	 * Constants declared for the name of list that will store error messages in session.
	 */
	public static final String ERROR_KEY = "errorMessages";
	
	/**
	 * Constants declared for the name of list that will store success messages in session.
	 */
	public static final String MESSAGES_KEY = "successMessages";
	
	public static final Integer  LICENSSE_EXPIRY_YEAR = 9999;
	
	public static final Integer LICENSE_EXPIRY_MONTH = 01;
	
	public static final  Integer LICENSE_EXPIRY_DAY = 01;
	
	public static final Double WALLET_INIT_VALUE = 0.0;
	
	
	/**
	 * @Declared userLoginLink Testing
	 */
	//public static final String BASE_LINK = "http://vbgame-loadbalancer-784901108.ap-southeast-1.elb.amazonaws.com/VentureBeanGame/userlogin?key=";
	
	/**
	 * @Declared userLoginLink Local
	 */
	public static final String BASE_LINK = "http://localhost:8080/VentureBeanGame/userlogin?key=";
	
	/**
	 * @Declared adminLogin Testing
	 */
	public static final String BASE_LINK_ADMIN = "http://vbgame-loadbalancer-784901108.ap-southeast-1.elb.amazonaws.com/VentureBeanGame/dashboard";
	
	/**
	 * @Declared adminLogin Local
	 */	
//	public static final String BASE_LINK_ADMIN = "http://localhost:8080/VentureBeanGame/dashboard";
	
	public static final String SADMIN_ROLE ="SUPER_ADMIN";
	
	public static final String ADMIN_ROLE ="VB_ADMIN";
	
	public static final String BH_ROLE ="BUSINESS_HEAD";
	
	public static final String BM_ROLE ="BUSINESS_MANAGER";
	
	public static final String BDM_ROLE ="BDM";
	
	public static final String BP_ROLE ="BUSINESS_PARTNER";
	
	public static final String FACILITATOR_ROLE = "FACILITATOR";
	
	public static final String PLAYER_ROLE = "USER";
	

	public static final String SADMIN_ROLE_SHORT = "SA";
	
	public static final String VB_ADMIN_SHORT ="admin";
	
	public static final String BH_ROLE_SHORT ="BH";
	
	public static final String BM_ROLE_SHORT ="BM";
	
	public static final String BDM_ROLE_SHORT ="BDM";
	
	public static final String BP_ROLE_SHORT ="BP";
	
	public static final Integer FACILITATOR_ROLE_ID = 6;
	
	public static final Integer PLAYER_ROLE_ID = 7;
	
	public static final String BH_ROLE_TEXT ="Business Head";
	
	public static final String BM_ROLE_TEXT ="Business Manager";
	
	public static final String BDM_ROLE_TEXT ="Business Dev. Manager";
	
	public static final String BP_ROLE_TEXT ="Business Partner";
	
	public static final String SA_ROLE_TEXT ="Super Admin";
	
	public static final String TRNX_APPROVE ="approve";
	
	public static final Integer THREE = 3;
	
	public static final Integer SIX = 6;
	
	/**
	 * @Declared ZERO
	 */
	public static final Integer ZERO = 0;
	
	/**
	 * S3 base url for testing.
	 */
	public static final String S3_BASE_URL = "https://s3-ap-southeast-1.amazonaws.com/";
	
	/**
	 * Declared S3 bucket name for file upload for testing.
	 */
	public static final String S3_BUCKET_FILE_UPLOAD = "vbgame";
	
	/**
	 * Delecred dot
	 */
	public static final String DOT =".";
	
	/**
	 * Declared Hyphen
	 */
	public static final String HYPEN = "-";
	
	/**
	 * Declared PIN_MIN_LENGTH.
	 */
	public static final Integer PIN_MIN_LENGTH = 30;
	
	/**
	 * Declared PIN_MAX_LENGTH.
	 */
	public static final Integer PIN_MAX_LENGTH = 30;
	
	/**
	 * Delcared PIN_NO_OF_CAPS_ALPHABETS.
	 */
	public static final Integer PIN_NO_OF_CAPS_ALPHABETS = 15;
	
	/**
	 * Declared PIN_NO_OF_DIGITS.
	 */
	public static final Integer PIN_NO_OF_DIGITS = 15;
	
	/**
	 * Declared PIN_NO_OF_SPECIALCHAR.
	 */
	public static final Integer PIN_NO_OF_SPECIALCHAR = 0;
	
	public static final String USER_TYPE_ADMIN = "ADMIN";
	
	public static final String USER_TYPE_USER = "USER";
	
	public static final Integer ONE_NEGATIVE = -1;
	
	
	
	
	
	

		
}
