package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.form.CompanyForm;
import com.ventureBean.model.Project;

public interface ProjectDAO extends GenericDAO<Project, Serializable>{

	/**@author Satyajit
	 * This will return Project(s) based on the parameters provided
	 * @param companyForm
	 * @return
	 */
	List<Project> getProjects(CompanyForm companyForm);

	
}
