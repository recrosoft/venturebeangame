package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.model.UserRoleMaster;

public interface UserRoleDAO extends GenericDAO<UserRoleMaster, Serializable> {

	/**@author Satyajit
	 * This will return UserRole(s) based on role name
	 * @param userName
	 * @return
	 */
	List<UserRoleMaster> getUserRoles(String userName);
		
}
