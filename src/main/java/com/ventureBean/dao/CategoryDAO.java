package com.ventureBean.dao;

import java.util.List;

import com.ventureBean.form.CategoryForm;
import com.ventureBean.model.Category;

public interface CategoryDAO extends GenericDAO<Category, String> {

	/**@author Satyajit
	 * This will return Category(s) based on the parameters provided
	 * @param categoryForm
	 * @return
	 */
	List<Category> getCategory(CategoryForm categoryForm);
	
}
