package com.ventureBean.dao.crisisGame;

import java.io.Serializable;

import com.ventureBean.dao.GenericDAO;
import com.ventureBean.model.CrisisGameIdealResult;

public interface CGameIdealResultDAO extends GenericDAO<CrisisGameIdealResult, Serializable> {

}
