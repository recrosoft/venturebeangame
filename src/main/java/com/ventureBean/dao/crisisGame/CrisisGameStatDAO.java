package com.ventureBean.dao.crisisGame;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.dao.GenericDAO;
import com.ventureBean.form.GameStatForm;
import com.ventureBean.model.CrisisGameStatistics;

public interface CrisisGameStatDAO extends GenericDAO<CrisisGameStatistics, Serializable>{

	/**@author Satyajit
	 * This will return CrisisGameStatistics(s) based on the parameters provided
	 * @param gameStatForm
	 * @return
	 */
	List<CrisisGameStatistics> getGameStats(GameStatForm gameStatForm);

	/**@author Satyajit
	 * This will update next Day Statistics
	 * @param statForm
	 */
	void updateNextDay(GameStatForm statForm);

}

