package com.ventureBean.dao.crisisGame;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.dao.GenericDAO;
import com.ventureBean.form.GameUserForm;
import com.ventureBean.model.CrisisGameUser;

public interface CrisisGameUserDAO extends GenericDAO<CrisisGameUser, Serializable>{

	/**This will return GameUser(s) based on parameter(s) provided
	 * @param gameUserForm
	 * @return
	 */
	List<CrisisGameUser> getGameUser(GameUserForm gameUserForm  );

	/**@author Satyajit
	 * this will remove users 
	 * @param form
	 */
	void removeUsers(GameUserForm form);
}
