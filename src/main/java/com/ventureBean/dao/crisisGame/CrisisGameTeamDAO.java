package com.ventureBean.dao.crisisGame;

import java.io.Serializable;

import com.ventureBean.dao.GenericDAO;
import com.ventureBean.model.CrisisGameTeam;

public interface CrisisGameTeamDAO  extends GenericDAO<CrisisGameTeam, Serializable>{

}
