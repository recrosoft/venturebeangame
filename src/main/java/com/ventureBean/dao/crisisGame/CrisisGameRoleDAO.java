package com.ventureBean.dao.crisisGame;

import java.io.Serializable;

import com.ventureBean.dao.GenericDAO;
import com.ventureBean.model.CrisisGameRoleMaster;

public interface CrisisGameRoleDAO extends GenericDAO<CrisisGameRoleMaster, Serializable> {

}
