package com.ventureBean.dao;

import java.util.List;

import com.ventureBean.form.CompanyForm;
import com.ventureBean.model.Company;

public interface CompanyDAO extends GenericDAO<Company, Integer>{

	/**@author Satyajit
	 * This will return Company(s) based on the parameters provided
	 * @param companyForm
	 * @return
	 */
	List<Company> getCompany(CompanyForm companyForm);

	
	
}
