package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.form.CategoryForm;
import com.ventureBean.model.Category;
import com.ventureBean.model.Game;

public interface GameDAO extends GenericDAO<Game, Serializable> {

	/**@author Satyajit
	 * This method will return all the Games from DB
	 * @return List of Games 
	 */
	List<Category> getCategory(CategoryForm categoryForm);
}
