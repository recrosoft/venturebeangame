package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.form.UserForm;
import com.ventureBean.model.ProjectUser;

public interface UserLoginDAO extends GenericDAO<ProjectUser, Serializable>{

	/**@author Satyajit
	 * This will return ProjectUser(s) based on the parameters provided
	 * @param form
	 * @return
	 */
	List<ProjectUser> getUser(UserForm form);

	/**@author Satyajit
	 * This will remove ProjectUser(s) based on the parameters provided
	 * @param userForm
	 */
	void removeUsers(UserForm userForm);
	
	/**@author Satyajit
	 * This is a utility method which will generate the unique username by checking in DB
	 * @param userName
	 * @return
	 */
	public String  generateUserName(String userName);

	
}
