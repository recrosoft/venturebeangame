package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.form.LicenseForm;
import com.ventureBean.model.License;

public interface LicenseDAO extends GenericDAO<License, Serializable> {

	/**@author Satyajit
	 * This will return License(s) based on the parameters provided
	 * @param licenseForm
	 * @return
	 */
	List<License> getLicense(LicenseForm licenseForm);

}
