package com.ventureBean.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.CompanyDAO;
import com.ventureBean.form.CompanyForm;
import com.ventureBean.model.Company;
import com.ventureBean.util.Format;

/**
 * @author Satyajit
 *
 */
@Repository(CompanyDAOImpl.DAO_NAME)
public class CompanyDAOImpl extends GenericDAOImpl<Company, Integer> implements CompanyDAO{

	/**
	 * @Declared companyDAO
	 */
	public static final String DAO_NAME="companyDAO";

	CompanyDAOImpl(){
		super(Company.class);
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.CompanyDAO#getCompany(com.ventureBean.form.CompanyForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> getCompany(CompanyForm form) {
		log.debug("start of method getCompany ");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(form.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+form.getFieldsToSelect()+" FROM Company where 1=1 ";
			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM Company u where 1=1 ");
		}
	
		if(Format.isStringNotEmptyAndNotNull(form.getUserName())){
			hql.append(" and businessPartner.userId = (SELECT userId from User where emailAddress = '"+form.getUserName()+"')");
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(form.getBpId())){
			hql.append(" and businessPartner.userId = "+form.getBpId());
		}
		List<Company> companyList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getCompany ");
		return companyList;
	}
	
	
}
