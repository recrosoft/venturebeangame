package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.ProjectDAO;
import com.ventureBean.form.CompanyForm;
import com.ventureBean.model.Project;
import com.ventureBean.util.Format;

@Repository(ProjectDAOImpl.DAO_NAME)
public class ProjectDAOImpl extends GenericDAOImpl<Project, Serializable> implements ProjectDAO{

	/**
	 * @Declared projectDAO
	 */
	public static final String DAO_NAME="projectDAO";
	
	ProjectDAOImpl(){
		super(Project.class);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.ProjectDAO#getProjects(com.ventureBean.form.CompanyForm)
	 */
	@Override
	public List<Project> getProjects(CompanyForm form) {
		log.debug("start of method getProjects");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(form.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+form.getFieldsToSelect()+" FROM Company where 1=1 ";
			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM Project p where 1=1 ");
		}
	
		if(Format.isNotNull(form.getCompanyId())){
			hql.append(" and company.companyId = "+form.getCompanyId());
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(form.getBpId())){
			hql.append(" and p.businessPartner.userId = "+form.getBpId());
		}
		@SuppressWarnings("unchecked")
		List<Project>  projects =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getProjects");
		return projects;
	}
	
}
