package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.constants.Constants;
import com.ventureBean.model.AppConfiguration;
import com.ventureBean.util.Format;
@Repository(AppConfigurationDAOImpl.DAO_NAME)
public class AppConfigurationDAOImpl extends GenericDAOImpl<AppConfiguration, Serializable> implements com.ventureBean.dao.AppConfigurationDAO {

	/**
	 * Declared DAO_NAME.
	 * 
	 * 
	 */
	public static final String DAO_NAME = "appConfigurationDAO";
	
	public AppConfigurationDAOImpl() {
		super(AppConfiguration.class);
	}

	/* (non-Javadoc)
	 * @see com.dealocx.dao.ApplicationConfigurationDAO#getApplicationConfigurationByName(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public AppConfiguration getAppConfigurationByName(
			String configName) {
		log.debug("Start of method getApplicationConfigurationByName ");
		StringBuffer hql = new StringBuffer("FROM AppConfiguration where 1=1");
		if(Format.isStringNotEmptyAndNotNull(configName)){
			hql.append(" and appConfigurationName ='"+configName+"'");
		}
		List<AppConfiguration> list = getSession().createQuery(hql.toString()).list();
		if(Format.isCollectionNotEmtyAndNotNull(list)){
			return (AppConfiguration)list.get(Constants.GET_ZERO_INDEX_OBJECT);
		}
		return null;
	}
}
