package com.ventureBean.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.CategoryDAO;
import com.ventureBean.form.CategoryForm;
import com.ventureBean.model.Category;
import com.ventureBean.util.Format;

@Repository(CategoryDAOImpl.DAO_NAME)
public class CategoryDAOImpl extends GenericDAOImpl<Category,String> implements CategoryDAO {

	/**
	 * Declared DAO_NAME
	 */
	public static final String DAO_NAME="categoryDAO";
	
	public CategoryDAOImpl(){
		super(Category.class);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.CategoryDAO#getCategory(com.ventureBean.form.CategoryForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> getCategory(CategoryForm categoryForm) {
		log.debug("start of method getCategory");
		StringBuffer hql = new StringBuffer(" FROM Category c where 1=1 ");
		
		if(Format.isIntegerNotEmtyAndNotZero(categoryForm.getCategoryId())){
			hql.append("and categoryId ="+categoryForm.getCategoryId());
		}
		if(Format.isStringNotEmptyAndNotNull(categoryForm.getCategoryName())){
			hql.append("and categoryName ='"+categoryForm.getCategoryName()+"'");
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(categoryForm.getGameId())){
			hql.append("and game.gameId ="+categoryForm.getGameId());
		}
		List<Category> userList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getCategory");
		return userList;
	}
}
