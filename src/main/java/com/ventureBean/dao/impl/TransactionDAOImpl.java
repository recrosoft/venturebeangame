package com.ventureBean.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.TransactionDAO;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.model.UserTransaction;
import com.ventureBean.util.Format;

@Repository(TransactionDAOImpl.DAO_NAME)
public class TransactionDAOImpl extends GenericDAOImpl<UserTransaction, String> implements TransactionDAO {

	/**
	 * @Declared DAO_NAME
	 */
	public static final String DAO_NAME ="transactionDAO";

	public TransactionDAOImpl() {
		super(UserTransaction.class);
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.TransactionDAO#getTransactions(com.ventureBean.form.TransactionForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserTransaction> getTransactions(TransactionForm transactionForm) {
		log.debug("start of method getTransactions");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(transactionForm.getFieldToSelect())){
			hql = new StringBuffer("SELECT "+transactionForm.getFieldToSelect()+" FROM UserTransaction where 1=1 ");
		}else{
			hql = new StringBuffer("FROM UserTransaction where 1=1 ");
		}
		if(Format.isStringNotEmptyAndNotNull(transactionForm.getTransactionStatus())){
			
		//	hql.append(" and transactionStatus='"+transactionForm.getTransactionStatus()+"'"+transactionForm.getGame().getCategories());
			hql.append(" and transactionStatus='"+transactionForm.getTransactionStatus());
		}
		if(Format.isIntegerNotEmtyAndNotZero(transactionForm.getTransactionId())){
			hql.append(" and transactionId="+transactionForm.getTransactionId());
		}
		if(Format.isIntegerNotEmtyAndNotZero(transactionForm.getGame().getGameId())){
			hql.append(" and gameId="+transactionForm.getGame().getGameId());
		}
		
		if(Format.isStringNotEmptyAndNotNull(transactionForm.getLicenseType())){
			hql.append(" and licensetype='"+transactionForm.getLicenseType());
		}
		
		if(Format.isDateNotEmtyAndNotNull((transactionForm.getModifiedOn()))){
			hql.append(" and modified_on BETWEEN ("+transactionForm.getStartDate()+"and "+transactionForm.getEndDate());
		}
		List<UserTransaction> list = getSession().createQuery(hql.toString()).list();
		log.debug("end of method getTransactions");
		return list;
	}
	
}
