package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.UserLoginDAO;
import com.ventureBean.form.UserForm;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.util.Format;

@Repository(UserLoginDAOImpl.DAO_NAME)
public class UserLoginDAOImpl extends GenericDAOImpl<ProjectUser, Serializable> implements UserLoginDAO{
	
	/**
	 * Declared DAO_NAME
	 */
	public static final String DAO_NAME="userLoginDAO";
	
	UserLoginDAOImpl(){
		super(ProjectUser.class);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.UserLoginDAO#getUser(com.ventureBean.form.UserForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ProjectUser> getUser(UserForm form) {
		log.debug("start of method getUser");
		
		StringBuffer hql = null;
		
		if(Format.isStringNotEmptyAndNotNull(form.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+form.getFieldsToSelect()+" FROM ProjectUser where 1=1 ";

			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM ProjectUser where 1=1 ");
		}
		
		if(Format.isStringNotEmptyAndNotNull(form.getAccessToken())){
			hql.append("and accessToken ='"+form.getAccessToken()+"'");
		}
		
		if(Format.isStringNotEmptyAndNotNull(form.getEmailAddress())){
			hql.append("and emailAddress ='"+form.getEmailAddress()+"'");
		}
		
		if(Format.isStringNotEmptyAndNotNull(form.getUserName())){	
			hql.append("and userName ='"+form.getUserName()+"'");
		}
		
		if(Format.isStringNotEmptyAndNotNull(form.getPassword())){
			hql.append("and password ='"+form.getPassword()+"'");
		}
		
		if(Format.isStringNotEmptyAndNotNull(form.getUserType())){
			hql.append("and userType ='"+form.getUserType()+"'");
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(form.getProjectId())){
			hql.append(" and project.projectId ="+form.getProjectId());
		}
		if(Format.isIntegerNotEmtyAndNotZero(form.getProjectUserId())){
			hql.append(" and projectUserId ="+form.getProjectUserId());
		}
		log.debug("hql : "+hql.toString());
		List<ProjectUser> userList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getUser");
		return userList;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.UserLoginDAO#removeUsers(com.ventureBean.form.UserForm)
	 */
	@Override
	public void removeUsers(UserForm userForm) {
		log.debug("start of method removeUsers");
		StringBuffer hql = new StringBuffer(" DELETE FROM ProjectUser ");
		
		if(Format.isIntegerNotEmtyAndNotZero(userForm.getProjectId())){
			hql.append(" where project.projectId ="+userForm.getProjectId());
		}
		if(Format.isStringNotEmptyAndNotNull(userForm.getUserType())){
			hql.append(" and userType ='"+userForm.getUserType()+"'");
		}
		getSession().createQuery(hql.toString()).executeUpdate();
		log.debug("end of method removeUsers");
		
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.UserLoginDAO#generateUserName(java.lang.String)
	 */
	@Override
	public  String  generateUserName(String userName) {
		log.debug("start of method getUserName");
		UserForm form = new UserForm();
		//form.setProjectId(projectForm.getProjectId());
		form.setUserName(userName);
		boolean isAlreadyExit = false;
		do {
			List<ProjectUser> projectUsers = getUser(form);
			if(Format.isCollectionNotEmtyAndNotNull(projectUsers)){
				log.debug("username already exists :");
				String uName = reGenerateUserName(form.getUserName());
				log.debug("Generated new UserName : "+ uName);
				form.setUserName(uName);
				isAlreadyExit = true;
			}else{
				isAlreadyExit= false;
			}
			
		} while (isAlreadyExit);
		log.debug("end of method getUserName");
		return form.getUserName();
	}
	
	public  static String reGenerateUserName(String userName) {
		int count =0;
		int position = 0;
		if(userName != null){
			if(userName.contains("0")){
				count = Integer.parseInt(userName.substring(userName.indexOf("0")))+1;
				position = userName.indexOf('0');
			}else if(userName.contains("1")){
				count = Integer.parseInt(userName.substring(userName.indexOf("1")))+1;
				position = userName.indexOf('1');
			}else if(userName.contains("2")){
				count = Integer.parseInt(userName.substring(userName.indexOf("2")))+1;
				position = userName.indexOf('2');
			}else if(userName.contains("3")){
				count = Integer.parseInt(userName.substring(userName.indexOf("3")))+1;
				position = userName.indexOf('3');
			}else if(userName.contains("4")){
				count = Integer.parseInt(userName.substring(userName.indexOf("4")))+1;
				position = userName.indexOf('4');
			}else if(userName.contains("5")){
				count = Integer.parseInt(userName.substring(userName.indexOf("5")))+1;
				position = userName.indexOf('5');
			}else if(userName.contains("6")){
				count = Integer.parseInt(userName.substring(userName.indexOf("6")))+1;
				position = userName.indexOf('6');
			}else if(userName.contains("7")){
				count = Integer.parseInt(userName.substring(userName.indexOf("7")))+1;
				position = userName.indexOf('7');
			}else if(userName.contains("8")){
				count = Integer.parseInt(userName.substring(userName.indexOf("8")))+1;
				position = userName.indexOf('8');
			}else if(userName.contains("9")){
				count = Integer.parseInt(userName.substring(userName.indexOf("9")))+1;
				position = userName.indexOf('9');
			}else{
				count=0;
			}
		}
		if(count == 0){
			userName +=count;
		}else{
			userName =userName.substring(0,position) + count;
		}
		return userName;
	}
}
