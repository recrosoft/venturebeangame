package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.ventureBean.dao.UserRoleDAO;
import com.ventureBean.model.UserRoleMaster;

@Repository(UserRoleDAOImpl.DAO_NAME)
public class UserRoleDAOImpl extends GenericDAOImpl<UserRoleMaster, Serializable> implements
		UserRoleDAO {

	/**
	 * @Injected userRoleDAO
	 */
	public static final String DAO_NAME = "userRoleDAO";

	public UserRoleDAOImpl() {
		super(UserRoleMaster.class);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.UserDAO#getUserRoles(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserRoleMaster> getUserRoles(String userName) {
		log.debug("start of method getUserRoles()");
		String hql =  " FROM UserRole where username =?";
		Query query = getSession().createQuery(hql);
		query.setParameter(0, userName);
		List<UserRoleMaster> userRoles = (List<UserRoleMaster>)query.list();
		log.debug("end of method getUserRoles()");
		return userRoles;
	}
	

}
