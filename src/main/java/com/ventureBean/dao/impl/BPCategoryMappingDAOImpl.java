package com.ventureBean.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.BPCategoryMappingDAO;
import com.ventureBean.form.BPCategoryMappingForm;
import com.ventureBean.model.BPCategoryMapping;
import com.ventureBean.util.Format;

@Repository(BPCategoryMappingDAOImpl.DAO_NAME)
public class BPCategoryMappingDAOImpl extends GenericDAOImpl<BPCategoryMapping, String>  implements BPCategoryMappingDAO{

	/**
	 * @Declared DAO_NAME
	 */
	public static final String DAO_NAME ="bPCategoryMappingDAO";

	 BPCategoryMappingDAOImpl(){
		 super(BPCategoryMapping.class);
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.BPCategoryMappingDAO#getMapping(com.ventureBean.form.BPCategoryMappingForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<BPCategoryMapping> getMapping( BPCategoryMappingForm form) {
		log.debug("start of method getMapping");
		StringBuffer hql = new StringBuffer("FROM BPCategoryMapping where 1=1");
		if(Format.isIntegerNotEmtyAndNotZero(form.getBpId())){
			hql.append(" and businessPartner.userId = "+form.getBpId());
		}
		if(Format.isIntegerNotEmtyAndNotZero(form.getGameId())){
			hql.append(" and game.gameId = "+form.getGameId());
		}
		log.debug("Query : "+ hql.toString());
		List<BPCategoryMapping> list = getSession().createQuery(hql.toString()).list();
		log.debug("end of method getMapping");
		return list;
	}
	
	
}
