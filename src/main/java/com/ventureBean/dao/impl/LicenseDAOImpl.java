package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.LicenseDAO;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.model.License;
import com.ventureBean.util.Format;

@Repository(LicenseDAOImpl.DAO_NAME)
public class LicenseDAOImpl extends GenericDAOImpl<License, Serializable> implements LicenseDAO{

	/**
	 * Declared licenseDAO name
	 */
	public static final String DAO_NAME="licenseDAO";
	
	public LicenseDAOImpl() {
		super(License.class);
	}

	@Override
	public List<License> getLicense(LicenseForm form) {
		log.debug("start of method getLicense ");
		StringBuffer hql =new StringBuffer(" FROM License l where 1=1 ");
	
		if(Format.isNotNull(form.getGameId())){
			hql.append(" and l.game.gameId = "+form.getGameId());
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(form.getBusinessPartnerId())){
			hql.append(" and l.businessPartner.userId = "+form.getBusinessPartnerId());
		}
		if(Format.isStringNotEmptyAndNotNull(form.getLicenseStatus())){
			hql.append(" and l.status = '"+form.getLicenseStatus()+"'");
		}
		@SuppressWarnings("unchecked")
		List<License>  licenses =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getLicense ");
		return licenses;
	}
}
