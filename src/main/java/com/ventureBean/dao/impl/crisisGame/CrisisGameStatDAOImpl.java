package com.ventureBean.dao.impl.crisisGame;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ventureBean.dao.crisisGame.CrisisGameStatDAO;
import com.ventureBean.dao.impl.GenericDAOImpl;
import com.ventureBean.form.GameStatForm;
import com.ventureBean.model.CrisisGameStatistics;
import com.ventureBean.util.Format;

@Repository(CrisisGameStatDAOImpl.DAO_NAME)
public class CrisisGameStatDAOImpl extends GenericDAOImpl<CrisisGameStatistics, Serializable> implements CrisisGameStatDAO {

	/**
	 * @Declared DAO_NAME
	 */
	public static final String DAO_NAME = "crisisGameStatDAO";
	
	CrisisGameStatDAOImpl(){
		super(CrisisGameStatistics.class);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.crisisGame.CrisisGameStatDAO#getGameStats(com.ventureBean.form.GameStatForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CrisisGameStatistics> getGameStats(GameStatForm gameStatForm) {
		log.debug("start of method  getGameStats");
		StringBuffer hql = new StringBuffer(" FROM CrisisGameStatistics cgs where 1=1 ");
		
		if(Format.isNotNull(gameStatForm.getGameUserId())){
			hql.append(" and cgs.gameUser.userId="+gameStatForm.getGameUserId());
		}
		
		if(Format.isNotNull(gameStatForm.getProjectId())){
			hql.append(" and cgs.project.projectId="+gameStatForm.getProjectId());
		}
		
		if(Format.isStringNotEmptyAndNotNull(gameStatForm.getTeamName())){
			hql.append(" and cgs.team.teamName ='"+gameStatForm.getTeamName()+"'");
		}
		
		if(Format.isNotNull(gameStatForm.getTeamId())){
			hql.append(" and cgs.team.teamId ="+gameStatForm.getTeamId());
		}
		if(Format.isNotNull(gameStatForm.getDayno())){
			hql.append(" and cgs.dayNo="+gameStatForm.getDayno().ordinal());
		}
		if(Format.isCollectionNotEmtyAndNotNull(gameStatForm.getStatusList())){
			/*String listValue="";
			for(int status : gameStatForm.getStatusList()){
				listValue += "'"+status+"' ";
			}*/
			String s = StringUtils.join(gameStatForm.getStatusList(),',');
			
			hql.append(" and cgs.status IN ("+s+")");
		}
		List<CrisisGameStatistics> gameStatistics =getSession().createQuery(hql.toString()).list();
		log.debug("end of method  getGameStats");
		return gameStatistics;
		
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.crisisGame.CrisisGameStatDAO#updateNextDay(com.ventureBean.form.GameStatForm)
	 */
	@SuppressWarnings("unused")
	@Override
	public void updateNextDay(GameStatForm statForm) {
		log.debug("start of method  updateNextDay");
		StringBuffer hql = new StringBuffer(" UPDATE  CrisisGameStatistics cgs ");
		if(Format.isNotNull(statForm.getStatus())){
			hql.append("set cgs.status="+statForm.getStatus().ordinal());
		}
		if(Format.isStringNotEmptyAndNotNull(statForm.getTeamName())){
			hql.append(" WHERE cgs.team.teamName='"+statForm.getTeamName().trim()+"'");
		}
		
		if(Format.isNotNull(statForm.getDayno())){
			hql.append(" and cgs.dayNo="+statForm.getDayno().ordinal());
		}
		Integer updateResut = getSession().createQuery(hql.toString()).executeUpdate();
		log.debug("end of method  updateNextDay");
		
	}
}
