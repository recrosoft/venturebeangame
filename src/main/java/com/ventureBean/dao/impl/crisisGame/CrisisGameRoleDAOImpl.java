package com.ventureBean.dao.impl.crisisGame;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.crisisGame.CrisisGameRoleDAO;
import com.ventureBean.dao.impl.GenericDAOImpl;
import com.ventureBean.model.CrisisGameRoleMaster;

@Repository(CrisisGameRoleDAOImpl.DAO_NAME)
public class CrisisGameRoleDAOImpl extends GenericDAOImpl<CrisisGameRoleMaster, Serializable> implements CrisisGameRoleDAO{

	/**
	 * Declared DAO_NAME
	 */
	public static final String DAO_NAME ="crisisGameRoleDAO";
	
	CrisisGameRoleDAOImpl(){
		super(CrisisGameRoleMaster.class);
	}
	
}
