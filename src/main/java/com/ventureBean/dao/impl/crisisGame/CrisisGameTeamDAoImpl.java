package com.ventureBean.dao.impl.crisisGame;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.crisisGame.CrisisGameTeamDAO;
import com.ventureBean.dao.impl.GenericDAOImpl;
import com.ventureBean.model.CrisisGameTeam;

@Repository()
public class CrisisGameTeamDAoImpl extends GenericDAOImpl<CrisisGameTeam, Serializable> implements CrisisGameTeamDAO{

	/**
	 * @Declared  DAO_NAME
	 */
	public static final String DAO_NAME="crisisGameTeamDAO";
	
	CrisisGameTeamDAoImpl(){
		super(CrisisGameTeam.class);
	}
}
