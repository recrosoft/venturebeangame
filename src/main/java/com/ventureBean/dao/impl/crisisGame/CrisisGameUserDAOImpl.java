package com.ventureBean.dao.impl.crisisGame;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.crisisGame.CrisisGameUserDAO;
import com.ventureBean.dao.impl.GenericDAOImpl;
import com.ventureBean.form.GameUserForm;
import com.ventureBean.model.CrisisGameUser;
import com.ventureBean.util.Format;

@Repository(CrisisGameUserDAOImpl.DAO_NAME)
public class CrisisGameUserDAOImpl extends GenericDAOImpl<CrisisGameUser, Serializable> implements CrisisGameUserDAO{

	/**
	 * @Declared DAO_NAME
	 */
	public static final String DAO_NAME="crisisGameUserDAO";
	
	CrisisGameUserDAOImpl(){
		super(CrisisGameUser.class);
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.crisisGame.CrisisGameUserDAO#getGameUser(com.ventureBean.form.GameUserForm)
	 */
	@Override
	public List<CrisisGameUser> getGameUser(GameUserForm gameUserForm) {
		log.debug("start of method getGameUser()");
		StringBuffer hql = new StringBuffer(" FROM CrisisGameUser where 1=1 ");
		
		if(Format.isStringNotEmptyAndNotNull(gameUserForm.getRoleName())){
			hql.append(" and role.roleName ='"+gameUserForm.getRoleName()+"'");
		}

		if(Format.isIntegerNotEmtyAndNotZero(gameUserForm.getProjectUserId())){
			hql.append(" and user.projectUserId ="+gameUserForm.getProjectUserId());
		}
		if(Format.isIntegerNotEmtyAndNotZero(gameUserForm.getProjectId())){
			hql.append(" and user.project.projectId ="+gameUserForm.getProjectId());
		}
		if(Format.isStringNotEmptyAndNotNull(gameUserForm.getTeamName())){
			hql.append(" and team.teamName ='"+gameUserForm.getTeamName()+"'");
		}
		@SuppressWarnings("unchecked")
		List<CrisisGameUser> userList =getSession().createQuery(hql.toString()).list();
		log.debug("start of method getGameUser()");
		return userList;
	}

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.crisisGame.CrisisGameUserDAO#removeUsers(com.ventureBean.form.GameUserForm)
	 */
	@Override
	public void removeUsers(GameUserForm form) {
		log.debug("start of method removeUsers");
		StringBuffer hql = new StringBuffer(" DELETE FROM CrisisGameUser");
		
		if(Format.isIntegerNotEmtyAndNotZero(form.getProjectId())){
			hql.append(" where project.projectId ="+form.getProjectId());
		}
		getSession().createQuery(hql.toString()).executeUpdate();
		log.debug("end of method removeUsers");
	}
	
}
