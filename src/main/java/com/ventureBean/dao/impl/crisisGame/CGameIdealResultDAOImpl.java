package com.ventureBean.dao.impl.crisisGame;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.crisisGame.CGameIdealResultDAO;
import com.ventureBean.dao.impl.GenericDAOImpl;
import com.ventureBean.model.CrisisGameIdealResult;

@Repository(CGameIdealResultDAOImpl.DAO_NAME)
public class CGameIdealResultDAOImpl extends GenericDAOImpl<CrisisGameIdealResult, Serializable> implements CGameIdealResultDAO{

	public static final String DAO_NAME = "cGameIdealResultDAO";
}
