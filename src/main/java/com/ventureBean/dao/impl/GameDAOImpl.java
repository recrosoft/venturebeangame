package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ventureBean.dao.GameDAO;
import com.ventureBean.form.CategoryForm;
import com.ventureBean.model.Category;
import com.ventureBean.model.Game;
import com.ventureBean.util.Format;

@Repository(GameDAOImpl.DAO_NAME)
public class GameDAOImpl extends GenericDAOImpl<Game, Serializable> implements GameDAO {

	/**
	 * Declared DAO name
	 */
	public static final String DAO_NAME = "gameDAO";
	
	public GameDAOImpl(){
		super(Game.class);
	}
	/*
	 (non-Javadoc)
	 * @see com.ventureBean.dao.GameDAO#getAllGames()
	 
	@SuppressWarnings("unchecked")
	@Override
	public List<Game> getAllGames() {
		log.debug("start of method getAllGames( )");
		String hql = "FROM GAME";
		List<Game> games = getSession().createQuery(hql).list();
		log.debug("end of method getAllGames( )");
		return games;
	}*/

	/* (non-Javadoc)
	 * @see com.ventureBean.dao.GameDAO#getCategory(com.ventureBean.form.GameForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Category> getCategory(CategoryForm categoryForm) {
		log.debug("start of method getCategory()");
		StringBuffer hql = new StringBuffer(" FROM Category c where 1=1 ");
		
		if(Format.isIntegerNotEmtyAndNotZero(categoryForm.getCategoryId())){
			hql.append("and categoryId ="+categoryForm.getCategoryId());
		}
		if(Format.isStringNotEmptyAndNotNull(categoryForm.getCategoryName())){
			hql.append("and categoryName ='"+categoryForm.getCategoryName()+"'");
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(categoryForm.getGameId())){
			hql.append("and game.gameId ="+categoryForm.getGameId());
		}
		List<Category> userList =getSession().createQuery(hql.toString()).list();
		return userList;
	}

}
