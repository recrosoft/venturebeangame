package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ventureBean.dao.GenericDAO;

@Repository("genericDAO")
public class GenericDAOImpl<T, ID extends Serializable> implements
		GenericDAO<T, ID> {

	/**
	 * Log variable for all child classes. Uses LogFactory.getLog(getClass())
	 * from Commons Logging
	 */
	protected final Log log = LogFactory.getLog(getClass());

	protected boolean isDebugEnabled = log != null ? log.isDebugEnabled()
			: false;
	protected boolean isInfoEnabled = log != null ? log.isInfoEnabled() : false;
	protected boolean isErrorEnabled = log != null ? log.isErrorEnabled()
			: false;
	protected boolean isWarnEnabled = log != null ? log.isWarnEnabled() : false;

	/** The persistent class. */
	private Class<T> persistentClass;

	/** The hibernate Session Factory. */
	@Autowired
	private SessionFactory sessionFactory;

	/**
	 * Instantiates a new generic dao.
	 */

	/**
	 * Constructor that takes in a class to see which type of entity to persist.
	 * Use this constructor when subclassing.
	 * 
	 * @param persistentClass
	 *            the class type you'd like to persist
	 */
	public GenericDAOImpl(final Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	public GenericDAOImpl() {

	}

	/**
	 * Create a new instance.
	 * 
	 * @return The new instance
	 */
	@SuppressWarnings("unchecked")
	public Class<T> newInstance() {
		Class<T> newInst = null;

		try {
			newInst = (Class<T>) this.persistentClass.newInstance();
		} catch (Exception e) {
			// LOG.error("Problem creating new instance of type: " +
			// this.persistentClass.newInstance());
		}

		return newInst;
	}

	/**
	 * Gets the persistent class.
	 * 
	 * @return the persistent class
	 */
	public Class<T> getPersistentClass() {
		return this.persistentClass;
	}

	/**
	 * Gets the session.
	 * 
	 * @return the session
	 */
	public Session getSession() {
		Session session = getSessionFactory().getCurrentSession();
		if (session == null) {
			session = getSessionFactory().openSession();
		}
		return session;
	}

	/**
	 * Find by id.
	 * 
	 * @param id
	 *            the id
	 * @param lock
	 *            the lock
	 * @return the t
	 */

	@SuppressWarnings("unchecked")
	public T findById(ID id, boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getSession().load(getPersistentClass(), id,
					LockOptions.UPGRADE);
		} else {
			entity = (T) getSession().load(getPersistentClass(), id);
		}
		return entity;
	}

	/**
	 * Gets the by id.
	 * 
	 * @param id
	 *            the id
	 * @param lock
	 *            the lock
	 * @return the by id
	 */

	@SuppressWarnings("unchecked")
	public T getById(ID id, boolean lock) {
		T entity;
		if (lock) {
			entity = (T) getSession().get(getPersistentClass(), id,
					LockOptions.UPGRADE);
		} else {
			entity = (T) getSession().get(getPersistentClass(), id);
		}
		return entity;
	}

	/**
	 * Save a new record.
	 * 
	 * @param entity
	 *            the entity
	 * @return the t
	 */

	public T create(T entity) {
		getSession().save(entity);
		return entity;
	}

	/**
	 * Save or update.
	 * 
	 * @param entity
	 *            the entity
	 * @return the t
	 */

	public T saveOrUpdate(T entity) {
		getSession().saveOrUpdate(entity);
		flush();
		return entity;
	}

	/**
	 * Removes the Record.
	 * 
	 * @param entity
	 *            the entity
	 */

	public void remove(T entity) {
		getSession().delete(entity);
		flush();
	}

	/**
	 * Flush.
	 * 
	 */

	public void flush() {
		getSession().flush();
	}

	/**
	 * Clear.
	 * 
	 */

	public void clear() {
		getSession().clear();
	}

	/**
	 * Use this inside subclasses as a convenience method.
	 * 
	 * @param criterion
	 *            the criterion
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	protected List<T> findByCriteria(Criterion... criterion) {
		try {
			Criteria crit = getSession().createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				crit.add(c);
			}
			return crit.list();
		} catch (Exception e) {

			if (isErrorEnabled)
				log.error("Error Occured.", e);
		}
		return null;
	}

	/**
	 * @return the sessionFactory
	 */
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	/**
	 * @param sessionFactory
	 *            the sessionFactory to set
	 */
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * @param entity
	 */
	public T merge(T entity) {
		getSession().merge(entity);
		return entity;
	}

	/**
	 * @param entity
	 */
	public T update(T entity) {
		getSession().update(entity);
		flush();
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> getAll(T t) {
		return (List<T>) getSession().createQuery(
				"From " + t.getClass().getSimpleName()).list();
	}

	@Override
	public T save(T entity) {
		getSession().save(entity);
		flush();
		return entity;
	}

}
