package com.ventureBean.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import com.ventureBean.constants.Constants;
import com.ventureBean.dao.AdminLoginDAO;
import com.ventureBean.form.AdminForm;
import com.ventureBean.form.UserSearchForm;
import com.ventureBean.model.User;
import com.ventureBean.model.UserRoleMaster;
import com.ventureBean.util.Format;



@Repository(AdminLoginDAOImpl.DAO_NAME)
public class AdminLoginDAOImpl extends GenericDAOImpl<User, Serializable> implements
		AdminLoginDAO {

	/**
	 * Declared DAO_NAME.
	 */
	public static final String DAO_NAME = "adminLoginDAO";

	/**
	 * Default constructor.
	 */
	AdminLoginDAOImpl() {
		super(User.class);
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.UserDAO#getByUserName(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public User getByUserName(String userName) {
		log.debug("start of method getByUserName()");
		User user = null;
		String hql = "FROM User where emailAddress='"+userName+"'";
		List<User> userList =getSession().createQuery(hql).list();
		if(Format.isCollectionNotEmtyAndNotNull(userList)){
			user= userList.get(Constants.GET_ZERO_INDEX_OBJECT);
		}
		log.debug("end of method getByUserName()");
		return  user;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.UserDAO#getUser(com.ventureBean.form.UserForm)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUser(AdminForm form) {
		log.debug("start of method getUser()");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(form.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+form.getFieldsToSelect()+" FROM User where 1=1 ";
			System.out.println(hqlwithProjection);

			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM User u where 1=1 ");
		}
	
		if(Format.isStringNotEmptyAndNotNull(form.getEmailAddress())){
			hql.append("and emailAddress ='"+form.getEmailAddress()+"'");
		}
		if(Format.isStringNotEmptyAndNotNull(form.getPassword())){
			hql.append("and password ='"+form.getPassword()+"'");
		}
		if(Format.isIntegerNotEmtyAndNotZero(form.getUserRoleId())){
			hql.append("and userRole.userRoleId ="+form.getUserRoleId());
		}
		if(Format.isStringNotEmptyAndNotNull(form.getRoleName())){
			hql.append("and userRole.roleName ='"+form.getRoleName()+"'");
		}
		if(Format.isIntegerNotEmtyAndNotZero(form.getSuperUserId())){
			hql.append("and assignTo ="+form.getSuperUserId());
		}
		List<User> userList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getUser()");
		return userList;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.AdminLoginDAO#getAllLicensor()
	 */
	@SuppressWarnings( "unchecked")
	@Override
	public List<User> getAllLicensor() {
		log.debug("start of method getAllLicensor()");
//		select a from Author as a join a.Book as ab where ab.AuthorId like '%"hello"%';
		String hql =" SELECT users FROM User users  JOIN users.userRole urole WHERE urole.userRoleId = 2";
		List<User> licensors =(List<User>) getSession().createQuery(hql).list();
		log.debug("end of method getAllLicensor()");
		return licensors;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.AdminLoginDAO#getAllRoles()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<UserRoleMaster> getAllRoles() {
		log.debug("start of method getAllRoles()");
		String hql = " FROM UserRole";
		Query query = getSession().createQuery(hql);
		log.debug("end of method getAllRoles()");
		return (List<UserRoleMaster>)query.list();
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.AdminLoginDAO#getUserWithOneSelect(com.ventureBean.form.UserSearchForm)
	 */
	@Override
	public List<User> getUserWithOneSelect(UserSearchForm searchForm) {
		log.debug("start of method getUserWithOneSelect()");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(searchForm.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+searchForm.getFieldsToSelect()+" FROM User u where 1=1 ";
			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM User u where 1=1 ");
		}
		
		if(Format.isStringNotEmptyAndNotNull(searchForm.getSelectedRole())){
			hql.append("and u.userRole.roleName ='"+searchForm.getSelectedRole()+"'");
		}
		
		if(Format.isIntegerNotEmtyAndNotZero(searchForm.getCurrentUserId())){
			hql.append("and u.assignTo ="+searchForm.getCurrentUserId());
		}
		log.debug("query : "+hql.toString());
		@SuppressWarnings("unchecked")
		List<User> userList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getUserWithOneSelect()");
		return userList;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.AdminLoginDAO#getUserWithTwoSelect(com.ventureBean.form.UserSearchForm)
	 */
	@Override
	public List<User> getUserWithTwoSelect(UserSearchForm searchForm) {
		log.debug("start of method getUserWithTwoSelect()");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(searchForm.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+searchForm.getFieldsToSelect()+" FROM User u where 1=1 ";
			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM User u where 1=1");
		}
		
		if(Format.isStringNotEmptyAndNotNull(searchForm.getSelectedRole())){
			hql.append(" and u.userRole.roleName ='"+searchForm.getSelectedRole()+"'");
		}
		if(Format.isIntegerNotEmtyAndNotZero(searchForm.getCurrentUserId())){
			hql.append(" and u.assignTo IN (SELECT child.userId FROM User child where child.assignTo="+searchForm.getCurrentUserId()+")");
		}
		log.debug("query : "+hql.toString());
		@SuppressWarnings("unchecked")
		List<User> userList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getUserWithTwoSelect()");
		return userList;
	}
	
	/* (non-Javadoc)
	 * @see com.ventureBean.dao.AdminLoginDAO#getUserWithThreeSelect(com.ventureBean.form.UserSearchForm)
	 */
	@Override
	public List<User> getUserWithThreeSelect(UserSearchForm searchForm) {
		log.debug("start of method getUserWithThreeSelect()");
		StringBuffer hql = null;
		if(Format.isStringNotEmptyAndNotNull(searchForm.getFieldsToSelect())){
			String hqlwithProjection = "SELECT "+searchForm.getFieldsToSelect()+" FROM User u where 1=1 ";
			hql = new StringBuffer(hqlwithProjection);
		}else{
			 hql = new StringBuffer(" FROM User u where 1=1");
		}
		
		if(Format.isStringNotEmptyAndNotNull(searchForm.getSelectedRole())){
			hql.append(" and u.userRole.roleName ='"+searchForm.getSelectedRole()+"'");
		}
		if(Format.isIntegerNotEmtyAndNotZero(searchForm.getCurrentUserId())){
			hql.append(" and u.assignTo IN (SELECT child.userId FROM User child where child.assignTo IN (SELECT child2.userId FROM User child2 where child2.assignTo="+searchForm.getCurrentUserId()+"))");
		}
		log.debug("query : "+hql.toString());
		@SuppressWarnings("unchecked")
		List<User> userList =getSession().createQuery(hql.toString()).list();
		log.debug("end of method getUserWithThreeSelect()");
		return userList;
	}

}
