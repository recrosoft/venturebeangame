package com.ventureBean.dao;

import java.util.List;

import com.ventureBean.form.BPCategoryMappingForm;
import com.ventureBean.model.BPCategoryMapping;

public interface BPCategoryMappingDAO extends GenericDAO<BPCategoryMapping, String>{

	/**Will return BPMapping List 
	 * @param bPCategoryMappingForm
	 * @return
	 */
	List<BPCategoryMapping> getMapping(BPCategoryMappingForm  bPCategoryMappingForm);
	
}
