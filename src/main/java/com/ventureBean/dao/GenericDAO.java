package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;



public interface GenericDAO<T, ID extends Serializable> {


    /**
     * This method will return the Hibernate proxy object. <br />
     * Data inside might have to be initialised within the same session and will not be accessible. <br />
     *
     * @param id the id
     * @param lock the lock
     * @return the t
     */
    T findById(ID id, boolean lock);

    /**
     * This method will never return a hibernate proxy object.
     *
     * @param id the id
     * @param lock the lock
     * @return the by id
     */
    T getById(ID id, boolean lock);

    /**
     * Find all.
     *
     * @return the list
     */
   

    /**
     * Save or update.
     *
     * @param entity the entity
     * @return the t
     */
    T saveOrUpdate(T entity);
    
    /**
     * create and Save the Entity.
     *
     * @param entity the entity
     * @return the t
     */
    T create(T entiry);

    /**
     * Removes the.
     *
     * @param entity the entity
     */
    void remove(T entity);

    /**
     * Flush.
     */
    void flush();

    /**
     * Clear.
     */
    void clear();
    
    /**
     * @param entity
     */
    public T merge(T entity);
    
    /**
     * @param entity
     */
    public T update(T entity);
    
    /**@author Satyajit
     * @param t
     * @return
     */
    List<T> getAll(T t);

    /**
     * @param entity
     * @return
     */
    T save(T entity);

}
