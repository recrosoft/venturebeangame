package com.ventureBean.dao;

import java.io.Serializable;

import com.ventureBean.model.AppConfiguration;

public interface AppConfigurationDAO extends GenericDAO<AppConfiguration, Serializable> {
	
	/**@author Satyajit
	 * Will return Configuration value based on config Name
	 * @param configName
	 * @return
	 */
	AppConfiguration getAppConfigurationByName (String configName);
}
