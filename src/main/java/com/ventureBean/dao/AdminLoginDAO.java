package com.ventureBean.dao;

import java.io.Serializable;
import java.util.List;

import com.ventureBean.form.AdminForm;
import com.ventureBean.form.UserSearchForm;
import com.ventureBean.model.User;
import com.ventureBean.model.UserRoleMaster;


public interface AdminLoginDAO extends GenericDAO<User, Serializable> {

	/**@author Satyajit
	 * Will return user(s) based on the condition provided.
	 * @param form
	 * @return List<User>
	 */
	List<User> getUser(AdminForm form);

	/**@author Satyajit
	 * This returns all the BP present in this system.
	 * @return
	 */
	List<User> getAllLicensor();

	/**This will return all the roles 
	 * @return
	 */
	List<UserRoleMaster> getAllRoles();
	
	/**This is a utility method that will return the User by querying the one inner query
	 * @param searchForm
	 * @return
	 */
	List<User> getUserWithOneSelect(UserSearchForm searchForm);
	
	/**This is a utility method that will return the User by querying the 2 inner query
	 * @param searchForm
	 * @return
	 */
	List<User> getUserWithTwoSelect(UserSearchForm searchForm);
	
	/**This is a utility method that will return the User by querying the 3 inner query
	 * @param searchForm
	 * @return
	 */
	List<User> getUserWithThreeSelect(UserSearchForm searchForm);

	/**@author Satyajit
	 * This method will return User by username
	 * @param createdBy
	 * @return 
	 */
	User getByUserName(String createdBy);
}
