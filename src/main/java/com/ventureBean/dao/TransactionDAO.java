package com.ventureBean.dao;

import java.util.List;

import com.ventureBean.form.TransactionForm;
import com.ventureBean.model.UserTransaction;

public interface TransactionDAO  extends GenericDAO<UserTransaction, String>{

	/**@author Satyajit
	 * This will return Transaction(s) based on the parameters provided
	 * @param transactionForm
	 * @return
	 */
	List<UserTransaction> getTransactions(TransactionForm transactionForm);

	
}
