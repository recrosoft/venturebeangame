package com.ventureBean.exception;

public class ProjectNotInCreatedStateException extends BaseExceptions {

	/**
	 * @Generated serialVersionUID
	 */
	private static final long serialVersionUID = -3752862064073322405L;

	public ProjectNotInCreatedStateException(){}
	
	public ProjectNotInCreatedStateException(String msg ){
		super(msg);
	}
	
	public ProjectNotInCreatedStateException(Throwable throwable){
		super(throwable);
	}
	
	public ProjectNotInCreatedStateException( String msg , Throwable throwable){
		super(msg , throwable);
	}
}
