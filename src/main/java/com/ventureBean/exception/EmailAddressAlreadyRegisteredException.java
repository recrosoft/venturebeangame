package com.ventureBean.exception;

public class EmailAddressAlreadyRegisteredException extends BaseExceptions {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailAddressAlreadyRegisteredException(String msg) {
		super(msg);
	}

	public EmailAddressAlreadyRegisteredException(Throwable e) {
		super(e);
	}

	public EmailAddressAlreadyRegisteredException(String msg, Throwable e) {
		super(msg,e);
	}

	public EmailAddressAlreadyRegisteredException() {
		super();
	}
	
}
