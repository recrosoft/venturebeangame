package com.ventureBean.exception;

import java.io.Serializable;

public class CrisisGameLoginException extends BaseExceptions implements Serializable  {

	/**
	 * @Declared serialVersionUID
	 */
	private static final long serialVersionUID = -4006514822751339632L;

	public CrisisGameLoginException(){}
	
	public CrisisGameLoginException(String msg){
		super(msg);
	}
	
	public CrisisGameLoginException(Throwable throwable){
		super(throwable);
	}
	
	public CrisisGameLoginException(String msg , Throwable throwable){
		super(msg,throwable);
	}
}
