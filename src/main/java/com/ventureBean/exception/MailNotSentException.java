package com.ventureBean.exception;
public class MailNotSentException extends BaseExceptions{

	private static final long serialVersionUID = 7832737117963456839L;

	public MailNotSentException(String msg) {
		super(msg);
	}

	public MailNotSentException(Throwable e) {
		super(e);
	}

	public MailNotSentException(String msg, Throwable e) {
		super(msg,e);
	}

	public MailNotSentException() {
		super();
	}
	
}
