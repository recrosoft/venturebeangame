package com.ventureBean.exception;

public class RoleNotAssignedException extends BaseExceptions{

	/**
	 * @Declared serialVersionUID
	 */
	private static final long serialVersionUID = -456522822751339632L;

	public RoleNotAssignedException(){}
	
	public RoleNotAssignedException(String msg){
		super(msg);
	}
	
	public RoleNotAssignedException(Throwable throwable){
		super(throwable);
	}
	
	public RoleNotAssignedException(String msg , Throwable throwable){
		super(msg,throwable);
	}
}
