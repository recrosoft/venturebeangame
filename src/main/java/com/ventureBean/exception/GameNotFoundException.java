package com.ventureBean.exception;

public class GameNotFoundException extends BaseExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = 100L;
	
	public GameNotFoundException(String msg) {
		super(msg);
	}

	public GameNotFoundException(Throwable e) {
		super(e);
	}

	public GameNotFoundException(String msg, Throwable e) {
		super(msg,e);
	}

	public GameNotFoundException() {
		super();
	}

}
