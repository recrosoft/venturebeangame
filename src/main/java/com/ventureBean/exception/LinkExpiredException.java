package com.ventureBean.exception;


public class LinkExpiredException extends BaseExceptions {

	/**
	 * Declared serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	public LinkExpiredException(){}
	
	public LinkExpiredException(Throwable e){
		super(e);
	}
	
	public LinkExpiredException(String msg){
		super(msg);
	}
	
	public LinkExpiredException( String msg , Throwable e){
		super(msg,e);
	}
}
