package com.ventureBean.exception;

import com.ventureBean.exception.BaseExceptions;

public class StartDateNotAchivedException extends BaseExceptions {

	/**
	 * @Generated serialVersionUID
	 */
	private static final long serialVersionUID = 845760393387054053L;
	
	
	public StartDateNotAchivedException(){}
	
	public StartDateNotAchivedException(String msg){
		super(msg);
	}
	
	public StartDateNotAchivedException(String msg , Throwable throwable){
		super(msg , throwable);
	}
	
	public StartDateNotAchivedException(Throwable throwable){
		super(throwable);
	}
	
}
