package com.ventureBean.exception;

public class GameNotCompletedException extends BaseExceptions {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7327350379119290138L;
	
	public GameNotCompletedException(){}
	
	public GameNotCompletedException(String msg){
		super(msg);
	}
	
	public GameNotCompletedException(Throwable throwable){
		super(throwable);
	}
	
	public GameNotCompletedException(String msg , Throwable throwable){
		super(msg,throwable);
	}

}
