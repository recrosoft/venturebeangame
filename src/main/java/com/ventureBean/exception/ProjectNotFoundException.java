package com.ventureBean.exception;

public class ProjectNotFoundException  extends BaseExceptions{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ProjectNotFoundException() {
		super();
	}
	public ProjectNotFoundException(String msg) {
		super(msg);
	}

	public ProjectNotFoundException(Throwable e) {
		super(e);
	}

	public ProjectNotFoundException(String msg, Throwable e) {
		super(msg,e);
	}

	
	
}