package com.ventureBean.exception;

import org.springframework.security.core.AuthenticationException;

public class UserBadCredentialsException extends AuthenticationException{

	public UserBadCredentialsException(String msg) {
		super(msg);
	}
	public UserBadCredentialsException(String msg , Throwable  throwable) {
		super(msg,throwable);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 4018675844673555157L;

}
