package com.ventureBean.exception;

import org.springframework.security.core.AuthenticationException;

public class AdminBadCredentialsException extends AuthenticationException{

	public AdminBadCredentialsException(String msg) {
		super(msg);
	}
	public AdminBadCredentialsException(String msg , Throwable  throwable) {
		super(msg,throwable);
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 5135828418716516510L;

}
