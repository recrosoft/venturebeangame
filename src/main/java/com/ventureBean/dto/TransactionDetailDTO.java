package com.ventureBean.dto;

public class TransactionDetailDTO {

	private Integer trnxId;
	
	private String bpName;
	
	private String gameName;
	
	private String licenseType;
	
	private Integer licenseCount;
	
	
	
	private Long amount;

	private String categoryName;

	

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getBpName() {
		return bpName;
	}

	public void setBpName(String bpName) {
		this.bpName = bpName;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public String getLicenseType() {
		return licenseType;
	}

	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}

	public Integer getLicenseCount() {
		return licenseCount;
	}

	public void setLicenseCount(Integer licenseCount) {
		this.licenseCount = licenseCount;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Integer getTrnxId() {
		return trnxId;
	}

	public void setTrnxId(Integer trnxId) {
		this.trnxId = trnxId;
	}

public TransactionDetailDTO() {
	// TODO Auto-generated constructor stub
}
}
