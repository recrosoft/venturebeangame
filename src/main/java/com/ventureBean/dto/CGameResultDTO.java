package com.ventureBean.dto;

public class CGameResultDTO {

	private Integer livesSaved;
	
	private Integer cost;
	
	private String dayNO;

	public CGameResultDTO() {
		this.livesSaved = 0;
		this.cost = 0;
	}
	public CGameResultDTO(String day) {
		this.livesSaved = 0;
		this.dayNO = day;
		this.cost = 0;
	}
	public Integer getLivesSaved() {
		return livesSaved;
	}

	public void setLivesSaved(Integer livesSaved) {
		this.livesSaved = livesSaved;
	}

	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public String getDayNO() {
		return dayNO;
	}

	public void setDayNO(String dayNO) {
		this.dayNO = dayNO;
	}
	
}
