package com.ventureBean.dto;

import java.util.List;

/**
 * @author Satyajit
 *
 */
public class GenericDTO {

	@SuppressWarnings("rawtypes")
	private List list ;

	private String successOrErrorMsg;
	
	private Double ppLicense ;
	
	@SuppressWarnings("rawtypes")
	public List getList() {
		return list;
	}

	public void setList(@SuppressWarnings("rawtypes") List list) {
		this.list = list;
	}

	public String getSuccessOrErrorMsg() {
		return successOrErrorMsg;
	}

	public void setSuccessOrErrorMsg(String successOrErrorMsg) {
		this.successOrErrorMsg = successOrErrorMsg;
	}

	public Double getPpLicense() {
		return ppLicense;
	}

	public void setPpLicense(Double ppLicense) {
		this.ppLicense = ppLicense;
	}
	
}
