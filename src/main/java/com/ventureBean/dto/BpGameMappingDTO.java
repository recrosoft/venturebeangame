package com.ventureBean.dto;

public class BpGameMappingDTO {

	private Integer categoryId;
	
	private String categoryName;
	
	private Integer gameId ;
	
	private String gameName;
	
	private Integer totalLicense;
	
	private Integer usedLicense;
	
	private Integer availableLicense;

	private String gameLicensemap;
	
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getGameId() {
		return gameId;
	}

	public void setGameId(Integer gameId) {
		this.gameId = gameId;
	}

	public String getGameName() {
		return gameName;
	}

	public void setGameName(String gameName) {
		this.gameName = gameName;
	}

	public Integer getTotalLicense() {
		return totalLicense;
	}

	public void setTotalLicense(Integer totalLicense) {
		this.totalLicense = totalLicense;
	}

	public Integer getUsedLicense() {
		return usedLicense;
	}

	public void setUsedLicense(Integer usedLicense) {
		this.usedLicense = usedLicense;
	}

	public Integer getAvailableLicense() {
		return availableLicense;
	}

	public void setAvailableLicense(Integer availableLicense) {
		this.availableLicense = availableLicense;
	}

	public String getGameLicensemap() {
		return gameLicensemap;
	}

	public void setGameLicensemap(String gameLicensemap) {
		this.gameLicensemap = gameLicensemap;
	}
	
}
