package com.ventureBean.dto;


import com.ventureBean.model.CrisisGameTeam;

public class ResourceGameStructereDTO {
	
	private Integer noOfResource;
	
	private CrisisGameTeam team;
	
	private String reSourceName;

	public Integer getNoOfResource() {
		return noOfResource;
	}

	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}

	public CrisisGameTeam getTeam() {
		return team;
	}

	public void setTeam(CrisisGameTeam team) {
		this.team = team;
	}

	public String getReSourceName() {
		return reSourceName;
	}

	public void setReSourceName(String reSourceName) {
		this.reSourceName = reSourceName;
	}



}
