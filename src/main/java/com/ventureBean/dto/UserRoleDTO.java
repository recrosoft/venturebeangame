package com.ventureBean.dto;

public class UserRoleDTO {

	private String userRoleId ;
	
	private String roleName;

	public String getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(String userRoleId) {
		this.userRoleId = userRoleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		if(roleName != null){
			this.roleName = roleName.replaceAll("_", " ");
		}else{
			this.roleName = roleName;
		}
		
	}
	
	@Override
	public String toString() {
		return "UserRoleDTO [roleId=" + userRoleId + ", roleName=" + roleName + "]";
	}
}
