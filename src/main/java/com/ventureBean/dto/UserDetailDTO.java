package com.ventureBean.dto;

public class UserDetailDTO {

	
	private String name;
	
	private int userId;
	
	private String emailAddress;
	
	private String phone;
	
	private String comesUnder;
	
	private Double wallet;
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getComesUnder() {
		return comesUnder;
	}

	public void setComesUnder(String comesUnder) {
		this.comesUnder = comesUnder;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Double getWallet() {
		return wallet;
	}

	public void setWallet(Double wallet) {
		this.wallet = wallet;
	}

}
