package com.ventureBean.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ventureBean.enums.CrisisGameDayEnum;
import com.ventureBean.enums.CrisisGameDayStatusEnum;

@Entity
@Table(name="tbl_crisis_game_stats")
public class CrisisGameStatistics implements Serializable{

	/**
	 * Declared serialVersionUID
	 */
	private static final long serialVersionUID = 3463461L;

	@Id
	@Column(name="game_stats_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer gameStatId;
	

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "project_id")
	private Project project;
	
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "crisis_game_user_id")
	private CrisisGameUser gameUser;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "team_id")
	private CrisisGameTeam team;
	
	@Column(name="lives_saved")
	private Integer livesSaved;

	@Column(name="lives_lost")
	private Integer livesLost;

	
	@Column(name="money_spent")
	private Double moneySpent;

	@Column(name="camps_built")
	private Integer campsBuilt;

	@Column(name="resources_used")
	private Integer resourcesUsed;


	@Column(name="resources_name")
	private String resourcesName;
	
	@Column(name="survivors_Count")
	private Integer survivorsCount;

	@Column(name="day_no")
	private CrisisGameDayEnum dayNo;

	@Column(name="start_time")
	private Date startTime;

	@Column(name="status")
	private CrisisGameDayStatusEnum status;
	
	@Column(name="note")
	private String note;
	
	@Column(name="mission_name")
	private String missionName;
	
	@Column(name="resource_given")
	private List<ResourceGameStructere> resourcesGiven;
	

	@Column(name="unused_resource")
	private List<ResourceGameStructere> unusedResources;
	
	@Column(name="resources_recived")
	private List<ResourceGameStructere> resourcesRecived;
	

	public Integer getGameStatId() {
		return gameStatId;
	}
	
	public String getMissionName() {
		return missionName;
	}

	public void setMissionName(String missionName) {
		this.missionName = missionName;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public List<ResourceGameStructere> getResourcesGiven() {
		return resourcesGiven;
	}

	public void setResourcesGiven(List<ResourceGameStructere> resourcesGiven) {
		this.resourcesGiven = resourcesGiven;
	}

	public List<ResourceGameStructere> getUnusedResources() {
		return unusedResources;
	}

	public void setUnusedResources(List<ResourceGameStructere> unusedResources) {
		this.unusedResources = unusedResources;
	}

	public List<ResourceGameStructere> getResourcesRecived() {
		return resourcesRecived;
	}
	
	public void setResourcesRecived(List<ResourceGameStructere> resourcesRecived) {
		this.resourcesRecived = resourcesRecived;
	}



	public String getResourcesName() {
		return resourcesName;
	}

	public void setResourcesName(String resourcesName) {
		this.resourcesName = resourcesName;
	}

	public void setGameStatId(Integer gameStatId) {
		this.gameStatId = gameStatId;
	}

	public CrisisGameUser getGameUser() {
		return gameUser;
	}

	public void setGameUser(CrisisGameUser gameUser) {
		this.gameUser = gameUser;
	}

	public Integer getLivesSaved() {
		return livesSaved;
	}

	public void setLivesSaved(Integer livesSaved) {
		this.livesSaved = livesSaved;
	}

	public Integer getLivesLost() {
		return livesLost;
	}

	public void setLivesLost(Integer livesLost) {
		this.livesLost = livesLost;
	}

	public Double getMoneySpent() {
		return moneySpent;
	}

	public void setMoneySpent(Double moneySpent) {
		this.moneySpent = moneySpent;
	}

	public Integer getCampsBuilt() {
		return campsBuilt;
	}

	public void setCampsBuilt(Integer campsBuilt) {
		this.campsBuilt = campsBuilt;
	}

	public Integer getResourcesUsed() {
		return resourcesUsed;
	}

	public void setResourcesUsed(Integer resourcesUsed) {
		this.resourcesUsed = resourcesUsed;
	}

	public Integer getSurvivorsCount() {
		return survivorsCount;
	}

	public void setSurvivorsCount(Integer survivorsCount) {
		this.survivorsCount = survivorsCount;
	}


	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public CrisisGameDayStatusEnum getStatus() {
		return status;
	}

	public void setStatus(CrisisGameDayStatusEnum status) {
		this.status = status;
	}

	public CrisisGameDayEnum getDayNo() {
		return dayNo;
	}

	public void setDayNo(CrisisGameDayEnum dayNo) {
		this.dayNo = dayNo;
	}

	public CrisisGameTeam getTeam() {
		return team;
	}

	public void setTeam(CrisisGameTeam team) {
		this.team = team;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

}
