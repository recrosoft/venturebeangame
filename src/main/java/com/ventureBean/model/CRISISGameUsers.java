package com.ventureBean.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CRISISGameUsers implements Serializable {
	
	/**
	 * @Seclared serialVersionUID
	 */
	private static final long serialVersionUID = 1224L;

	@Id
	@Column(name="crisis-user-id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int crisisUsersId;
	
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="modified_by")
	private String modifiedBy;
	
	@Column(name="modified_on")
	private Date modifiedOn;
	
}
