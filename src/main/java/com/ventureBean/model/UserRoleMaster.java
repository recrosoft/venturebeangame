package com.ventureBean.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user_role_master")
public class UserRoleMaster implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 55L;
	@Id
	@Column(name="user_role_id")
	private Integer userRoleId;
	
	@Column(name="role_name")
	private String roleName;
	
	public Integer getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	
}
