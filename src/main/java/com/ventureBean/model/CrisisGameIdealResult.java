package com.ventureBean.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tlb_cg_ideal_result")
public class CrisisGameIdealResult implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5683322941004408741L;

	@Id
	@Column(name="result_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer resultId;
	
	@Column(name="live_saved")
	private Integer livesSaved;
	
	@Column(name="cost")
	private Integer cost;
	
	@Column(name="day_no")
	private String dayNo;
	
	@Column(name="team_name")
	private String teamName;

	public Integer getResultId() {
		return resultId;
	}

	public void setResultId(Integer resultId) {
		this.resultId = resultId;
	}

	public Integer getCost() {
		return cost;
	}

	public void setCost(Integer cost) {
		this.cost = cost;
	}

	public Integer getLivesSaved() {
		return livesSaved;
	}

	public void setLivesSaved(Integer livesSaved) {
		this.livesSaved = livesSaved;
	}

	public String getDayNo() {
		return dayNo;
	}

	public void setDayNo(String dayNo) {
		this.dayNo = dayNo;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	
}
