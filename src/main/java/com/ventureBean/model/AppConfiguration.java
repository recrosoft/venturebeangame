package com.ventureBean.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="app_configuration")
public class AppConfiguration {
	
	@Id
	@Column(name="app_config_Id")
	public Integer appConfigurationId;
	
	@Column(name="app_config_name")
	public String appConfigurationName;
	
	@Column(name="app_config_value")
	public String appConfigurationValue;

	@Column(name="created_on")
	private Date createdOn;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="modified_by")
	private String modifiedBy;
	

	@Column(name="modified_on")
	private Date modifiedOn;


	public Integer getAppConfigurationId() {
		return appConfigurationId;
	}


	public void setAppConfigurationId(Integer appConfigurationId) {
		this.appConfigurationId = appConfigurationId;
	}


	public String getAppConfigurationName() {
		return appConfigurationName;
	}


	public void setAppConfigurationName(String appConfigurationName) {
		this.appConfigurationName = appConfigurationName;
	}


	public String getAppConfigurationValue() {
		return appConfigurationValue;
	}


	public void setAppConfigurationValue(String appConfigurationValue) {
		this.appConfigurationValue = appConfigurationValue;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public Date getModifiedOn() {
		return modifiedOn;
	}


	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	
	
}
