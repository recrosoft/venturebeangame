package com.ventureBean.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="category")
public class Category implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 10L;

	@Id
	@Column(name="category_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer categoryId;
	
	@Column(name="category_name")
	private String categoryName;
	
	@Column(name="description")
	private String description;
	
	@Column(name="price_per_license")
	private Double pricePerLicense;
	
	@Column(name="free_license")
	private Integer freeLicense;
	
	@Column(name="threshold_vale")
	private Integer threshold;
	
	@OneToOne
	@JoinColumn(name="game_id")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Game game;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPricePerLicense() {
		return pricePerLicense;
	}

	public void setPricePerLicense(Double pricePerLicense) {
		this.pricePerLicense = pricePerLicense;
	}

	public Integer getFreeLicense() {
		return freeLicense;
	}

	public void setFreeLicense(Integer freeLicense) {
		this.freeLicense = freeLicense;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}
}
