package com.ventureBean.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="licenses")
public class License implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 111L;

	@Id
	@Column(name="license_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer licenseId;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="game_id" ,insertable=true , updatable=false)
	private Game game;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="bp_id" ,insertable=true , updatable=false)
	private User businessPartner;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="player_id" ,insertable=true , updatable=false)
	private ProjectUser player;
	
	@Column(name="lisence_code")
	private String licenseCode;
	
	@Column(name="lisence_status")
	private String status;
	
	@Column(name="created_on")
	private Date createdOn;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="expiry_date")
	private Date expiryDate;
	

	@Column(name="modified_on")
	private Date modifiedOn;
	
	@Column(name="modified_by")
	private String modifiedBy;
	
	@Column(name="type")
	private String type;
	
	@Column(name="price")
	private float price;
	
	public Integer getLicenseId() {
		return licenseId;
	}

	public void setLicenseId(Integer licenseId) {
		this.licenseId = licenseId;
	}

	public String getLicenseCode() {
		return licenseCode;
	}

	public void setLicenseCode(String licenseCode) {
		this.licenseCode = licenseCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public User getBusinessPartner() {
		return businessPartner;
	}

	public void setBusinessPartner(User businessPartner) {
		this.businessPartner = businessPartner;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public ProjectUser getPlayer() {
		return player;
	}

	public void setPlayer(ProjectUser player) {
		this.player = player;
	}

	public boolean equals(Object obj){
		return this.licenseId.equals(((License)obj).getLicenseId());
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getModifiedOn() {
		return modifiedOn;
	}

	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
