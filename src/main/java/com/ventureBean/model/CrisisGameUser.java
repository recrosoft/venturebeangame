package com.ventureBean.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="tbl_crisis_game_user")
public class CrisisGameUser implements Serializable{

	public Project getProject() {
		return project;
	}


	public void setProject(Project project) {
		this.project = project;
	}


	/**
	 * @Declared serialVersionUID
	 */
	private static final long serialVersionUID = -2344925813317876709L;

	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "role_id")
	private CrisisGameRoleMaster role;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "team_id")
	private CrisisGameTeam team;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "project_user_id" , insertable= true , updatable =false)
	private ProjectUser user;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "project_id" , insertable= true , updatable =false)
	private Project project;

	@Column(name="created_on")
	private Date createdOn;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="modified_by")
	private String modifiedBy;

	@Column(name="modified_on")
	private Date modifiedOn;


	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}


	public CrisisGameRoleMaster getRole() {
		return role;
	}


	public void setRole(CrisisGameRoleMaster role) {
		this.role = role;
	}


	public CrisisGameTeam getTeam() {
		return team;
	}


	public void setTeam(CrisisGameTeam team) {
		this.team = team;
	}


	public ProjectUser getUser() {
		return user;
	}


	public void setUser(ProjectUser user) {
		this.user = user;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public Date getModifiedOn() {
		return modifiedOn;
	}


	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
}
