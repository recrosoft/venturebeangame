package com.ventureBean.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="resourse_game_structer")
public class ResourceGameStructere  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="no_ofResource")
	private Integer noOfResource;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "team_id")
	private CrisisGameTeam team;
	
	@Column(name="reSource_Name")
	private String reSourceName;
	
	public Integer getNoOfResource() {
		return noOfResource;
	}
	public void setNoOfResource(Integer noOfResource) {
		this.noOfResource = noOfResource;
	}
	
	public String getReSourceName() {
		return reSourceName;
	}
	public void setReSourceName(String reSourceName) {
		this.reSourceName = reSourceName;
	}
	public CrisisGameTeam getTeam() {
		return team;
	}
	public void setTeam(CrisisGameTeam team) {
		this.team = team;
	}
	
	
	
	
}
