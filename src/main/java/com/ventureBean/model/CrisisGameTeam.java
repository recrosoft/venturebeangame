package com.ventureBean.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tbl_crisis_game_team")
public class CrisisGameTeam implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 123L;

	@Id
	@Column(name="team_id")
	private Integer teamId;
	
	@Column(name="team_name")
	private String teamName;

	@Column(name="team_color")
	private String teamColor;

		
	public Integer getTeamId() {
		return teamId;
	}

	public void setTeamId(Integer teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public String getTeamColor() {
		return teamColor;
	}

	public void setTeamColor(String teamColor) {
		this.teamColor = teamColor;
	}
}
