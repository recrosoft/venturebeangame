package com.ventureBean.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="player")
public class Player implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 222L;

	@Id
	@Column(name="player_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int playerId;
	
	@Column(name="player_name")
	private String playerName;
	
	@Column(name="player_username")
	private String playerUserName;
	
	@Column(name="created_on")
	private Date cretaedOn;
	
	@Column(name="plyaer_password")
	private String playerPassword;

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getPlayerUserName() {
		return playerUserName;
	}

	public void setPlayerUserName(String playerUserName) {
		this.playerUserName = playerUserName;
	}

	public Date getCretaedOn() {
		return cretaedOn;
	}

	public void setCretaedOn(Date cretaedOn) {
		this.cretaedOn = cretaedOn;
	}

	public String getPlayerPassword() {
		return playerPassword;
	}

	public void setPlayerPassword(String playerPassword) {
		this.playerPassword = playerPassword;
	}
	
	
}
