package com.ventureBean.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="project")
public class Project implements Serializable{

	/**
	 * @Declared serialVersionUID
	 */
	private static final long serialVersionUID = 21L;

	@Id
	@Column(name="project_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer projectId;
	
	@Column(name="no_of_players")
	private Integer noOfPlayers;
	
	@Column(name="project_status")
	private String projectStatus;

	@Column(name="hr_email")
	private String hrEmail;
	
	@Column(name="hr_name")
	private String hrName;
	
	@Column(name="facilitator_email")
	private String facilitatorEmail;
	
	@Column(name="facilitator_name")
	private String facilitatorName;
	
	@Column(name="project_start_date")
	private Date startDate;
	
	@Column(name="created_on")
	private Date createdOn;
	
	@Column(name="created_by")
	private String createdBy;

	@Column(name="modified_on")
	private Date modifiedOn;
	
	@Column(name="modified_by")
	private String modifiedBy;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name="bp_id")
	private User businessPartner;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="game_id")
	private Game game;
	
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name="company_id")
	private Company company;


	public Integer getProjectId() {
		return projectId;
	}


	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}


	public Integer getNoOfPlayers() {
		return noOfPlayers;
	}


	public void setNoOfPlayers(Integer noOfPlayers) {
		this.noOfPlayers = noOfPlayers;
	}


	public String getProjectStatus() {
		return projectStatus;
	}


	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}


	public String getHrEmail() {
		return hrEmail;
	}


	public void setHrEmail(String hrEmail) {
		this.hrEmail = hrEmail;
	}


	public String getHrName() {
		return hrName;
	}


	public void setHrName(String hrName) {
		this.hrName = hrName;
	}


	public String getFacilitatorEmail() {
		return facilitatorEmail;
	}


	public void setFacilitatorEmail(String facilitatorEmail) {
		this.facilitatorEmail = facilitatorEmail;
	}


	public String getFacilitatorName() {
		return facilitatorName;
	}


	public void setFacilitatorName(String facilitatorName) {
		this.facilitatorName = facilitatorName;
	}


	public Date getStartDate() {
		return startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public Date getModifiedOn() {
		return modifiedOn;
	}


	public void setModifiedOn(Date modifiedOn) {
		this.modifiedOn = modifiedOn;
	}


	public String getModifiedBy() {
		return modifiedBy;
	}


	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}


	public User getBusinessPartner() {
		return businessPartner;
	}


	public void setBusinessPartner(User businessPartner) {
		this.businessPartner = businessPartner;
	}


	public Game getGame() {
		return game;
	}


	public void setGame(Game game) {
		this.game = game;
	}


	public Company getCompany() {
		return company;
	}


	public void setCompany(Company company) {
		this.company = company;
	}
	

}
