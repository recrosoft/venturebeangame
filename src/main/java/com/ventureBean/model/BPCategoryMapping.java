package com.ventureBean.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="business_partner_category_mapping")
public class BPCategoryMapping implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 111L;

	@Id
	@Column(name="bp_category_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer bpCategoryId;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "bp_id")
	private User businessPartner;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "game_id")
	private Game game;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "category_id")
	private Category category;
	
	@Column(name="free_license")
	private Integer freeLicense;
	
	@Column(name="paid_license")
	private Integer paidLicense;

	public Integer getBpCategoryId() {
		return bpCategoryId;
	}

	public void setBpCategoryId(Integer bpCategoryId) {
		this.bpCategoryId = bpCategoryId;
	}

	public User getBusinessPartner() {
		return businessPartner;
	}

	public void setBusinessPartner(User businessPartner) {
		this.businessPartner = businessPartner;
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Integer getFreeLicense() {
		return freeLicense;
	}

	public void setFreeLicense(Integer freeLicense) {
		this.freeLicense = freeLicense;
	}

	public Integer getPaidLicense() {
		return paidLicense;
	}

	public void setPaidLicense(Integer paidLicense) {
		this.paidLicense = paidLicense;
	}
	
}
