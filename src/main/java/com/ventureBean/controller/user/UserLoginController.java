package com.ventureBean.controller.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ventureBean.constants.Constants;
import com.ventureBean.constants.CrisisGameConstants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.CrisisGameStatDTO;
import com.ventureBean.enums.CrisisGameDayStatusEnum;
import com.ventureBean.enums.ProjectStatusEnum;
import com.ventureBean.exception.CrisisGameLoginException;
import com.ventureBean.exception.LinkExpiredException;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.UserForm;
import com.ventureBean.model.CrisisGameStatistics;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.model.Project;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.service.CrisisGameService;
import com.ventureBean.service.UserLoginService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;

@Controller
public class UserLoginController extends BaseController{

	@Autowired
	private UserLoginService userLoginService;
	
	/**
	 * @Injected crisisGameService
	 */
	@Autowired
	private CrisisGameService  crisisGameService;
	
	@RequestMapping("/userlogin")
	public ModelAndView userLogin(AdminDetailsSessionForm adminDetailsSessionForm , String key , Integer projectId){
		log.debug("start of method userLogin()");
		ModelAndView modelAndView = null;
		try {
			if(userLoginService.ValidateAccessToken(key)){
				modelAndView = new ModelAndView("showLoginPage");
				modelAndView.addObject("userType",Constants.USER_TYPE_USER);
				if(Format.isNotNull(key)){
					modelAndView.addObject("accessToken", key);
				}
			}
		}catch(LinkExpiredException ex){
			log.error("LinkExpired Exception : Link got Expired");
			modelAndView = new ModelAndView("vbErrorPage");
			modelAndView.addObject("msg", "Your link has expired");
		}catch(NoRecordsFoundException ex){
			log.error("Exception Occured : No records found with this link");
			modelAndView = new ModelAndView("vbErrorPage");
			//saveErrorMessage(getText("error.rest.noRecords"));
			modelAndView.addObject("msg", "Your link has expired");
		}catch (Exception e) {
			log.error("Exception Occured : ", e);
			modelAndView = new ModelAndView("vbErrorPage");
			modelAndView.addObject("msg", getText("error.common.tryLater"));
		}
	   addSuccessOrErrorMessageToModel(modelAndView);
	   log.debug("end of method userLogin()");
	   return modelAndView;
	}
	
	/**@author Satyajit
	 * This service will redirect the user to the respective DashBoard RoleBased
	 * @param request
	 * @return ModelAndView 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/userShowHome")
	public ModelAndView userShowHome(HttpServletRequest request , AdminDetailsSessionForm sessionForm){
		log.debug("start of method userShowHome()");
		ModelAndView modelAndView = null;
			try {
				Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
				final String currentUser =authentication.getName();
				log.debug("Current user  : " + currentUser );
				// get the user and set the userId in AdminDetailsSessionForm for future use.
				UserForm userForm = new UserForm();
				userForm.setUserName(currentUser);
				//userForm.setPassword(encoder.encodePassword((String)authentication.getCredentials() ,null));
				ProjectUser user = userLoginService.getUser(userForm);
				List<GrantedAuthority> authorityList = (List<GrantedAuthority>)authentication.getAuthorities();
				String roleName = authorityList.get(Constants.GET_ZERO_INDEX_OBJECT).getAuthority();
				AdminDetailsSessionForm adminDetailsSessionForm = (AdminDetailsSessionForm) request.getSession(false).getAttribute("adminDetailsSessionForm");
				if ( adminDetailsSessionForm == null ) {
					adminDetailsSessionForm = new AdminDetailsSessionForm();
				} 
					adminDetailsSessionForm.setUserName(currentUser);
					adminDetailsSessionForm.setUserRole(roleName);
					adminDetailsSessionForm.setUserId(user.getProjectUserId());
					adminDetailsSessionForm.setUrlSpecRoleName(ConverterUtil.getUrlSpecRole(roleName));
					request.getSession(false).setAttribute("adminDetailsSessionForm", adminDetailsSessionForm);
					// here we are redirecting the user to the dashBoard based on role.
					modelAndView = userDashboad(adminDetailsSessionForm , user , null);
			} catch (Exception e) {
				log.error("exception occured : ",e);
				modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
			}
		log.debug("end of method userShowHome()");
    	return modelAndView;
	}

	@RequestMapping("/user/dashboard")
	public ModelAndView userDashboad(AdminDetailsSessionForm adminDetailsSessionForm, ProjectUser user , RedirectAttributes redirectAttributes) {
		log.debug("start of method userDashboad");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			switch (adminDetailsSessionForm.getUserRole()) {
				case Constants.FACILITATOR_ROLE : modelAndView = facilitatorDashBaord(user , adminDetailsSessionForm);break;
				case Constants.PLAYER_ROLE : modelAndView = userDashboard(user , adminDetailsSessionForm);break;
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		log.debug("end of method userDashboad");
		return modelAndView;
	}

	private ModelAndView userDashboard(ProjectUser user, AdminDetailsSessionForm adminDetailsSessionForm) {
		log.debug("start of method userDashboard ");
		ModelAndView modelAndView = new ModelAndView("leaderHome");
		try {
			if(Format.isNull(user.getProjectUserId())){
				UserForm userForm = new UserForm();
				userForm.setUserName(adminDetailsSessionForm.getUserName());
			    user = userLoginService.getUser(userForm);
			}
			if(userLoginService.validateTeamLeader(user.getProject().getProjectId())){
				log.debug("User validated successfully "+user.getName());
				List<CrisisGameStatistics> statistics = crisisGameService.getGameStatsByTeam(user.getProjectUserId());
				checkRefreshValue(modelAndView, statistics);
				List<CrisisGameStatDTO> gameStatDTOs = ConverterUtil.converToCrisisGameStatDTO(statistics);
				modelAndView.addObject("gameStats", gameStatDTOs);
			}
		}catch(CrisisGameLoginException ex){
			log.error("CrisisGameLoginException : "+ ex.getMessage());
			modelAndView = new ModelAndView("gameErrorPage");
			modelAndView.addObject("msg", ex.getMessage());
			if(ex.getMessage().equals(CrisisGameConstants.GAME_NOT_STARTED))
			modelAndView.addObject("refreshButton","Y");
		}catch (Exception e) {
			log.error("Exception Occured : ", e);
			modelAndView = new ModelAndView("gameErrorPage");
			modelAndView.addObject("msg", getText("error.common.tryLater"));
		}
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("start of method userDashboard ");
		return modelAndView;
	}

	private void checkRefreshValue(ModelAndView modelAndView,
			List<CrisisGameStatistics> statistics) {
		int completedDayCount = 0;
		int activeDayCount = 0;
		String buttonVal = "N";
		if(Format.isNotNull(statistics)){
			for(CrisisGameStatistics statistic:statistics){
				if(statistic.getStatus().equals(CrisisGameDayStatusEnum.COMPLETED)){
					completedDayCount++;
				}else if(statistic.getStatus().equals(CrisisGameDayStatusEnum.ACTIVE)){
					activeDayCount++;
				}
			}
			if((completedDayCount == Constants.ZERO && activeDayCount == Constants.ZERO)|| (completedDayCount == 3 && activeDayCount == Constants.ZERO)){
				buttonVal = "Y";
			}
			modelAndView.addObject("refreshButton",buttonVal);
		}
		log.debug("refreshButton : "+buttonVal);
	}

	private ModelAndView facilitatorDashBaord(ProjectUser user , AdminDetailsSessionForm detailsSessionForm) {
		log.debug("start of method facilitatorDashBaord");
		Project project = null;
		boolean startSession2= false;
		boolean endGame = false;
		ModelAndView modelAndView = new ModelAndView("facilitatorDashboard");
		try {
			if(Format.isNotNull(user.getProjectUserId())){
				project = user.getProject();
			}else{
				UserForm userForm = new UserForm();
				userForm.setUserName(detailsSessionForm.getUserName());
				 user = userLoginService.getUser(userForm);
				 if(Format.isNotNull(user)){
					 project = user.getProject(); 
				 }
			}
		detailsSessionForm.setProjectId(project.getProjectId());
		modelAndView.addObject("facilitator", user);
		modelAndView.addObject("game", ConverterUtil.convertProjectToForm(project));
		log.debug("project Status : "+project.getProjectStatus());
		if(Format.isStringNotEmptyAndNotNull(project.getProjectStatus()) && !(project.getProjectStatus().equals(ProjectStatusEnum.CREATED.toString())) ){
			List<CrisisGameStatistics>  gameStatistics = crisisGameService.getGameStats(detailsSessionForm.getProjectId());
			List<CrisisGameStatDTO> gameStatDTOs = ConverterUtil.converToCrisisGameStatDTO(gameStatistics);
			Map<String ,List<CrisisGameStatDTO>>  teamStatMap = new HashMap<>();
			List<CrisisGameTeam> teams = crisisGameService.getAllTeam();
			modelAndView.addObject("teams", teams);
			for(CrisisGameTeam team : teams){
				teamStatMap.put(team.getTeamName().trim(), new ArrayList<CrisisGameStatDTO>() );
			}
			for(CrisisGameStatDTO statDTO : gameStatDTOs){
				String teamName = statDTO.getTeamName();
				switch(teamName){
				case "Alpha" : teamStatMap.get(teamName).add(statDTO);break;
				case "Bravo" : teamStatMap.get(teamName).add(statDTO);break;
				case "Charlie" : teamStatMap.get(teamName).add(statDTO);break;
				case "Delta" : teamStatMap.get(teamName).add(statDTO);break;
				case "Echo" : teamStatMap.get(teamName).add(statDTO);break;
				}
			}
			// check whether to start Session2 or not
			Iterator<String> itr = teamStatMap.keySet().iterator();
			while(itr.hasNext()){
				List<CrisisGameStatDTO> statList = teamStatMap.get(itr.next());
				int completedStatCount =Constants.ZERO;
				for(CrisisGameStatDTO statDTO : statList ){
					if(statDTO.getStatus().equals(CrisisGameDayStatusEnum.COMPLETED.toString())){
						completedStatCount++;
					}
				}
				if(completedStatCount == Constants.THREE){
					startSession2 = true;
				}else if(completedStatCount == Constants.SIX){
					endGame = true;
				}else{
					startSession2 = false;
					endGame = false;
					break;
				}
			}
			if(endGame == startSession2)
				{
				startSession2 = false;
				endGame = false;
				}
			modelAndView.addObject("startSession2" , startSession2);
			modelAndView.addObject("endGame" , endGame);
			modelAndView.addObject("teamStatMap", teamStatMap);
		}
		} catch (Exception e) {
			log.error("Exception Occured : ",e);
			saveErrorMessage(getText("error.facilitator.dashboard"));
		}
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("end of method facilitatorDashBaord");
		return modelAndView;
	}
	
	/**@author Satyajit
	 * This will redirect user to the loginPage with error msg.
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping("/userLoginFailure")
	public ModelAndView userloginFail(AdminDetailsSessionForm adminDetailsSessionForm){
		log.debug("start of method userloginFail()");
		ModelAndView modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		modelAndView.addObject("error","error");
		String msg = getText("error.user.login.fail");
		saveErrorMessage(msg);
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("end of method userloginFail()");
		return modelAndView;
	}
	
	@RequestMapping("/user/logout")
	public ModelAndView logoutUser( HttpServletRequest request , HttpServletResponse response){
		log.debug("start of method logoutUser()");
		
		CookieClearingLogoutHandler cookieClearingLogoutHandler = new CookieClearingLogoutHandler(AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY);
		SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
		cookieClearingLogoutHandler.logout(request, response, null);
		securityContextLogoutHandler.logout(request, response, null);
		log.debug( " ---isInvalidateHttpSession--> " + securityContextLogoutHandler.isInvalidateHttpSession());
		if (securityContextLogoutHandler.isInvalidateHttpSession()) {
			SecurityContextHolder.getContext().setAuthentication(null);
			SecurityContextHolder.getContextHolderStrategy().clearContext();
			securityContextLogoutHandler.setClearAuthentication(true);
			securityContextLogoutHandler.setInvalidateHttpSession(true);
		}
		HttpSession session =request.getSession(false);
		if(session!= null)
		{
			request.getSession(false).setAttribute("userDetailsFormSession", null);
			session.invalidate();
		}
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
	
		log.debug("end of method logoutUser()");
		return redirectToLoginPage(Constants.USER_TYPE_USER);
	}
	/*
	@RequestMapping("/userLogin")
	public ModelAndView userLoginPage(AdminDetailsSessionForm adminDetailsSessionForm){
		log.debug("start of method userLoginPage()");
		ModelAndView modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		log.debug("end of method userLoginPage()");
		return modelAndView;
	}*/
	
}
