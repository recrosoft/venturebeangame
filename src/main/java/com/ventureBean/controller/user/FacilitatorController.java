package com.ventureBean.controller.user;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.PlayerDTO;
import com.ventureBean.dto.crisis.CrisisGameUserDTO;
import com.ventureBean.exception.EmailAddressAlreadyRegisteredException;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.ProjectNotInCreatedStateException;
import com.ventureBean.exception.RoleNotAssignedException;
import com.ventureBean.exception.StartDateNotAchivedException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.form.TeamPlayerMappingForm;
import com.ventureBean.form.UserForm;
import com.ventureBean.form.UserListForm;
import com.ventureBean.model.Company;
import com.ventureBean.model.CrisisGameRoleMaster;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.model.CrisisGameUser;
import com.ventureBean.model.Project;
import com.ventureBean.model.ProjectUser;
import com.ventureBean.service.CrisisGameService;
import com.ventureBean.service.GameService;
import com.ventureBean.service.UserLoginService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;

@Controller
public class FacilitatorController extends BaseController{

	/**
	 * @Injected userLoginService
	 */
	@Autowired
	private UserLoginService  userLoginService;
	
	/**
	 * @Injected gameService
	 */
	@Autowired
	private GameService gameService;
	
	/**
	 * @injected crisisGameService
	 */
	@Autowired
	private CrisisGameService crisisGameService;
	
	
	@RequestMapping("user/showUsers")
	public ModelAndView showUsers(AdminDetailsSessionForm adminDetailsSessionForm ,UserListForm userListForm , UserForm userForm){
		log.debug("start of method showUsers");
		ModelAndView modelAndView = null;
		Project project = null;
		Company company = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView("createPlayer");
			try {
				//userForm.setFieldsToSelect("projectUserId , name , emailAddress ");
				userForm.setProjectId(adminDetailsSessionForm.getProjectId());
				List<CrisisGameUser> players = crisisGameService.getGameUsers(userForm);
				List<UserForm> playerDTOs = ConverterUtil.convertToCrisisGameUsers(players);
				
				UserForm form = new UserForm();
				form.setProjectId(adminDetailsSessionForm.getProjectId());
				form.setUserName(adminDetailsSessionForm.getUserName());
				
				 ProjectUser user = userLoginService.getUser(form);
				 if(Format.isNotNull(user)){
					  project = user.getProject();
					  System.out.println(project);
					  company = project.getCompany();
					  System.out.println(company);
				 }
			List<CrisisGameRoleMaster> gameRoleMasters = crisisGameService.getAllGameRoles();
			modelAndView.addObject("gameRoles", gameRoleMasters);
			List<CrisisGameTeam> teams = crisisGameService.getAllTeam();
			modelAndView.addObject("teams", teams);	 
			modelAndView.addObject("userForms" , playerDTOs);	 
			modelAndView.addObject("totalNoOfPlayer" , project.getNoOfPlayers());
			modelAndView.addObject("emailSuffix", company.getEmailSuffix());
			modelAndView.addObject("projectStatus", project.getProjectStatus());
			userListForm.setUserForm(playerDTOs);
			} catch (Exception e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("end of method showUsers");
		return modelAndView;
	}
	
	/*@RequestMapping("user/editPlayer")
	public @ResponseBody DatatableJsonResponse editPlayer( PlayerForm playerForm , HttpServletRequest  request ){
		log.debug("start of method editPlayer");
		DatatableJsonResponse jsonResponse = new DatatableJsonResponse();
		log.debug("end of method editPlayer");
		List<PlayerDTO> dtos = new ArrayList<>();
		PlayerDTO playerDTO = new PlayerDTO();
		playerDTO.setEmailAddress("asb@gmail.com");
		playerDTO.setName("ABC");
		playerDTO.setPlayerId(1000);
		dtos.add(playerDTO);
		jsonResponse.setData(dtos);
		return jsonResponse;user/showAssignDesignation
	}*/
	
	@RequestMapping("user/showAssignDesignation")
	public ModelAndView showAssignDesignation(AdminDetailsSessionForm adminDetailsSessionForm  ){
		log.debug("start of method showAssignDesignation");
		ModelAndView modelAndView = null;
		Map<String,Map<String,CrisisGameUserDTO>> teamUserMap = new HashMap<>();
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView("assignDesignations");
			try {
				List<CrisisGameRoleMaster> gameRoleMasters = crisisGameService.getAllGameRoles();
				modelAndView.addObject("gameRoles", gameRoleMasters);
				List<CrisisGameTeam> teams = crisisGameService.getAllTeam();
				modelAndView.addObject("teams", teams);
				
				UserForm form = new UserForm();
				form.setFieldsToSelect("projectUserId , name , emailAddress");
				form.setProjectId(adminDetailsSessionForm.getProjectId());
				List<ProjectUser> players = userLoginService.getPlayerList(form);
				List<PlayerDTO> playerDTOs = ConverterUtil.convertProjectUserToPlayer(players , true);
				modelAndView.addObject("playerList", playerDTOs);
				
				form.setProjectId(adminDetailsSessionForm.getProjectId());
				List<CrisisGameUser> gameUsers = crisisGameService.getAssignedPlayers(form);
				List<CrisisGameUserDTO> userDTOs = ConverterUtil.convertToCrisisGameUserDTO(gameUsers);
				if(Format.isCollectionNotEmtyAndNotNull(gameUsers)){
					for(CrisisGameTeam team : teams){
						teamUserMap.put(team.getTeamName().trim(), new HashMap<String,CrisisGameUserDTO>());
					}
					for(CrisisGameUserDTO user : userDTOs){
						String teamName = user.getTeamName().trim();
						String roleName = user.getRoleName().toLowerCase();
						 switch(teamName.trim()){
						 case "Alpha" : (teamUserMap.get(teamName)).put(roleName, user); break;
						 case "Bravo" : teamUserMap.get(teamName).put(roleName, user); break;
						 case "Charlie" : teamUserMap.get(teamName).put(roleName, user); break;
						 case "Delta" : teamUserMap.get(teamName).put(roleName, user); break;
						 case "Echo" : teamUserMap.get(teamName).put(roleName, user); break;
						 }
					}
					
				}
				modelAndView.addObject("teamUserMap", teamUserMap);
				
				// get game status : if Started then role can't be reassigned 
				Project project = crisisGameService.getProjectById(adminDetailsSessionForm.getProjectId());
				if(Format.isNotNull(project)){
					modelAndView.addObject("projectStatus", project.getProjectStatus());
				}
			} catch (Exception e) {
				log.error("exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("end of method showAssignDesignation");
		return modelAndView;
	}
	
	@RequestMapping("user/addUsers")
	public ModelAndView addEditUser(AdminDetailsSessionForm adminDetailsSessionForm , @ModelAttribute("userListForm")UserListForm userListForm , RedirectAttributes redirectAttributes ){
		log.debug("start of method showAddEditUser");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("showUsers"));
			try {
				userListForm.setProjectId(adminDetailsSessionForm.getProjectId());
				userListForm.setCreatedBy(adminDetailsSessionForm.getUserName());
				userLoginService.addEditPlayers(userListForm);
				saveSuccessMessage(getText("success.add.player"));
			} catch(EmailAddressAlreadyRegisteredException e){
				log.error("EmailAddressAlreadyRegisteredException : ",e);
				saveErrorMessage(getText("error.adduser.duplicateEmail")+e.getMessage());
			}catch (Exception e) {
				log.error("exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method showAddEditUser");
		return modelAndView;
	}
	
	@RequestMapping("user/startGame")
	public ModelAndView startGame(AdminDetailsSessionForm adminDetailsSessionForm , ProjectForm form , RedirectAttributes attributes){
		log.debug("start of method startGame");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("dashboard"));
			try {
				form.setProjectId(adminDetailsSessionForm.getProjectId());
				gameService.startProject(form);
				saveSuccessMessage(getText("success.project.start"));
			}catch (StartDateNotAchivedException e) {
				log.error("cant start before start date : ",e);
				saveErrorMessage(getText("error.project.start.satrtdatenotachived"));
			}catch (ProjectNotInCreatedStateException e) {
				log.error("Exception : ",e);
				saveErrorMessage(getText("error.project.start.statusnotCreated"));
			}catch (RoleNotAssignedException e) {
				log.error("Exception : ",e);
				saveErrorMessage(getText("error.project.start.roleNotAssigned"));
			}catch (Exception e) {
				log.error("exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , attributes);
		log.debug("end of method startGame");
		return modelAndView;
	}
	
	@RequestMapping("user/assignDesignation")
	public ModelAndView assignDesignation(AdminDetailsSessionForm adminDetailsSessionForm ,@ModelAttribute("userListForm")TeamPlayerMappingForm teamPlayerMappingForm , RedirectAttributes redirectAttributes){
		log.debug("start of method assignDesignation");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("dashboard"));
			try {
				teamPlayerMappingForm.setCreatedby(adminDetailsSessionForm.getUserName());
				teamPlayerMappingForm.setProjectId(adminDetailsSessionForm.getProjectId());
				crisisGameService.assignDesignation(teamPlayerMappingForm);
				saveSuccessMessage(getText("success.project.roleassogn"));
			}catch (MailNotSentException e) {
				log.error("exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}catch (Exception e) {
				log.error("exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method assignDesignation");
		return modelAndView;
	}
	
	/*
	@RequestMapping(value = "/user/getPlayerList")
	public @ResponseBody
	DatatableJsonResponse getPlayerList( UserForm form) {
		log.debug("Start of method getPlayerList");
		form.setFieldsToSelect("projectUserId , name , emailAddress ");
		List<ProjectUser> palyers = userLoginService.getPlayerList(form);
		DatatableJsonResponse jsonResponse = new DatatableJsonResponse();
		jsonResponse.setData(ConverterUtil.convertProjectUserToPlayer(palyers));
		log.debug("End of method getPlayerList");
		return jsonResponse;
	}*/
}
