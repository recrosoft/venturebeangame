package com.ventureBean.controller.user;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.CGameResultDTO;
import com.ventureBean.exception.GameNotCompletedException;
import com.ventureBean.exception.ProjectNotFoundException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.GameStatForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.model.CrisisGameIdealResult;
import com.ventureBean.model.CrisisGameTeam;
import com.ventureBean.service.CrisisGameService;
import com.ventureBean.util.Format;

@Controller
public class CrisisGameController extends BaseController {

	/**
	 * @injected crisisGameService
	 */
	@Autowired
	private CrisisGameService crisisGameService;
	
	@RequestMapping("/user/recordGameStat")
	public ModelAndView recordGameStat(AdminDetailsSessionForm adminDetailsSessionForm, GameStatForm statForm , RedirectAttributes redirectAttributes){
		log.debug("start of method recordGameStat");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			try {
				crisisGameService.recordGameStats(statForm);
				modelAndView = new ModelAndView(new RedirectView("dashboard"));
			} catch (Exception e) {
				log.error("Exception occured : ",e);
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method recordGameStat");
		return modelAndView;
	}
	
	@RequestMapping("/user/resetDay")
	public ModelAndView resetDay(AdminDetailsSessionForm adminDetailsSessionForm, GameStatForm statForm , RedirectAttributes redirectAttributes){
		log.debug("start of method resetDay");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("dashboard"));
			try {
				crisisGameService.reSetDay(statForm);
				saveSuccessMessage(getText("success.project.day.reset"));
			} catch (Exception e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.project.day.reset"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method resetDay");
		return modelAndView;
	}
	
	@RequestMapping("/user/startSession2")
	public ModelAndView startSession2(AdminDetailsSessionForm adminDetailsSessionForm, GameStatForm statForm , RedirectAttributes redirectAttributes){
		log.debug("start of method startSession2");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("dashboard"));
			try {
				crisisGameService.startSession2(statForm);
				saveSuccessMessage(getText("success.project.start.session2"));
			} catch (Exception e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method startSession2");
		return modelAndView;
	}
	
	@RequestMapping("/user/endGame")
	public ModelAndView endGame(AdminDetailsSessionForm adminDetailsSessionForm, ProjectForm projectForm , RedirectAttributes redirectAttributes){
		log.debug("start of method endGame");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("dashboard"));
			try {
				crisisGameService.endGame(projectForm);
				saveSuccessMessage(getText("success.project.end"));
			} catch (GameNotCompletedException e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.project.end"));
			} catch (Exception e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method endGame");
		return modelAndView;
	}

	@RequestMapping("/user/viewGameResult")
	public ModelAndView viewCompletedGame(AdminDetailsSessionForm adminDetailsSessionForm, ProjectForm projectForm , RedirectAttributes redirectAttributes){
		log.debug("start of method viewCompletedGame");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView("viewGameStat");
			try {
				Map<String, List<CGameResultDTO>> teamStatMap = crisisGameService.getGameResult(projectForm);
				modelAndView.addObject("teamStatMap", teamStatMap);
				Map<String, List<CrisisGameIdealResult>> idealResultMap = crisisGameService.getIdealResult();
				modelAndView.addObject("idealResultMap", idealResultMap); 
				List<CrisisGameTeam> teams = crisisGameService.getAllTeam();
				modelAndView.addObject("teams", teams);
			} catch (ProjectNotFoundException e) {
				modelAndView = new ModelAndView(new RedirectView("dashboard"));
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			} catch (Exception e) {
				modelAndView = new ModelAndView(new RedirectView("dashboard"));
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.common.tryLater"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_USER);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method viewCompletedGame");
		return modelAndView;
	}
	
}
