package com.ventureBean.controller.admin;

import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.security.web.authentication.rememberme.AbstractRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.BpGameMappingDTO;
import com.ventureBean.exception.MailNotSentException;
import com.ventureBean.exception.NoRecordsFoundException;
import com.ventureBean.exception.UserBadCredentialsException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.AdminForm;
import com.ventureBean.form.ChangePasswordForm;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.form.RoleForm;
import com.ventureBean.model.User;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.service.GameService;
import com.ventureBean.service.LicenseService;
import com.ventureBean.service.UserRoleService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;

@Controller
public class AdminLoginController extends BaseController{

	/**
	 * @injected userLoginService
	 */
	@Autowired
	private AdminLoginService adminLoginService;
	
	
	@Autowired
	private UserRoleService userRoleService;
	
	/**
	 * 
	 */
	@Autowired
	private GameService  gameService;
	
	@Autowired
	private LicenseService licenseService;
	
	/**
	 * Instantiated resourceBundle.
	 */
	private ResourceBundle resourceBundle = ResourceBundle.getBundle("ApplicationResources");
	
	@RequestMapping("/adminLogin")
	public ModelAndView adminLoginPage(AdminDetailsSessionForm adminDetailsSessionForm){
		log.debug("start of method adminLogin()");
		ModelAndView modelAndView = new ModelAndView("showLoginPage");
		modelAndView.addObject("userType", Constants.USER_TYPE_ADMIN);
		log.debug("end of method adminLogin()");
		return modelAndView;
	}
	
	
	/*@RequestMapping(value={"/vbadminHome"})
	public ModelAndView vbAdminHome(AdminDetailsSessionForm adminDetailsSessionForm){
		log.debug("start of method adminShowHome()");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView("adminHome");
		}else{
			modelAndView = redirectToLoginPage();
		}
		log.debug("end of method adminShowHome()");
		return modelAndView;
	}
	*/
	@RequestMapping(value={"/dashboard"})
	public ModelAndView dashboad(AdminDetailsSessionForm adminDetailsSessionForm, User user ){
		log.debug("start of method dashboard()");
		ModelAndView modelAndView = new ModelAndView("");;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			switch(adminDetailsSessionForm.getUserRole())
			{
				case Constants.SADMIN_ROLE : modelAndView = new ModelAndView("sadminDashboard");break;
				case Constants.ADMIN_ROLE : modelAndView = new ModelAndView("vbDashboard");break;
				case Constants.BH_ROLE : modelAndView = new ModelAndView("bhDashboard");break;
				case Constants.BM_ROLE : modelAndView = new ModelAndView("managerDashboard");break;
				case Constants.BDM_ROLE : modelAndView = new ModelAndView("bdmDashboard");break;
				case Constants.BP_ROLE :modelAndView = getBPDashBoard(adminDetailsSessionForm , user);break;
				default : modelAndView =redirectToLoginPage(Constants.USER_TYPE_ADMIN);
			}
			//List<UserRole> userRoles = userRoleService.getRoles();
			//modelAndView.addObject("roleList", convertRoleToDTO(userRoles));
			/*CategoryType[] packages = CategoryType.values();
			modelAndView.addObject("packages", packages);*/
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		modelAndView.addObject("roleForm", new RoleForm());
		log.debug("end of method dashboard()");
		return modelAndView;
	}
	
	/**@author Satyajit
	 * This method will return BP Dashboard Data
	 * @return
	 */
	private ModelAndView getBPDashBoard( AdminDetailsSessionForm sessionForm , User user) {
		log.debug("start of method getBPDashBoard");
		ModelAndView modelAndView = new ModelAndView("bpDashboard");
		if(Format.isNull(user) || Format.isStringEmptyOrNull(user.getEmailAddress())){
			user = adminLoginService.getById(sessionForm.getUserId(), true);
		}
		modelAndView.addObject("bpDetails",ConverterUtil.convertUserToDTO(user));
		/*BPCategoryMappingForm mappingForm = new BPCategoryMappingForm();
		mappingForm.setBpId(user.getUserId());
		List<BPCategoryMapping> gameDetails = gameService.getassignedGameDetails(mappingForm);
		List<BpGameMappingDTO> mappingDTOs = ConverterUtil.convertBPCategoryMappingToDTO(gameDetails);*/
		
		LicenseForm licenseForm = new LicenseForm();
		licenseForm.setBusinessPartnerId(user.getUserId());
		List<BpGameMappingDTO> mappingDTOs = licenseService.getGameLicenseMapForBP(licenseForm);
		
		modelAndView.addObject("bpGameMappings", mappingDTOs);
		log.debug("end of method getBPDashBoard");
		return modelAndView;
	}

	/**@author Satyajit
	 * This service will redirect the user to the respective DashBoard  on RoleBased
	 * @param request
	 * @return ModelAndView 
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping("/admin/showHome")
	public ModelAndView adminShowHome(HttpServletRequest request){
		log.debug("start of method adminShowHome()");
		ModelAndView modelAndView = null;
		try {
			Authentication authentication= SecurityContextHolder.getContext().getAuthentication();
			final String currentUser =authentication.getName();
			log.debug("Current user  : " + currentUser );
			// get the user and set the userId in AdminDetailsSessionForm for future use.
			AdminForm adminForm = new AdminForm();
			
			adminForm.setEmailAddress(currentUser);
			User user = adminLoginService.getUser(adminForm);
			List<GrantedAuthority> authorityList = (List<GrantedAuthority>)authentication.getAuthorities();
			String roleName = authorityList.get(Constants.GET_ZERO_INDEX_OBJECT).getAuthority();
			AdminDetailsSessionForm adminDetailsSessionForm = (AdminDetailsSessionForm) request.getSession(false).getAttribute("adminDetailsSessionForm");
			if ( adminDetailsSessionForm == null ) {
				adminDetailsSessionForm = new AdminDetailsSessionForm();
			} 
				adminDetailsSessionForm.setUserName(currentUser);
				adminDetailsSessionForm.setUserRole(roleName); 	
				adminDetailsSessionForm.setUserId(user.getUserId());
				adminDetailsSessionForm.setUrlSpecRoleName(ConverterUtil.getUrlSpecRole(roleName));
			 adminDetailsSessionForm.setName(user.getName());
				
				
				
				
				
				request.getSession(false).setAttribute("adminDetailsSessionForm", adminDetailsSessionForm);
				// here we are redirecting the user to the dashBoard based on role.
				if(roleName.equals(Constants.BP_ROLE)){
					modelAndView = new ModelAndView("bpCompanyList");
				}else{
					modelAndView = dashboad(adminDetailsSessionForm , user);
				}
				
		} catch (Exception e) {
			log.error("exception occured : ",e);
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		log.debug("end of method adminShowHome()");
    	return modelAndView;
	}
	
	/**@author Satyajit
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping("/adminLogout")
	public ModelAndView adminLogout( HttpServletRequest request , HttpServletResponse response){
		log.debug("start of method adminLogout()");
		/*
		 * Authentication authentication=
		 * SecurityContextHolder.getContext().getAuthentication();
		 * List<SimpleGrantedAuthority> authorities =
		 * (List<SimpleGrantedAuthority>) authentication.getAuthorities();
		 * String role =
		 * authorities.get(Constants.GET_ZERO_INDEX_OBJECT).getAuthority();
		 */
		
		CookieClearingLogoutHandler cookieClearingLogoutHandler = new CookieClearingLogoutHandler(AbstractRememberMeServices.SPRING_SECURITY_REMEMBER_ME_COOKIE_KEY);
		SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
		cookieClearingLogoutHandler.logout(request, response, null);
		securityContextLogoutHandler.logout(request, response, null);
		log.debug( " ---isInvalidateHttpSession--> " + securityContextLogoutHandler.isInvalidateHttpSession());
		if (securityContextLogoutHandler.isInvalidateHttpSession()) {
			SecurityContextHolder.getContext().setAuthentication(null);
			SecurityContextHolder.getContextHolderStrategy().clearContext();
			securityContextLogoutHandler.setClearAuthentication(true);
			securityContextLogoutHandler.setInvalidateHttpSession(true);
		}
		HttpSession session =request.getSession(false);
		if(session!= null)
		{	request.getSession(false).setAttribute("userDetailsFormSession", null);
			session.invalidate();
		}
		response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
		response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
		response.setDateHeader("Expires", 0); // Proxies.
	
		log.debug("end of method adminLogout()");
		return redirectToLoginPage(Constants.USER_TYPE_ADMIN);
	}
	@RequestMapping("/adminLoginFailure")
	public ModelAndView loginFail(AdminDetailsSessionForm adminDetailsSessionForm){
		log.debug("start of method adminLoginFailure()");
		ModelAndView modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		modelAndView.addObject("error","error");
		String msg = getText("error.admin.login.fail");
		saveErrorMessage(msg);
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("end of method adminLoginFailure()");
		return modelAndView;
	}
	
	@RequestMapping("/changePassword")
	public ModelAndView   changePassword(AdminDetailsSessionForm adminDetailsSessionForm , ChangePasswordForm form , RedirectAttributes redirectAttributes){
		log.debug("start of method changePassword()");
		ModelAndView modelAndView = null;
		modelAndView = new ModelAndView(new RedirectView("dashboard"));
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			try {
				adminLoginService.changePassword(form);
				saveSuccessMessage(resourceBundle.getString("success.ajax.passwordchange"));
			} catch (NoRecordsFoundException e) {
				log.debug("Error Occured : NoRecordsFoundException ",e);
				saveErrorMessage(resourceBundle.getString("error.ajax.passwordchange"));
			}catch (UserBadCredentialsException e) {
				log.debug("Error Occured : BadPasswordException ",e);
				saveErrorMessage(e.getMessage());
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method changePassword()");
		return modelAndView;
	}
	@RequestMapping("/forgotPassword")
	public ModelAndView   forgotPassword(ChangePasswordForm form , RedirectAttributes redirectAttributes){
		log.debug("start of method forgotPassword()");
		ModelAndView modelAndView = new ModelAndView(new RedirectView("adminLogin"));
			try {
				adminLoginService.forgotPassword(form);
				saveSuccessMessage(resourceBundle.getString("success.forgt.password"));
			} catch (NoRecordsFoundException e) {
				log.debug("Error Occured : NoRecordsFoundException ",e);
				redirectAttributes.addFlashAttribute("forgotpassError",resourceBundle.getString("error.username.invalid"));
			} catch (MailNotSentException e) {
				log.debug("Error Occured : MailNotSentException ",e);
				redirectAttributes.addFlashAttribute("forgotpassError", resourceBundle.getString("error.common.tryLater"));
			}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method forgotPassword()");
		return modelAndView;
	}
	
}
