package com.ventureBean.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.DatatableJsonResponse;
import com.ventureBean.dto.GenericDTO;
import com.ventureBean.exception.ProjectNotFoundException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.CompanyForm;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.model.Company;
import com.ventureBean.model.Project;
import com.ventureBean.service.CompanyService;
import com.ventureBean.service.LicenseService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;

@Controller
public class BPController extends BaseController {

	/**
	 * @Injected companyService
	 */
	@Autowired
	private CompanyService companyService;  
	
	/**
	 * @Injected licenseService
	 */
	@Autowired
	private LicenseService licenseService;
	
	/**@author Satyajit
	 * This method will redirect to the BP's view Company Page
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping("bp/viewCompanyList")
	public ModelAndView viewCompanyList(AdminDetailsSessionForm adminDetailsSessionForm){
		log.debug("start of method viewCompanyList");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView("bpCompanyList");
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		log.debug("end of method viewCompanyList");
		return modelAndView;
	}
	
	@RequestMapping("bp/viewCompany")
	public ModelAndView viewCompany(AdminDetailsSessionForm adminDetailsSessionForm, CompanyForm  companyForm){
		log.debug("start of method viewCompany");
		ModelAndView modelAndView = null;
		Company company = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView("companyProfile");
			try {
				company = companyService.getCompanyById(companyForm);
				modelAndView.addObject("company", ConverterUtil.convertCompanyToDTO(company));
				// get the projects under this company
				companyForm.setBpId(adminDetailsSessionForm.getUserId());
				List<Project>  projects = companyService.getProjectsByCompany(companyForm);
				List<ProjectForm> forms = ConverterUtil.convertProjectListToFormList(projects);
				modelAndView.addObject("projectList", forms);
			}catch (ProjectNotFoundException e) {
				log.error(" ProjectNotFoundException :[ No projects foundfor this compnay]",e);
				saveErrorMessage(getText("error.project.notfound"));
			}catch (Exception e) {
				log.error("Exception occured : ",e);
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		//addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method viewCompany");
		return modelAndView;
	}
	
	/**@author Satyajit
	 * This method will Add a Company
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping("bp/addCompany")
	public ModelAndView addCompany(AdminDetailsSessionForm adminDetailsSessionForm , CompanyForm companyForm , RedirectAttributes redirectAttributes){
		log.debug("start of method addCompany");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			modelAndView = new ModelAndView(new RedirectView("viewCompanyList"));
			try {
				companyForm.setCreatedBy(adminDetailsSessionForm.getUserName());
				companyService.addCompany(companyForm);
				saveSuccessMessage(getText("success.company.add"));
			} catch (Exception e) {
				log.error("Exception occured : ", e);
				saveErrorMessage(getText("error.company.add"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("end of method addCompany");
		return modelAndView;
	}
	/**@author Satyajit
	 * 
	 * @return
	 */
	@RequestMapping("/bp/getAllCompany")
	public @ResponseBody DatatableJsonResponse getAllCompanyList(CompanyForm companyForm){
		log.debug("start of method getAllCompanyList");
		DatatableJsonResponse response = new DatatableJsonResponse();
		List<Company> companies = companyService.getCompanyList(companyForm);
		response.setData(ConverterUtil.convertCompanyToDTO(companies));
		log.debug("end of method getAllCompanyList");
		return response;
	}
	
	
	/**@author Satyajit
	 * This will return peice per license for the given bp
	 * @param licenseForm
	 * @return
	 */
	@RequestMapping(value = "/bp/getPrice")
	public @ResponseBody
	GenericDTO getPricePerLicense(LicenseForm licenseForm) {
		log.debug("Start of method getPricePerLicense");
		GenericDTO genericDTO = new GenericDTO();
		try {
			Double ppLicense = licenseService.getPricePerLicense(licenseForm);
			genericDTO.setPpLicense(ppLicense);
		} catch (Exception e) {
			log.debug("Exception occured ",e);
			//genericDTO.setSuccessOrErrorMsg(getText("error.bp.pricePerLicense"));
		}
		log.debug("End of method getPricePerLicense");
		return genericDTO;
	}

	
}
