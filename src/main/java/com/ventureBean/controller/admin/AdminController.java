package com.ventureBean.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.DatatableJsonResponse;
import com.ventureBean.form.AdminForm;
import com.ventureBean.model.User;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.util.ConverterUtil;

@Controller
public class AdminController extends BaseController{

	/**
	 * @injected userLoginService
	 */
	@Autowired
	private AdminLoginService adminLoginService;

	
	/**@author Satyajit
	 * This service will return nestedAdmin List based on superUser's id.
	 * @param userForm
	 * @return
	 */
	@RequestMapping(value = "/getNestedList")
	public @ResponseBody	
	DatatableJsonResponse getNestedList( Integer superUserId) {
		log.debug("Start of method getNestedList");
		DatatableJsonResponse jsonResponse = null;
		AdminForm form = new AdminForm();
		form.setSuperUserId(superUserId);
		form.setFieldsToSelect("userId , name , emailAddress , manager.name");
		List<User> users = adminLoginService.getAdminList(form);
		jsonResponse = new DatatableJsonResponse();
		jsonResponse.setData(ConverterUtil.convertUserToDTO(users));
		log.debug("End of method getNestedList");
		return jsonResponse;
	}
	
}
