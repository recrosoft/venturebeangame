package com.ventureBean.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.GenericDTO;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.model.Game;
import com.ventureBean.model.User;
import com.ventureBean.service.GameService;
import com.ventureBean.service.LicenseService;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.util.Format;

@Controller
public class LicenseController extends BaseController {

	/**
	 * @injected userLoginService
	 */
	@Autowired
	private AdminLoginService adminLoginService;
	
	/**
	 * @Injected  licenseService
	 */
	@Autowired
	private LicenseService licenseService;
	
	/**
	 * @injected gameService
	 */
	@Autowired
	private GameService gameService;
	
	/**@author Satyajit
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping("/showGenerateLicense")
	public ModelAndView showGenerateLicense(AdminDetailsSessionForm adminDetailsSessionForm ){
		log.debug("start of method showGenerateLicense()");
		ModelAndView modelAndView = null;
		if(Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())){
			try{
				modelAndView = new ModelAndView("showGenerateLicense");
				List<Game> games = gameService.getAllGames();
				List<User> licensors = adminLoginService.getAllLicensor();
				modelAndView.addObject("gameList", games);
				modelAndView.addObject("LicensorList", licensors);
			}catch(Exception e){
				log.debug("Exception occured : "+e);
				modelAndView = new ModelAndView(new RedirectView("adminHome"));
			}
		}else{
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		log.debug("end of method showGenerateLicense()");
		modelAndView.addObject("licenseForm", new LicenseForm());
		return modelAndView;
	}
	/**@author Satyajit
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping(value={"admin/addLicenses" , "bp/addLicenses"})
	public @ResponseBody GenericDTO addLicenses(LicenseForm licenseForm){
		log.debug("start of method addLicenses()");
		GenericDTO dto = new GenericDTO();
		licenseService.addLicenses(licenseForm);
		saveSuccessMessage(getText("success.licenseGenerate.success"));
		log.debug("end of method addLicenses()");
		return dto;
	}
}
