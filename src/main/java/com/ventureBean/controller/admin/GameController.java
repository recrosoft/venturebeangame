package com.ventureBean.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.BpGameMappingDTO;
import com.ventureBean.dto.GenericDTO;
import com.ventureBean.exception.GameNotFoundException;
import com.ventureBean.exception.ProjectNotInCreatedStateException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.CategoryForm;
import com.ventureBean.form.GameForm;
import com.ventureBean.form.LicenseForm;
import com.ventureBean.form.ProjectForm;
import com.ventureBean.model.Game;
import com.ventureBean.model.Project;
import com.ventureBean.service.GameService;
import com.ventureBean.service.LicenseService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;

@Controller
public class GameController extends BaseController {


	/**
	 * @injected gameService
	 */
	@Autowired
	private GameService gameService;
	
	@Autowired
	private LicenseService licenseService;
	/**
	 * Instantiated resourceBundle.
	 */
	//private ResourceBundle resourceBundle = ResourceBundle.getBundle("ApplicationResources");
	
	
	/**@author Satyajit
	 * This method will show the Game Configuration Page where the Admin can view and edit the Game and releted Category Details.
	 * @param adminDetailsSessionForm
	 * @return ModelAndView
	 */
	@RequestMapping("/admin/gameConfig")
	public ModelAndView showGameConfig(AdminDetailsSessionForm adminDetailsSessionForm) {
		log.debug("Start of method showGameConfig");
		ModelAndView modelAndView = null;
		List<Game> games =null ;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			modelAndView = new ModelAndView("gameConfig");
			games = gameService.getAllGames();
			modelAndView.addObject("games",ConverterUtil.conertoGameToDTO(games));
			modelAndView.addObject("gameCategories",new Gson().toJson(ConverterUtil.conertoCategoryToDTO(games)));
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		log.debug("End of method showGameConfig");
		return modelAndView;
	}	
	
	/**@author Satyajit
	 * This service will update the Game
	 * @param adminDetailsSessionForm
	 * @param form
	 * @return
	 */
	@RequestMapping("/admin/updateGame")
	public ModelAndView updateGame(AdminDetailsSessionForm adminDetailsSessionForm , GameForm form) {
		log.debug("Start of method updateGame");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			modelAndView = new ModelAndView(new RedirectView("gameConfig"));
			try {
				form.setModifiedBy(adminDetailsSessionForm.getUserName());
				gameService.updateGame(form);
				saveSuccessMessage(getText("success.game.update"));
			} catch (GameNotFoundException e) {
				log.debug("Game =NotFount Exception :" , e);
				saveErrorMessage(getText("error.game.update"));
			}
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		addSuccessOrErrorMessageToModel(modelAndView);
		log.debug("End of method updateGame");
		return modelAndView;
	}
	
	/**@author Satyajit
	 * This method will update the Game Category .
	 * @param adminDetailsSessionForm
	 * @return ModelAndView
	 */
	@RequestMapping("/admin/updateCategory")
	public @ResponseBody GenericDTO updateGameCategory(AdminDetailsSessionForm adminDetailsSessionForm , CategoryForm categoryForm) {
		log.debug("Start of method updateGameCategory");
	
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			gameService.updateGameCategory(categoryForm);
		} else {
		}
		
		log.debug("End of method updateGameCategory");
		return null;
	}	
	
	/**@author Satyajit
	 * @param adminDetailsSessionForm
	 * @return
	 */
	@RequestMapping("/admin/getAllGame")
	public @ResponseBody GenericDTO getAllGame(AdminDetailsSessionForm adminDetailsSessionForm ){
		log.debug("start of method getAllGame()");
		List<Game> games =null ;
		try{
			 games = gameService.getAllGames();
			}catch(Exception e){
				log.debug("Exception occured : ",e);
			}
		log.debug("end of method getAllGame()");
		GenericDTO genericDto = new GenericDTO();
		genericDto.setList(ConverterUtil.convertGameToDTO(games));
		return genericDto;
	}
	
	/**This will redirect to the create project view Page
	 * @param adminDetailsSessionForm
	 * @param projectForm
	 * @return
	 */
	@RequestMapping("/bp/showCreateProject/{companyId}")
	public ModelAndView showCreateProject(AdminDetailsSessionForm adminDetailsSessionForm , @PathVariable(value="companyId")Integer companyId , ProjectForm projectForm) {
		log.debug("Start of method showCreateProject");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			modelAndView = new ModelAndView("createProject");
			LicenseForm licenseForm = new LicenseForm();
			licenseForm.setBusinessPartnerId(adminDetailsSessionForm.getUserId());
			List<BpGameMappingDTO> mappingDTOs = licenseService.getGameLicenseMapForBP(licenseForm);
			modelAndView.addObject("gameLicenseMap",mappingDTOs);
			modelAndView.addObject("operation","create");
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		ProjectForm form = new ProjectForm();
		form.setCompanyName(projectForm.getCompanyName());
		form.setCompanyId(companyId);
		modelAndView.addObject("projectForm" , form);
		log.debug("End of method showCreateProject");
		return modelAndView;
	}	
	
	/**This will create a new project against given Game.
	 * @param adminDetailsSessionForm
	 * @param projectForm
	 * @return
	 */
	@RequestMapping("/bp/createProject")
	public ModelAndView createProject(AdminDetailsSessionForm adminDetailsSessionForm  , ProjectForm projectForm, RedirectAttributes redirectAttributes) {
		log.debug("Start of method createProject");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			try {
				projectForm.setCreatedBy(adminDetailsSessionForm.getUserName());
				
				if(Format.isNotNull(projectForm.getProjectId())){
					gameService.editProject(projectForm);
					saveSuccessMessage(getText("success.project.update"));
				}else{
					gameService.createProject(projectForm);
					saveSuccessMessage(getText("success.project.creation"));
				}
				modelAndView = new ModelAndView(new RedirectView("viewCompany"));
				modelAndView.addObject("companyId", projectForm.getCompanyId());
			}catch (ProjectNotInCreatedStateException e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.project.creation"));
				modelAndView = new ModelAndView(new RedirectView("showCreateProject/"+projectForm.getCompanyId()+"?companyName="+projectForm.getCompanyName()));
			}catch (Exception e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.common.error"));
				modelAndView = new ModelAndView(new RedirectView("showCreateProject/"+projectForm.getCompanyId()+"?companyName="+projectForm.getCompanyName()));
			}
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("End of method createProject");
		return modelAndView;
	}	
	
	@RequestMapping("/bp/project/cancel")
	public ModelAndView cancelProject(AdminDetailsSessionForm adminDetailsSessionForm  , ProjectForm projectForm, RedirectAttributes redirectAttributes) {
		log.debug("Start of method cancelProject");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			modelAndView = new ModelAndView(new RedirectView("/VentureBeanGame/bp/viewCompany"));
			modelAndView.addObject("companyId", projectForm.getCompanyId());
			try {
				projectForm.setCreatedBy(adminDetailsSessionForm.getUserName());
				gameService.cancelProject(projectForm);
				saveSuccessMessage(getText("success.project.cancel"));
			} 
			catch (Exception e) {
				log.error("Exception occured : ",e);
				saveErrorMessage(getText("error.project.cancel"));
			}
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		addSuccessOrErrorMessageToModel(modelAndView , redirectAttributes);
		log.debug("End of method cancelProject");
		return modelAndView;
	}
	
	@RequestMapping("/bp/project/edit")
	public ModelAndView showEditProject(AdminDetailsSessionForm adminDetailsSessionForm  , ProjectForm projectForm) {
		log.debug("Start of method showEditProject");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			modelAndView = new ModelAndView("createProject");
			Project project = gameService.getProject(projectForm);
			ProjectForm form = ConverterUtil.convertProjectToForm(project);
			modelAndView.addObject("projectForm" , form);
			modelAndView.addObject("operation","edit");
			LicenseForm licenseForm = new LicenseForm();
			licenseForm.setNoOfLicense(project.getNoOfPlayers());
			licenseForm.setGameId(project.getGame().getGameId());
			licenseForm.setBusinessPartnerId(adminDetailsSessionForm.getUserId());
			List<BpGameMappingDTO> mappingDTOs = licenseService.getGameLicenseMapForBP(licenseForm);
			modelAndView.addObject("gameLicenseMap",mappingDTOs);
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		log.debug("End of method showEditProject");
		return modelAndView;
	}
}
