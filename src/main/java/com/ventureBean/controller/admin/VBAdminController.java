package com.ventureBean.controller.admin;

import java.text.MessageFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.DatatableJsonResponse;
import com.ventureBean.dto.GenericDTO;
import com.ventureBean.dto.UserDetailDTO;
import com.ventureBean.enums.CategoryType;
import com.ventureBean.exception.EmailAddressAlreadyRegisteredException;
import com.ventureBean.form.AdminDetailsSessionForm;
import com.ventureBean.form.AdminForm;
import com.ventureBean.form.RoleForm;
import com.ventureBean.form.UserSearchForm;
import com.ventureBean.model.User;
import com.ventureBean.service.AdminLoginService;
import com.ventureBean.service.UserRoleService;
import com.ventureBean.util.ConverterUtil;
import com.ventureBean.util.Format;

@Controller
public class VBAdminController extends BaseController {

	/**
	 * @injected adminLoginService
	 */
	@Autowired
	private AdminLoginService adminLoginService;

	@Autowired
	private UserRoleService userRoleService;

	/**
	 * @author Satyajit
	 *  This will redirect the Admin to the users List based on Role .
	 *  This is basically called when user clicks the Dropdown
	 * @param userForm
	 * @return List<UserDetail>
	 */
	@RequestMapping(value = "{currentUser}/user/{selectedRole}")
	public ModelAndView showAdminUserListPage(
			AdminDetailsSessionForm adminDetailsSessionForm,
			@PathVariable("selectedRole") String selectedRole , @PathVariable(value="currentUser" ) String currentUser ) {
		log.debug("Start of method showAdminUserListPage");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			if(currentUser.equals("admin")){
				modelAndView = new ModelAndView("vbUserList");
			}else{
				modelAndView = new ModelAndView("otherAdminList");
				modelAndView.addObject("currentUserId", adminDetailsSessionForm.getUserId());
				modelAndView.addObject("currentUserRole", adminDetailsSessionForm.getUserRole());
			}
			CategoryType[] packages = CategoryType.values();
			modelAndView.addObject("packages", packages);
			modelAndView.addObject("selectedRole", selectedRole);
			modelAndView.addObject("headingText", ConverterUtil.getText(selectedRole));
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		RoleForm roleForm = new RoleForm();
		roleForm.setCategoryName(CategoryType.SILVER.toString());
		modelAndView.addObject("roleForm", roleForm);
		log.debug("End of method showAdminUserListPage");
		return modelAndView;
	}
	
	/**
	 * @author Satyajit THis will redirect the Admin to the user List based on Role , this is called when user clicks on the hyperlinked  Names on the page
	 * @param userForm
	 * @return List<UserDetail>
	 admin/createRole*/
	@RequestMapping(value = "{currentUser}/user/{selectedRole}/{id}")
	public ModelAndView showNestedAdmin(
			AdminDetailsSessionForm adminDetailsSessionForm,
			@PathVariable("selectedRole") String selectedRole , @PathVariable(value="currentUser" ) String currentUser , @PathVariable("id") Integer id) {
		log.debug("Start of method showNestedAdmin");
		ModelAndView modelAndView = null;
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm.getUserName())) {
			if(currentUser.equals(Constants.VB_ADMIN_SHORT)){
				modelAndView = new ModelAndView("vbUserList");
				CategoryType[] packages = CategoryType.values();
				modelAndView.addObject("packages", packages);
			}else{
				modelAndView = new ModelAndView("otherAdminList");
				modelAndView.addObject("currentUserId", adminDetailsSessionForm.getUserId());
				modelAndView.addObject("currentUserRole", adminDetailsSessionForm.getUserRole());
			}
			//if Selected Role is BP then redirect to BP Company List page
			if(selectedRole.equals(Constants.BP_ROLE_SHORT)){
				modelAndView = new ModelAndView("bpCompanyList");
			}
			modelAndView.addObject("superUserId", id);
			String roleForHeading = adminLoginService.getUserRoleByUserId(id);
			if(Format.isStringNotEmptyAndNotNull(roleForHeading)){
				modelAndView.addObject("headingText", ConverterUtil.getText(roleForHeading));
			}else{
				modelAndView.addObject("headingText", ConverterUtil.getLowerLevelRole(selectedRole));
				
			}
			modelAndView.addObject("selectedRole", ConverterUtil.getUrlSpecRole(roleForHeading));
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		RoleForm roleForm = new RoleForm();
		roleForm.setCategoryName(CategoryType.SILVER.toString());
		modelAndView.addObject("roleForm", roleForm);
		log.debug("End of method showNestedAdmin");
		return modelAndView;
	}
	
	/**
	 * @author Satyajit
	 *  This will return List of admins which role is superior
	 *         than the current user's role . this is basically called to populate the drwopdown 'assignTo'.
	 * @param adminForm
	 * @return List<UserDetail>
	 */
	@RequestMapping(value = "/getList")
	public @ResponseBody
	DatatableJsonResponse getListBasedOnRole( AdminForm adminForm) {
		log.debug("Start of method getListBasedOnRole");
		DatatableJsonResponse jsonResponse = null;
		adminForm.setFieldsToSelect("userId , name , emailAddress , manager.name");
		List<User> users = adminLoginService.getUsersBasedOnRole(adminForm);
		jsonResponse = new DatatableJsonResponse();
		jsonResponse.setData(ConverterUtil.convertUserToDTO(users));
		log.debug("End of method getListBasedOnRole");
		return jsonResponse;
	}

	@RequestMapping(value = "/getForNonVBAdmins")
	public @ResponseBody
	DatatableJsonResponse getForNonVBAdmins(
			AdminDetailsSessionForm adminDetailsSessionForm, UserSearchForm searchForm) {
		log.debug("Start of method getForNonVBAdmins");
		DatatableJsonResponse jsonResponse = null;
		searchForm.setFieldsToSelect("u.userId , u.name , u.emailAddress , u.manager.name");
		List<User> users = adminLoginService.getUsersForNonVBAdmin(searchForm);
		jsonResponse = new DatatableJsonResponse();
		jsonResponse.setData(ConverterUtil.convertUserToDTO(users));
		log.debug("End of method getForNonVBAdmins");
		return jsonResponse;
	}

	/**
	 * @author Satyajit This method will Add a new Admin and then redirect to the
	 *         "create/add Role" page
	 * @param adminDetailsSessionForm
	 * @param roleForm
	 * @return ModelAndView
	 */
	@RequestMapping(value = "/admin/createRole", method = RequestMethod.POST)
	public ModelAndView createRole(
			AdminDetailsSessionForm adminDetailsSessionForm, RoleForm roleForm , RedirectAttributes redirectAttributes) {
		log.debug("Start of method createRole");
		ModelAndView modelAndView = null;
		String roleName = "";
		if (Format.isStringNotEmptyAndNotNull(adminDetailsSessionForm
				.getUserName())) {
			try {
				roleForm.setCreatedBy(adminDetailsSessionForm.getUserName());
				 roleName = adminLoginService.createRole(roleForm);
				saveSuccessMessage( MessageFormat.format(getText("success.createRole.success"),ConverterUtil.getText(roleName)));
			} catch (EmailAddressAlreadyRegisteredException e) {
				log.error("Exception occured : " , e);
				saveErrorMessage(getText("error.createRole.alreadyExists"));
			} catch (Exception ex) {
				log.error("Exception occured : " ,ex);
				saveErrorMessage(getText("error.createRole.error"));
			}
		} else {
			modelAndView = redirectToLoginPage(Constants.USER_TYPE_ADMIN);
		}
		if(Format.isStringNotEmptyAndNotNull(roleName) && !roleName.equals(Constants.ADMIN_ROLE))
			{
				roleName = 	ConverterUtil.getUrlSpecRole(roleName);
			}else{
				roleName = Constants.BH_ROLE_SHORT;
			}
		
		modelAndView = new ModelAndView(new RedirectView("user/"+roleName));
		addSuccessOrErrorMessageToModel(modelAndView, redirectAttributes);
		log.debug("End of method createRole");
		return modelAndView;
	}

	/**
	 * @author Satyajit THis will return List of admins which role is superior
	 *         than the current user's role
	 * @param adminForm
	 * @return List<UserDetail>
	 */
	@RequestMapping(value = "/admin/getSuperiorAdmin")
	public @ResponseBody
	GenericDTO getSuperiorAdminList(AdminForm adminForm) {
		log.debug("Start of method getSuperiorAdminList");
		adminForm.setFieldsToSelect("userId, name , emailAddress");
		List<User> users = adminLoginService.getAdminList(adminForm);
		List<UserDetailDTO> adminList = ConverterUtil.convertUserToDTO(users);
		GenericDTO genericDTO = new GenericDTO();
		genericDTO.setList(adminList);
		log.debug("End of method getSuperiorAdminList");
		return genericDTO;
	}


	/**
	 * @author Satyajit THis will return List of admins which role is superior
	 *         than the current user's role
	 * @param adminForm
	 * @return List<UserDetail>
	 */
	@RequestMapping(value = "/admin/getBusinessHeadList")
	public @ResponseBody
	DatatableJsonResponse getBusinessHeadList(AdminForm adminForm) {
		log.debug("Start of method getBusinessHeadList");
		adminForm.setFieldsToSelect("userId , name , emailAddress , manager.name ");
		List<User> users = adminLoginService.getAdminList(adminForm);
		List<UserDetailDTO> adminList = ConverterUtil.convertUserToDTO(users);
		DatatableJsonResponse resp = new DatatableJsonResponse();
		resp.setData(adminList);
		log.debug("End of method getBusinessHeadList");
		return resp;
	}

}
