package com.ventureBean.controller.admin;

import java.util.List;

import javax.jws.WebMethod;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import com.ventureBean.constants.Constants;
import com.ventureBean.controller.common.BaseController;
import com.ventureBean.dto.DatatableJsonResponse;
import com.ventureBean.dto.GenericDTO;
import com.ventureBean.enums.TransactionStatusEnum;
import com.ventureBean.form.TransactionForm;
import com.ventureBean.model.UserTransaction;
import com.ventureBean.service.TransactionService;
import com.ventureBean.util.ConverterUtil;

@Controller
public class SAdminController extends BaseController{

	/**
	 * @Injected transactionService
	 */
	@Autowired
	private TransactionService transactionService;
	
	
	@RequestMapping(value = "/trnx/{trnxType}", method = RequestMethod.POST)
	public @ResponseBody GenericDTO approveRejectTrnx(TransactionForm transactionForm , @PathVariable(value="trnxType") String trnxType) {
		log.debug("Start of method approveRejectTrnx");
		GenericDTO genericDTO = null;
			try {
				transactionService.approveRejectTrnx(transactionForm ,trnxType );
				genericDTO = new GenericDTO();
				if(trnxType.equals(Constants.TRNX_APPROVE)){
					genericDTO.setSuccessOrErrorMsg(getText("success.approve.trnx"));
				}else{
					genericDTO.setSuccessOrErrorMsg(getText("success.reject.trnx"));
				}
			} catch (Exception ex) {
				log.error("Exception occured : " ,ex);
				genericDTO.setSuccessOrErrorMsg("Error");
			}
		log.debug("End of method approveRejectTrnx");
		return genericDTO;
	}
	
	
	@RequestMapping(value={"getPendingTrnxList"} )
	public @ResponseBody DatatableJsonResponse getPendingTrnxList(TransactionForm transactionForm){
		log.debug("start of method getPendingTrnxList");
		DatatableJsonResponse dto = new DatatableJsonResponse();
		transactionForm.setTransactionStatus(TransactionStatusEnum.PENDING.toString());
		transactionForm.setFieldToSelect("transactionId , businessPartner.name ,game.gameName ,licenseType,category.categoryName ,paidLicenses , freeLicenses , amount");
		List<UserTransaction> trnxList = transactionService.getPendingTrnxList(transactionForm);
		dto.setData(ConverterUtil.convertUTrnxToDTO(trnxList));
		log.debug("end of method getPendingTrnxList");
	return dto;	
	}
}
